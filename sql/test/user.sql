-- pass123
INSERT INTO `user`
		SET `id` = 1,
			`firstname` = 'Test',
			`surname` = 'User1',
			`username` = 'tuser1',
			`password` = '$2y$14$ZvLh6KMHAzT4kmCnsCRbEuob2pF5a9myX/rezlUOTL/XwvbNbY2AS',
			`email` = 'tuser1@domain.com';