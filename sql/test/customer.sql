-- pass123
INSERT INTO `user`
		SET `id` = 1,
			`firstname` = 'John',
			`surname` = 'Smith',
			`username` = 'jsmith',
			`password` = '$2y$14$ZvLh6KMHAzT4kmCnsCRbEuob2pF5a9myX/rezlUOTL/XwvbNbY2AS',
			`email` = 'john@smith.com';
INSERT INTO `customer`
        SET `user_id` = 1,
            `phone` = '0161 123 4567';