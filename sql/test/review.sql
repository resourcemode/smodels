-- Test reviews
INSERT INTO `comment` (`id`, `content`, `added_from`, `approved`, `score`, `created`, `created_by`) 
VALUES ('1', 'Review Content 1', 'localhost', '0', '0', CURRENT_TIMESTAMP, '1');
INSERT INTO `review` (`comment_id`, `rating`, `title`, `helpful`) 
VALUES ('1', '2.5', 'Test Review', '1');
INSERT INTO `product_review` (`product_id`, `review_id`) 
VALUES ('1', '1');