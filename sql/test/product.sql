-- Test Product
INSERT INTO `product`
		SET `id` = 1,
			`title` = 'Test Product 1',
			`path` = 'test-product-1',
			`created_by` = 1,
			`active`=1,
			`description`='Test Product Description 1',
			`featured` = 1,
			`price` = 100.00;
-- Test Product
INSERT INTO `product`
		SET `id` = 2,
			`title` = 'Test Product 2',
			`path` = 'test-product-2',
			`created_by` = 1,
			`active`=1,
			`description`='Test Product Description 2',
			`featured` = 1,
			`price` = '49.99';