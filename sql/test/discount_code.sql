-- Test discount codes
INSERT INTO `discount_code` (`id`, `code`, `discount`, `expire`, `discount_type`, `created`, `count`)
VALUES (1, 'code1', '1', NULL, '1', CURRENT_TIMESTAMP, '5');
INSERT INTO `discount_code` (`id`, `code`, `discount`, `expire`, `discount_type`, `created`, `count`)
VALUES (2, 'code2', '1', NULL, '2', CURRENT_TIMESTAMP, '15');
INSERT INTO `discount_code` (`id`, `code`, `discount`, `expire`, `discount_type`, `created`, `count`)
VALUES (3, 'code3', '1', NULL, '2', CURRENT_TIMESTAMP, '0'); -- invalid: no count left
INSERT INTO `discount_code` (`id`, `code`, `discount`, `expire`, `discount_type`, `created`, `count`)
VALUES (4, 'code4', '1', '2013-01-30 00:00:00', '2', CURRENT_TIMESTAMP, NULL); -- invalid: expired
INSERT INTO `discount_code` (`id`, `code`, `discount`, `expire`, `discount_type`, `created`, `count`)
VALUES (5, 'code5', '1', '2013-01-30 00:00:00', '2', CURRENT_TIMESTAMP, 0); -- invalid: expired, 0 counts
INSERT INTO `discount_code` (`id`, `code`, `discount`, `expire`, `discount_type`, `created`, `count`)
VALUES (6, 'code6', '20', '2013-01-30 00:00:00', '1', CURRENT_TIMESTAMP, 10);
INSERT INTO `discount_code` (`id`, `code`, `discount`, `expire`, `discount_type`, `created`, `count`)
VALUES (7, 'code7', '20', '2013-01-30 00:00:00', '2', CURRENT_TIMESTAMP, 10);