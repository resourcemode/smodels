-- pass123
INSERT INTO `user`
        SET `id` = 1,
            `firstname` = 'Test',
            `surname` = 'User1',
            `username` = 'tuser1',
            `password` = '$2y$14$ZvLh6KMHAzT4kmCnsCRbEuob2pF5a9myX/rezlUOTL/XwvbNbY2AS',
            `email` = 'tuser1@domain.com';
-- Test Product2
INSERT INTO `product`
        SET `id` = 1,
            `title` = 'Test Product 1',
            `path` = 'test-product-1',
            `created_by` = 1,
            `active`= 1,
            `description`='Test Product Description 1',
            `featured` = 1,
            `price` = 100.00;
INSERT INTO `product`
        SET `id` = 2,
            `title` = 'Test Product 2',
            `path` = 'test-product-2',
            `created_by` = 1,
            `active`= 1,
            `description`='Test Product Description 2',
            `featured` = 0,
            `price` = 123.00;
-- Test Basket Product
INSERT INTO `basket_product`
		SET `id` = 1,
			`product` = 1,
			`title` = 'Test Product 1',
			`notes` = 'Some Notes',
			`price` = 100.00,
			`vat` = 20.00,
			`qty` = 1;