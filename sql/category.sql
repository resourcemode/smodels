-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 20, 2012 at 10:46 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smodels`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `parentID` int(11) unsigned DEFAULT NULL,
  `lft` mediumint(8) unsigned NOT NULL,
  `rgt` mediumint(8) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `metadataid` int(11) unsigned DEFAULT NULL,
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` text,
  PRIMARY KEY (`id`),
  KEY `parentID` (`parentID`),
  KEY `lft` (`lft`),
  KEY `rgt` (`rgt`),
  KEY `active` (`active`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `active`, `parentID`, `lft`, `rgt`, `title`, `path`, `metadataid`, `sort`, `created`, `description`) VALUES
(12, 1, 45, 2, 21, 'Security', NULL, NULL, 1, '2007-12-12 10:10:40', NULL),
(13, 1, 12, 3, 6, 'Indoor Security Mirrors', 'indoor-security-mirrors', NULL, 2, '2007-12-12 10:11:01', '<strong>Indoor security mirrors</strong> from Bohle are finely polished to provide a crystal clear reflection, allowing for close surveillance to reduce in-store theft. These security mirrors have been designed specifically for interior use, and represent fantastic value and real piece of mind.<br />\r\n<br />\r\nClick on any of the indoor security mirrors below for more information, or call us today for prices on larger quantities. <br />\r\n'),
(14, 1, 45, 22, 35, 'Safety', NULL, NULL, 2, '2007-12-12 10:14:59', NULL),
(15, 1, 45, 36, 39, 'Traffic', NULL, NULL, 3, '2007-12-12 10:15:18', NULL),
(16, 1, 12, 13, 14, 'Security Mirrors', 'security-mirrors', NULL, 1, '2007-12-12 10:16:27', '<p>\r\n<strong>Security Mirrors</strong> from Bohle are finely polished to provide a crystal clear reflection, allowing for close surveillance to reduce in-store theft. These security mirrors are sturdy enough to be used both inside and out, and are available at fantastic prices. \r\n</p>\r\n<p>\r\nClick on any of the security mirrors below for more information, or call us today for prices on larger quantities. <br />\r\n</p>\r\n'),
(17, 1, 12, 7, 12, 'Dome Security Mirrors', 'dome-security-mirrors', NULL, 4, '2007-12-12 10:16:52', '<strong>Dome security mirrors </strong>from Bohle are finely polished to provide a crystal clear reflection and a wide angle of reflection, allowing for close surveillance of your premises. These hard wearing dome mirrors represent real piece of mind and are available at fantastic prices.<br />\r\n<br />\r\nClick on any of the dome security mirrors below for more information, or call us today for prices on larger quantities. <br />\r\n'),
(18, 1, 12, 15, 16, 'Outdoor Security MirrorsA', 'outdoor-security-mirrors', NULL, 3, '2007-12-12 10:17:12', 'Outdoor security mirrors from Bohle are finely polished to provide a crystal clear reflection, allowing for close surveillance to reduce vandalism or thefts from your premises. These hard wearing security mirrors have been designed specifically for exterior use, and represent great value and real piece of mind.\r\n\r\nClick on any of the outdoor security mirrors below for more information, or call us today for prices on larger quantities.'),
(20, 1, 12, 17, 20, 'Search and Inspection', 'search-and-inspection', NULL, 5, '2007-12-12 10:18:03', 'These sturdy <strong>search and inspection mirrors</strong> from Bohle are built to be virtually unbreakable and tamper proof, representing real piece of mind when applied in circumstances such as prisons and young offenders institutes. They are highly polished, providing a crystal clear image, even on blind angles.<br />\r\n<br />\r\nClick on any of the search and inspection mirrors below for more information, or call us today for prices on larger quantities. <br />\r\n'),
(21, 1, 14, 23, 24, 'Indoor Safety Mirrors', 'indoor-safety-mirrors', NULL, 2, '2007-12-12 10:18:28', 'Indoor safety mirrors from Bohle are finely polished\r\nto provide a crystal clear reflection, allowing for close surveillance\r\nto help reduce accidents in the workplace, whether in the shop, office or warehouse. These safety\r\nmirrors have been designed specifically for interior use, and represent\r\nfantastic value and real piece of mind.\r\n TEST TES \r\nClick on any of the indoor safety mirrors below for more information, or call us today for prices on larger quantities.'),
(22, 1, 14, 25, 26, 'Safety Mirrors', 'safety-mirrors', NULL, 1, '2007-12-12 10:18:57', '<strong>Safety Mirrors</strong> from Bohle are finely polished to provide a crystal clear reflection, allowing for close surveillance to improve workplace safety. These safety mirrors are sturdy enough to be used both inside and out, and are available at fantastic prices.<br />\r\n<br />\r\nClick on any of the safety mirrors below for more information, or call us today for prices on larger quantities. <br />\r\n'),
(23, 1, 14, 27, 28, 'Dome Safety Mirrors', 'dome-safety-mirrors', NULL, 4, '2007-12-12 10:25:53', '<strong>Dome safety mirrors </strong>from Bohle are finely polished\r\nto provide a crystal clear reflection and a wide angle of reflection,\r\nallowing for close surveillance of your premises to improve safety and reduce accidents in the workplace. These hard wearing\r\ndome safety mirrors represent real piece of mind and are available at\r\nfantastic prices.<br />\r\n<br />\r\nClick on any of the dome safety mirrors below for more information, or call us today for prices on larger quantities. \r\n'),
(24, 1, 14, 29, 30, 'Outdoor Safety Mirrors', 'outdoor-safety-mirrors', NULL, 3, '2007-12-12 10:26:05', '<strong>Outdoor safety mirrors</strong> from Bohle are finely\r\npolished to provide a crystal clear reflection, allowing for close\r\nsurveillance to improve safety outside your premises; in courtyards and car parks, for example. These\r\nhard wearing safety mirrors have been designed specifically for\r\nexterior use, and represent great value and real piece of mind.<br />\r\n<br />\r\nClick on any of the outdoor safety mirrors below for more information, or call us today for prices on larger quantities.\r\n'),
(28, 1, 15, 37, 38, 'Traffic Mirrors', 'traffic-mirrors', NULL, 1, '2007-12-12 10:27:46', '<strong>Traffic Mirrors</strong> from Bohle provide essential extra visibility to road users on tight bends, or areas from which vehicles may emerge suddenly. Contructed from sturdy materials including an extra-thick mirror surface and UV resistant frame, these traffic mirrors are built to last.<br />\r\n<br />\r\nClick on the traffic mirrors below for more information, or call us today for prices on larger quantities. <br />\r\n'),
(29, 1, 45, 40, 43, 'Mirror related products', NULL, NULL, 4, '2008-02-11 11:53:36', NULL),
(30, 1, 29, 41, 42, 'Glass Cleaner, Demisting Pads,  and Mirror Adhesive', 'glass-cleaner-demisting-pads-and-mirror-adhesive', NULL, 1, '2008-02-11 12:11:30', '<p>\r\nIn addition to our large range of <strong>Convex Security Mirrors</strong> and <strong>Safety Mirrors</strong>, Bohle also supplies a number of&nbsp;products&nbsp;for standard mirrors, to suit&nbsp;your domestic and corporate needs. These include Glass Cleaner, Demisting Pads,&nbsp;and Mirror Adhesive and Primer. \r\n</p>\r\n<p>\r\nClick on any of the&nbsp;mirror related products below for more information, or call us today for prices on larger quantities. <br />\r\n</p>\r\n'),
(31, 1, 14, 31, 32, 'Forklift Mirrors', 'forklift-mirrors', NULL, 5, '2008-07-04 10:35:59', '<p>\r\n<strong>Forklift mirrors</strong> from Bohle are made from premium quality, virgin acrylic with crystal clear reflection. Leading edge mirroring technology gives a clear undistorted image, allowing improved visibilty when manoeuvring, and to help reduce accidents in the workplace. Easy to install and maintain, these lightweight durable Forklift mirrors represent real piece of mind, and are available at fantastic prices. \r\n</p>\r\n<p>\r\nEach mirror comes complete with an adjustable bracket. \r\n</p>\r\n<p>\r\nClick on any of the forklift mirrors below for more information, or call us today for prices on larger quantities. \r\n</p>\r\n'),
(32, 1, 45, 44, 47, 'Other products', NULL, NULL, 5, '2008-07-23 14:33:04', NULL),
(33, 1, 32, 45, 46, 'Ventilators', 'ventilators', NULL, 1, '2008-07-23 14:53:28', '<p>\r\nHigh quality <strong>Vent-A-Matic ventilators</strong> from Bohle are available in various models, including static or rotary, shuttered or without shutters, for double or single glazed units. \r\n</p>\r\n<p>\r\nClick on the ventilator below for more information, or call us today for prices on larger quantities. \r\n</p>\r\n'),
(34, 1, 13, 4, 5, 'TEST', 'TEST', NULL, 0, '2009-04-04 22:37:17', 'TEST TEST TEST TEST'),
(35, 1, 17, 8, 11, 'TEST2', 'TEST2', NULL, 0, '2009-04-04 22:46:18', 'TEST2 TEST2 TEST2 TEST2'),
(36, 1, 35, 9, 10, 'TEST3', 'TEST3', NULL, 0, '2009-04-04 22:48:20', 'TEST3 TEST3 TEST3'),
(44, 1, 14, 33, 34, 'TEST4', 'TEST4', NULL, 0, '2009-04-10 17:48:43', 'Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test'),
(45, 1, NULL, 1, 50, 'Products', NULL, NULL, 0, '2009-04-11 14:55:33', 'Root Category'),
(46, 1, 45, 48, 49, 'A Test Roor Cat', 'atest url', NULL, 0, '2009-05-17 13:30:13', NULL),
(47, 1, 20, 18, 19, 'A New Sub Cat', 'A Test url', NULL, 0, '2009-05-17 13:32:45', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
