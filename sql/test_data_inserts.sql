-- pass123
INSERT INTO `user`
		SET `id` = 1,
			`firstname` = 'Test',
			`surname` = 'User1',
			`username` = 'tuser1',
			`password` = '$2y$14$ZvLh6KMHAzT4kmCnsCRbEuob2pF5a9myX/rezlUOTL/XwvbNbY2AS',
			`email` = 'tuser1@domain.com';
-- Root category
INSERT INTO `category`
		SET `id` = 1,
			`parent_id` = NULL,
			`lft` = 1,
			`rgt` = 6,
			`title` = 'Products',
			`path` = 'products',
			`created_by` = 1,
			`description` = 'Test Category Description 1';
-- Test child category
INSERT INTO `category`
		SET `id` = 2,
			`parent_id` = 1,
			`lft` = 2,
			`rgt` = 3,
			`title` = 'Test Category 1',
			`path` = 'test-category-1',
			`created_by` = 1,
			`description` = 'Test Category Description 2';
-- Test child category
INSERT INTO `category`
		SET `id` = 3,
			`parent_id` = 1,
			`lft` = 4,
			`rgt` = 5,
			`title` = 'Test Category 2',
			`path` = 'test-category-2',
			`created_by` = 1,
			`description` = 'Test Category Description 3';
-- Test Product
INSERT INTO `product`
		SET `id` = 1,
			`title` = 'Test Product 1',
			`path` = 'test-product-1',
			`created_by` = 1,
			`active`=1,
			`description`='Test Product Description 1',
			`featured` = 1,
			`price` = 100.00;
-- Test Product, price with quotes
INSERT INTO `product`
		SET `id` = 2,
			`title` = 'Test Product 2',
			`path` = 'test-product-2',
			`created_by` = 1,
			`active`=1,
			`description`='Test Product Description 2',
			`featured` = 1,
			`price` = '49.99';
-- Test Product Attribute and values
INSERT INTO `product_attribute` (`id`, `name`, `created_by`, `last_editied_by`, `created`, `last_editied`)
	 VALUES	(1, 'Colour', 1, 1, '2012-11-01 00:00:00', '2012-12-01 00:00:00'),
	 		(2, 'Sizes', 1, 1, '2012-11-01 00:00:00', '2012-12-01 00:00:00'),
	 		(3, 'Acme', 1, 1, '2012-11-01 00:00:00', '2012-12-01 00:00:00'),
	 		(4, 'Space', 1, 1, '2012-11-01 00:00:00', '2012-12-01 00:00:00');
INSERT INTO `product_attribute_value` (`id`, `name`, `attribute_id`)
	 VALUES	(1, 'Small', 2),
	 		(2, 'Medium', 2),
			(3, 'Large', 2),
			(4, 'Blue', 1),
			(5, 'Black', 1),
			(6, 'Foo', 3);
-- Test Page
INSERT INTO `page` (`id`, `active`, `title`, `path`, `content`, `created_by`, `last_editied_by`, `created`, `last_editied`)
	 VALUES (1, 1, 'Test Page', 'this-is-a-test-page', 'Some Content', 1, 1, '2012-12-01 00:00:00', NULL);
-- Test Image
INSERT INTO `image` (`id`, `filename`, `original_filename`, `hash`, `uploader`, `created`, `created_by`)
VALUES (1, 'test_filename', 'original_test_filename.jpg', NULL, '127.0.0.1', CURRENT_TIMESTAMP, '1');
-- Test Image
INSERT INTO `image` (`id`, `filename`, `original_filename`, `hash`, `uploader`, `created`, `created_by`)
VALUES (2, 'filename', 'original_filename.jpg', NULL, '127.0.0.1', CURRENT_TIMESTAMP, '1');
-- Test product images
INSERT INTO `product_image_link` SET `product_id` = 1, `image_id` = 1;
INSERT INTO `product_image_link` SET `product_id` = 1, `image_id` = 2;
-- Test category images
INSERT INTO `category_image_link` SET `category_id` = 2, `image_id` = 1;
INSERT INTO `category_image_link` SET `category_id` = 2, `image_id` = 2;
