-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 29, 2013 at 09:35 PM
-- Server version: 5.1.40
-- PHP Version: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `smodels_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `basket_product`
--

CREATE TABLE IF NOT EXISTS `basket_product` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `product` mediumint(11) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  `vat` decimal(10,2) unsigned NOT NULL,
  `qty` mediumint(11) unsigned NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `basket_product_transaction_link`
--

CREATE TABLE IF NOT EXISTS `basket_product_transaction_link` (
  `transaction` mediumint(11) unsigned NOT NULL,
  `basket_product` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`transaction`,`basket_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `parent_id` int(11) unsigned DEFAULT NULL,
  `lft` mediumint(8) unsigned NOT NULL,
  `rgt` mediumint(8) unsigned NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `metadataid` int(11) unsigned DEFAULT NULL,
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` mediumtext NOT NULL,
  `created_by` mediumint(11) unsigned NOT NULL,
  `last_editied_by` mediumint(11) unsigned DEFAULT NULL,
  `last_editied` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parentID` (`parent_id`),
  KEY `lft` (`lft`),
  KEY `rgt` (`rgt`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_image_link`
--

CREATE TABLE IF NOT EXISTS `category_image_link` (
  `category_id` mediumint(11) unsigned NOT NULL,
  `image_id` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`category_id`,`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` text NOT NULL,
  `added_from` varchar(255) NOT NULL,
  `approved` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `score` mediumint(11) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `user_id` mediumint(11) unsigned NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `discount_code`
--

CREATE TABLE IF NOT EXISTS `discount_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(12) NOT NULL DEFAULT '',
  `discount` smallint(5) unsigned NOT NULL,
  `expire` datetime DEFAULT NULL,
  `discount_type` tinyint(1) unsigned NOT NULL COMMENT '1 = Percentage, 2 = Amount',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `count` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `original_filename` varchar(255) NOT NULL,
  `hash` varchar(32) DEFAULT NULL,
  `uploader` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `created_by` mediumint(11) unsigned NOT NULL,
  `last_editied_by` mediumint(11) unsigned DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_editied` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `path` (`path`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` mediumint(11) unsigned NOT NULL,
  `last_editied_by` mediumint(11) unsigned DEFAULT NULL,
  `last_editied` timestamp NULL DEFAULT NULL,
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `path` (`path`),
  KEY `active` (`active`),
  KEY `featured` (`featured`),
  KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute`
--

CREATE TABLE IF NOT EXISTS `product_attribute` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` mediumint(11) unsigned NOT NULL,
  `last_editied_by` mediumint(11) unsigned DEFAULT NULL,
  `last_editied` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Attributes e.g. Color, Size, etc.';

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute_value`
--

CREATE TABLE IF NOT EXISTS `product_attribute_value` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `attribute_id` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `attribute_id` (`attribute_id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Product Attribute Values for example Blue, Large, etc.';

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute_value_association`
--

CREATE TABLE IF NOT EXISTS `product_attribute_value_association` (
  `product_id` mediumint(11) unsigned NOT NULL,
  `attribute_id` mediumint(11) unsigned NOT NULL,
  `order` tinyint(3) unsigned DEFAULT NULL,
  `cost` double DEFAULT '0',
  KEY `product_id` (`product_id`),
  KEY `attribute_id` (`attribute_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Association of products and attribute values';

-- --------------------------------------------------------

--
-- Table structure for table `product_image_link`
--

CREATE TABLE IF NOT EXISTS `product_image_link` (
  `product_id` mediumint(11) unsigned NOT NULL,
  `image_id` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_review`
--

CREATE TABLE IF NOT EXISTS `product_review` (
  `product_id` mediumint(11) unsigned NOT NULL,
  `review_id` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_to_category`
--

CREATE TABLE IF NOT EXISTS `product_to_category` (
  `productId` mediumint(11) unsigned NOT NULL,
  `categoryId` mediumint(11) unsigned NOT NULL,
  PRIMARY KEY (`productId`,`categoryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `comment_id` mediumint(11) unsigned NOT NULL,
  `rating` decimal(5,1) NOT NULL DEFAULT '0.0',
  `title` varchar(255) NOT NULL,
  `helpful` mediumint(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Number of users who rated review',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `subtotal` decimal(10,2) unsigned NOT NULL,
  `delivery` decimal(10,2) unsigned NOT NULL,
  `vat` decimal(10,2) unsigned NOT NULL,
  `customer` mediumint(11) unsigned NOT NULL,
  `notes` varchar(255) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customer` (`customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` mediumint(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(75) NOT NULL,
  `surname` varchar(75) NOT NULL,
  `username` varchar(75) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(75) NOT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product_attribute_value_association`
--
ALTER TABLE `product_attribute_value_association`
  ADD CONSTRAINT `product_attribute_value_association_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `product_attribute_value_association_ibfk_2` FOREIGN KEY (`attribute_id`) REFERENCES `product_attribute_value` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;