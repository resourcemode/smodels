Ext.application({
    name: 'Sencha',
    requires: ['Ext.tab.Panel'],
    
    controllers: ['Main'],
    views: ['Main'],
    stores: ['Presidents'],
    models: ['President'],

    launch: function() {
    	
    	mainTabs = Ext.create("Ext.TabPanel", {
            fullscreen: true,
            tabBarPosition: 'bottom',
            id: "mainTabPanel",
            items: [{
                title: 'Products',
                iconCls: 'Products',
                cls: 'products',
                xtype: 'mainpanel'
            }],
	        listeners: {
		        initialize: function(panel, eOpts) {
		        	
		        }
	        }
        });
    	
    }
});