Ext.define('Sencha.view.PresidentDetail', {
    extend: 'Ext.form.Panel',
    xtype: 'presidentdetail',
    requires: ['Ext.form.FieldSet', 'Ext.field.Text'],
    config: {
        items: [{
            xtype: 'fieldset',
            items: [
                {
                    xtype: 'textfield',
                    name : 'firstName',
                    label: 'Name'
                },
                {
                    xtype: 'emailfield',
                    name : 'email',
                    label: 'Email'
                },
                {
                    xtype: 'passwordfield',
                    name : 'password',
                    label: 'Password'
                },
                {
        			docked: "bottom",
        			xtype: "toolbar",
        			items:
        			[
        				{
        					text: "Cancel",
        					handler: function() {
        					   var form = this.up('formpanel');
        					   form.hide();
        					}
        				},
        				{
        					xtype: "spacer"
        				},
        				{
        					text: "Update",
        					ui: "action",
        					handler: function() {
                               var form = this.up('formpanel');
                                form.submit({
                                    success: function(theFormPanel, result) {
                                        //the callback function is run when the user taps the 'ok' button
                                        Ext.Msg.alert('Thank You', 'Update Successful'+result.messages, function() {
        					                form.hide()
                                        });
                                    }
                                });
        					}
        				}
        			]
        		}
            ]
        }]
    }
});