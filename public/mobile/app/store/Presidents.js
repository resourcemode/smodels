Ext.define('Sencha.store.Presidents', {
    extend: 'Ext.data.Store',
    
    config: {
        model: 'Sencha.model.President',
        sorters: 'lastName',
        grouper : function(record) {
            return record.get('lastName')[0];
        },
        proxy: {
            type: 'ajax',
            url: 'http://localhost.smodels.com/json/products.php',
            reader: {
                type: 'json',
                rootProperty: 'results',
                idProperty: 'uid',
                totalProperty: 'totalCount'
            }
        },
        autoLoad: true,
    }
});
