<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Page\Filter;

use Supa\Page\Filter\EditFilter;
use Supa\Page\Validator\AbstractRecord;
use Supa\AbstractFilter;

/**
 * Filter class for editing pages
 */
class EditFilter extends \Supa\Page\Filter\CreateFilter {
    	
	/**
	 * Constructor for edit page filter class
	 */
    public function __construct() {
    	parent::__construct();        
    }
        
    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    }
}