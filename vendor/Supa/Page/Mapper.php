<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Page;

use Supa\Page\Page;
use \Exception;
use \PDO;
use Supa\User\User;

class Mapper extends \Supa\AbstractMapper {
    
	/**
	 * Constructor for page mapper class
	 * 
	 * @param PDO $db
	 */
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load a page by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return Page
	 */
	public function loadByUid($uid) {
		
		$uid = (int)$uid;
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND page.active = 1 ';
		}
		
		$sql = "SELECT page.id as uid, page.path, page.title, page.created_by AS createdBy, page.last_editied_by AS lastEditiedBy,
						page.content, page.active, page.created, page.last_editied AS lastEditied
				  FROM page
				 WHERE page.id = :uid
				   $activeCheckSQL 
				 LIMIT 1";
		
		$statement = $this->query($sql, array(':uid'=>$uid));
		
		if(!$statement) {
			throw new Exception('Error when loading page by uid: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$data) {
			throw new Exception('Failed fetching data when loading page for uid: ' . $this->getDbError($statement));
		}
		
		$page = new Page($data);
		
		return $page;
	}
	
	/**
	 * Load a page by url path
	 * 
	 * @param string $path
	 * @throws Exception
	 * @return Page
	 */
	public function loadByPath($path) {
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND page.active = 1 ';
		}
		
		$sql = "SELECT page.id as uid, page.path, page.title, page.created_by AS createdBy, page.last_editied_by AS lastEditiedBy,
						page.content, page.active, page.created, page.last_editied AS lastEditied
				  FROM page
				 WHERE page.path = :path
				   $activeCheckSQL 
				 LIMIT 1";
		
		$statement = $this->query($sql, array(':path'=>$path));
		
		if(!$statement) {
			throw new Exception('Failed loading page by path: ' . $this->getDbError());
		}
		
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		$page = null;
		if($data) {
			$page = new Page($data);
		}
		
		return $page;
	}
	
	/**
	 * Create a page
	 * 
	 * @param Page $page
	 * @throws Exception
	 * @return Page
	 */
	public function createPage(Page $page) {
		
		$insertData = array();
		$insertData[':title'] = $page->getTitle();
		$insertData[':path'] = $page->getPath();
		$insertData[':content'] = $page->getContent();
		$insertData[':userUid'] = $page->getCreatedBy();
		$insertData[':created'] = $ts = $this->getTimestamp();
		
		$sql = 'INSERT INTO `page`
						SET `title` = :title,
							`path` = :path,
							`content` = :content,
							`created_by` = :userUid,
							`created` = :created';
		$affected = $this->exec($sql, $insertData);
		
		if(false === $affected) {
			throw new Exception('Failed creating a page: ' . $this->getDbError());
		}
		
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			throw new Exception("lastInsertId return invalid when creating a page: $uid");
		}
		
		$page = $this->loadByUid($uid);
		$page->setCreated($ts);
		$page->setActiveState(1);
		
		return $page;
	}
	
	/**
	 * Edit a page
	 * 
	 * @param Page $page
	 * @throws Exception
	 * @return Page
	 */
	public function update(Page $page) {
		
		$editData = array();
		$editData[':title'] = $page->getTitle();
		$editData[':content'] = $page->getContent();
		$editData[':uid'] = $page->getUID();
		$editData[':lastEditiedBy'] = $page->getLastEditiedBy();
		$editData[':lastEditied'] = $ts = $this->getTimestamp();
		
		$sql = "UPDATE `page`
				   SET `title` = :title,
						`content` = :content,
						`last_editied_by` = :lastEditiedBy,
						`last_editied` = :lastEditied
				 WHERE `id` = :uid";
		$affected = $this->exec($sql, $editData);
		
		if(false === $affected) {
			throw new Exception('Failed editing page: ' . $this->getDbError());
		}
		
		$page->setLastEditied($ts);
		
		return $page;
	}
}