<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Page;

/**
 * Page class
 */
class Page {
	
    /* @var $uid int */
	protected $uid;
	
	/* @var $active int */
	protected $active;
	
	/* @var $title string */
	protected $title;
	
	/* @var $path string */
	protected $path;
	
	/* @var $uid int */
	protected $content;
	
	/* @var $uid int */
	protected $createdBy;
	
	/* @var $uid int */
	protected $lastEditiedBy;
	
	/**
	 * Formatted timestamp of when page last editied
	 * @var string
	 */
	protected $lastEditied;
	
	/**
	 * Formatted timestamp of when page was created
	 * @var string
	 */
	protected $created;

	/**
	 * Constructor for page class
	 *
	 * @param array $data Page data
	 */
	public function __construct($data) {
       	$this->assignClassVariables($data);
	}

	/**
	 * Determine if page is active
	 *
	 * @return bool True if page active, false otherwise
	 */
	public function isActive() {
		return (bool)$this->active;
	}
	
	/**
	 * Get the page title
	 * 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Get the page uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return $this->uid;
	}
	
	/**
	 * Set the page title
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}
	
	/**
	 * Set the page url path
	 * 
	 * @param string $path
	 */
	public function setPath($path) {
		$this->path = $path;
	}
	
	/**
	 * Set the page content
	 * 
	 * @param string $string
	 */
	public function setContent($content) {
		$this->content = $content;
	}
	
	/**
	 * Set the uid of user who last editied this page
	 * 
	 * @param int $uid
	 */
	public function setLastEditiedBy($uid) {
		$this->lastEditiedBy = $uid;
	}
	
	/**
	 * Set timestamp of when page last editied
	 * 
	 * @param string $timestamp
	 */
	public function setLastEditied($timestamp) {
		$this->lastEditied = $timestamp;
	}
	
	/**
	 * Set timestamp of when page was created
	 * 
	 * @param string $timestamp
	 */
	public function setCreated($timestamp) {
		$this->created = $timestamp;
	}
	
	/**
	 * Set the uid of user who created this page
	 * 
	 * @param int $uid
	 */
	public function setCreatedBy($uid) {
		$this->createdBy = $uid;
	}
	
	
	/**
	 * Get the uid of user who created this page
	 * 
	 * @return int
	 */
	public function getCreatedBy() {
		return $this->createdBy;
	}
	
	/**
	 * Get the uid of user who last editied this page
	 * 
	 * @return int
	 */
	public function getLastEditiedBy() {
		return $this->lastEditiedBy;
	}
	
	/**
	 * Get timestamp when page last editied
	 * 
	 * @return string
	 */
	public function getLastEditied() {
		return $this->lastEditied;
	}
	
	/**
	 * Get timestamp when page was created
	 * 
	 * @return string
	 */
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * Get the url path of the page
	 * 
	 */
	public function getPath() {
		return $this->path;
	}
	
	/**
	 * Get the page content
	 * 
	 * @return string
	 */
	public function getContent() {
		return $this->content;
	}

	/**
	 * Set the active state for the product
	 * 1 = active, 0 = not active
	 * 
	 * @param int $state A 1 (active) or 0 (not-active)
	 */
	public function setActiveState($state) {
		$this->active = $state;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
}