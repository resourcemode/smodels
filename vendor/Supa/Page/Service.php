<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Page;

use Supa\Page\Page;
use Exception;

/**
 * Page service class
 */
class Service {
    
    /**
     * Data mapper for page object
     * @var \Supa\AbstractMapper
     */
	protected $mapper;
	
	/**
	 * Array of registered messages
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by this class
	 * @var \Exception
	 */
	protected $lastException = null;
	
	/**
	 * Filter for creating a Page
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
		
	/**
	 * Filter for editing a Page
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;
	
	/**
	 * Constructor for page class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Get page by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Page\Page|null
	 */
	public function getByUid($uid) {
		try {
			$page = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$page = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $page;
	}
	
	/**
	 * Get a page by url path
	 * 
	 * @param string $path
	 * @return \Supa\Page\Page|null
	 */
	public function getPageByPath($path) {
		try {
			$page = $this->mapper->loadByPath($path);
		}catch(Exception $e) {
			$page = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $page;
	}
	
	/**
	 * Edit a page
	 * 
	 * @param array $pageData The updated page data
	 * @param \Supa\User\User $user The user performing the action
	 * @return \Supa\Page\Page|null
	 */
	public function updatePage(array $pageData = array(), \Supa\User\User $user) {
	    try {
	    	
			$filter = $this->editFilter;
			$filter->prepareFilter();
			$filter->setData($pageData);
			
	    	if(!$filter->isValid()) {
	    		$this->messages = $filter->getFlatMessages();
	    		throw new Exception('Invalid data given when trying to update page');
			}
			
			$page = new Page($pageData);
       		$page->setLastEditiedBy($user->getUID());
	        $page = $this->mapper->update($page);
	        
	    }catch(Exception $e) {
	        $page = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $page;
	}
	
	/**
	 * Create a page
	 * 
	 * @param array $data
	 * @param \Supa\User\User $user
	 * @return \Supa\Page\Page|null
	 */
	public function createPage(array $data = array(), \Supa\User\User $user) {
	    try {
	    	
			$filter = $this->createFilter;
			$filter->prepareFilter();
			$filter->setData($data);
			
	    	if(!$filter->isValid()) {
	    		$this->messages = $filter->getFlatMessages();
				throw new Exception('Invalid data given when creating page');
			}
			
			$page = new Page($data);
			$page->setCreatedBy($user->getUID());
			$page = $this->mapper->createPage($page);
	        
	    }catch(Exception $e) {
	        $page = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $page;
	}
	
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	    
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
	
	/**
	 * Set the filter for editing a Page
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
    
	/**
	 * Set the filter for creating a Page
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
}