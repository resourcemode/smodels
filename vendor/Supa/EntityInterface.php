<?php

namespace Supa;

interface EntityInterface {
	
    /**
     * Get Unique ID
     *
     * @return int
     */
    public function getUID();

    /**
     * Set the Unique ID
     *
     * @param int $id
     * @return EntityInterface
     */
    public function setUID($id);
}