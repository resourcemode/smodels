<?php

namespace Supa;

use \Zend\InputFilter\Factory;

abstract class AbstractFilter {
	
	/**
	 * Filter object for entity
	 * @var \Zend\InputFilter\BaseInputFilter
	 */
	protected $filter = null;
	    
	public function __construct() {
		$factory = new Factory();
		$this->filter = $factory->createInputFilter(array());
	}
	
	abstract public function prepareFilter();
	
    public function add(array $filter) {
    	$this->filter->add($filter);
    }
    
    public function addFilters(array $filters) {
    	foreach ($filters as $filter) {
    		$this->filter->add($filter);
    	}
    }
    
    public function getFilter() {
        return $this->filter;
    }
    
    public function setData(array $data = array()) {
        $this->filter->setData($data);
    }
    
    public function isValid() {
        return $this->filter->isValid();
    }
    
    public function getMessages() {
        return $this->filter->getMessages();
    }

    public function getFlatMessages($useFieldAsKey = true) {
    	
        $messages = $this->filter->getMessages();
        if(empty($messages)) return $messages;
        
        $returnMessages = array();
        foreach ($messages as $field => $messageData) {
        	
        	$tempMessageData = array();
        	
        	if(is_array($messageData)) {
        		foreach ($messageData as $validator=>$message) {
        			$tempMessageData[] = $message;
        		}
        	}else {
        		$tempMessageData[] = $messageData;
        	}
        	
        	if($useFieldAsKey) {
        		$returnMessages[$field] = $tempMessageData;
        	}else {
        		$returnMessages = $tempMessageData;
        	}
        	
        }
        
        return $returnMessages;
    }
    
	public function getActiveFilter() {
		return array(
			'name' => 'active',
			'required' => false,
			'validators' => array(
				array(
					'name' => 'digits',
					'options' => array(
						'messages' => array(
							\Zend\Validator\Digits::NOT_DIGITS => 'Invalid Active state, please re-enter.'
						)	
					),
				),
				array(
					'name' => 'in_array',
			        'options' => array(
			        	'haystack' => array('0', '1'),
						'messages' => array(
							\Zend\Validator\InArray::NOT_IN_ARRAY => 'Invalid Active state, please re-enter.'
						)
					),
				),
			)
		);		
	}

	public function getCreatedByFilter() {
		return array(
			'name' => 'createdBy',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'digits',
					'options' => array(
						'messages' => array(
							\Zend\Validator\Digits::NOT_DIGITS => 'Invalid Created By, please re-enter.'
							)
					 ),
				),
				array(
					'name' => 'greater_than',
					'options' => array(
						'min' => 0,
						'messages' => array (
							\Zend\Validator\GreaterThan::NOT_GREATER => 'Invalid Created By, please re-enter.'
						)
					),
				),
			)
		);
	}

	public function getLastEditiedByFilter() {
		return array(
			'name' => 'lastEditiedBy',
			'required' => true,
			'validators' => array(
				array(
					'name' => 'digits',
					'options' => array(
						'messages' => array(
							\Zend\Validator\Digits::NOT_DIGITS => 'Invalid Last Created By, please re-enter.'
						)
					),
				),
				array(
					'name' => 'greater_than',
					'options' => array(
						'min' => 0,
						'messages' => array (
							\Zend\Validator\GreaterThan::NOT_GREATER => 'Invalid Last Created By, please re-enter.'
						)
					),
				),
			)
		);
	}
	
	public function getUIDFilterConf() {
		return array(
			'name' => 'uid',
			'required' => false,
			'validators' => array(
				array(
					'name' => 'digits',
					'options' => array(
						'messages' => array(
							\Zend\Validator\Digits::NOT_DIGITS => 'Invalid UID, please re-enter.'
						)
					),
				),
				array(
					'name' => 'greater_than',
					'options' => array(
						'min' => 0,
						'messages' => array (
							\Zend\Validator\GreaterThan::NOT_GREATER => 'Invalid UID, please re-enter.'
						)
					),
				),
			)
		);
	}

}