<?php

namespace Supa;

// @TODO check if valid connection

use Supa\EntityInterface;
use Supa\MysqlDatabase;
use Supa\User\Mapper as UserMapper;

/**
 * Global function for lazy loading users
 * 
 * @param \Supa\EntityInterface|int|null $var The 'uid' field of user class
 * @param array $options Any set up options
 * @return \Supa\User\User The loaded user object
 */
function lazy_load_user(&$var, $options = null) {
    	
    if(!($var instanceof EntityInterface)) {
        $userMapper = new UserMapper(MysqlDatabase::getConn());
        $var = $userMapper->loadByUid($var);
	}
	
	return $var;
}

function lazy_load_entity(&$var, $mapper, $options = null) {
        	
    if(!($var instanceof EntityInterface)) {
        $mapper = new $mapper(MysqlDatabase::getConn());
        $var = $mapper->loadByUid($var);
	}
	
	return $var;
}