<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Image;

use Supa\Image\Image;
use Supa\Image\Collection as ImageCollection;
use Supa\Category\Category;
use Supa\Product\Product;
use \Exception;
use \PDO;
use Supa\User\User;

/**
 * Image mapper class
 */
class Mapper extends \Supa\AbstractMapper {
    
	/**
	 * Constructor for Image mapper class
	 * 
	 * @param \PDO $db
	 */
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load a Image by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return Image
	 */
	public function loadByUid($uid) {
		
		$uid = (int)$uid;
		
		$sql = "SELECT `image`.`id` AS uid, `image`.`created`, `image`.`filename`, `image`.`original_filename` AS originalFilename,
						`image`.`created_by` AS createdBy, `image`.`uploader` AS uploadedFrom
				  FROM `image`
				 WHERE `image`.`id` = $uid
				 LIMIT 1";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Error when loading image by uid: ' . $this->getDbError());
		}
		
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		$image = null;
		if($data) {
            $image = new Image($data);
		}
		
		return $image;
	}
	
	/**
	 * Create a image record in the data store that
	 * relates to a file on disk
	 * 
	 * @param Image $image Image record object
	 * @throws Exception
	 * @return Image The Image object relating to file on disk
	 */
	public function create(Image $image) {
		
		$filename = $this->db->quote($image->getFilename(), PDO::PARAM_STR);
		$originalFilename = $this->db->quote($image->getOriginalFilename(), PDO::PARAM_STR);
		$uploadedFrom = $this->db->quote($image->getUploadedFrom(), PDO::PARAM_STR);
		$createdBy = $this->db->quote($image->getCreatedBy()->getUID(), PDO::PARAM_INT);
		$created = $this->db->quote($this->getTimestamp(), PDO::PARAM_STR);
		
		$sql = "INSERT INTO `image`
						SET `filename` = $filename,
							`original_filename` = $originalFilename,
							`uploader` = $uploadedFrom,
							`created_by` = $createdBy,
							`created` = $created";
		
		$affected = $this->exec($sql);
		
		if(false === $affected) {
			throw new Exception('Failed creating a image record: ' . $this->getDbError());
		}
		
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			throw new Exception("lastInsertId return invalid when creating a image record: $uid");
		}
		
		$image->setUID($uid);
		$image->setCreated($created);
		
		return $image;
	}
	
	/**
	 * Delete image record from data store
	 * 
	 * @param Image $image The Image to delete
	 * @throws Exception
	 * @return bool True on success, false otherwise
	 */
	public function delete(Image $image) {
		
		$imageUid = (int)$image->getUID();
		
		$sql = "DELETE FROM `image` WHERE `image`.`id` = $imageUid";
		
		$statement = $this->query($sql);
		
		return $statement;
	}
	
	/**
	 * Link images to a product in the data store
	 * 
	 * @param \ArrayIterator $imageCollection
	 * @param Product $product
	 * @throws Exception
	 * @return bool True on success, false otherwise 
	 */
	public function linkImagesToProduct(\ArrayIterator $imageCollection, Product $product) {
		
		$prodUid = (int)$product->getUID();
		
		if($imageCollection->count() == 0) {
			return false;		
		}
		
		$sql = 'INSERT INTO `product_image_link` SET `product_id` = :prodUid, `image_id` = :imageUid';
		
		$statement = $this->db->prepare($sql);
		foreach($imageCollection as $image) {
			$data[':prodUid'] = $prodUid;
			$data[':imageUid']  = (int)$image->getUID();
			$res = $statement->execute($data);
			if(!$res) {
				throw new Exception('Error linking image to product: ' . $this->getDbError($statement));
			}
		}
		
		// reset lazy load
		$product->setImages($product->getUID());
		
		return true;		
	}
	
	/**
	 * Remove images from product
	 * 
	 * @param \ArrayIterator $images The images to remove
	 * @param Product $product Product to remove images from
	 * @return bool True on success, false otherwise
	 */
	public function unlinkImagesFromProduct(\ArrayIterator $images, Product $product) {
		
		$prodUid = (int)$product->getUID();
		
		if($images->count() == 0) {
			return false;		
		}

		$sql = 'DELETE FROM `product_image_link` WHERE `product_id` = :prodUid AND `image_id` = :imageUid';		
		
		$statement = $this->db->prepare($sql);
		foreach($images as $image) {
			$data[':prodUid'] = $prodUid;
			$data[':imageUid']  = (int)$image->getUID();
			$res = $statement->execute($data);
			if(!$res) {
				throw new Exception('Error removing image from product: ' . $this->getDbError($statement));
			}
		}
		
		// reset lazy load
		$product->setImages($product->getUID());
		
		return true;
	}
	
	/**
	 * Link images to a category in the data store
	 * 
	 * @param \ArrayIterator $imageCollection Images to link to category
	 * @param Category $category Category to link images to
	 * @throws Exception
	 * @return bool True on success, false otherwise 
	 */
	public function linkImagesToCategory(\ArrayIterator $imageCollection, Category $category) {
		
		$catUid = (int)$category->getUID();
		
		if($imageCollection->count() == 0) {
			return false;		
		}
		
		$sql = 'INSERT INTO `category_image_link` SET `category_id` = :catUid, `image_id` = :imageUid';
		
		$statement = $this->db->prepare($sql);
		foreach($imageCollection as $image) {
			$data[':catUid'] = $catUid;
			$data[':imageUid']  = (int)$image->getUID();
			$res = $statement->execute($data);
			if(!$res) {
				throw new Exception('Error linking image to category: ' . $this->getDbError($statement));
			}
		}
		
		// reset lazy load
		$category->setImages($category->getUID());
		
		return true;		
	}
	
	/**
	 * Remove images from category
	 * 
	 * @param \ArrayIterator $images The images to remove
	 * @param Category $category Category to remove images from
	 * @return bool True on success, false otherwise
	 */
	public function unlinkImagesFromCategory(\ArrayIterator $images, Category $category) {
		
		$catUid = (int)$category->getUID();
		
		if($images->count() == 0) {
			return false;		
		}

		$sql = 'DELETE FROM `category_image_link` WHERE `category_id` = :catUid AND `image_id` = :imageUid';		
		
		$statement = $this->db->prepare($sql);
		foreach($images as $image) {
			$data[':catUid'] = $catUid;
			$data[':imageUid']  = (int)$image->getUID();
			$res = $statement->execute($data);
			if(!$res) {
				throw new Exception('Error removing image from category: ' . $this->getDbError($statement));
			}
		}
		
		// reset lazy load
		$category->setImages($category->getUID());
		
		return true;
	}
	
	/**
	 * Load images for a Product
	 * 
	 * @param Product $product
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function loadProductImages(Product $product) {
		
		$prodUid = (int)$product->getUID();
		
		$sql = "SELECT `image`.`id` AS uid, `image`.`created`, `image`.`filename`, `image`.`original_filename` AS originalFilename,
						`image`.`created_by` AS createdBy, `image`.`uploader` AS uploadedFrom
				  FROM `product_image_link`
			INNER JOIN `image` ON `image`.`id` = `product_image_link`.`image_id`
				 WHERE `product_image_link`.`product_id` = $prodUid";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when trying to get product images: ' . $this->getDbError());
		}
		
		$imageCollection = new ImageCollection(array());
		if($statement->rowCount() >= 1) {
			$images = $statement->fetchAll(PDO::FETCH_ASSOC);
			if(!$images) {
				throw new Exception('Failed fetching images when trying to get product images: ' . $this->getDbError($statement));
			}
			foreach($images as $image) {
				$imageCollection->append(new Image($image));
			}
		}
		
		return $imageCollection;
		
	}
	
	/**
	 * Load images for a Category
	 * 
	 * @param Category $category
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function loadCategoryImages(Category $category) {
		
		$catUid = (int)$category->getUID();
		
		$sql = "SELECT `image`.`id` AS uid, `image`.`created`, `image`.`filename`, `image`.`original_filename` AS originalFilename,
						`image`.`created_by` AS createdBy, `image`.`uploader` AS uploadedFrom
				  FROM `category_image_link`
			INNER JOIN `image` ON `image`.`id` = `category_image_link`.`image_id`
				 WHERE `category_image_link`.`category_id` = $catUid";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when trying to get category images: ' . $this->getDbError());
		}
		
		$imageCollection = new ImageCollection(array());
		if($statement->rowCount() >= 1) {
			$images = $statement->fetchAll(PDO::FETCH_ASSOC);
			if(!$images) {
				throw new Exception('Failed fetching images when trying to get category images: ' . $this->getDbError($statement));
			}
			foreach($images as $image) {
				$imageCollection->append(new Image($image));
			}
		}
		
		return $imageCollection;
		
	}
}