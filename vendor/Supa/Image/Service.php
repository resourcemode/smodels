<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Image;

use Supa\Image\Image;
use Supa\Image\Collection as ImageCollection;
use Exception;

/**
 * Image service class
 */
class Service {
    
    /**
     * Data mapper for Image object
     * @var \Supa\AbstractMapper
     */
	protected $mapper;
	
	/**
	 * Array of registered messages
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by this class
	 * @var \Exception
	 */
	protected $lastException = null;
	
	/**
	 * Filter for creating a Image
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
		
	/**
	 * Filter for editing a Image
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;
		
	/**
	 * Constructor for Image service class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Get Image by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Image\Image|null
	 */
	public function getByUid($uid) {
		try {
			$image = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$image = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $image;
	}
	
	/**
	 * Create a image record that relates to an image
	 * file on disk
	 * 
	 * @param array $data
	 * @param \Supa\User\User $user
	 * @return \Supa\Image\Image|null
	 */
	public function createImage(array $data = array(), \Supa\User\User $user) {
	    try {
	    	
			$filter = $this->createFilter;
			$filter->prepareFilter();
			$filter->setData($data);
			
	    	if(!$filter->isValid()) {
	    		$this->messages = $filter->getFlatMessages();
				throw new Exception('Invalid data given when trying to create image');
			}
			
			$image = new Image($data);
       		$image->setCreatedBy($user->getUID());
       		
       		// upload image
       		if(isset($data['filesArray']) && is_array($data['filesArray'])) {
       			$image->uploadFile($data['filesArray']);
       		}
       		
	        $image = $this->mapper->create($image);
	        
	    }catch(Exception $e) {
	        $image = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $image;
	}
	
	// @todo Check to see if image is being used in link tables?
	/**
	 * Delete image
	 * 
	 * @param int $imageUid The uid of image we are deleting
	 * @return bool True on success, false otherwise
	 */
	public function deleteImage($imageUid) {
		try {
			$image = $this->getByUid($imageUid);			
			if(is_null($image)) {
				throw new Exception($this->getLastException()->getMessage());	
			}
			
			// delete image from disk
			if($image->isUploaded()) {
				$image->removeFile();
			}
			
			$result = $this->mapper->delete($image);
			
		}catch(Exception $e) {
			$result = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $result;
	}
	
	/**
	 * Get product images
	 * 
	 * @param \Supa\Product\Product $product
	 * @return \ArrayIterator|null
	 */
	public function getProductImages(\Supa\Product\Product $product) {
		
		try {
			$images = $this->mapper->loadProductImages($product);
		}catch(Exception $e) {
			$images = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $images;
	}
	
	/**
	 * Get category images
	 * 
	 * @param \Supa\Category\Category $category
	 * @return \ArrayIterator|null
	 */
	public function getCategoryImages(\Supa\Category\Category $category) {
		
		try {
			$images = $this->mapper->loadCategoryImages($category);
		}catch(Exception $e) {
			$images = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $images;
	}
		
    /**
     * Add 1 or more images to a product
     * 
     * @param array $imageUids The uid's of images for the product
     * @param \Supa\Product\Product $product The product to add images to
     * @return bool
     */
	public function addImagesToProduct(array $imageUids = array(), \Supa\Product\Product $product) {
		
		$uids = array();
		$result = true;			
		try {
			
			if(empty($imageUids)) {
				throw new Exception('No image uids given to add to product');
			}
			
			$sanitizedValues = filter_var($imageUids, FILTER_VALIDATE_INT, array(
			  'flags'   => FILTER_REQUIRE_ARRAY, // require array as inputs
			  'options' => array('min_range' => 1)
			));
			
			$imageCollection = new ImageCollection(array());
			foreach($sanitizedValues as $uid) {
				if(!$uid) continue;
				$image = $this->getByUid($uid);
				if(is_null($image)) throw new Exception($this->getLastException()->getMessage());
				$imageCollection->append($image);
			}
		    
			if($imageCollection->count() == 0) {
				throw new Exception('No valid images to add to product');
			}
			
			$result = $this->mapper->linkImagesToProduct($imageCollection, $product);
			
		}catch(Exception $e) {
			$result = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $result;
	}
		
    /**
     * Add 1 or more images to a category
     * 
     * @param array $imageUids The uid's of images for the category
     * @param \Supa\Category\Category $category The category to add images to
     * @return bool
     */
	public function addImagesToCategory(array $imageUids = array(), \Supa\Category\Category $category) {
		
		$uids = array();
		$result = true;			
		try {
			
			if(empty($imageUids)) {
				throw new Exception('No image uids given to add to product');
			}
			
			$sanitizedValues = filter_var($imageUids, FILTER_VALIDATE_INT, array(
			  'flags'   => FILTER_REQUIRE_ARRAY, // require array as inputs
			  'options' => array('min_range' => 1)
			));
			
			$imageCollection = new ImageCollection(array());
			foreach($sanitizedValues as $uid) {
				if(!$uid) continue;
				$image = $this->getByUid($uid);
				if(is_null($image)) throw new Exception($this->getLastException()->getMessage());
				$imageCollection->append($image);
			}
		    
			if($imageCollection->count() == 0) {
				throw new Exception('No valid images to add to category');
			}
			
			$result = $this->mapper->linkImagesToCategory($imageCollection, $category);
			
		}catch(Exception $e) {
			$result = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $result;
	}
	
	/**
	 * Remove images from a product
	 * 
	 * @param array $imageUids The images uids to remove from product
	 * @param \Supa\Product\Product $product Product to remove images from
	 * @return bool True on succes, False otherwise
	 */
	public function removeProductImages(array $imageUids = array(), \Supa\Product\Product $product) {
		
		try {
			
			if(empty($imageUids)) {
				throw new Exception('No image uids given to remove from product');
			}
			
			$sanitizedValues = filter_var($imageUids, FILTER_VALIDATE_INT, array(
			  'flags'   => FILTER_REQUIRE_ARRAY, // require array as inputs
			  'options' => array('min_range' => 1)
			));
			
			$images = new ImageCollection(array());
			foreach($sanitizedValues as $uid) {
				if(!$uid) continue;
				$image = $this->getByUid($uid);
				if(is_null($image)) throw new Exception($this->getLastException()->getMessage());
				$images->append($image);
			}
		    
			if($images->count() == 0) {
				throw new Exception('No valid images to add to product');
			}
			
			$result = $this->mapper->unlinkImagesFromProduct($images, $product);
		}catch(Exception $e) {
			$result = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $result;
	}
	
	/**
	 * Remove images from a category
	 * 
	 * @param array $imageUids The images uids to remove from category
	 * @param \Supa\Category\Category $category Category to remove images from
	 * @return bool True on succes, False otherwise
	 */
	public function removeCategoryImages(array $imageUids = array(), \Supa\Category\Category $category) {
		
		try {
			
			if(empty($imageUids)) {
				throw new Exception('No image uids given to remove from category');
			}
			
			$sanitizedValues = filter_var($imageUids, FILTER_VALIDATE_INT, array(
			  'flags'   => FILTER_REQUIRE_ARRAY, // require array as inputs
			  'options' => array('min_range' => 1)
			));
			
			$images = new ImageCollection(array());
			foreach($sanitizedValues as $uid) {
				if(!$uid) continue;
				$image = $this->getByUid($uid);
				if(is_null($image)) throw new Exception($this->getLastException()->getMessage());
				$images->append($image);
			}
		    
			if($images->count() == 0) {
				throw new Exception('No valid images to add to category');
			}
			
			$result = $this->mapper->unlinkImagesFromCategory($images, $category);
		}catch(Exception $e) {
			$result = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $result;
	}
	
	/**
	 * Set the filter for editing a Image
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
    
	/**
	 * Set the filter for creating a Image
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
    	
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	    
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
}