<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Image;

/**
 * Image collection class
 */
class Collection extends \ArrayIterator  {

	/**
	 * Constructor for Image collection class
	 * 
	 * @param array $array Array of \Supa\Image\Image objects
	 * @see \Supa\Image\Image For Items in collection
	 * @see ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}