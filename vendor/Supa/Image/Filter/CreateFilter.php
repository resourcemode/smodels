<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Image\Filter;

use Supa\Image\Filter\CreateFilter;
use Supa\AbstractFilter;

// TODO: work out a way to make fields required on the fly

/**
 * Filter class for creating image records
 */
class CreateFilter extends AbstractFilter {
	
	/**
	 * Constructor for create image record filter class
	 */
    public function __construct() {
    	parent::__construct();        
    }
    
    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	$this->add(array(
                'name' => 'filename',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                    	'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a filename.'
                    		)
                    	)
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^.{1,255}$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid Filename, please re-enter'
                    		)
                    	)
                    ),
                ),
            ));
            
    	$this->add(array(
                'name' => 'originalFilename',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a original filename.'
                    		)
                    	)
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^.{1,255}$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid Original Filename, please re-enter'
                    		)
                    	)
                    ),
                ),
            ));
            
    	$this->add(array(
                'name' => 'uploadedFrom',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                    	'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter uploader hostname or IP.'
                    		)
                    	)
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^.{1,255}$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid Uploaded From value, please re-enter'
                    		)
                    	)
                    ),
                ),
            ));
            
        // common filters
        // TODO: change to use: $this->addCreatedByFilter(); ...
        //$this->add($this->getCreatedByFilter());
        //$this->add($this->getLastEditiedByFilter());
        //$this->add($this->getActiveFilter());
    }
}