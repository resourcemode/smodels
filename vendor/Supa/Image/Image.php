<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Image;

use Supa\User\User;
use Supa\Image\Image;
use \Exception;

/**
 * Image class
 */
class Image implements \Supa\EntityInterface {

    // max filesize = 10MB
    const MAX_FILESIZE = 10485760;
    const IMAGE_DIR = 'c:/Apache2/htdocs/smodels/smodels/images/product/';
    
    protected $uid;
    protected $filename;
    protected $originalFilename;
    protected $uploadedFrom;
    
	/**
	 * Timestamp of when Category was created
	 * @var string
	 */
	protected $created;
	
	/**
	 * User who created category
	 * @var \Supa\User\User|int
	 */
	protected $createdBy;
    
	/**
	 * Constructor for image class
	 *
	 * @param array $data image data
	 */    
    public function __construct(array $data = array()) {
    	$this->assignClassVariables($data);
    }
    
	/**
	 * Get the image uid
	 * 
	 * @return int
	 */    
    public function getUID() {
        return $this->uid;
    }
    
	/**
	 * Get image filename
	 * 
	 * @param string $type Resize type for image
	 * @param string|int $w Width of image
	 * @param string|int $h Height of image
	 * @return string
	 */
    public function getFilename($type = null, $w = null, $h = null) {
        
        // r = ratio, c = crop, t = test
        if(!is_null($type) && !is_null($w) && !is_null($h) && in_array($type, array('t', 'r', 'c'))) {
            return "{$this->uid}{$type}{$w}x{$h}{$this->getExtension()}";
        }
        
        return $this->filename;
    }
    
	/**
	 * Get hostname/ip of uploader
	 * 
	 * @return string
	 */    
    public function getUploadedFrom() {
        return $this->uploadedFrom;
    }
    
	/**
	 * Get original filename of image
	 * 
	 * @return string
	 */      
    public function getOriginalFilename() {
        return $this->originalFilename;
    }
    
	/**
	 * Get the timestamp of when image was created
	 * 
	 * @return string
	 */    
    public function getCreated() {
        return $this->created;
    }
    
	/**
	 * Get the User who created this image
	 * 
	 * @return \Supa\User\User
	 */
	public function getCreatedBy() {
		return \Supa\lazy_load_user($this->createdBy, 'UserMapper', 'loadByUid');
	}
    
	/**
	 * Set the image uid
	 * 
	 * @param int $uid
	 * @return \Supa\EntityInterface
	 */    
    public function setUID($uid) {
        $this->uid = $uid;
        return $this;
    }
    
	/**
	 * Set filename of image
	 * 
	 * @param string $filename
	 */      
    public function setFilename($filename) {
        $this->filename = $filename;
    }
    
	/**
	 * Set original filename of image
	 * 
	 * @param string $filename
	 */
    public function setOriginalFilename($filename) {
        $this->originalFilename = $filename;
    }
    
	/**
	 * Set uploader hostname/ip
	 * 
	 * @param string $filename
	 */  
    public function setUploadedFrom($uploadedFrom) {
        $this->uploadedFrom = $uploadedFrom;
    }
    
	/**
	 * Set timestamp of when the image was created
	 * 
	 * @param string $created
	 */    
    public function setCreated($created) {
        $this->created = $created;
    }
    
	/**
	 * Set the uid of user who created this image
	 * 
	 * @param int $userUid
	 */    
    public function setCreatedBy($userUid) {
        $this->createdBy = $userUid;
    }
    
    /**
     * Upload image file
     * 
     * @param array $vars
     * @return bool True on success, false otherwise 
     */
    public function uploadFile($vars) {
    	
        // set new path
        $newPath = self::IMAGE_DIR . $this->getOriginalFilename();
        
        if(!file_exists($vars['tmp_name'])) {
            // upload failed
            throw new Exception('Temporary upload failed, check filesize and location');
        }
        
        // ignore the type category
        $type = $this->parseFileType($vars['type']);
        
        // is filetype allowed?        
        if(!in_array($type, $this->getAllowedFiletypes())) {
            throw new Exception('Invalid filetype "'.$type.'" supplied, must be one of: '.
            	implode(", ", $this->getAllowedFiletypes()));
        }
        
        // check filesize
        if($vars['size'] > static::MAX_FILESIZE) {
            throw new Exception('Filesize cannot be greater than ' . static::MAX_FILESIZE);
        }
            
        // move file
        if($this->move($vars['tmp_name'], $newPath)) {

            // moved successfully
            chmod($newPath, 0777);
            
            if(!$checksum = sha1_file($newPath)) {
            	throw new Exception('Failed to generate checksum from upload file');
            }
            
            return true;
            
        } else {
            // move failed
            throw new Exception('Failed to store upload');
        }
        
        return false;
    }
    
	/**
	 * Check if file uploaded
	 * 
	 * @see http://php.net/manual/en/function.file-exists.php
	 * @return bool True on success, false on failure
	 */	   
    public function isUploaded() {
        return file_exists(self::IMAGE_DIR . $this->getOriginalFilename());
    }
	
	/**
	 * Remove image file from disk
	 * 
	 * @see http://www.php.net/manual/en/function.unlink.php
	 * @return bool True on success, false on failure
	 */	
    public function removeFile() {
        return @unlink(self::IMAGE_DIR . $this->getOriginalFilename());
    }
    
	/**
	 * Echos raw image data
	 * 
	 * @see http://www.php.net/manual/en/function.readfile.php
	 * @throws \Exception
	 * @return int Number of bytes read from file
	 */
	public function dumpImageData() {
	    // check if source file uploaded
		if($this->isUploaded()) {
			echo readfile(self::IMAGE_DIR . $this->getOriginalFilename());
		}else {
			throw new Exception('Cannot get image data, image file is not uploaded');
		}
	}
    
	/**
	 * Move uploaded file to new destination
	 * 
	 * @see http://www.php.net/manual/en/function.move-uploaded-file.php
	 * @param string $tmpPath Temp upload path
	 * @param string $newPath New path for file upload
	 * @return bool True on success, false otherwise
	 */
    protected function move($tmpPath, $newPath) {
        return @move_uploaded_file($tmpPath, $newPath);
    }

    /**
     * Helper method to assign class property values
     * 
     * @param array $data Class property name/value pairs
     * @return void
     */
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
    
	/**
	 * Get file type from mime name
	 * 
	 * @param string The mime type name to parse
	 * @return string Image file extension 
	 */
    protected function parseFileType($typeToParse) {
        switch($typeToParse) {
            case 'image/jpg':
            case 'image/pjpeg':
                return 'jpeg';
                break;
            case 'application/x-png':
            case 'image/x-png':
                return 'png';
                break;
            default:
                return preg_replace('([A-za-z]*/)','',$typeToParse);
        }
    }
    
    /**
     * Allowed image file types
     * 
     * @return array List of allowed file types
     */
	public function getAllowedFiletypes() {
		return array(1 => 'gif', 2 => 'jpeg', 3 => 'png');
	}

	/**
	 * Get file extension of image, with or without the '.'
	 * 
	 * @param bool Add a '.' to type, i.e '.jpg', true by default
	 * @return string|null File extension of uploaded image or null
	 */
	public function getExtension($withDot = true) {
	    $ext = null;
	    if(isset($this->originalFilename)) {
	        $info = pathinfo($this->originalFilename);
	        $ext = $withDot ? '.' . $info['extension'] : $info['extension'];
	    }
	    return $ext;
	}
}