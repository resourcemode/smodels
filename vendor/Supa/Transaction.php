<?php

namespace Supa;

class Transaction {
	
	protected $uid;
	protected $subtotal;
	protected $created;
	
	public function __construct($data) {
		$this->uid = (int)$data['id'];
		$this->subtotal = (int)$data['subtotal'];
		$this->created = $data['created'];
	}
}