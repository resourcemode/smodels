<?php

namespace Supa;

class Order {
	
	protected $uid;
	protected $transactionId;
	protected $created;
	
	public function __construct($data) {
		$this->uid = (int)$data['id'];
		$this->transactionId = (int)$data['transactionId'];
		$this->created = $data['created'];
	}
}