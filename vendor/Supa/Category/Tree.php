<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Category;

/**
 * Helper class to output an indented tree structure from $categories
 */
class Tree  {
	
    /**
     * Get the category tree
     * 
     * @param \ArrayIterator $categories
     * @param string $indent The indentation string to use
     * @param string $nl The new line option
     * @return string
     */
	public function getTree($categories, $indent = ' - ', $nl = '<br />') {
		
		$out = "";		
		// start with an empty $right stack  
	    $right = array();  
		
	    foreach($categories as $cat) {
	    	
	        // only check stack if there is one  
	        if (count($right) > 0) {  
	            // check if we should remove a node from the stack  
	            while ($right[count($right)-1] < $cat->getRgt()) {  
	                array_pop($right);  
	            }  
	        }  
	  
	        // display indented node title  
	        $out .= str_repeat($indent, count($right)). $cat->getLft(). ' ' . $cat->getTitle() . ' ' . $cat->getRgt() . $nl;  
	  
	        // add this node to the stack  
	        $right[] = $cat->getRgt();  
	    }
	    
	    return $out;
	}
	
}