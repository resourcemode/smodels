<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Category;

use Supa\Category\Collection as CategoryCollection;
use Exception;
use PDO;

/**
 * Category service class
 */
class Service {

	/**
	 * Data mapper for Category object
	 * @var \Supa\AbstractMapper
	 */
	protected $mapper;
	
	/**
	 * Holds success or error messages 
	 * from performed operations
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by service class
	 * @var \Exception
	 */
	protected $lastException;
	
	/**
	 * Filter for creating a Category
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
		
	/**
	 * Filter for editing a Category
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;
	
	/**
	 * Constructor for category mapper
	 *  
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Get the category by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Category\Category|null
	 */
	public function getByUid($uid) {
		try {
			$category = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$category = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $category;
	}
	
	/**
	 * Get categories by parent uid
	 * 
	 * @param int $parentUid
	 * @return \ArrayIterator|null
	 */
	public function getCategoriesByParentUid($parentUid) {
		try {
			$categories = $this->mapper->loadCatsByParentUid($parentUid);
		}catch(Exception $e) {
			$categories = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $categories;
	}
	
	/**
	 * Get the categories in tree by category uid
	 * 
	 * @param int $uid
	 * @return \ArrayIterator|null
	 */
	public function getCategoriesInTreeByUid($uid) {
		try {
			$category = $this->getByUid($uid);
			if(is_null($category)) throw new Exception($this->getLastException()->getMessage());
			$tree = $this->mapper->loadCatTree($category);
		}catch(Exception $e) {
			$tree = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $tree;
	}
	
	/**
	 * Add a category to the tree
	 *  
	 * @param int $parentCatUid
	 * @param array $catData
	 * @param \Supa\User\User $user
	 * @return \Supa\Category\Category|null
	 */
	public function addCategory($parentCatUid, array $catData = array(), \Supa\User\User $user) {
		try {
			
			$parentCatUid = (int)$parentCatUid;
			
			$filter = $this->createFilter;
			$filter->prepareFilter();
			$filter->setData($catData);
			
	    	if(!$filter->isValid()) {
	    		$this->messages = $filter->getFlatMessages();
				throw new Exception('Unable to save category: '.print_r($filter->getMessages(), true));
			}
			
			$parentCat = $this->getByUid($parentCatUid);
			if(is_null($parentCat)) throw new Exception($this->getLastException()->getMessage());
			$newCategory = new Category($catData);
			// TODO: move setters to mapper? - Pass User to mapper or get current user from service? 
			$newCategory->setCreatedBy($user->getUID());
			$newCategory = $this->mapper->addCatToTree($parentCat, $newCategory);
			$newCategory->setParent($parentCatUid);
			
		}catch(Exception $e) {
			$newCategory = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $newCategory;
	}
	
	/**
	 * Delete a category from the tree
	 * 
	 * @param int $catUid
	 * @return bool True on success, false otherwise
	 */
	public function deleteCategory($catUid) {
		try {
			$category = $this->getByUid($catUid);			
			if(is_null($category)) {
				throw new Exception($this->getLastException()->getMessage());	
			}
			
			if($this->mapper->hasProducts($category)) {
				throw new Exception('Unable to delete category because it has products (active or not) linked to it');
			}
			
			$result = $this->mapper->delCatFromTree($category);
		}catch(Exception $e) {
			$result = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $result;
	}
	
	/**
	 * Get categories in a path e.g ROOT > cat 1 > ...
	 * 
	 * @param int $catUid
	 * @return \ArrayIterator|null
	 */
	public function getCategoryPath($catUid) {
		try {
			$category = $this->getByUid($catUid);
			if(is_null($category)) throw new Exception($this->getLastException()->getMessage());
			$catColl = $this->mapper->loadCatPath($category);
		}catch(Exception $e) {
			$catColl = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $catColl;
	}
	
	/**
	 * Update a category
	 * 
	 * @param array $catData
	 * @param \Supa\User\User $user
	 * @return \Supa\Category\Category|null
	 */
	public function updateCategory(array $catData = array(), \Supa\User\User $user) {
		try {
			
			$filter = $this->editFilter;
			$filter->prepareFilter();
			$filter->setData($catData);
			
	    	if(!$filter->isValid()) {
	    		$this->messages = $filter->getFlatMessages();
				throw new Exception('Unable to update category: '.print_r($filter->getMessages(), true));
			}

			$category = new Category($catData);			
			$category->setLastEditiedBy($user->getUID());
			$category->setLastEditied($this->mapper->getTimestamp());
			$category->setTitle($catData['title']);
			$category->setPath($catData['path']);
			$category->setDescription($catData['description']);
			$category = $this->mapper->update($category);
			
		}catch(Exception $e) {
			$category = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $category;
	}
	
	/**
	 * Move a category to a new category
	 * 
	 * @param int $oldCatId The old category uid
	 * @param int $newCatId The category uid
	 * @param \Supa\User\User $user
	 * @return \Supa\Category\Category|null
	 */
	public function moveCategory($oldCatId, $newCatId, \Supa\User\User $user) {
		try {
			// category being moved
			$category = $this->getByUid($oldCatId);
			if(is_null($category)) throw new Exception($this->getLastException()->getMessage());
						
			// category being moved to
			$moveToCategory = $this->getByUid($newCatId);
			if(is_null($category)) throw new Exception($this->getLastException()->getMessage());

			// TODO: move to own method: $this->updateLastEditiedBy($userId);?
			$lastEditiedBy = $user->getUID();
			$category->setLastEditiedBy($lastEditiedBy);
			
			$category = $this->mapper->moveCategory($category, $moveToCategory);
			
		}catch(Exception $e) {
			$category = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		
		return $category;
	}
	
	/**
	 * Get any category service class messages
	 * 
	 * @return array
	 */
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
    
    /**
     * Add product to 1 or more categories
     * 
     * @param array $catUids The uid's of categories for the product
     * @param \Supa\Product\Product $product The product to add to categories
     * @return bool
     */
	public function addProductToCategories(array $catUids = array(), \Supa\Product\Product $product) {
		
		$uids = array();
		$result = true;			
		try {
			
			if(empty($catUids)) {
				throw new Exception('No category uids given to add product to');
			}
			
			$sanitizedValues = filter_var($catUids, FILTER_VALIDATE_INT, array(
			  'flags'   => FILTER_REQUIRE_ARRAY, // require array as inputs
			  'options' => array('min_range' => 1)
			));
			
			$categoryCollection = new CategoryCollection(array());
			foreach($sanitizedValues as $uid) {
				if(!$uid) continue;
				$category = $this->getByUid($uid);
				if(is_null($category)) throw new Exception($this->getLastException()->getMessage());
				$categoryCollection->append($category);
			}
		    
			if($categoryCollection->count() == 0) {
				throw new Exception('No valid categories to add product to');
			}
			
			$result = $this->mapper->linkProductToCategories($categoryCollection, $product);
			
		}catch(Exception $e) {
			$result = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $result;
	}
	
	/**
	 * Remove or add product categories to a Product
	 * 
	 * @param \Supa\Product\Product $product The product we are updating categories for
	 * @param array $updatedCatUids The updated category uid's for product
	 * @param array $currentCatUids Current category uid's the product is in
	 * @return bool True on success, false otherwise
	 */
	public function updateProductCats(\Supa\Product\Product $product, $updatedCatUids = array(), $currentCatUids = array()) {
		try {
		
			// throw exception if both arrays are empty
			$bothEmpty = (empty($currentCatUids) ? (empty($updatedCatUids) ? true : false) : false);
			if($bothEmpty) throw new Exception('No categories given when trying to update product categories');
			
			$catUidsToDelete = array();
			$catUidsToAdd = array();
			if(!empty($currentCatUids)) {
				// delete
				$catUidsToDelete = array_diff($currentCatUids, $updatedCatUids);
				// add
				$catUidsToAdd = array_diff($updatedCatUids, $currentCatUids);
			}else {
				// just add
				$catUidsToAdd = $updatedCatUids;
			}
			$updated = $this->mapper->updateProductCats($product, $catUidsToAdd, $catUidsToDelete);
			
			// this forces the product's categories to be "reset" so they can be lazy-load'ed again
			// TODO: change? 
			$product->setCategories($product->getUID());
		}catch(Exception $e) {
			$updated = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $updated;
	}
	
	/**
	 * Get category children
	 * 
	 * @param int $catUid Parent category uid
	 * @return \ArrayIterator|null
	 */	
	public function getCategoryChildren($catUid) {
		
		try {
			
			$category = $this->getByUid($catUid);
			if(is_null($category)) throw new Exception($this->getLastException()->getMessage());
			
			$children = $this->mapper->loadCatChildrenByLftAndRgt($category);
			
		}catch(Exception $e) {
			$children = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $children;		
	}
	
	/**
	 * Get categories' by product
	 * 
	 * @param \Supa\Product\Product $product
	 * @return \ArrayIterator|null
	 */
	public function getProductCategories(\Supa\Product\Product $product) {
		
		try {
			$categories = $this->mapper->loadProductCategories($product);
		}catch(Exception $e) {
			$categories = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
		
		return $categories;
	}
	
	/**
	 * Get the category by url path
	 * 
	 * @param string $path A Category url path e.g. 'ace-category'
	 * @return \Supa\Category\Category|null
	 */
	public function getByPath($path) {
		try {
			$category = $this->mapper->loadByPath($path);
		}catch(Exception $e) {
			$category = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $category;
	}
	
	/**
	 * Set the filter for creating a Category
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
	
	/**
	 * Set the filter for editing a Category
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
}