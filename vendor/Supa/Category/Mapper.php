<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Category;

use Supa\Category\Category;
use Supa\Category\Collection as CategoryCollection;
use Supa\Product\Product;
use \PDO;
use \Exception;

/**
 * Categegory mapper class. The categories work on a tree structure
 */
class Mapper extends \Supa\AbstractMapper {
	
	/**
	 * Constructor for category mapper
	 * 
	 * @param PDO $db
	 */
	public function __construct($db) {
		parent::__construct($db);
	}
		
	/**
	 * Load a category by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return Category
	 */
	public function loadByUid($uid) {
		
		$uid = (int)$uid;
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND category.active = 1 ';
		}
		
		$sql = "SELECT category.id as uid, category.active, category.path, category.title, category.lft, category.rgt,
						category.parent_id AS parent, category.last_editied_by AS lastEditiedBy, category.created_by AS createdBy,
						category.last_editied AS lastEditied, category.created, category.description
				  FROM category
				 WHERE category.id = $uid
				 $activeCheckSQL
				 LIMIT 1";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when selecting category by uid: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		if(!$data) {
			throw new Exception('Failed fetching category by uid: ' . $this->getDbError($statement));
		}
		
		$category = new Category($data);
		
		return $category;
	}
			
	/**
	 * Load a category by url path
	 * 
	 * @param string $path
	 * @throws Exception
	 * @return Category
	 */
	public function loadByPath($path) {
		
		$path = $this->db->quote($path, PDO::PARAM_STR);
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND category.active = 1 ';
		}
		
		$sql = "SELECT category.id as uid, category.active, category.path, category.title, category.lft, category.rgt,
						category.parent_id AS parent, category.last_editied_by AS lastEditiedBy, category.created_by AS createdBy,
						category.last_editied AS lastEditied, category.created, category.description
				  FROM category
				 WHERE `category`.`path` = $path
				 $activeCheckSQL
				 LIMIT 1";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when selecting category by path: ' . $this->getDbError());
		}
		
	    $data = $statement->fetch(PDO::FETCH_ASSOC);
		$category = null;
	    if($data) {
	    	$category = new Category($data);
	    }
	
	    return $category;
	}
		
	/**
	 * Load categories by parent uid
	 * 
	 * @param int $parentUid
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function loadCatsByParentUid($parentUid) {
		
		$parentId = (int)$parentUid;
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND category.active = 1 ';
		}
		
		$sql = "SELECT category.id as uid, category.active, category.path, category.title, category.lft, category.rgt,
					   category.parent_id AS parent, category.last_editied_by AS lastEditiedBy, category.created_by AS createdBy,
					   category.last_editied AS lastEditied, category.created, category.description
				  FROM category
				 WHERE category.parent_id = $parentId
				   $activeCheckSQL";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when selecting categories by parent id: ' . $this->getDbError());
		}
		
		$categoryCollection = new CategoryCollection(array());
		
		$categories = $statement->fetchAll(PDO::FETCH_ASSOC);
		if($categories) {
			$categoryCollection = new CategoryCollection(array());
			foreach($categories as $category) {
				$categoryCollection->append(new Category($category));
			}
		}
		
		return $categoryCollection;
	}
	
	/**
	 * Load category children between given left and right values
	 * 
	 * NOTE: This function will return the category we are deleting AND all child cats,
	 * so if a category has no child cats, a CategoryCollection with 1 element will still be returned.
	 * 
	 * @param Category $category
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function loadCatChildrenByLftAndRgt(Category $category) {
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND category.active = 1 ';
		}
		
		// retrieve all descendants for $lft, $rgt
		$lft = (int)$category->getLft();
		$rgt = (int)$category->getRgt();
		$sql = "SELECT category.id as uid, category.active, category.path, category.title, category.lft, category.rgt,
					   category.parent_id AS parent, category.last_editied_by AS lastEditiedBy, category.created_by AS createdBy,
					   category.last_editied AS lastEditied, category.created, category.description
		  		  FROM category
		 		 WHERE (category.lft BETWEEN $lft AND $rgt) $activeCheckSQL ORDER BY lft ASC";
		
		$statement = $this->query($sql);
		if(!$statement) {
			throw new Exception('Statement Error when loading category children by left/right: ' . $this->getDbError());
		}
		
		$categoryCollection = new CategoryCollection(array());
		
		$categories = $statement->fetchAll(PDO::FETCH_ASSOC);
		
		if(!$categories) {
			throw new Exception('Failed loading category children by left/right: ' . $this->getDbError($statement));
		}
		
		if($categories) {
			foreach($categories as $category) {
				$categoryCollection->append(new Category($category));
			}
		}
		
		return $categoryCollection;
	}
	
	/**
	 * Load category tree from category uid
	 * 
	 * @param Category $category
	 * @return \ArrayIterator
	 */
	public function loadCatTree(Category $category) {
	    $categories = $this->loadCatChildrenByLftAndRgt($category);
	  	return $categories;
	}

	/**
	 * Add category to tree
	 * 
	 * @param Category $category
	 * @param array $newCategory
	 * @throws Exception
	 * @return Category
	 */
	public function addCatToTree(Category $category, Category $newCategory) {
				
		$oldRight = (int)$category->getRgt();
		$parentRgt = $oldRight+2;
		$updateValue = $oldRight;
		$newLft = $oldRight;
		$newRgt = $oldRight+1;
		
		$this->db->beginTransaction();
		$t = $this->getTimestamp();
			
		$sql = "UPDATE `category` 
				   SET `lft` = (`lft`+2),
				   	   `last_editied` = '$t',
				   	   `last_editied_by` = {$newCategory->getCreatedBy()->getUID()}
				 WHERE `lft` > $updateValue";
		$affected = $this->exec($sql);
		if(false === $affected) {
			$this->db->rollBack();
			throw new Exception('Error updating left value when adding a category to the tree: ' . $this->getDbError());
		}
		
		$sql = "UPDATE `category` 
				   SET `rgt` = (`rgt`+2),
				   	   `last_editied` = '$t',
				   	   `last_editied_by` = {$newCategory->getCreatedBy()->getUID()}
				 WHERE `rgt` > $updateValue";
		$affected = $this->exec($sql);
		if(false === $affected) {
			$this->db->rollBack();
			throw new Exception('Error updating right value when adding a category to the tree: ' . $this->getDbError());
		}
		
		$title = $this->db->quote($newCategory->getTitle());
		$path = $this->db->quote($newCategory->getPath());
		$description = $this->db->quote($newCategory->getDescription());
		$sql = "INSERT INTO `category`
						SET `parent_id` = {$category->getUID()},
							`lft` = $newLft,
							`rgt` = $newRgt,
							`title` = $title,
							`path` = $path,
							`description` = $description,
							`created_by` = {$newCategory->getCreatedBy()->getUID()},
							`created` = '$t'";		
		$affected = $this->exec($sql);
		if(false === $affected) {
			$this->db->rollBack();
			throw new Exception('Failed adding new category to the tree: ' . $this->getDbError());
		}
		
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			$this->db->rollBack();
			throw new Exception("lastInsertId return invalid when adding a category to the tree: $uid");
		}
		
		$newCategory->setActiveState(1);
		$newCategory->setCreated($t);
		$newCategory->setParent($category->getUID());
		$newCategory->setLft($newLft);
		$newCategory->setRgt($newRgt);
		$newCategory->setUID($uid);
		
		$sql = "UPDATE `category`
				   SET `rgt` = (`rgt`+2),
				   		`last_editied` = '$t',
				   		`last_editied_by` = {$newCategory->getCreatedBy()->getUID()}
				 WHERE `id` = {$category->getUID()}";
		
		$affected = $this->exec($sql);
		if(false === $affected) {
			$this->db->rollBack();
			throw new Exception('Failed updating parent category when adding a category: ' . $this->getDbError());
		}
		$category->setRgt($oldRight+2);
		
		$this->db->commit();
		
		return $newCategory;
		
	}
	
	/**
	 * Delete category from tree
	 * 
	 * @param Category $category
	 * @throws Exception
	 * @return bool True if success, false otherwise
	 */
	public function delCatFromTree(Category $category) {
		
		$rgtVal = $category->getRgt();
		$lftVal = $category->getLft();
		
		$uid = (int)$category->getUID();
		
		$categoryChildren = $this->loadCatChildrenByLftAndRgt($category);
		
		if($categoryChildren->count() > 0) {
			$sql = "DELETE FROM category WHERE id = ?";
			$statement = $this->prepare($sql);
			foreach ($categoryChildren as $categoryChild) {
				$categoryChildUid = $categoryChild->getUID();
				$delete = $statement->execute(array($categoryChildUid));
				if(!$delete) {
					throw new Exception('Failed deleting category child when deleting category from tree: ' . $this->getDbError($statement));
				}
			}
		}else {
			$sql = "DELETE FROM category WHERE id = $uid";
			$affected = $this->exec($sql);
			if(false === $affected) {
				throw new Exception('Failed deleting category from tree: ' . $this->getDbError());
			}
		}
		
		$updateValue = $rgtVal;
		$data = array();
		$data[':lastEditied'] = $this->getTimestamp();
		//$data[':lastEditiedBy'] = $user->getUID();
		$data[':rgt'] = $updateValue;
		
		$sql = "UPDATE category SET rgt = rgt-2, last_editied = :lastEditied WHERE rgt > :rgt";
		$affected = $this->exec($sql, $data);
		if(false === $affected) {
			throw new Exception('Failed updating right value when deleting category: ' . $this->getDbError());
		}
		$data[':lft'] = $updateValue;
		$sql = "UPDATE category SET lft = lft-2, last_editied = :lastEditied WHERE lft > :lft";
		$affected = $this->exec($sql, $data);
		if(false === $affected) {
			throw new Exception('Failed updating left value when deleting category: ' . $this->getDbError());
		}
		
		unset($category);
		
		return true;
	}
	
	/**
	 * Load the path to a category in the tree
	 * 
	 * @param Category $category
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function loadCatPath(Category $category) {
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND category.active = 1 ';
		}
		
		$lft = (int)$category->getLft();
		$rgt = (int)$category->getRgt();
		
		$sql = "SELECT category.id as uid, category.active, category.path, category.title, category.lft, category.rgt,
					   category.parent_id AS parent, category.last_editied_by AS lastEditiedBy, category.created_by AS createdBy,
					   category.last_editied AS lastEditied, category.created, category.description
				  FROM category
				 WHERE lft <= $lft
				   AND rgt >= $rgt
				   $activeCheckSQL
				 ORDER BY lft ASC";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when trying to get category path: ' . $this->getDbError());
		}
		
		$categories = $statement->fetchAll(PDO::FETCH_ASSOC);
		if(!$categories) {
			throw new Exception('Failed fetching all categories when trying getting category path ' . $this->getDbError($statement));
		}
		
		$categoryCollection = new CategoryCollection(array());
		foreach($categories as $category) {
			$categoryCollection->append(new Category($category));
		}
		
		return $categoryCollection;
		
	}
	
	/**
	 * Move a category and associated sub categories to another category
	 * 
	 * @param Category $category The category being moved
	 * @param Category $newCategory The new category being moved to
	 * @throws Exception
	 */
	public function moveCategory(Category $category, Category $newCategory) {
		
		$parentId = (int)$newCategory->getUID();
		$lastEditiedBy = (int)$category->getLastEditiedBy()->getUID();
		$lastEditied = $this->getTimestamp();
		$uid = (int)$category->getUID();
		
		$sql = "UPDATE `category` 
				   SET `parent_id` = $parentId,
				   	   `last_editied_by` = $lastEditiedBy,
				   	   `last_editied` = '$lastEditied'
				 WHERE `id` = $uid";
		
		$affected = $this->exec($sql);
		if(false === $affected) {
			throw new Exception('Statement error when trying to move category: ' . $this->getDbError());
		}
		
		$this->rebuildTree(45, 1, $category->getLastEditiedBy()->getUID());
		
		$category = $this->loadByUid($category->getUID());
		
		return $category;
	}
	
	/**
	 * Rebuild the category tree using parent_id field
	 * 
	 * @param int $parent
	 * @param int $left
	 * @param int $userUid
	 * @throws Exception
	 * @return int A new right value
	 */
	protected function rebuildTree($parent, $left, $userUid = null) {
	    
	    // the right value of this node is the left value + 1
	    $right = $left+1;
	    // get all children of this node	    
	    $categoryChildren = $this->loadCatsByParentUid($parent);
	    
	    if($categoryChildren->count() > 0) {
    	    foreach($categoryChildren as $child) {
    	        // recursive execution of this function for each
    	        // child of this node
    	        // $right is the current right value, which is
    	        // incremented by the rebuildTree function
    	        $right = $this->rebuildTree($child->getUID(), $right, $userUid);
    	    }
	    }
	    // we've got the left value, and now that we've processed
	    // the children of this node we also know the right value
		$rgt = $this->db->quote($right, PDO::PARAM_INT);
		$lft = $this->db->quote($left, PDO::PARAM_INT);
		$lastEditied = $this->db->quote($this->getTimestamp());
		$lastEditiedBy = $this->db->quote($userUid, PDO::PARAM_INT);
		$uid = $this->db->quote($parent, PDO::PARAM_INT);
	    
	    $sql = "UPDATE category SET rgt = $rgt, lft = $lft, last_editied = $lastEditied, last_editied_by = $lastEditiedBy WHERE id = $uid";
	    $affected = $this->exec($sql);
	    
	    if(false === $affected) {
			throw new Exception('Statement error when trying to rebuild tree: ' . $this->getDbError());
		}
	    
	    // return the right value of this node + 1
	    return $right+1;
	}
	
	/**
	 * Update a category
	 * 
	 * @param Category $category
	 * @throws Exception
	 * @return Category
	 */
	public function update(Category $category) {
		
		// update category data
		$updateData = array();
		$updateData[':title'] = $category->getTitle();
		$updateData[':path'] = $category->getPath();
		$updateData[':uid'] = $category->getUID();
		$updateData[':lastEditiedBy'] = $category->getLastEditiedBy()->getUID();
		$updateData[':lastEditied'] = $category->getLastEditied();
		$updateData[':description'] = $category->getDescription();
		
		$sql = "UPDATE `category` 
				   SET `title` = :title,
				   	   `path` = :path,
				   	   `last_editied_by` = :lastEditiedBy,
				   	   `last_editied` = :lastEditied,
				   	   `description` = :description
				 WHERE `id` = :uid";
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		if($affected === false) {
			throw new Exception('Statement error when updating category: ' . $this->getDbError());
		}
		
		return $category;
	}
	
	/**
	 * Checks if a category has products linked to it
	 * 
	 * @param Category $category
	 * @throws Exception
	 * @return bool True if category has products, false otherwise
	 */
	public function hasProducts(Category $category) {
		
		$catUid = (int)$category->getUID();
		
		$sql = "SELECT COUNT(1)
				  FROM product_to_category
				 WHERE categoryId = $catUid";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when checking if category has products: ' . $this->getDbError());
		}
		
		return ($statement->fetchColumn() > 0);
	}
	
	/**
	 * Link product to categories in the data store
	 * 
	 * @param \ArrayIterator $categoryCollection
	 * @param Product $product
	 * @throws Exception
	 * @return bool 
	 */
	public function linkProductToCategories(\ArrayIterator $categoryCollection, Product $product) {
		
		$prodUid = $product->getUID();
		
		if($categoryCollection->count() == 0) {
			return false;		
		}
		
		$sql = 'INSERT INTO `product_to_category`
						SET `productId` = :prodUid, 
							`categoryId` = :catUid';
		
		$statement = $this->db->prepare($sql);
		foreach($categoryCollection as $category) {
			$data[':prodUid'] = $prodUid;
			$data[':catUid']  = $category->getUID();
			$res = $statement->execute($data);
			if(!$res) {
				throw new Exception('Error linking category to product: ' . $this->getDbError($statement));
			}
		}
		
		return true;		
	}
	
	/**
	 * Load categories by Product uid
	 * 
	 * @param Product $product
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function loadProductCategories(Product $product) {
		
		$prodUid = (int)$product->getUID();
		
		$sql = "SELECT category.id as uid, category.active, category.path, category.title, category.lft, category.rgt, 
						category.parent_id AS parent, category.last_editied_by AS lastEditiedBy, category.created_by AS createdBy, 
						category.last_editied AS lastEditied, category.created, category.description
				  FROM `product_to_category`
			INNER JOIN `category` ON `category`.`id` = `product_to_category`.`categoryId`
				 WHERE `product_to_category`.`productId` = $prodUid";
		
		$statement = $this->query($sql);
		
		if(!$statement) {
			throw new Exception('Statement error when trying to get product categories: ' . $this->getDbError());
		}
		
		$categoryCollection = new CategoryCollection(array());
		if($statement->rowCount() >= 1) {
			$categories = $statement->fetchAll(PDO::FETCH_ASSOC);
			if(!$categories) {
				throw new Exception('Failed fetching all categories when trying to get product categories: ' . $this->getDbError($statement));
			}
			foreach($categories as $category) {
				$categoryCollection->append(new Category($category));
			}
		}
		
		return $categoryCollection;
		
	}

	/**
	 * Update categories a product is in
	 * 
	 * @param Product $product
	 * @param array $newCatUids The new category uid's to link to product
	 * @param array $delCatUids The category uid's to remove from product
	 * @throws Exception
	 * @return bool True on success, false otherwise
	 */
	public function updateProductCats(Product $product, array $newCatUids = array(), array $delCatUids = array()) {
		
		$prodUid = (int)$product->getUID();
		$res = true;
		
		if(!empty($delCatUids)) {
			// delete cats
			$sql = "DELETE FROM product_to_category WHERE productId = ? AND categoryId = ? LIMIT 1";
			$statement = $this->db->prepare($sql);
			foreach ($delCatUids as $delCatUid) {
				$delCatUid = (int)$delCatUid;
				if($delCatUid <= 0) continue;
				$res = $statement->execute(array($prodUid, $delCatUid));
				
				if(!$res) {
					throw new Exception("Failed deleting prod/cat: $prodUid/$delCatUid, {$this->getDbError()}");
				}
			}
		}

		if(!empty($newCatUids)) {
			$sql = "INSERT INTO product_to_category(`productId`, `categoryId`) VALUES(?,?)";
			$statement = $this->db->prepare($sql);
			foreach($newCatUids as $catUid) {
				$catUid = (int)$catUid;
				if($catUid <= 0) continue;
				$res = $statement->execute(array($prodUid, $catUid));
				
				if(!$res) {
					throw new Exception("Failed inserting prod/cat: $prodUid/$catUid, {$this->getDbError()}");
				}
			}
		}
		
		return $res;
		
	}
}