<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Category;

use Supa\User\User;
use Supa\Category\Category;

/**
 * Category class
 */
class Category implements \Supa\EntityInterface {
	
	/* @var $uid int */
	protected $uid;
	
	/* @var $active int */
	protected $active;
	
	/* @var $title string */
	protected $title;
	
	/* @var $uid int */
	protected $path;
	
	/* @var $lft int */
	protected $lft;
	
	/* @var $rgt int */
	protected $rgt;
	
	/**
	 * Description of category 
	 * @var string
	 */
	protected $description;
	
	/**
	 * Parent Category
	 * @var \Supa\Category\Category|int|null
	 */
	protected $parent;
	
	/**
	 * User who created category
	 * @var \Supa\User\User|int|null
	 */
	protected $createdBy;
	
	/**
	 * User who last editied category
	 * @var \Supa\User\User|int|null
	 */
	protected $lastEditiedBy;
	
	/**
	 * Timestamp of when Category was last editied
	 * @var string|null
	 */
	protected $lastEditied;
	
	/**
	 * Timestamp of when Category was created
	 * @var string|null
	 */
	protected $created;
	
	/**
	 * Category images
	 * @var \ArrayIterator
	 */
	protected $images;

	/**
	 * Constructor for category class
	 *
	 * @param array $data category data
	 */
	public function __construct(array $data = array()) {
		$this->assignClassVariables($data);
	}
	
	/**
	 * Determines if category is active
	 * 
	 * @return bool True if active, False otherwise
	 */
	public function isActive() {
		return (bool)$this->active;
	}
	
	/**
	 * Get the category title
	 * 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Get the category uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return $this->uid;
	}
	
	/**
	 * Get the category path
	 * 
	 * @return string
	 */
	public function getPath() {
		return $this->path;
	}
	
	/**
	 * Get the left value of this category in the tree
	 * 
	 * @return int
	 */
	public function getLft() {
		return $this->lft;
	}

	/**
	 * Get the right value of this category in the tree
	 * 
	 * @return int
	 */
	public function getRgt() {
		return $this->rgt;
	}
	
	/**
	 * Get the User who created this category
	 * 
	 * @return \Supa\User\User
	 */
	public function getCreatedBy() {
		return \Supa\lazy_load_user($this->createdBy, 'UserMapper', 'loadByUid');
	}
	
	/**
	 * Get the uid of the parent category this category belongs to 
	 * 
	 * @return \Supa\Category\Category|null
	 */
	public function getParent() {
		return lazy_load($this->parent, 'CategoryMapper', 'loadByUid');
	}
	
	/**
	 * Get the timestamp of when category was last editied
	 * 
	 * @return string
	 */
	public function getLastEditied() {
		return $this->lastEditied;
	}
	
	/**
	 * Get the uid of user who last editied this category
	 * 
	 * @return \Supa\User\User
	 */
	public function getLastEditiedBy() {
		return \Supa\lazy_load_user($this->lastEditiedBy, 'UserMapper', 'loadByUid');
	}
	
	/**
	 * Get the timestamp of when category was created
	 * 
	 * @return string
	 */
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * Get the category description
	 * 
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * Set the category description
	 * 
	 * @param string $desc
	 */
	public function setDescription($desc) {
		$this->description = $desc;
	}
		
	/**
	 * Set the product uid
	 * 
	 * @param int $uid
	 * @return \Supa\EntityInterface
	 */
	public function setUID($uid) {
		$this->uid = $uid;
		return $this;
	}
	
	/**
	 * Set the right value of this category in the tree
	 * 
	 * @param int $right Right value in tree for this category
	 */
	public function setRgt($right) {
		return $this->rgt = $right;
	}
	
	/**
	 * Set the left value of this category in the tree
	 * 
	 * @param int $left Left value in tree for this category
	 */
	public function setLft($left) {
		return $this->lft = $left;
	}

	/**
	 * Set the category title
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Set the category path
	 * 
	 * @param string $path
	 */
	public function setPath($path) {
		$this->path = $path;
	}
	
	/**
	 * Set the uid of user who last editied this category
	 * 
	 * @param int $userId
	 */
	public function setLastEditiedBy($userId) {
		$this->lastEditiedBy = $userId;
	}

	/**
	 * Set the uid of user who created this category
	 * 
	 * @param int $userUid
	 */
	public function setCreatedBy($userUid) {
		$this->createdBy = $userUid;
	}
	
	/**
	 * Set the parent category uid for this category
	 * 
	 * @param int $catUid
	 */
	public function setParent($catUid) {
		$this->parent = $catUid;
	}
	
	/**
	 * Set timestamp of when the category was last editied
	 * 
	 * @param string $lastEditied
	 */
	public function setLastEditied($lastEditied) {
		return $this->lastEditied = $lastEditied;
	}
	
	/**
	 * Set timestamp of when the category was created
	 * 
	 * @param string $created
	 */
	public function setCreated($created) {
		return $this->lastEditied = $created;
	}

	/**
	 * Set the active state for the product
	 * 1 = active, 0 = not active
	 * 
	 * @param int $state A 1 (active) or 0 (not-active)
	 */
	public function setActiveState($state) {
		$this->active = $state;
	}
	
	/**
	 * Get images for the category
	 * 
	 * @return \ArrayIterator
	 */
	public function getImages() {
		if(!($this->images instanceof \ArrayIterator)) {
			$this->images = lazy_load_by_mapper2($this, 'ImageMapper', 'loadCategoryImages');
		}
		return $this->images;
	}
	
	/**
	 * Set images for the category
	 * 
	 * @param \ArrayIterator|int $images
	 */
	public function setImages($images) {
		$this->images = $images;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
	
	public function toArray() {
	    return get_object_vars($this);
	}
}