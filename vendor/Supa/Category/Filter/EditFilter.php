<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Category\Filter;

use Supa\Category\Filter\EditFilter;
use Supa\Category\Validator\AbstractRecord;
use Supa\AbstractFilter;

/**
 * Filter class for editing categories
 */
class EditFilter extends \Supa\Category\Filter\CreateFilter {
        
	/**
	 * Constructor for edit category filter class
	 */
    public function __construct() {
    	parent::__construct();        
    }
    
    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    }
}