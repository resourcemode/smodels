<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Category;

/**
 * Category collection class
 */
class Collection extends \ArrayIterator  {

	/**
	 * Constructor for category collection class
	 * 
	 * @param array $array Array of Supa\Category\Category objects
	 * @see Supa\Category\Category For Items in collection
	 * @see ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}