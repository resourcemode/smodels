<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Category;

/**
 * Helper class to output a breadcrumb like trail for a particular
 * category, e.g Root > cat 1 > cat 2 ...
 */
class Path  {
	
    /**
     * Get the path to the category separated by $sep
     * 
     * @param \ArrayIterator $categories
     * @param string $sep
     * @return string
     */
	public function getPath($categories, $sep = ' - ') {
		$out = array();		
	    foreach($categories as $cat) {
	    	$out[] = $cat->getTitle();
	    }
	    $out = implode($sep, $out);
	    return $out;
	}
	
}