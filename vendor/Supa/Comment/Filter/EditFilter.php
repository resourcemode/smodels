<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment\Filter;

use \Zend\InputFilter\Factory;

/**
 * Filter class for editing comments
 */
class EditFilter extends \Supa\Comment\Filter\Filter {
    
    /**
	 * Constructor for edit comments filter class
	 */
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    }
}