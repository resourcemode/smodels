<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */


namespace Supa\Comment\Filter;

use Supa\AbstractFilter;
use \Zend\InputFilter\Factory;

/**
 * Filter class for creating comments
 */
class Filter extends AbstractFilter {
    
    /**
	 * Constructor for create user filter class
	 */
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	
    	$this->add(array(
                'name' => 'uid',
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits',
                    ),
                ),
            ));
        
            $this->add(array(
                'name' => 'score',
				'required' => false,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits',
                    ),
                ),
            ));
                        
    	$this->add(array(
                'name' => 'content',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
                ),
            ));
            
        $this->add($this->getApprovedFilter());
        $this->filter->get('uid')->setRequired(false);
    }
    
	public function getApprovedFilter() {
		return array(
			'name' => 'approved',
			'required' => false,
			'validators' => array(
				array(
					'name' => 'digits',
					'options' => array(
						'messages' => array(
							\Zend\Validator\Digits::NOT_DIGITS => 'Invalid approved state, please re-enter.'
						)	
					),
				),
				array(
					'name' => 'in_array',
			        'options' => array(
			        	'haystack' => array('0', '1'),
						'messages' => array(
							\Zend\Validator\InArray::NOT_IN_ARRAY => 'Invalid approved state, please re-enter.'
						)
					),
				),
			)
		);		
	}
}