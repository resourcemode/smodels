<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment;

use Supa\User\User;

/**
 * Concrete Comment class
 */
class Comment implements \Supa\Comment\CommentInterface, \Supa\EntityInterface {

	/**
	 * Unique id for review
	 * @var int
	 */
	protected $uid;
	
	/**
	 * User who posted comment
	 * @var \Supa\User\User
	 */
	protected $createdBy;	
	
	/**
	 * Comment content
	 * @var string
	 */
	protected $content;
	
	/**
	 * Timestamp of creation
	 * @var string
	 */
	protected $created;
	
	/**
	 * Approval flag
	 * @var int|bool
	 */
	protected $approved;
	
	/**
	 * Comment score e.g. vote count: up/down
	 * @var int
	 */
	protected $score;
	
	/**
	 * Reviewer hostname/IP information
	 * @var string
	 */
	protected $addedFrom;
	
	public function __construct(array $data = array()) {
		$this->assignClassVariables($data);
	}
	
	/**
	 * Comment create timestamp
	 * 
	 * @param string $created
	 */
	public function setCreated($created) {
		$this->created = $created;
	}
	
	/**
	 * Set create timestamp
	 * 
	 * @return string
	 */
	public function getCreated() {
		return $this->created;
	}

	/**
	 * Comment content
	 * 
	 * @return string
	 */
	public function getContent() {
		return $this->content;
	}
	
	/**
	 * Set comment content
	 * 
	 * @param string $content
	 */
	public function setContent($content) {
		$this->content = $content;
	}

	/**
	 * Get comment score
	 * 
	 * @return int
	 */
	public function getScore() {
		return (int)$this->score;
	}
	
	/**
	 * Increment comment score, vote up
	 * 
	 * @return void
	 */
	public function incrementScore() {
		$score = $this->getScore();
		$score++;
		$this->setScore($score);
		lazy_load_by_mapper2($this, 'CommentMapper', 'updateScore');
	}
	
	/**
	 * Helper method for increasing or decreasing a comment's score
	 * 
	 * @param string $incOrDec Can be one of: "-", "+", "inc" or "dec"
	 * @return bool True on success, false otherwise
	 */
	public function score($incOrDec) {
		$bool = true;
		switch ($incOrDec) {
			case '-' :
			case 'dec' :
				$this->decrementScore();
				break;
			case '+' :
			case 'inc' :
				$this->incrementScore();
				break;
			default:
				$bool = false;
		}
		return $bool;
	}
	
	/**
	 * Decrement comment score, vote down
	 * 
	 * @return void
	 */
	public function decrementScore() {
		$score = $this->getScore();
		$score--;
		$this->setScore($score);
		lazy_load_by_mapper2($this, 'CommentMapper', 'updateScore');
	}
	
	/**
	 * Set comment score
	 * 
	 * @param int $score
	 */
	public function setScore($score) {
		$this->score = $score;
	}
	
	/**
	 * Get Comment author
	 *
	 * @return \Supa\User\User
	 */
	public function getCreatedBy() {
		return \Supa\lazy_load_user($this->createdBy, 'UserMapper', 'loadByUid');
	}
	
	/**
	 * Set comment author
	 * 
	 * @param \Supa\User\User $user
	 */
	public function setCreatedBy(\Supa\User\User $user) {
		$this->createdBy = $user;
	}
	
	/**
	 * Has comment been approved
	 *
	 * @return bool
	 */
	public function isApproved() {
		return (bool)$this->approved;
	}
	
	/**
	 * Set comment approval
	 * 
	 * @param int|bool $flag
	 */
	public function setApproved($flag) {
		$this->approved = (bool)$flag;
		lazy_load_by_mapper2($this, 'CommentMapper', 'updateApprovedStatus');
	}
	
	/**
	 * Get the comment uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return (int)$this->uid;
	}
	
	/**
	 * Set the comment uid
	 * 
	 * @param int $uid
     * @return \Supa\EntityInterface
	 */
	public function setUID($uid) {
		$this->uid = (int)$uid;
		return $this;
	}
	
	/**
	 * Get added from data
	 * 
	 * @return string
	 */
	public function getAddedFrom() {
		return $this->addedFrom;
	}
	
	/**
	 * Set added from data
	 * 
	 * @param string $addedFromData
     * @return \Supa\EntityInterface
	 */
	public function setAddedFrom($addedFromData) {
		$this->addedFrom = $addedFromData;
		return $this;
	}
		
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
	
}