<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment;

/**
 * Comment Interface
 */
interface CommentInterface {
	
	public function getContent();
	public function setContent($content);
	
}