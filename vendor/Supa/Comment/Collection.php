<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment;

use ArrayIterator;
use Supa\Comment\Comment;

/**
 * Comment collection class
 */
class Collection extends ArrayIterator  {

	/**
	 * Constructor for Comment collection class
	 * 
	 * @param array $array Array of Comment objects
	 * @see Comment For Items in collection
	 * @see ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}