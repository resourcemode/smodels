<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment;

use Exception;

/**
 * Comment service class
 */
class Service {

	/**
	 * Data mapper for Comment object
	 * @var \Supa\AbstractMapper
	 */
	protected $mapper;
	
	/**
	 * Holds success or error messages 
	 * from performed operations
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by service class
	 * @var \Exception
	 */
	protected $lastException;
	
	/**
	 * Filter for creating a Comment
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
		
	/**
	 * Filter for editing a Comment
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;

	/**
	 * Constructor for Comment service class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}

	/**
	 * Get a comment by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Comment\Comment
	 */
	public function getByUid($uid) {
		try {
			$comment = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$comment = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $comment;
	}
	
	/**
	 * Create a comment
	 * 
	 * @param array $data The new comment data
	 * @param \Supa\User\User $user Comment author
	 * @return \Supa\Comment\Comment|null
	 */
	public function createComment(array $data = array(), \Supa\User\User $user) {
	    
		try {
			
			$this->createFilter->prepareFilter();
			$this->createFilter->setData($data);
	    	if(!$this->createFilter->isValid()) {
	        	$this->messages = $this->createFilter->getFlatMessages();
				throw new Exception('Invalid data given when creating comment');
			}
			
			$comment = new Comment($data);
			$comment->setCreatedBy($user);
	        $newComment = $this->mapper->createComment($comment);
	    }catch(Exception $e) {
	        $newComment = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $newComment;
	}
	
	/**
	 * Update a comment
	 * 
	 * @param array $data The updated comment data
	 * @param \Supa\User\User $user The user who is updating this comment
	 * @return \Supa\Comment\Comment|null
	 */
	public function updateComment($data, \Supa\User\User $user) {
		try {
			
			$this->editFilter->prepareFilter();
			$this->editFilter->setData($data);
	    	if(!$this->editFilter->isValid()) {
	        	$this->messages = $this->editFilter->getFlatMessages();
				throw new Exception('Invalid data given when editing comment');
			}
			
			// TODO: load then set?
			$comment = new Comment($data);
			$updatedComment = $this->mapper->update($comment);
		}catch(Exception $e) {
			$updatedComment = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $updatedComment;
	}
	
	/**
	 * Score a comment (vote up or down)
	 * 
	 * @param int $uid Comment uid
	 * @param string $incOrDec
	 * @return bool True on success, false otherwise
	 */
	public function score($uid, $incOrDec = '') {
		
		try {
			$comment = $this->getByUid($uid);
			if(is_null($comment)) throw new Exception('Failed getting comment to update score');
			$bool = $comment->score($incOrDec);

		}catch(Exception $e) {
			$bool = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $bool;
	}
	
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
	
	/**
	 * Set the filter for creating a Comment
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
	
	/**
	 * Set the filter for editing a Comment
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
}