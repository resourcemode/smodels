<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */
namespace Supa\Comment;

use \Exception;
use PDO;
use Supa\Comment\CommentInterface;
use \Zend\Db\Sql\Sql;

/**
 * Comment mapper class
 */
class Mapper extends \Supa\AbstractMapper {

    /**
     * Constructor for Comment mapper class
     *
     * @param PDO $db
     */
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load a comment by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return Comment
	 */
	public function loadByUid($uid) {

		$sql = 'SELECT `comment`.`id` as uid, `comment`.`content`, `comment`.`created_by` as createdBy,
						`comment`.`added_from` as addedFrom, `comment`.`created` ,`comment`.`approved`, `comment`.`score`
		          FROM `comment`
		         WHERE `comment`.`id` = :uid';
		         
		$statement = $this->query($sql, array(':uid'=>$uid));
		if(!$statement) {
			throw new Exception('Error loading Comment by uid: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		if(!$data) {
			throw new Exception('Failed loading Comment for uid: ' . $this->getDbError($statement));
		}
		
		$comment = new Comment($data);
			
		return $comment;
	}
	
	/**
	 * Create a comment
	 * 
	 * @param Comment $comment
	 * @throws Exception
	 * @return Comment
	 */
	public function createComment(Comment $comment) {
		
		$insertData = array();
		$insertData[':content'] = $comment->getContent();
		$insertData[':addedFrom'] = $comment->getAddedFrom();
		$insertData[':createdBy'] = $comment->getCreatedBy()->getUID();
		$insertData[':created'] = $this->getTimestamp();
		
		$sql = 'INSERT INTO `comment`
						SET `content` = :content,
							`added_from` = :addedFrom,
							`created_by` = :createdBy,
							`created` = :created';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($insertData);
		if($affected === false) {
			throw new Exception('Statement error when inserting comment: ' . $this->getDbError());
		}
		
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			throw new Exception('Insert id return invalid when creating comment');
		}
				
		$comment->setUID($uid);
		$comment->setCreated($insertData[':created']);
		$comment->setScore(0);
		
		return $comment;
	}
	
	/**
	 * Update a comment
	 * 
	 * @param \Supa\EntityInterface $comment
	 * @throws Exception
	 * @return Comment
	 */
	public function update(\Supa\EntityInterface $comment) {
		
		$updateData = array();
		$updateData[':uid'] = $comment->getUID();
		$updateData[':content'] = $comment->getContent();
		$updateData[':approved'] = (int)$comment->isApproved();
		$updateData[':score'] = $comment->getScore();	
		
		$sql = 'UPDATE `comment`
				   SET `content` = :content,
				   		`approved` = :approved,
				   		`score` = :score
				 WHERE id = :uid';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		
		if(false === $affected) {
			throw new Exception('Error updating comment: ' . $this->getDbError());
		}
		
		return $comment;
		
	}
	
	/**
	 * Update a comment score
	 * 
	 * @param Comment $comment
	 * @throws Exception
	 * @return Comment
	 */
	public function updateScore(Comment $comment) {
		
		$updateData = array();
		$updateData[':uid'] = $comment->getUID();
		$updateData[':score'] = $comment->getScore();	
		
		$sql = 'UPDATE `comment`
				   SET `score` = :score
				 WHERE id = :uid';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		
		if(false === $affected) {
			throw new Exception('Error updating comment score: ' . $this->getDbError());
		}
		
		return $comment;
	}
	
	/**
	 * Update approved status
	 * 
	 * @param Comment $comment
	 * @throws Exception
	 * @return Comment
	 */
	public function updateApprovedStatus(Comment $comment) {
		
		$updateData = array();
		$updateData[':uid'] = $comment->getUID();
		$updateData[':approved'] = (int)$comment->isApproved();	
		
		$sql = 'UPDATE `comment`
				   SET `approved` = :approved
				 WHERE id = :uid';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		
		if(false === $affected) {
			throw new Exception('Error updating comment approved status: ' . $this->getDbError());
		}
		
		return $comment;
	}
}