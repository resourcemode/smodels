<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment\Review;

use ArrayIterator;
use Supa\Comment\Review\Review;

/**
 * Review collection class
 */
class Collection extends ArrayIterator  {

	/**
	 * Constructor for Review collection class
	 * 
	 * @param array $array Array of Review objects
	 * @see Review For Items in collection
	 * @see ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}