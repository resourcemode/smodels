<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment\Review;

use Supa\Comment\Comment;

/**
 * Review class
 */
class Review extends Comment {
    
    /**
     * Title for review
     * @var string
     */
    protected $title;
    
    /**
     * Review rating
     * @var float
     */
    protected $rating;
    
    /**
     * A rating value, e.g x out of $rateValue
     * @var int
     */
    protected $rateValue = 5;
    
    /**
     * Helpful count
     * @var int
     */
    protected $helpfulCount;
    
    public function __construct(array $data = array()) {
    	parent::__construct($data);
    	$this->assignClassVariables($data);
    }

	/**
	 * Get review title
	 * 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Get review rating
	 * 
	 * We use $this->helpfulCount to help us find the average rating
	 * 
	 * @param bool $rawValue Whether to get total summation value, or just rating
	 * @return float The calculated review rating
	 */
	public function getRating($rawValue = false) {
		
		$rating = (float)$this->rating;
		if($rating <= 0) {
			return 0;
		}
		
		$pplCount = $this->getHelpfulCount();
		if($pplCount <= 0) {
			return 0;
		}
		
		if($rawValue) {
			return $rating;
		}
		
		// calculate an average rating value
		$rating = round((($rating/($pplCount*$this->rateValue))*$this->rateValue), 1, PHP_ROUND_HALF_DOWN);
		
		// floor to nearest .5, .0 
		$rating = (floor($rating * 2) / 2);
		
		// sanity check
		if($rating < 0 || $rating > $this->rateValue) {
		    $rating = 0;
		}
		
		return $rating;
	}

	/**
	 * Get count of how many clicked
	 * "Did you find this review helpful..."
	 * 
	 * @return int
	 */
	public function getHelpfulCount() {
		return (int)$this->helpfulCount;
	}
	
	/**
	 * Set review title
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Set the rating value for review
	 * 
	 * $this->rating is a summation of all ratings for this review
	 * by all users, $this->helpfulCount is a count of users who have rated
	 * 
	 * So for example, if $this->rating is 12, and $this->helpfulCount is 3
	 * it means that 3 users have rated this review and the total rating is
	 * 12, out of a possible 15 (rating out of 5)
	 * 
	 * @param float $rating
	 * @param bool $resetUserRatedCount Reset the number of users who rated review to 1
	 */
	public function setRating($rating, $resetUserRatedCount = true) {
		if($resetUserRatedCount) {
			$this->helpfulCount = 1;			
		}
		$this->rating = $rating;
	}
	
	/**
	 * Rate this review
	 * 
	 * @param float $rating
	 * @return bool
	 */
	public function rate($rating) {		
		$currentRating = $this->getRating(true);
		$rating = (float)$rating;
		if($rating > 0 && $rating <= $this->rateValue) {
			$currentRating += $rating;
			$this->incrementHelpfulCount();
			$this->setRating($currentRating, false);
			lazy_load_by_mapper2($this, 'ReviewMapper', 'updateRating');
		}
		return true;
	}
	
	public function getRateValue() {
	    return $this->rateValue;
	}

	/**
	 * Set helpful count
	 * 
	 * @param int $count
	 */
	public function setHelpfulCount($count) {
		$this->helpfulCount = $count;
	}
	
	/**
	 * Increment helpful count
	 * e.g A click on "Did you find review useful..."
	 * 
	 * @return void
	 */
	public function incrementHelpfulCount() {		
		$count = $this->getHelpfulCount();
		$count++;
		$this->setHelpfulCount($count);
		lazy_load_by_mapper2($this, 'ReviewMapper', 'updateHelpfulCount');
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}

	public function toArray() {
	    return get_object_vars($this);
	}
}