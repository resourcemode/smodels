<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */


namespace Supa\Comment\Review\Filter;

use \Zend\InputFilter\Factory;

/**
 * Filter class for creating reviews
 */
class Filter extends \Supa\Comment\Filter\Filter {
    
    /**
	 * Constructor for create review filter class
	 */
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	
        $this->add(array(
                'name' => 'helpfulCount',
				'required' => false,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits',
                    ),
                ),
            ));
            
        $this->add(array(
                'name' => 'rating',
				'required' => false,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits',
                    ),
                ),
            ));
            
    }
}