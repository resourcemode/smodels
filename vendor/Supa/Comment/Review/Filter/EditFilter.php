<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */


namespace Supa\Comment\Review\Filter;

use \Zend\InputFilter\Factory;

/**
 * Filter class for editing reviews
 */
class EditFilter extends \Supa\Comment\Review\Filter\Filter {
    
    /**
	 * Constructor for edit review filter class
	 */
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    }
}