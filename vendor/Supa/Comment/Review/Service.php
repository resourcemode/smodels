<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment\Review;

use Supa\Comment\Review\Collection as ReviewCollection;
use Supa\Comment\Review\Review;
use Exception;

/**
 * Review service class
 */
class Service {

	/**
	 * Data mapper for Review object
	 * @var \Supa\AbstractMapper
	 */
	protected $mapper;
	
	/**
	 * Holds success or error messages 
	 * from performed operations
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by service class
	 * @var \Exception
	 */
	protected $lastException;
	
	/**
	 * Filter for creating a Review
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
	
	/**
	 * Filter for edting a Review
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;

	/**
	 * Constructor for Review service class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}

	/**
	 * Get a review by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Comment\Review\Review|null
	 */
	public function getByUid($uid) {
		try {
			$review = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$review = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $review;
	}
	
	/**
	 * Create a review
	 * 
	 * @param array $data The review data
	 * @param \Supa\User\User $user Review author
	 * @return \Supa\Comment\Review\Review|null
	 */
	public function createReview(array $data = array(), \Supa\User\User $user) {
	    
		try {
			
			$this->createFilter->prepareFilter();
			$this->createFilter->setData($data);
	    	if(!$this->createFilter->isValid()) {
	        	$this->messages = $this->createFilter->getFlatMessages();
				throw new Exception('Invalid data given when creating review');
			}
			
			$review = new Review($data);
			$review->setCreatedBy($user);
	        $review = $this->mapper->createReview($review);
	    }catch(Exception $e) {
	        $review = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $review;
	}
	
	/**
	 * Update a review
	 * 
	 * @param array $data The updated review data, prop=>value pairs
	 * @param \Supa\User\User $user The user who is updating this review
	 * @return \Supa\Comment\Review\Review|null
	 */
	public function updateReview($data, \Supa\User\User $user) {
		
		try {
			
			$this->editFilter->prepareFilter();
			$this->editFilter->setData($data);
			
	    	if(!$this->editFilter->isValid()) {
	        	$this->messages = $this->editFilter->getFlatMessages();
				throw new Exception('Invalid data given when editing review');
			}
			
			// TODO: load then set?
			$review = new Review($data);
			$review->setRating($data['rating']);
			$review->setHelpfulCount($data['helpfulCount']);
			$updatedReview = $this->mapper->update($review);
			
		}catch(Exception $e) {
			$updatedReview = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $updatedReview;
	}

	/**
	 * Rate a review
	 * 
	 * @param int $uid Review uid
	 * @param float $rate
	 * @return bool True on success, false otherwise
	 */
	public function rate($uid, $rate) {
		
		try {
			$rating = $this->getByUid($uid);
			if(is_null($rating)) throw new Exception('Failed getting review to update rating');
			$bool = $rating->rate($rate);

		}catch(Exception $e) {
			$bool = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $bool;
	}
	
	/**
	 * Create a product review
	 * 
	 * @param array $data The review data
	 * @param \Supa\Product\Product $product The reviewed Product
	 * @param \Supa\User\User $user Review author
	 * @return \Supa\Comment\Review\Review|null
	 */
	public function createProductReview(array $data = array(), \Supa\Product\Product $product, \Supa\User\User $user) {
	    
		try {
			
			$this->createFilter->prepareFilter();
			$this->createFilter->setData($data);
	    	if(!$this->createFilter->isValid()) {
	        	$this->messages = $this->createFilter->getFlatMessages();
				throw new Exception('Invalid data given when creating product review');
			}
			
			$review = new Review($data);
			$review->setCreatedBy($user);
	        $review = $this->mapper->createReview($review);
	        $this->mapper->addReviewToProduct($review, $product);
	    }catch(Exception $e) {
	        $review = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $review;
	}
	
	/**
	 * Get reviews for a single product
	 * 
	 * @param \Supa\Product\Product $product
	 * @return \Supa\Comment\Review\Collection|false
	 */
	public function getProductReviews(\Supa\Product\Product $product) {
	    
	    try {	        
	        $reviews = $this->mapper->getReviewsForProduct($product);
	    }catch(Exception $e) {
	        $reviews = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    
	    return $reviews;
	}
	
	/**
	 * Get all reviews for all products
	 * 
	 * @return \Supa\Comment\Review\Collection|false
	 */
	public function getAllProductReviews() {
	    
	    try {
	        $reviews = $this->mapper->getAllReviewsForAllProducts();
	    }catch(Exception $e) {
	        $reviews = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    
	    return $reviews;
	}
	
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
	
	/**
	 * Set the filter for creating a Comment
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
	
	/**
	 * Set the filter for editing a Comment
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
}