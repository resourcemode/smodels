<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Comment\Review;

use Supa\Comment\Review\Collection as ReviewCollection;
use Supa\Product\Product;
use \Exception;
use PDO;
use Supa\Comment\CommentInterface;

/**
 * Review mapper class
 */
class Mapper extends \Supa\Comment\Mapper {

    /**
     * Constructor for Review mapper class
     *
     * @param PDO $db
     */
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load a review by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return Review
	 */
	public function loadByUid($uid) {

		$sql = 'SELECT `comment`.`id` as uid, `comment`.`content`, `comment`.`created_by` as createdBy,
						`comment`.`added_from` as addedFrom, `comment`.`created` ,`comment`.`approved`,
						`review`.`rating`, `review`.`helpful` as helpfulCount, `review`.`title`, `comment`.`score`
		          FROM `review`
		    INNER JOIN `comment` ON `comment`.`id` = `review`.`comment_id`
		         WHERE `review`.`comment_id` = :uid';
		         
		$statement = $this->query($sql, array(':uid'=>$uid));
		if(!$statement) {
			throw new Exception('Error loading Review by uid: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		if(!$data) {
			throw new Exception('Failed loading Review for uid: ' . $this->getDbError($statement));
		}
		
		$review = new Review($data);
			
		return $review;
	}
	
	/**
	 * Create a review
	 * 
	 * @param Review $review
	 * @throws Exception
	 * @return Review
	 */
	public function createReview(Review $review) {
		
		$review = $this->createComment($review);
		
		$insertData = array();
		$insertData[':title'] = $review->getTitle();
		$insertData[':commentId'] = $review->getUID(); // 1-1 relationship with a comment
		
		$sql = 'INSERT INTO `review`
						SET `title` = :title,
							`comment_id` = :commentId';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($insertData);
		if($affected === false) {
			throw new Exception('Statement error when inserting review: ' . $this->getDbError());
		}

		$review->setHelpfulCount(0);
		$review->setRating(0.0, false);
		
		return $review;
	}
	
	/**
	 * Update a review
	 * 
	 * @param \Supa\EntityInterface $review
	 * @throws Exception
	 * @return Review
	 */
	public function update(\Supa\EntityInterface $review) {
		
		$review = parent::update($review);
		
		$updateData = array();
		$updateData[':title'] = $review->getTitle();
		$updateData[':rating'] = $review->getRating(true);
		$updateData[':helpful'] = $review->getHelpfulCount();
		$updateData[':commentId'] = $review->getUID(); // 1-1 relationship with a comment
		
		$sql = 'UPDATE `review`
				   SET `title` = :title,
				   	   `rating` = :rating,
				   	   `helpful` = :helpful
				 WHERE `comment_id` = :commentId';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		
		if($affected === false) {
			throw new Exception('Statement error when updating review: ' . $this->getDbError());
		}
		
		return $review;
	}
	
	/**
	 * Update review helpful count
	 * 
	 * @param Review $review
	 * @throws Exception
	 * @return Review
	 */
	public function updateHelpfulCount(Review $review) {
		
		$updateData = array();
		$updateData[':commentId'] = $review->getUID();
		$updateData[':helpful'] = $review->getHelpfulCount();	
		
		$sql = 'UPDATE `review`
				   SET `helpful` = :helpful
				 WHERE `comment_id` = :commentId';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		
		if(false === $affected) {
			throw new Exception('Error updating review helpful count: ' . $this->getDbError());
		}
		
		return $review;
	}	
	
	/**
	 * Update review rating
	 * 
	 * @param Review $review
	 * @throws Exception
	 * @return Review
	 */
	public function updateRating(Review $review) {
		
		$updateData = array();
		$updateData[':commentId'] = $review->getUID();
		$updateData[':rating'] = $review->getRating(true);
		$updateData[':helpful'] = $review->getHelpfulCount();	
		
		$sql = 'UPDATE `review`
				   SET `rating` = :rating,
				   		`helpful` = :helpful
				 WHERE `comment_id` = :commentId';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		
		if(false === $affected) {
			throw new Exception('Error updating review rating: ' . $this->getDbError());
		}
		
		return $review;
	}
	
	/**
	 * Add review to product
	 * 
	 * @param Review $review The product review
	 * @param Product $product The reviewed product
	 * @throws Exception
	 * @return $bool
	 */
	public function addReviewToProduct(Review $review, Product $product) {
		
		$data = array();
		$data[':reviewId'] = $review->getUID();
		$data[':productId'] = $product->getUID();
		
		$sql = 'INSERT INTO `product_review`
				   SET `product_id` = :productId,
				   		`review_id` = :reviewId';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($data);
		
		if(false === $affected) {
			throw new Exception('Error adding review to product: ' . $this->getDbError());
		}
		
		return $review;
	}
	
	/**
	 * Get reviews for a single product
	 * 
	 * @param Product $product
	 * @throws Exception
	 * @return ReviewCollection
	 */
	public function getReviewsForProduct(Product $product) {
	    
	    $data = array();
	    $data[':productId'] = $product->getUID();
	    
	    $sql = 'SELECT `comment`.`id` as uid, `comment`.`content`, `comment`.`created_by` as createdBy,
	    			`comment`.`added_from` as addedFrom, `comment`.`created` ,`comment`.`approved`,
					`review`.`rating`, `review`.`helpful` as helpfulCount, `review`.`title`, `comment`.`score`
				  FROM `review`
			INNER JOIN `comment` ON `comment`.`id` = `review`.`comment_id`
			INNER JOIN `product_review` ON `product_review`.`review_id` = `comment`.`id`
				 WHERE `product_review`.`product_id` = :productId';
	    
	    $sth = $this->db->prepare($sql);
	    $res = $sth->execute($data);
	    if(!$res) {
	        throw new Exception('Failed gettin reiviews for product');
	    }
	    
	    $reviewCollection = new ReviewCollection(array());
	    	        
	    $reviews = $sth->fetchAll(PDO::FETCH_ASSOC);
	    if($reviews) {
	        foreach($reviews as $review) {
    	        $reviewCollection->append(new Review($review));
    	    }
	    }
	    	    
	    return $reviewCollection;
	}
	
	/**
	 * Get all reviews for all products
	 * 
	 * @throws Exception
	 * @return ReviewCollection
	 */
	public function getAllReviewsForAllProducts() {
	    
	    $sql = 'SELECT `comment`.`id` as uid, `comment`.`content`, `comment`.`created_by` as createdBy,
	    			`comment`.`added_from` as addedFrom, `comment`.`created` ,`comment`.`approved`,
					`review`.`rating`, `review`.`helpful` as helpfulCount, `review`.`title`, `comment`.`score`,
					`product_review`.`product_id` as productId
				  FROM `review`
			INNER JOIN `comment` ON `comment`.`id` = `review`.`comment_id`
			INNER JOIN `product_review` ON `product_review`.`review_id` = `comment`.`id`';
	    
	    $sth = $this->db->query($sql);
	    
	    if(!$sth) {
	        throw new Exception('Failed getting all reviews for all products');
	    }
	    
	    $reviews = $sth->fetchAll(PDO::FETCH_ASSOC);
	    if(!$reviews) {
	        throw new Exception('Failed fetching all reviews for all products: ' . $this->getDbError($statement));
	    }
	    
	    $reviewCollection = new ReviewCollection(array());
	    foreach($reviews as $review) {
	        $reviewCollection->append(new Review($review));
	    }
	    
	    return $reviewCollection;
	}
}