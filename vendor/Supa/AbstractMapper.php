<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */
namespace Supa;

use \PDO;
use \PDOException;
use \Exception;
use \PDOStatement;

/**
 * AbstractMapper class
 */
abstract class AbstractMapper {
    
	/**
	 * Swith to bring back just active records 
	 * @var bool
	 */
	protected $activeOnlyCheck = true;
	
    /**
     * The data store
     * @var \PDO
     */
	protected $db = null;
	
	/**
	 * Default pagination limit
	 * @var int
	 */
	const PAGE_SIZE = 25;

	/**
	 * Constructor
	 * 
	 * @param PDO $db
	 */
	public function __construct($db) {
		$this->db = $db;
	}
	
	/**
	 * Helper method to quote sql params
	 * 
	 * @param string $sql SQL statement with name'd placeholders
	 * @param array $params The name/value pairs
	 * @throws Exception
	 * @return string
	 */
	protected function quoteParams($sql, array $params = array()) {
	    
	    if(empty($params)) {
	        throw new Exception('Params is empty');
	    }
	    
	    foreach($params as $key=>$value) {
	        $sql = str_replace($key, $this->db->quote($value), $sql);
	    }
	    
	    return $sql;
	}
	
	/**
	 * Return db related error message
	 * 
	 * @param PDOStatement $statement Optional statement object to check for error message
	 * @see http://php.net/manual/en/pdo.errorinfo.php
	 * @see http://php.net/manual/en/pdostatement.errorinfo.php
	 * @return string The db error message
	 */
	protected function getDbError(PDOStatement $statement = null) {
	    
	    if(!is_null($statement) && $statement instanceof PDOStatement) {
	        $errorInfo = $statement->errorInfo();
	        if(is_array($errorInfo) && isset($errorInfo[2])) {
	            $error = trim($errorInfo[2]);
	            if(!empty($error)) {
	            	return $error;
	            }
	        }
	    }
	    
	    $errorInfo = $this->db->errorInfo();
	    if(is_array($errorInfo) && isset($errorInfo[2])) {
	        return $errorInfo[2];
	    }
	    
	    return '';  
	}
	
	/**
	 * Query the db
	 * 
	 * @param string $sql The sql command to run
	 * @param array $params The params for the sql command
	 * @see http://php.net/manual/en/pdo.query.php
	 * @return PDOStatement Or false on failure
	 */
	protected function query($sql, array $params = array()) {
		if(empty($sql)) {
			throw new Exception('No statement given to query db');
		}
		if(!empty($params)) {
	    	$sql = $this->quoteParams($sql, $params);
		}
	    return $this->db->query($sql);
	}
	
	/**
	 * Execute a command on the db
	 * 
	 * @param string $sql The sql command to run
	 * @param array $params The params for the sql command
	 * @see http://php.net/manual/en/pdo.exec.php
	 * @return int Number of affected rows
	 */
	protected function exec($sql, array $params = array()) {
		$affected = 0;
		if(empty($sql)) {
			throw new Exception('No statement given to execute on db');
		}
		if(!empty($params)) {
	    	$sql = $this->quoteParams($sql, $params);
		}
	    //file_put_contents('debug.txt', $sql, FILE_APPEND);
	    $affected = $this->db->exec($sql);
	    return $affected;
	}
	
	/**
	 * Proxy method to PDO::prepare() so we can
	 * deal with exceptions
	 * 
	 * @param string $sql
	 * @throws PDOException
	 * @see http://php.net/manual/en/pdo.prepare.php
	 * @return PDOStatement
	 */
	protected function prepare($sql) {
		try {
			$statement = $this->db->prepare($sql);
			if(false === $statement) throw new PDOException('Prepare returned false');
		}catch(PDOException $e) {
			throw new Exception($e->getMessage());
		}		
		return $statement;
	}
	
	/**
	 * Helper method to get a formatted timestamp.
	 * 
	 * @param string $format PHP date/time format, default is: 'Y-m-d H:i:s'
	 * @see http://php.net/manual/en/function.date-create.php For func details
	 * @see http://php.net/manual/en/function.date.php For available formats to use
	 * @return string The formatted timestamp
	 */
	public function getTimestamp($format = 'Y-m-d H:i:s') {
		return date_create()->format($format);
	}
	
	
	/**
	 * Set 'active only records' flag
	 * 
	 * @param bool $bool
	 */
	public function setActiveOnlyCheck($bool) {
		$this->activeOnlyCheck = $bool;
	}
	
	/**
	 * Check if we are getting only active records
	 * 
	 * @return bool
	 */
	public function isActiveOnly() {
		return $this->activeOnlyCheck;
	}
}