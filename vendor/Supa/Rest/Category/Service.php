<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Rest\Category;

/**
 * REST Category service class
 */
class Service extends \Supa\Category\Service {
    
    protected $dataFormat = 'json';

	/**
	 * Constructor for Category REST service class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct($mapper) {
		parent::__construct($mapper);
	}
	
	/**
	 * Get category children
	 * 
	 * @param int $catUid Parent category uid
	 * @return string JSON encoded object
	 */	
	public function getCategoryChildren($catUid) {

	    $result = array();
	    $result['success'] = true;
	    $result['info'] = 'Summarised call info.';
	    $result['messages'] = array('OK');
	    $result['result'] = array();

	    $cats = parent::getCategoryChildren($catUid);
	    if(!is_null($cats) && $cats->count() > 0) {
            $parsed = array();
            foreach($cats as $cat) {
                $parsed[] = $cat->toArray();
            }
            $result['result'] = $parsed;     
	    }else {
    	    $result['success'] = false;
    	    $result['messages'] = array('ERROR');
    	    $result['result'] = array();
    	    $result['info'] = $this->getLastException()->getMessage();        
	    }
	    	    
	    return $this->processData($result);
	}

	public function setDataFormat($format) {
	    $this->dataFormat = $format;
	}
	
	public function getDataFormat() {
	    return $this->dataFormat;
	}
	
	protected function processData(array $data = array()) {
	    
	    $ret = null;
	    switch($this->dataFormat) {
	        case 'json' :
	            $ret = json_encode($data);
	            break;
	        default :
	            $ret = $data; // just return the array
	    }
	    return $ret;
	}
}