<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Rest\Review;

/**
 * REST Review service class
 */
class Service extends \Supa\Comment\Review\Service {
    
    protected $dataFormat = 'json';
    
	/**
	 * Constructor for product REST service class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct($mapper) {
		parent::__construct($mapper);
	}
	
	/**
	 * Get all reviews for all products
	 * 
	 * @return string JSON encoded object
	 */
	public function getAllProductReviews() {

	    $result = array();
	    $result['success'] = true;
	    $result['info'] = 'Summarised call info.';
	    $result['messages'] = array('OK');
	    $result['result'] = array();
	    	    
	    try {
	        
	        $reviews = $this->mapper->getAllReviewsForAllProducts();
	        if($reviews->count() > 0) {
	            $parsed = array();
	            foreach($reviews as $review) {
	                $parsed[] = $review->toArray();
	            }
	            $result['result'] = $parsed;
	        }
	        
	    }catch(Exception $e) {
    	    $result['success'] = false;
    	    $result['messages'] = array('ERROR');
    	    $result['result'] = array();
    	    $result['info'] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    
	    return $this->processData($result);
	}

	public function setDataFormat($format) {
	    $this->dataFormat = $format;
	}
	
	public function getDataFormat() {
	    return $this->dataFormat;
	}
	
	protected function processData(array $data = array()) {
	    
	    $ret = null;
	    switch($this->dataFormat) {
	        case 'json' :
	            $ret = json_encode($data);
	            break;
	        default :
	            $ret = $data; // just return the array
	    }
	    return $ret;
	}
}