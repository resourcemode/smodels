<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */
namespace Supa\Product;

use \Exception;
use \ArrayIterator;
use Supa\Product\Product;
use Supa\Product\Collection as ProductCollection;
use PDO;

/**
 * Product mapper class
 */
class Mapper extends \Supa\AbstractMapper {

    /**
     * Constructor for product mapper class
     *
     * @param PDO $db
     */
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load a product by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return Product
	 */
	public function loadByUid($uid) {
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND product.active = 1 ';
		}
		
		$uid = (int)$uid;
		$sql = "SELECT product.id as uid, product.active, product.`title`, product.path, product.created, product.created_by AS createdBy,
						product.last_editied_by AS lastEditiedBy, product.last_editied AS lastEditied, product.description, product.featured,
						product.price
		          FROM product
		         WHERE product.id = :uid
		         $activeCheckSQL";
		         
		$statement = $this->query($sql, array(':uid'=>$uid));
		if(!$statement) {
			throw new Exception('Error loading product by uid: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		if(!$data) {
			throw new Exception('Failed loading Product by uid: ' . $this->getDbError($statement));
		}
		
		$product = new Product($data);
			
		return $product;
	}

	/**
	 * Load a product by it's url path
	 * 
	 * @param string $path
	 * @throws Exception
	 * @return Product|null
	 */
	public function loadByPath($path) {
		
		$path = $this->db->quote($path, PDO::PARAM_STR);
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND product.active = 1 ';
		}
		
		$sql = "SELECT product.id as uid, product.active, product.`title`, product.path, product.created, product.created_by AS createdBy,
						product.last_editied_by AS lastEditiedBy, product.last_editied AS lastEditied, product.description, product.featured,
						product.price
		          FROM product
		         WHERE product.path = $path
		         $activeCheckSQL";
		         
		$statement = $this->query($sql);
		if(!$statement) {
			throw new Exception('Error loading product by url path: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		$product = false;
		if($data) {
			$product = new Product($data);
		}
					
		return $product;
	}
	
	/**
	 * Load products by category uid
	 * 
	 * @param int $catId
	 * @param bool $onlyActiveProds True to bring back only active products
	 * @param bool $onlyActiveCats True to bring back only active categories
	 * @throws Exception
	 * @return \ArrayIterator The products in the given category
	 */	
	public function loadProductsInCatByCatId($catId, $onlyActiveProds = true, $onlyActiveCats = true) {
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND product.active = 1 ';
		}
		
		$data = array();
		$data[':catUid'] = (int)$catId;
		$data[':catActiveState'] = (int)$onlyActiveCats;
		$data[':prodActiveState'] = (int)$onlyActiveProds;
		
		$sql = "SELECT product.id as uid, product.active, product.`title`, product.path, product.created, product.created_by AS createdBy,
						product.last_editied_by AS lastEditiedBy, product.last_editied AS lastEditied, product.description, product.featured,
						product.price
				  FROM product
				 INNER JOIN product_to_category ON product_to_category.productId = product.id
				 INNER JOIN category ON category.Id = product_to_category.categoryId
				 WHERE product_to_category.categoryId = :catUid
				   AND category.active = :catActiveState
				   AND product.active = :prodActiveState 
				   $activeCheckSQL";
		
		$statement = $this->query($sql, $data);
		if(!$statement) {
			throw new Exception('Error loading products in category: ' . $this->getDbError());
		}
		$products = $statement->fetchAll(PDO::FETCH_ASSOC);
		if(!$products) {
			throw new Exception('Failed fetching Products in category: ' . $this->getDbError($statement));
		}
		
		$productCollection = new ProductCollection(array());
		foreach($products as $product) {
			$productCollection->append(new Product($product));
		}
				
		return $productCollection;
		
	}

	/**
	 * Load featured products
	 * 
	 * @throws Exception
	 * @return ArrayIterator The products flagged as 'featured'
	 * @see ProductCollection
	 */
	public function loadFeaturedProducts() {
	    
	    $sql = 'SELECT product.id as uid, product.active, product.`title`, product.path, product.created, product.created_by AS createdBy,
						product.last_editied_by AS lastEditiedBy, product.last_editied AS lastEditied, product.description, product.featured,
						product.price
				FROM product
				WHERE product.featured = 1
				LIMIT 5';
	    
	    $statement = $this->db->query($sql);
		if(!$statement) {
			throw new Exception('Error loading featured products: ' . $this->getDbError());
		}
		$products = $statement->fetchAll(PDO::FETCH_ASSOC);
		if(!$products) {
			throw new Exception('Failed fetching featured Products: ' . $this->getDbError($statement));
		}
		
		$productCollection = new ProductCollection(array());
		foreach($products as $product) {
			$productCollection->append(new Product($product));
		}
				
		return $productCollection;	    
	    
	}

	/**
	 * @todo merge with loadFeaturedProducts()
	 * Load products
	 * 
	 * @return \ArrayIterator
	 * @throws Exception
	 */
	public function loadProducts() {
	    
	    $sql = 'SELECT product.id as uid, product.active, product.`title`, product.path, product.created, product.created_by AS createdBy,
						product.last_editied_by AS lastEditiedBy, product.last_editied AS lastEditied, product.description, product.featured,
						product.price
				FROM product
				LIMIT 10';
	    
	    $statement = $this->db->query($sql);
		if(!$statement) {
			throw new Exception('Error loading products: ' . $this->getDbError());
		}
		$products = $statement->fetchAll(PDO::FETCH_ASSOC);
		if(!$products) {
			throw new Exception('Failed fetching Products: ' . $this->getDbError($statement));
		}
		
		$productCollection = new ProductCollection(array());
		foreach($products as $product) {
			$productCollection->append(new Product($product));
		}
				
		return $productCollection;	    
	    
	}
	
	/**
	 * Create a product
	 * 
	 * @param array $productData The product data
	 * @throws Exception
	 * @return Product
	 */
	public function createProduct(Product $product) {
		
		$insertData = array();
		$insertData[':title'] = $product->getTitle();
		$insertData[':path'] = $product->getPath();
		$insertData[':createdByUid'] = $product->getCreatedBy()->getUID();
		$insertData[':created'] = $this->getTimestamp();
		$insertData[':description'] = $product->getDescription();
		$insertData[':price'] = $product->getPrice();
		
		$sql = 'INSERT INTO `product`
						SET `title` = :title,
							`path` = :path,
							`created_by` = :createdByUid,
							`created` = :created,
							`description` = :description,
							`price` = :price';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($insertData);
		if(false === $affected) {
			throw new Exception('Error inserting product: ' . $this->getDbError());
		}
		
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			throw new Exception("Create product lastInsertId return invalid: $uid");
		}
		
		$product->setUid($uid);
		$product->setCreated($insertData[':created']);
		$product->setActiveState(1);
		
		return $product;
		
	}

	/**
	 * Update a product
	 * 
	 * @param Product $product
	 * @throws Exception
	 * @return Product
	 */
	public function update(Product $product) {
		
		$updateData = array();
		$updateData[':title'] = $product->getTitle();
		$updateData[':path'] = $product->getPath();
		$updateData[':uid'] = (int)$product->getUID();
		$updateData[':lastEditiedBy'] = $product->getLastEditiedBy()->getUID();
		$updateData[':lastEditied'] = $this->getTimestamp();
		$updateData[':description'] = $product->getDescription();
		
		$sql = 'UPDATE `product`
				   SET `title` = :title,
				   		`path` = :path,
				   		`last_editied_by` = :lastEditiedBy,
				   		`last_editied` = :lastEditied,
				   		`description` = :description
				 WHERE `id` = :uid';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		if(false === $affected) {
			throw new Exception('Error updating product: ' . $this->getDbError());
		}
		
		$product->setLastEditied($updateData[':lastEditied']);
		
		return $product;
	}

	/**
	 * Delete a product
	 * 
	 * @param Product $product
	 * @throws Exception
	 * @return bool True on success, false otherwise
	 */
	public function deleteProduct(Product $product) {
		
		$uid = (int)$product->getUID();
		
		$sql = "DELETE FROM product WHERE id = :uid LIMIT 1";
		
		$affected = $this->exec($sql, array(':uid'=>$uid));		
		if(false === $affected) {
			throw new Exception('Product not deleted: ' . $this->getDbError());
		}
		
		return $affected;
		
	}
	
	/**
	 * Update a product's 'featured' flag
	 * 
	 * @param \Supa\Product\Product $product
	 * @param bool $featured
	 * @throws \Exception
	 * @return \Supa\Product\Product
	 */
	public function updateFeaturedFlag(\Supa\Product\Product $product) {
		
		$updateData = array();
		$updateData[':uid'] = (int)$product->getUID();
		$updateData[':featured'] = (int)$product->isFeatured();
		
		$sql = 'UPDATE `product`
				   SET `featured` = :featured
				 WHERE `id` = :uid';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		if(false === $affected) {
			throw new Exception('Error flagging product as featured: ' . $this->getDbError());
		}
		
		return $product;
	}
}