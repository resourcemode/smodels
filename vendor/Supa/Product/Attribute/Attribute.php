<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */


namespace Supa\Product\Attribute;

use Supa\EntityInterface;

/**
 * Product Attribute class
 */
class Attribute implements EntityInterface {
   
	/**
	 * The uid of the product
	 * @var int
	 */
    protected $uid;
    
    /**
     * The product attribute name
     * @var string
     */
    protected $name;
    
    /**
     * Values associated with this attribute
     * @var \ArrayIterator
     */
    protected $values;
	
	/**
	 * User who created attribute
	 * @var User|int
	 */
	protected $createdBy;
	
	/**
	 * User who last editied attribute
	 * @var User|int
	 */
	protected $lastEditiedBy;
	
	/**
	 * Timestamp of when attribute was last editied
	 * @var string
	 */
	protected $lastEditied;
	
	/**
	 * Timestamp of when attribute was created
	 * @var string
	 */
	protected $created;

    /**
     * Constructor for product attribute class
     * 
     * @param array $data
     */
    public function __construct(array $data = array()) {
		$this->assignClassVariables($data);
    }
    
	/**
	 * Get the product attribute uid
	 * 
	 * @return int
	 */   
    public function getUID() {
        return $this->uid;
    }

	/**
	 * Set the product attribute uid
	 * 
	 * @param int $uid
	 */
    public function setUID($id) {
        $this->uid = $id;
    }
    
	/**
	 * Get the product name
	 * 
	 * @return string
	 */   
    public function getName() {
        return $this->name;
    }
    
	/**
	 * Set the product name
	 * 
	 * @param string $name
	 */   
    public function setName($name) {
        $this->name = $name;
    }
    
    /**
     * Get the values associated with this attribute
     * 
     * @return \ArrayIterator
     */
	public function getValues() {
		if(!($this->values instanceof \ArrayIterator))  {
			$this->values = lazy_load_by_mapper2($this, 'ProductAttributeMapper', 'getAttrValues');
		}
		return $this->values;
	}
	
	/**
	 * Sets the attribute values for this attribute
	 * 
	 * @param \ArrayIterator|int $values
	 * @return void
	 */
	public function setValues($values) {
	    $this->values = $values;
	}
	
	/**
	 * Get the uid of user who last editied this attribute
	 * 
	 * @return \Supa\User\User
	 */
	public function getLastEditiedBy() {
		return \Supa\lazy_load_user($this->lastEditiedBy, 'UserMapper', 'loadByUid');
	}
	
	/**
	 * Get the timestamp of when this attribute was created
	 * 
	 * @return string
	 */	
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * Set the timestamp of when this attribute was created
	 * 
	 * @param string $strFormattedTime
	 */	
	public function setCreated($strFormattedTime) {
		$this->created = $strFormattedTime;
	}

	/**
	 * Get the user who created this attribute
	 * 
	 * @return User
	 */
	public function getCreatedBy() {
		return \Supa\lazy_load_user($this->createdBy, 'UserMapper', 'loadByUid');
	}

	/**
	 * Get timestamp of when this attribute was last editied
	 * 
	 * @return string
	 */
	public function getLastEditied() {
		return $this->lastEditied;
	}
	
	/**
	 * set the uid of user who created this attribute
	 * 
	 * @param int
	 */
	public function setCreatedBy($userId) {
		$this->createdBy = $userId;
	}

	/**
	 * Set timestamp of when this attribute was last editied
	 * 
	 * @param string $lastEditiedTime
	 */
	public function setLastEditied($lastEditiedTime) {
		$this->lastEditied = $lastEditiedTime;
	}
	
	/**
	 * Set the uid of user who last editied this attribute
	 * 
	 * @param int $uid
	 */
	public function setLastEditiedBy($uid) {
		$this->lastEditiedBy = $uid;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
}