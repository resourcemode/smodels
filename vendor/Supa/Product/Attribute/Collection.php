<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute;

use \ArrayIterator;
use Supa\Product\Attribute\ProductAttribute;

/**
 * Product attribute collection class
 */
class Collection extends \ArrayIterator  {

	/**
	 * Constructor for product attribute collection class
	 * 
	 * @param array $array Array of ProductAttribute objects
	 * @see ProductAttribute For Items in collection
	 * @see ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}