<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute\Value;

/**
 * Product attribute value collection class
 */
class Collection extends \ArrayIterator  {
	
	/**
	 * Constructor for product attribute value collection class
	 * 
	 * @param array $array Array of \Supa\Product\Attribute\Value\Value objects
	 * @see \Supa\Product\Attribute\Value\Value For Items in collection
	 * @see \ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}