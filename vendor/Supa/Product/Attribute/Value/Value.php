<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute\Value;

/**
 * Product attribute value class
 */
class Value implements \Supa\EntityInterface {
	
	/**
	 * The unique id for this obejct 
	 * @var uid
	 */
	protected $uid;
	
	/**
	 * The value
	 * @var string
	 */
	protected $name;

	/**
	 * Constructor for product attribute value class
	 * 
	 * @param array $data
	 */
	public function __construct(array $data = array()) {
		$this->assignClassVariables($data);
	}
	
	/**
	 * Get the product uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return $this->uid;
	}
	
	/**
	 * Set the product uid
	 * 
	 * @param int $uid
	 */
	public function setUID($uid) {
		$this->uid = $uid;
	}
	
	/**
	 * Get the value
	 * 
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set the value
	 * 
	 * @param string $value
	 */
	public function setName($value) {
	    $this->name = $value;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
}