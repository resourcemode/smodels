<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute;

use Supa\Product\Attribute\Value\Collection as ProductAttributeValueCollection;
use Supa\Product\Attribute\Mapper as ProductAttributeMapper;
use Supa\Product\Attribute\Attribute as ProductAttribute;
use Supa\Product\Attribute\Value\Value as ProductAttributeValue;
use Supa\Product\Product;
use Supa\User\User;
use Supa\AbstractMapper;
use \Supa\AbstractFilter;
use Exception;

/**
 * Product attribute service class
 */
class Service {

	/**
	 * Data mapper for product attribute object
	 * @var \Supa\AbstractMapper
	 */
    protected $mapper;
    
    protected $messages = array();
    
    protected $lastException = null;
    
    /**
     * Filter for creating an attribute
     * @var \Supa\AbstractFilter
     */
    protected $createFilter;
    
    /**
     * Filter for editing an attribute
     * @var \Supa\AbstractFilter
     */
    protected $editFilter;
   
	/**
	 * Constructor for page class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}
    
    /**
	 * Get product attribute by uid
	 * 
	 * @param int $uid
	 * @return ProductAttribute|null
	 */
    public function getByUid($uid) {
	    try {
	        $productAttr = $this->mapper->loadByUid($uid);
	    }catch(Exception $e) {
	        $productAttr = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $productAttr;
	}
	
	/**
	 * Create a product attribute
	 * 
	 * @param string $attributeName
	 * @param User $user User who created attribute
	 * @param array $attributeValues
	 * @return ProductAttribute|null
	 */
	public function createAttribute($attributeName, User $user, array $attributeValues = array()) {
		
	    try {
	    	
	    	$attribute = array('name'=>$attributeName);
	    	
			$filter = $this->createFilter; //getServiceLocator()->get('ProductAttributeCreateFilter');
			$filter->prepareFilter();
			$filter->setData($attribute);
			
			if(!$filter->isValid()) {
				$this->messages = $filter->getFlatMessages();
				throw new Exception('Invalid data when creating product attribute');
			}
			
	        $productAttr = new ProductAttribute($attribute);
	        $productAttr->setCreatedBy($user);
			$productAttr = $this->mapper->create($productAttr);
			$productAttrValues = new ProductAttributeValueCollection(array());
			
			if(!empty($attributeValues)) {
							   
		        foreach($attributeValues as $attrValue) {
		            
		            $attr['name'] = $attrValue;
		            
		            $filter->setData($attr);
		            if(!$filter->isValid()) {
		            	$this->messages = $filter->getFlatMessages();
		            	throw new Exception('Invalid attribute value given when adding attribute values');
		            }
		            
		            $productAttrValues->append(new ProductAttributeValue($attr));
		        }
		        
		        if($this->mapper->addAttrValues($productAttr, $productAttrValues)) {
		        	$productAttr->setValues($productAttrValues);
		        }
		        
	    	}
			
		}catch(Exception $e) {
			$productAttr = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $productAttr;
	}
	
	/**
	 * Delete a product attribute
	 * 
	 * @param int $prodAttrUid
	 * @throws Exception
	 * @return bool True if product was deleted, false otherwise
	 */
	public function deleteAttribute($prodAttrUid) {
	    
	    try {
	        
	        $prodAttrUid = (int)$prodAttrUid;
	        if($prodAttrUid <= 0) {
	        	throw new Exception('Invalid uid when deleting product attribute');
	        }

			$productAttr = $this->getByUid($prodAttrUid);
			if(is_null($productAttr)) throw new Exception($this->getLastException()->getMessage());
			
			if($this->mapper->isUsedByProducts($productAttr)) {
				throw new Exception('Unable to delete product attribute as products are using it');
			}
			
			$result = $this->mapper->delete($productAttr);
	    }catch(Exception $e) {
	    	$result = false;
	    	$this->messages[] = $e->getMessage();
	    	$this->lastException = $e;
	    }
	    return $result;
	     
	}
	
	/**
	 * Update values that belong to an attribute
	 * 
	 * @param ProductAttribute $productAttr
	 * @param array $values
	 * @return bool
	 */
	public function updateAttributeValues(ProductAttribute $productAttr, array $values = array()) {

		if(empty($values)) {
			return false;
		}
		
		try {
			
			$bool = true;
			$filter = $this->editFilter;//$this->getServiceLocator()->get('ProductAttributeValueEditFilter');
			$filter->prepareFilter();
			
			$prodAttrValColl = new ProductAttributeValueCollection(array());
			foreach($values as $attrValue) {
				$filter->setData($attrValue);
				if(!$filter->isValid()) {
					$this->messages[] = $filter->getFlatMessages();
					continue;
				}
				$v = new ProductAttributeValue($attrValue);
				$prodAttrValColl->append($v);
			}
			
			$productAttr->setValues($prodAttrValColl);
			$this->mapper->updateAttrValue($productAttr);
			
		}catch(Exception $e) {
			$bool = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;			
		}
		
		return $bool;
	}
	
	public function addAttributeValues(ProductAttribute $productAttr, array $newAttrValues = array()) {
		
		if(empty($newAttrValues)) {
			return false;
		}
		
		try {
			
			$bool = true;
			$filter = $this->createFilter; //$this->getServiceLocator()->get('ProductAttributeValueCreateFilter');
			$filter->prepareFilter();
			
			$prodAttrValColl = new ProductAttributeValueCollection(array());
			foreach($newAttrValues as $attrValue) {
				$filter->setData($attrValue);
				if(!$filter->isValid()) {
					$this->messages[] = $filter->getFlatMessages();
					continue;
				}
				$v = new ProductAttributeValue($attrValue);
				$prodAttrValColl->append($v);
			}
			
			$this->mapper->addAttrValues($productAttr, $prodAttrValColl);
			
		}catch(Exception $e) {
			$bool = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;				
		}
		
		return $bool;
	}
	
	public function deleteAttributeValues(ProductAttribute $productAttr, array $attrValueUidsToDelete = array()) {
		
		if(empty($attrValueUidsToDelete)) {
			return false;
		}
			
		try {
			$bool = true;
			$prodAttrValColl = new ProductAttributeValueCollection(array());
			foreach($attrValueUidsToDelete as $attrValueUid) {
				// @todo change to get from db
				$attrValue = new ProductAttributeValue(array('uid'=>$attrValueUid)); //$this->getByUid($attrValueUid);
//				if(!$attrValue) {
//					$this->messages[] = sprintf('Unable to load attribute value: %s', $attrValue);
//				}
				$prodAttrValColl->append($attrValue);
			}
			$this->mapper->deleteAttrValues($productAttr, $prodAttrValColl);
			
		}catch(Exception $e) {
			$bool = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;				
		}
		
		return $bool;
	}
		
	/**
	 * Update a product attribute
	 * 
	 * @param array $data The updated data for product attribute
	 * @param User $user User who is updating attribute
	 * @throws Exception
	 * @return ProductAttribute|null
	 */
	public function updateAttribute(array $data = array(), User $user) {

	    try {
	    	
			$filter = $this->editFilter;//getServiceLocator()->get('ProductAttributeEditFilter');
			$filter->prepareFilter();
			$filter->setData($data);
			if(!$filter->isValid()) {
	        	$this->messages = $filter->getFlatMessages();
	        	throw new Exception('Invalid data given when updating product attribute');
	        }
	        
	        // @todo only update changed values 
	    	$productAttr = new ProductAttribute($data);
	        $productAttr->setLastEditiedBy($user);
	        $productAttr->setName($data['name']);
	        $values = (isset($data['values']) && is_array($data['values']) ? $data['values'] : array());
	        $this->updateAttributeValues($productAttr, $values);
	        
	        // determine attribute values to add or remove
	        $currentAttrValueUids = (empty($data['currentAttrValueUids']) ? array() : $data['currentAttrValueUids']);
	        $updatedAttrValueUids = (empty($data['updatedAttrValueUids']) ? array() : $data['updatedAttrValueUids']);
	        $newAttrValues = (empty($data['newAttrValues']) ? array() : $data['newAttrValues']);
	        $attrValueUidsToDelete = array(); $attrValueUidsToAdd = array();
			if(!empty($updatedAttrValueUids) || !empty($currentAttrValueUids)) {			
				if(!empty($currentAttrValueUids)) {
					$attrValueUidsToDelete = array_diff($currentAttrValueUids, $updatedAttrValueUids);
					$attrValueUidsToAdd = array_diff($updatedAttrValueUids, $currentAttrValueUids);
				}else {
					$attrValueUidsToAdd = $updatedAttrValueUids;
				}
			}

			$this->addAttributeValues($productAttr, $newAttrValues);
			$this->deleteAttributeValues($productAttr, $attrValueUidsToDelete);
	        $productAttr = $this->mapper->update($productAttr);
	        
		    // reset lazy load
			$productAttr->setValues($productAttr->getUID());	        
	    }catch(Exception $e) {
	        $productAttr = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $productAttr;
	    
	}
	
	/**
	 * Get the values for a product attribute
	 * 
	 * @param int $productAttrId
	 * @throws Exception
	 * @return ProductAttributeValueCollection|null
	 */
	public function getAttrValues($productAttrId) {   
	    try {	        
	        $productAttr = $this->getByUid($productAttrId);
	        if(is_null($productAttr)) throw new Exception($this->getLastException()->getMessage());
	        
    	    $productAttrValuesCollection = $this->mapper->getAttrValues($productAttr);
	    }catch(Exception $e) {
	        $productAttrValuesCollection = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $productAttrValuesCollection;   
	}
	
	/**
	 * Update attribute values for a product
	 * 
	 * @param Product $product
	 * @param array $updatedAttrValueUids
	 * @param arrray $currentAttrValueUids
	 * @throws Exception
	 * @return bool True on success, false on failure
	 */
	public function updateAttrValuesForProduct(Product $product, $updatedAttrValueUids = array(), $currentAttrValueUids = array()) {
		try {
		
			// throw exception if both arrays are empty
			$bothEmpty = (empty($updatedAttrValueUids) ? (empty($currentAttrValueUids) ? true : false) : false);
			if($bothEmpty) throw new Exception('No attribute values given when trying to update for product');
						
			$attrValueUidsToDelete = array();
			$attrValueUidsToAdd = array();
			if(!empty($currentAttrValueUids)) {
				$attrValueUidsToDelete = array_diff($currentAttrValueUids, $updatedAttrValueUids);
				$attrValueUidsToAdd = array_diff($updatedAttrValueUids, $currentAttrValueUids);
			}else {
				$attrValueUidsToAdd = $updatedAttrValueUids;
			}
			
			if($attrValueUidsToAdd == $attrValueUidsToDelete) {
				return true;
			}		

			$updated = $this->mapper->updateProductAttrValues($product, $attrValueUidsToAdd, $attrValueUidsToDelete);
			
			// reset lazy load
			// TODO: change?
			$product->setAttributes($product->getUID());
		}catch(Exception $e) {
			$updated = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $updated;
	}
	
	/**
	 * Get's the attributes belonging to a Product
	 * 
	 * @param Product $product
	 * @throws Exception
	 * @return \ArrayIterator|false
	 */
	public function getAttributesForProduct($product) {   
	    try {
    	    $productAttrCollection = $this->mapper->getProductAttributes($product);
	    }catch(Exception $e) {
	        $productAttrCollection = false;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $productAttrCollection;   
	}
   
	/**
	 * Get any service class messages
	 * 
	 * @return array
	 */
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get last exception thrown by this class
	 * 
	 * @return \Exception
	 */
	public function getLastException() {
		return $this->lastException;
	}
	
	/**
	 * Set the filter for editing a ProductAttribute
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
    
	/**
	 * Set the filter for creating a ProductAttribute
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
}