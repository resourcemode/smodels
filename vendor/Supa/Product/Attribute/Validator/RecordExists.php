<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute\Validator;

use Supa\Product\Attribute\Validator\AbstractRecord;

/**
 * RecordExists class
 */
class RecordExists extends AbstractRecord {
	
	/**
	 * Checks if a record exists in the data store
	 * 
	 * @see AbstractRecord For constructor definition and 'key' information
	 * @see \Zend\Validator\ValidatorInterface For method definition
	 * @param mixed $value
	 * @return bool
	 */	
    public function isValid($value) {
    	
        $valid = true;
        $this->setValue($value);

        $result = $this->query($value);
        if (!$result) {
            $valid = false;
            $this->error(self::ERROR_NO_RECORD_FOUND);
        }

        return $valid;
    }
}
