<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute\Filter;

use Supa\Product\Attribute\Filter\EditFilter;
use Supa\Product\Attribute\Validator\AbstractRecord;
use Supa\AbstractFilter;

/**
 * Filter class for editing product attribute values
 */
class EditFilter extends \Supa\Product\Attribute\Filter\CreateFilter {
    	
	/**
	 * Constructor for edit product attribute filter class
	 */
    public function __construct() {
    	parent::__construct();        
    }
            
    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    }
}