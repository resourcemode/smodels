<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute\Filter;

use Supa\Product\Attribute\Filter\CreateFilter;
use Supa\Product\Attribute\Validator\AbstractRecord;
use Supa\AbstractFilter;

// TODO: work out a way to make fields required on the fly

/**
 * Filter class for creating product attribute values
 */
class CreateFilter extends AbstractFilter {
	
	/**
	 * Validator for attribute name
	 * @var AbstractRecord
	 */
    protected $nameValidator = null;
	
	/**
	 * Constructor for the product attribute filter class
	 */
    public function __construct() {
    	parent::__construct();        
    }
    
    /**
     * Set the name validator
     * 
     * @param AbstractRecord $nameValidator
     * @return CreateFilter
     */
    public function setNameValidator(AbstractRecord $nameValidator) {
    	$this->nameValidator = $nameValidator;
    	return $this;
    }
    
    /**
     * Get the name validator
     * 
     * @return AbstractRecord
     */
    public function getNameValidator() {
    	return $this->nameValidator;
    }
        
    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	$this->add(array(
                'name' => 'uid',
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits',
                    ),
                ),
            ));
               
    	$this->add(array(
                'name' => 'name',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a attribute value name'
                    		)
                    	)
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^.{1,50}$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid attribute value, please re-enter'
                    		)
                    	)
                    ),
                    $this->nameValidator
                ),
            ));
           	$this->getFilter()->get('uid')->setRequired(false);
    }
}