<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Attribute;

use Supa\Product\Product;
use Supa\Product\Attribute\Attribute as ProductAttribute;
use Supa\Product\Attribute\Collection as ProductAttributeCollection;
use Supa\Product\Attribute\Value\Value as ProductAttributeValue;
use Supa\Product\Attribute\Value\Collection as ProductAttributeValueCollection;
use \Exception;
use \PDO;

/**
 * Product attribute mapper class
 */
class Mapper extends \Supa\AbstractMapper {

	/**
	 * Constructor for product attribute mapper class
	 * 
	 * @param \PDO $db
	 */
	public function __construct($db) {
		parent::__construct($db);
	}
    
	/**
	 * Load a product attribute by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return ProductAttribute
	 */
    public function loadByUid($uid) {
	
	    $uid = (int)$uid;
	
	    $sql = 'SELECT product_attribute.id as uid, product_attribute.name, product_attribute.created,
	    				product_attribute.created_by as createdBy, product_attribute.last_editied_by as lastEditiedBy,
	    				product_attribute.last_editied as lastEditied
	              FROM product_attribute
	             WHERE product_attribute.id = :uid
	             LIMIT 1';
	
	    $statement = $this->query($sql, array(':uid' => $uid));
	    if(!$statement) {
			throw new Exception('Error when loading product attribute by uid: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$data) {
			throw new Exception('Failed fetching data when loading product attribute by uid: ' . $this->getDbError($statement));
		}
	
	    $productAttr = new ProductAttribute($data);
	
	    return $productAttr;
	}
    
	/**
	 * Load a product attribute by name
	 * 
	 * @param string $name
	 * @throws Exception
	 * @return ProductAttribute|null
	 */
    public function loadByName($name) {
    		
	    $sql = 'SELECT product_attribute.id as uid, product_attribute.name, product_attribute.created,
	    				product_attribute.created_by as createdBy, product_attribute.last_editied_by as lastEditiedBy,
	    				product_attribute.last_editied as lastEditied
	              FROM product_attribute
	             WHERE product_attribute.`name` = :name
	             LIMIT 1';
	
	    $statement = $this->query($sql, array(':name' => $name));
	    if(!$statement) {
			throw new Exception('Error when loading product attribute by name: ' . $this->getDbError());
		}
		
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		$productAttr = null;
	    if($data) {
	    	$productAttr = new ProductAttribute($data);
	    }
	    
	    return $productAttr;
	}
		
	/**
	 * Create a product attribute
	 * 
	 * @param ProductAttribute $productAttr
	 * @throws Exception
	 * @return ProductAttribute
	 */
	public function create(ProductAttribute $productAttr) {
		 
		$attr = array();
		$attr[':name'] = $productAttr->getName();
		$attr[':createdBy'] = $productAttr->getCreatedBy()->getUID();
		$values = $productAttr->getValues();
		
		$sql = 'INSERT INTO `product_attribute` (`name`, `created_by`) VALUES (:name, :createdBy)';
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($attr);
		if(!$affected) {
			throw new Exception('Error creating product attribute: ' . $this->getDbError());
		}
		 
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			throw new Exception('lastInsertId return value invalid when creating product attribute: ' . $this->getDbError());
		}
		 
		if($values instanceof \ArrayIterator && $values->count() > 0) {
			$sql = "INSERT INTO `product_attribute_value` (`name`, `attribute_id`) VALUES (:name, :attributeId)";
			$statement = $this->db->prepare($sql);
			foreach($values as $value) {
				$data[':name'] = $value;
				$data[':attributeId'] = $uid;
				$res = $statement->execute($data);
				if(!$res) {
					throw new Exception('Error inserting attribute values when creating new product attribute: ' . $this->getDbError());
				}
			}
		}
		 
		$productAttr->setUID($uid);
		 
		return $productAttr;
		 
	}
    
	/**
	 * Delete a product attribute
	 * 
	 * @param ProductAttribute $productAttr
	 * @throws Exception
	 * @return bool True on success, false otherwise
	 */
    public function delete(ProductAttribute $productAttr) {
    	
    	$uid = (int)$productAttr->getUID();
	    $sql = 'DELETE FROM `product_attribute` WHERE id = :uid';
	    $affected1 = $this->exec($sql, array(':uid'=>$uid));
	    if(!$affected1) {
	        throw new Exception('No attributes deleted: ' . $this->getDbError());
	    }
	    
	    $sql = 'DELETE FROM `product_attribute_value` WHERE attribute_id = :uid';
	    $affected = $this->exec($sql, array(':uid'=>$uid));
	    if(false === $affected) {
	        throw new Exception('No attribute values deleted: ' . $this->getDbError());
	    }
	    	    
	    unset($productAttr);
	    
	    return $affected1;
	    
	}
	
	/**
	 * Checks if a product attribute is currently used by products
	 * 
	 * @param ProductAttribute $productAttr
	 * @throws Exception
	 * @return bool True if product attribute is currently used, false otherwise
	 */
	public function isUsedByProducts(ProductAttribute $productAttr) {
		
    	$prodAttrUid = (int)$productAttr->getUID();
		
		$sql = 'SELECT COUNT(1)
				  FROM `product_attribute`
				 INNER JOIN `product_attribute_value` ON `product_attribute_value`.`attribute_id` = `product_attribute`.`id`
				 INNER JOIN `product_attribute_value_association` ON `product_attribute_value_association`.`attribute_id` = `product_attribute_value`.`id`
				 WHERE `product_attribute`.`id` = :prodAttrUid';
		
		$statement = $this->query($sql, array(':prodAttrUid'=>$prodAttrUid));
		
		if(!$statement) {
			throw new Exception('Statement error when checking if product attribute is used by products: ' . $this->getDbError());
		}
		
		return ($statement->fetchColumn() > 0);
	}
	
	
	/**
	 * Update a product attribute
	 * 
	 * @param ProductAttribute $productAttr
	 * @throws Exception
	 * @return ProductAttribute
	 */
	public function update(ProductAttribute $productAttr) {
	
	    $updateData = array();
	    $updateData[':uid'] = (int)$productAttr->getUID();
	    $updateData[':name'] = $productAttr->getName();
	    $updateData[':lastEditied'] = $this->getTimestamp();
	    $updateData[':lastEditiedBy'] = $productAttr->getLastEditiedBy()->getUID();
	
	    $sql = "UPDATE `product_attribute`
	    		   SET name = :name,
	    		   	   last_editied = :lastEditied,
	    		   	   last_editied_by = :lastEditiedBy
	    		WHERE id = :uid";
	    
	    $sth = $this->db->prepare($sql);
	    $affected = $sth->execute($updateData);
	    if(false === $affected) {
	        throw new Exception('Failed updating Product Attribute: ' . $this->getDbError());
	    }
	    
	    return $productAttr;
	
	}
	
	/**
	 * Add attribute value to a product attribute
	 * 
	 * @param ProductAttribute $productAttr
	 * @param \ArrayIterator $productAttrValueColl
	 * @throws Exception
	 * @return bool True on success, false on failure
	 */
	public function addAttrValues(ProductAttribute $productAttr, \ArrayIterator $productAttrValueColl) {
	
		if($productAttrValueColl->count() == 0) {
			return false;
		}
		
	    $sql = 'INSERT INTO `product_attribute_value`
	    				SET `name` = :name,
	    					`attribute_id` = :attrUid';
	    
		$statement = $this->db->prepare($sql);
		/* @var $productAttrValue ProductAttributeValue */
		foreach($productAttrValueColl as $productAttrValue) {
			
			$data[':attrUid'] = $productAttr->getUID();
			$data[':name'] = $productAttrValue->getName();
			$res = $statement->execute($data);
			if(!$res) {
				throw new Exception('Error adding attribute values to a product: ' . $this->getDbError($statement));
			}
			
			$uid = (int)$this->db->lastInsertId();
			if($uid <= 0) {
				throw new Exception('Last insert id returned error when adding attribute value to a product: ' . $this->getDbError());
			}
			
			$productAttrValue->setUID($uid);
		}
	    // rest lazy load
		$productAttr->setValues($productAttr->getUID());		
	    return true;
	}
	
	/**
	 * Update product attribute values 
	 * 
	 * @param ProductAttribute $productAttr
	 * @throws Exception
	 * @return ProductAttribute The updated product attribute object
	 */
	public function updateAttrValue(ProductAttribute $productAttr) {
		
		if($productAttr->getValues()->count() == 0) {
			return $productAttr;
		}
		
		$sql = 'UPDATE `product_attribute_value` SET `name` = :name WHERE `id` = :uid';
		$statement = $this->db->prepare($sql);
		/* @var $productAttrValue ProductAttributeValue */
		$productAttrValueColl = $productAttr->getValues();
		foreach($productAttrValueColl as $productAttrValue) {
			$data[':uid'] = $productAttrValue->getUID();
			$data[':name'] = $productAttrValue->getName();
			$res = $statement->execute($data);
			if(!$res) {
				throw new Exception('Error updating attribute values: ' . $this->getDbError($statement));
			}				
		}
		
		return $productAttr;
	}
	
	/**
	 * Get attribute values for a product attribute
	 * 
	 * @param ProductAttribute $productAttr
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function getAttrValues(ProductAttribute $productAttr) {
	    
	    $attrId = (int)$productAttr->getUID();
	    
	    $sql = 'SELECT `name`, `id`
	              FROM `product_attribute_value`
	             WHERE `product_attribute_value`.`attribute_id` = :attrUid';
	    
	    $sth = $this->db->prepare($sql);
	    $statement = $sth->execute(array(':attrUid'=>$attrId));
	    
	    if(!$statement) {
	        throw new Exception('Failed getting attribute values: ' . $this->getDbError());
	    }
	    
	    $productAttrValueCollection = new ProductAttributeValueCollection(array());
	    $attrValues = $sth->fetchAll(PDO::FETCH_ASSOC);
	    if($attrValues) {
	        foreach($attrValues as $attrValue) {
	            $productAttrValueCollection->append(new ProductAttributeValue($attrValue));
	        }
	    }
	    
	    return $productAttrValueCollection;
	}
	
	public function loadProductAttrValues(ProductAttribute $productAttr) {
	    
	    $attrId = (int)$productAttr->getUID();
	    
	    $sql = 'SELECT av.id AS attrValueUid, av.name AS attrValueName
				  FROM product_attribute_value av, product_attribute_value_association ava, product_attribute a
				 WHERE a.id = av.attribute_id
				   AND av.id = ava.attribute_id
				   AND ava.product_id = ';
	    
	    $sth = $this->db->prepare($sql);
	    $statement = $sth->execute(array(':attrUid'=>$attrId));
	    
	    if(!$statement) {
	        throw new Exception('Failed getting attribute values: ' . $this->getDbError());
	    }
	    
	    $productAttrValueCollection = new ProductAttributeValueCollection(array());
	    $attrValues = $sth->fetchAll(PDO::FETCH_ASSOC);
	    if($attrValues) {
	        foreach($attrValues as $attrValue) {
	            $productAttrValueCollection->append(new ProductAttributeValue($attrValue));
	        }
	    }
	    
	    return $productAttrValueCollection;
	}
	
	public function getProductAttributes(Product $product) {
		
		$prodUid = (int)$product->getUID();
		
		$sql = "SELECT av.id AS attrValueUid, av.name AS attrValueName, a.id AS attrUid, a.name AS attrName
                  FROM product_attribute_value av, product_attribute_value_association ava, product_attribute  a
                 WHERE a.id = av.attribute_id 
                   AND av.id = ava.attribute_id
                   AND ava.product_id = $prodUid";
		
		$statement = $this->query($sql);
	    
	    if(!$statement) {
	    	throw new Exception('Failed getting attribute values for product: ' . $this->getDbError());
	    }
	    
	    $attrs = array();
	    
	    $attrValues = $statement->fetchAll(PDO::FETCH_ASSOC);
	    
	    $productAttributeCollection = new ProductAttributeCollection(array());
	    
	    if($statement->rowCount() > 0) {
	    
		    // get all attribute values
		    foreach($attrValues as $av) {
		    	$attrs[$av['attrUid']] = $av['attrName'];
		    	$attrValues[$av['attrName']][] = new ProductAttributeValue(array('uid'=>$av['attrValueUid'], 'name'=>$av['attrValueName']));
		    }
		    
		    // assign attribute values to corresponding attribute
		    foreach($attrs as $attrId => $attr) {
		    	$coll = new ProductAttributeValueCollection(array_values($attrValues[$attr]));
		    	$productAttributeCollection->append(new ProductAttribute(array('uid'=>$attrId, 'name'=>$attr, 'values'=>$coll)));
		   	}
	   	
	    }

	   	$product->setAttributes($productAttributeCollection);
	    
	    return $productAttributeCollection;
		
		
	}
	
	/**
	 * Update attribute values on a product
	 * 
	 * @param Product $product
	 * @param array $newAttrValueUids The attribute value uids to link to product
	 * @param array $delAttrValueUids The attribute value uids to remove from product
	 * @throws Exception
	 * @return bool True on success, false otherwise
	 */
	public function updateProductAttrValues(Product $product, array $newAttrValueUids = array(), array $delAttrValueUids = array()) {
		
		$prodUid = (int)$product->getUID();
		$res = true;
		
		if(!empty($delAttrValueUids)) {
			$sql = "DELETE FROM product_attribute_value_association WHERE product_id = ? AND attribute_id = ? LIMIT 1";
			$statement = $this->db->prepare($sql);
			foreach ($delAttrValueUids as $delAttrValueUid) {
				$delAttrValueUid = (int)$delAttrValueUid;
				if($delAttrValueUid <= 0) continue;
				$res = $statement->execute(array($prodUid, $delAttrValueUid));
				
				if(!$res) {
					throw new Exception("Failed deleting attribute value: $prodUid/$delAttrValueUid, {$this->getDbError()}");
				}
			}
		}

		if(!empty($newAttrValueUids)) {
			$sql = "INSERT INTO product_attribute_value_association(`product_id`, `attribute_id`) VALUES(?,?)";
			$statement = $this->db->prepare($sql);
			foreach($newAttrValueUids as $newAttrValueUid) {
				$newAttrValueUid = (int)$newAttrValueUid;
				if($newAttrValueUid <= 0) continue;
				$res = $statement->execute(array($prodUid, $newAttrValueUid));
				
				if(!$res) {
					throw new Exception("Failed inserting attribute value: $prodUid/$newAttrValueUid, {$this->getDbError()}");
				}
			}
		}
		
		return $res;
		
	}
	
	public function deleteAttrValues(ProductAttribute $productAttr, \ArrayIterator $delAttrValues) {
		$res = true;
		if(!empty($delAttrValues)) {
			$sql = "DELETE FROM `product_attribute_value` WHERE `id` = :uid LIMIT 1";
			$statement = $this->db->prepare($sql);
			foreach ($delAttrValues as $delAttrValue) {
				$delAttrValueUid = (int)$delAttrValue->getUID();
				if($delAttrValueUid <= 0) continue;
				$res = $statement->execute(array(':uid'=>$delAttrValueUid));
				
				if(!$res) {
					throw new Exception("Failed deleting attribute value: $delAttrValueUid, {$this->getDbError()}");
				}
			}
		}
		// rest lazy load
		$productAttr->setValues($productAttr->getUID());
		return $res;	
	}
	
	public function updateAttrValues(ProductAttribute $productAttr, array $newAttrValues = array()) {
		
		$attrUid = (int)$productAttr->getUID();
		
		$res = true;
		if(!empty($newAttrValues)) {
			$sql = "INSERT INTO product_attribute_value(`name`, `attribute_id`) VALUES(?,?)";
			$statement = $this->db->prepare($sql);
			foreach($newAttrValues as $newAttrValue) {
				$newAttrValue = trim($newAttrValue['name']);
				if(!$newAttrValue) continue;
				$res = $statement->execute(array($newAttrValue, $attrUid));
				
				if(!$res) {
					throw new Exception("Failed inserting attribute value: $attrUid, {$this->getDbError()}");
				}
			}
		}
	    // rest lazy load
		$productAttr->setValues($productAttr->getUID());
		return $res;
	}
}