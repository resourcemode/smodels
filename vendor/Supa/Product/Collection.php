<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product;

use \ArrayIterator;
use Supa\Product\Product;

/**
 * Product Collection class
 */
class Collection extends ArrayIterator  {

	/**
	 * Constructor for product collection class
	 * 
	 * @param array $array Array of Product objects
	 * @see Product For Items in collection
	 * @see ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}