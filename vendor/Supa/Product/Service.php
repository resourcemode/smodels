<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product;

use Supa\Product\Product;
use Exception;
use Supa\AbstractFilter;

/**
 * Product Service class
 */
class Service {

	/**
	 * Data mapper for Product object
	 * @var \Supa\AbstractMapper
	 */
	protected $mapper;
	
	/**
	 * Holds success or error messages 
	 * from performed operations
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by service class
	 * @var \Exception
	 */
	protected $lastException;
	
	/**
	 * Filter for creating a Product
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
		
	/**
	 * Filter for editing a Product
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;

	/**
	 * Constructor for product service class
	 *
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}

	/**
	 * Get a product by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Product\Product|null
	 */
	public function getByUid($uid) {
		try {
			$product = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$product = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $product;
	}

	/**
	 * Get products by category uid
	 * 
	 * @param int $catUid
	 * @return \ArrayIterator|null
	 */
	public function getProductsByCatUid($catUid) {
		try {
			$products = $this->mapper->loadProductsInCatByCatId($catUid);
		}catch(Exception $e) {
			$products = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $products;
	}

	/**
	 * Create a product
	 *
	 * @param array $productData
	 * @param \Supa\User\User $user The user who is creating this product
	 * @return \Supa\Product\Product|null
	 */
	public function createProduct(array $productData, \Supa\User\User $user) {
		
		try {
			$filter = $this->createFilter;//getServiceLocator()->get('ProductCreateFilter');
			$filter->prepareFilter();
			$filter->setData($productData);
			
	    	if(!$filter->isValid()) {
	        	$this->messages = $filter->getFlatMessages();
				throw new Exception('Invalid data given when creating product');
			}
			
			// TODO: load from DB then set?
			$product = new Product($productData);
			$product->setCreatedBy($user);
			$product = $this->mapper->createProduct($product);
			
		}catch(Exception $e) {
			$product = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $product;
	}
	
	/**
	 * Update a product
	 * 
	 * @param array $updateData The updated product data
	 * @param \Supa\User\User $user The user who is updating this product
	 * @return \Supa\Product\Product|null
	 */
	public function updateProduct($updateData, \Supa\User\User $user) {
		try {
			$filter = $this->editFilter; //getServiceLocator()->get('ProductEditFilter');
			$filter->prepareFilter();
			$filter->setData($updateData);
			
	    	if(!$filter->isValid()) {
	    		$this->messages = $filter->getFlatMessages();
				throw new Exception('Product data not valid when trying to update product');
			}
			
			// TODO: load from db?
			$theProduct = new Product($updateData);
			// TODO: do we need?
			$theProduct->setTitle($updateData['title']);
			$theProduct->setPath($updateData['path']);
			$theProduct->setDescription($updateData['description']);
			$theProduct->setLastEditiedBy($user->getUID());
			$product = $this->mapper->update($theProduct);
		}catch(Exception $e) {
			$product = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $product;
	}

	/**
	 * Delete a product
	 * 
	 * @param int $prodId
	 * @return bool True on success, false otherwise
	 */
	public function deleteProduct($prodId) {
		try {
			$product = $this->getByUid($prodId);
			if(is_null($product)) throw new Exception('Failed getting product for deletion');
			$deleted = $this->mapper->deleteProduct($product);
		}catch(Exception $e) {
			$deleted = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $deleted;
	}
	
	/**
	 * Get featured products
	 * 
	 * @return \ArrayIterator The featured products
	 */
	public function getFeaturedProducts() {
		try {
			$products = $this->mapper->loadFeaturedProducts();
		}catch(Exception $e) {
			$products = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $products;
	}
	
	/**
	 * @todo merge with getFeaturedProducts()
	 * Get products
	 * 
	 * @return \ArrayIterator The products
	 */
	public function getProducts() {
		try {
			$products = $this->mapper->loadProducts();
		}catch(Exception $e) {
			$products = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $products;
	}
	
	/**
	 * Set 'featured' flag on product
	 * 
	 * @param int $prodId
	 * @param bool $featured
	 * @return \Supa\Product\Product|false
	 */
	public function setFeaturedStatus($prodId, $featured) {
		try {
			$product = $this->getByUid($prodId);
			if(is_null($product)) throw new Exception('Failed getting product to set featured flag');
			$product->setFeatured($featured);
			$product = $this->mapper->updateFeaturedFlag($product, $featured);
		}catch(Exception $e) {
			$product = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $product;
	}
		
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
	
	/**
	 * Set the filter for editing a Product
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
    
	/**
	 * Set the filter for creating a Product
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
}