<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode;

use Supa\Product\DiscountCode\AbstractDiscountCode;

class PercentageDiscount extends AbstractDiscountCode {

	/**
	 * Apply the discount to $amount
	 * 
	 * @see DiscountCodeInterface::applyDiscount($amount)
	 * @param int The amount to apply the discount on
	 * @return int The discounted amount
	 */
	public function applyDiscount($amount) {
		
		$deduct = ($this->discount/100)*$amount;
		$amount = $amount - $deduct;
		
		return $amount;		
	}
}