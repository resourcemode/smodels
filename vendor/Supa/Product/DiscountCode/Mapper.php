<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode;

use \Exception;
use Supa\User\User;
use Supa\User\NullUser;
use PDO;
use Supa\Product\DiscountCode\DiscountCode;

/**
 * Discount code mapper
 */
class Mapper extends \Supa\AbstractMapper {
    
    /**
     * Constructor for user mapper class
     * 
     * @param PDO $db
     */	
	public function __construct($db) {
		parent::__construct($db);
	}
	
	/**
	 * Load a discount code by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return DiscountCode
	 */	
	public function loadByUid($uid) {
		
		$uid = (int)$uid;
		$sql = "SELECT `discount_type` as discountType, `id` as uid, `code`, `expire`, `count`, `created`, `discount`
				  FROM `discount_code` WHERE id = :uid";
		         
		$statement = $this->query($sql, array(':uid'=>$uid));
		if(!$statement) {
			throw new Exception('Error loading discount code: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		if(!$data) {
			throw new Exception('Failed loading discount code by uid: ' . $this->getDbError($statement));
		}
		
		$code = new DiscountCode($data);
			
		return $code;
	}
	
	/**
	 * Load a discount code by its code
	 * 
	 * @param string $code
	 * @throws Exception
	 * @return DiscountCode|false
	 */		
	public function loadByCode($code) {
		
		$sql = "SELECT `discount_type` as discountType, `id` as uid, `code`, `expire`, `count`, `created`, `discount`
				  FROM `discount_code` WHERE `code` = :code";
		         
		$statement = $this->query($sql, array(':code'=>$code));
		if(!$statement) {
			throw new Exception('Error loading discount code: ' . $this->getDbError());
		}
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		
		$code = false;
		if($data) {
			$code = new DiscountCode($data);
		}
		return $code;
	}

	/**
	 * Update a discount code
	 * 
	 * @param AbstractDiscountCode $code
	 * @throws Exception
	 * @return AbstractDiscountCode
	 */
	public function update(AbstractDiscountCode $code) {
		
		$updateData = array();
		$updateData[':count'] = (int)$code->getCount();
		$updateData[':expire'] = $code->getExpireDate();
		$updateData[':uid'] = (int)$code->getUID();
		$updateData[':code'] = $code->getCode();
		
		$sql = 'UPDATE `discount_code`
				   SET `count` = :count,
				   		`expire` = :expire,
				   		`code` = :code
				 WHERE id = :uid';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($updateData);
		if(false === $affected) {
			throw new Exception('Error updating product: ' . $this->getDbError());
		}
		
		return $code;
		
	}

	/**
	 * Persist a discount code
	 * 
	 * @param DiscountCode $discountCode
	 * @throws Exception
	 * @return DiscountCode
	 */
	public function create(DiscountCode $discountCode) {
		
		$insertData = array();
		$insertData[':code'] = $discountCode->getStrategy()->getCode();
		$insertData[':type'] = $discountCode->getStrategy()->getType();
		$insertData[':count'] = $discountCode->getStrategy()->getCount();
		$insertData[':discount'] = $discountCode->getStrategy()->getDiscount();
		$insertData[':created'] = $t = $this->getTimestamp();
				
		$sql = 'INSERT INTO `discount_code`
						SET `code` = :code,
							`count` = :count,
							`discount` = :discount,
							`created` = :created,
							`discount_type` = :type';
		
		$sth = $this->db->prepare($sql);
		$affected = $sth->execute($insertData);
		if($affected != 1) {
			throw new Exception('Error inserting new discount code: ' . $this->getDbError());
		}
		
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			throw new Exception("Create discount code lastInsertId return invalid: $uid");
		}
		
		$discountCode->getStrategy()->setUID($uid);
		$discountCode->getStrategy()->setCreated($t);
		
		return $discountCode;
		
	}

	/**
	 * Delete a discount code
	 * 
	 * @param DiscountCode $discountCode
	 * @throws Exception
	 * @return bool True on success, false otherwise
	 */
	public function delete(DiscountCode $discountCode) {
		
		$uid = (int)$discountCode->getStrategy()->getUID();
		
		$sql = "DELETE FROM `discount_code` WHERE id = :uid LIMIT 1";
		
		$affected = $this->exec($sql, array(':uid'=>$uid));		
		if(false === $affected) {
			throw new Exception('discount code not deleted: ' . $this->getDbError());
		}
		
		return $affected;
		
	}
}