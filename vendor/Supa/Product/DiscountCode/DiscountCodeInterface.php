<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode;

interface DiscountCodeInterface {

	/**
	 * Apply the discount to $amount
	 *
	 * @param int $amount The amount to apply the discount on
	 * @return int The discounted amount
	 */
	public function applyDiscount($amount);
}