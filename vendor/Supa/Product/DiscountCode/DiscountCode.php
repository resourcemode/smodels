<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode;

use Supa\Product\DiscountCode\DiscountCodeInterface;
use Supa\Product\DiscountCode\PercentageDiscount;
use Supa\Product\DiscountCode\AmountDiscount;

use \Exception;

class DiscountCode {
	
	const PERCENTAGE = 1;
	const AMOUNT = 2;
	
	/**
	 * The strategy to use
	 * @var DiscountCodeInterface
	 */
    protected $strategy; 
    
    /**
     * Constructor. Determines which discount
     * strategy to use
     * 
     * @param array $strategy
     */
    public function __construct(array $strategy = array()) {
    	
    	if(!isset($strategy['discountType'])) {
    		throw new Exception('No type set for discount code');
    	}
    	
        switch ($strategy['discountType']) {
            case self::PERCENTAGE: 
                $this->strategy = new PercentageDiscount($strategy);
            	break;
            case self::AMOUNT:
                $this->strategy = new AmountDiscount($strategy);
                break;
            default :
            	throw new Exception('Unknown discount strategy');
        }
        
    }

    /**
     * Apply the discount to $amount using discount
     * strategy set in constructor
     *
     * @param int The amount to apply the discount on
     * @return int The discounted amount
     * @see DiscountCodeInterface::applyDiscount($amount)
     */    
    public function applyDiscount($amount) {
    	return $this->strategy->applyDiscount($amount);
    }

    /**
     * Check if the discount code is value
     * 
     * @return bool True is code valid, false otherwise
     */
    public function isValid() {
    	return $this->strategy->validate();
    }

    /**
     * Get the discount strategy for this code
     * 
     * @return DiscountCodeInterface
     */
    public function getStrategy() {
    	return $this->strategy;
    }
}