<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */


namespace Supa\Product\DiscountCode\Filter;

use Supa\Product\DiscountCode\AmountDiscount;
use Supa\Product\DiscountCode\DiscountCode;
use Supa\Product\DiscountCode\Filter\CreateFilter;
use Supa\Product\DiscountCode\Validator\AbstractRecord;
use Supa\AbstractFilter;
use \Zend\InputFilter\Factory;

/**
 * Filter class for creating users
 */
class Create extends AbstractFilter {
	
	/**
	 * Validator for a discount code
	 * @var AbstractRecord
	 */
    protected $discountCodeValidator = null;
    
    /**
     * Valid discount types
     * @var array
     */
    protected $discountCodeTypes = array();
    
    /**
	 * Constructor for create user filter class
	 */
    public function __construct() {
    	parent::__construct();
    }
    
    /**
     * Set the discount code validator
     * 
     * @param AbstractRecord $discountCodeValidator
     * @return CreateFilter
     */
    public function setDiscountCodeValidator(AbstractRecord $discountCodeValidator) {
    	$this->discountCodeValidator = $discountCodeValidator;
    	return $this;
    }

    /**
     * Get the discount code validator
     * 
     * @return AbstractRecord
     */
    public function getDiscountCodeValidator() {
    	return $this->discountCodeValidator;
    }
    
    /**
     * Set valid discount code types
     * 
     * @param array $types
     */
    public function setDiscountCodeTypes(array $types = array()) {
    	$this->discountCodeTypes = $types;
    }
    
    /**
     * Get valid discount code types
     * 
     * @return array The valid discount code types
     */
    public function getDiscountCodeTypes() {
    	return $this->discountCodeTypes;
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	$this->add(array(
                'name' => 'uid',
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits',
                    ),
                ),
            ));
                        
    	$this->add(array(
                'name' => 'code',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^.{3,12}$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid Code, please re-enter'
                    		)
                    	)
                    ),
                    $this->discountCodeValidator
                ),
            ));

         $this->add(array(
                'name' => 'expire'
            ));
            
         $this->add(array(
                'name' => 'count'
            ));
            
        //$validator->setHaystack(array('value1', 'value2',...'valueN'));
        $this->add(array(
                'name' => 'discountType',
                'required' => true,
                'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty'
                    ),
//                    array(
//                        'name' => 'int',
//                    ),
                ),
            ));
            
         $this->add(array(
                'name' => 'discount',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
//                    array(
//                        'name' => 'int',
//                    ),
                ),     
            ));
        
        $this->getFilter()->get('uid')->setRequired(false);
    	$this->getFilter()->get('expire')->setRequired(false);
    	$this->getFilter()->get('count')->setRequired(false);
  
        
    }
    
    public function isValid() {
    	
    	$bool = parent::isValid();
    	if(!$bool) {
    		return $bool;
    	}
    	
    	// TODO: remove int cast and use Int validator when moved into zf2 framework
    	$type = (int)$this->filter->getValue('discountType');
    	$discount = (int)$this->filter->getValue('discount');
    	switch($type) {
    		case DiscountCode::PERCENTAGE :
    			if($discount < 1 || $discount > 100) {
    				$bool = false;
    			}
    			break;
    		case DiscountCode::AMOUNT :
    			if(0 >= $discount || AmountDiscount::MAX_AMOUNT < $discount) {
    				$bool = false;
    			}
    			break;
    		default :
    			$bool = false;
    	}
    	
    	return $bool;
    }
}