<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode\Filter;

use Supa\Product\DiscountCode\Validator\AbstractRecord;
use Supa\AbstractFilter;
use Zend\InputFilter\Factory;

/**
 * Filter class for editing a discount code
 */
class Edit extends \Supa\Product\DiscountCode\Filter\Create {
    
	/**
	 * Constructor for edit discount code filter class
	 */	
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    	$this->getFilter()->get('expire')->setRequired(true);
    	$this->getFilter()->get('count')->setRequired(true);
    }
}