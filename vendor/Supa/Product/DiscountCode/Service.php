<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode;

use Supa\Product\DiscountCode\DiscountCode;
use Exception;

/**
 * Discount service 
 */
class Service {
	
	/**
	 * Data mapper for Product object
	 * @var \Supa\AbstractMapper
	 */	
	protected $mapper;
	
	/**
	 * Holds success or error messages 
	 * from performed operations
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by service class
	 * @var \Exception
	 */
	protected $lastException;
	
	/**
	 * Filter for creating a DiscountCode
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
		
	/**
	 * Filter for editing a DiscountCode
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;
	
	/**
	 * Constructor for page class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Get a discount code by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Product\DiscountCode\DiscountCodeInterface|null
	 */
	public function getByUid($uid) {
		try {
			$code = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$code = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $code;
	}

	/**
	 * Get a discount code by its code
	 * 
	 * @param string $code
	 * @return \Supa\Product\DiscountCode\DiscountCodeInterface|null
	 */
	public function getByCode($code) {
		try {
			$code = $this->mapper->loadByCode($code);
		}catch(Exception $e) {
			$code = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $code;
	}
	
	public function updateDiscountCode(array $data = array(), \Supa\User\User $user) {
		try {
			
			// @todo make filter
			$uid = (int)$data['uid'];
			$count = (isset($data['count']) && !is_null($data['count']) ? (int)$data['count'] : null);
			$expireDate = (isset($data['expireDate']) && !is_null($data['expireDate']) ? $data['expireDate'] : null);
			if($uid <= 0) {
				throw new Exception('Required uid not present when updating discount code');
			}
			if(!isset($data['code'])) {
				throw new Exception('Required code not present when updating discount code');
			}
			$code = trim($data['code']);
			
			$discountCode = $this->getByUid($uid);
			
			$strategy = $discountCode->getStrategy();
			$strategy->setCount($count);
			$strategy->setExpireDate($expireDate);
			$strategy->setCode($code);
			
			$strategy = $this->mapper->update($strategy);
			
		}catch(Exception $e) {
			$discountCode = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $discountCode;		
	}
	
	/**
	 * Delete a discount code
	 * 
	 * @param int $uid The discount code uid
	 * @return bool True on success, false otherwise
	 */
	public function deleteDiscountCode($uid) {
		try {
			$discountCode = $this->getByUid($uid);
			if(is_null($discountCode)) throw new Exception('Failed getting discount code for deletion');
			$deleted = $this->mapper->delete($discountCode);
		}catch(Exception $e) {
			$deleted = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $deleted;
	}

	/**
	 * Create a discount code
	 *
	 * @param array $data The new discount code data
	 * @param \Supa\User\User $user The user who is creating the discount code
	 * @return \Supa\Product\DiscountCode\DiscountCodeInterface|null
	 */
	public function createDiscountCode(array $data = array(), \Supa\User\User $user) {
		
		try {
			
			$filter = $this->createFilter;
			$filter->prepareFilter();
			$filter->setData($data);
			
			if(!$filter->isValid()) {
				$this->messages = $filter->getFlatMessages();
				throw new Exception('Invalid data given when creating discount code');
			}
			
			$discountCode = new DiscountCode($data);
			//$discountCode->setCreatedBy($user);
			$discountCode = $this->mapper->create($discountCode);
			
		}catch(Exception $e) {
			$discountCode = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $discountCode;
	}
		
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get last exception thrown by this class
	 * 
	 * @return \Exception
	 */	
	public function getLastException() {
		return $this->lastException;
	}
	
	/**
	 * Set the filter for editing a DiscountCode
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
    
	/**
	 * Set the filter for creating a DIscountCode
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
}