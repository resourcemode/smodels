<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode;

use Supa\Product\DiscountCode\DiscountCodeInterface;
use DateTime;

abstract class AbstractDiscountCode implements DiscountCodeInterface {

	/**
	 * Unique id for discount code
	 * @var int
	 */
	protected $uid;
	
	/**
	 * The discount code
	 * @var string
	 */
	protected $code;

	/**
	 * Expire date of discount code, null for never
	 * @var string
	 */
	protected $expire;
	
	/**
	 * Usage count, int or null for unlimited
	 * @var int|null
	 */
	protected $count;
	
	/**
	 * Type of discount
	 * @var int
	 */
	protected $discountType;
	
	/**
	 * Timestamp of creation
	 * @var string
	 */
	protected $created;
	
	/**
	 * Discount value
	 * @var int
	 */
	protected $discount;
	
	/**
	 * Constructor
	 * 
	 * @param array $data
	 */
	public function __construct(array $data = array()) {
		$this->assignClassVariables($data);
	}
	
	/**
	 * Validate the discount code
	 *
	 * @param $code string
	 * @throws Exception
	 * @return bool True if a valid code, false otherwise
	 */
	public function validate() {

		$expired = false;
		if(!is_null($this->expire)) {
			$expire = new DateTime($this->expire);
			$now = new DateTime("now");
			if($this->expire < $now) {
				$expired = true;
			}			
		}
		
		$countLeft = true;
		if(!is_null($this->count) && $this->count == 0) {
			$countLeft = false;			
		}
		
		return (!$expired && $countLeft);		
	}

	/**
	 * Apply the discount to $amount
	 * 
	 * @param int $amount The amount to apply the discount on
	 * @return int The discounted amount
	 * @see DiscountCodeInterface::applyDiscount($amount)
	 */
	public function applyDiscount($amount) {
		return 0;
	}

	/**
	 * Get the uid for this code
	 * 
	 * @return int
	 */
	public function getUID() {
		return (int)$this->uid;
	}
	
	/**
	 * Set the uid for this code
	 * 
	 * @param int $uid
	 */
	public function setUID($uid) {
		$this->uid = $uid;
	}
		
	/**
	 * Get the discount code
	 *
	 * @return string
	 */
	public function getCode() {
		return $this->code;
	}
	
	/**
	 * Set the discount code
	 * 
	 * @param string $code
	 */
	public function setCode($code) {
		$this->code = $code;
	}
		
	/**
	 * Get the discount value
	 *
	 * @return int
	 */
	public function getDiscount() {
		return (int)$this->discount;
	}
	
	/**
	 * Check if this code has any limitations e.g
	 * usage count or expiration date
	 *
	 * @return boolean
	 */
	public function isLimited() {
		return (!is_null($this->count) || !is_null($this->expire));
	}
	
	/**
	 * Get count of how many codes are left, null if unlimited
	 *
	 * @return int|null
	 */
	public function getCount() {
		return $this->count;
	}
	
	/**
	 * Get expire date timestamp (null for unlimited) of discount code
	 *
	 * @return string
	 */
	public function getExpireDate() {
		return $this->expire;
	}
	
	/**
	 * Set expire date timestamp (null for unlimited) of discount code
	 *
	 * @param string|null $expire The expire timestamp of discount code or null
	 */
	public function setExpireDate($expire) {
		$this->expire = $expire;
	}
		
	/**
	 * Get timestamp when discount code created
	 *
	 * @return string
	 */
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * Set created date timestamp of discount code
	 *
	 * @param string|null $created The created timestamp of discount code or null
	 */
	public function setCreated($created) {
		$this->created = $created;
	}
	
	/**
	 * Consume a discount code
	 *
	 * @throws \Exception
	 * @return bool True on success, else false
	 */
	public function consume() {
		// invalidate this discount for future use
		if($this->isLimited()) {
			if($this->getCount() >= 1) {
				$this->setCount($this->getCount()-1);
			}else {
				throw new Exception('Usage count for discount code returned 0');
			}
		}else {
			// has date expiry so do nothing
		}
	}

	public function getType() {
		return $this->discountType;
	}
	
	public function setType($type) {
		$this->discountType = $type;
	}
	
	/**
	 * Invalidate a discount code for future use
	 *
	 * @return bool True on success, else false
	 */
	public function consumeAll() {
		// invalidate this discount for future use
		// -- set count to 0
		// -- set expire to date in past
	}
	
	/**
	 * Set the usage count for this code
	 *
	 * @param int $count
	 */
	public function setCount($count) {
		$this->count = $count;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
		
}