<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\DiscountCode;

use Supa\Product\DiscountCode\AbstractDiscountCode;

class AmountDiscount extends AbstractDiscountCode {

	const MAX_AMOUNT = 32767;
	/**
	 * Apply the discount to $amount
	 * 
	 * @see DiscountCodeInterface::applyDiscount($amount)
	 * @param int The amount to apply the discount on
	 * @return int The discounted amount
	 */	
	public function applyDiscount($amount) {
		
		$amount = ($amount - $this->discount);
		
		return $amount;
	}
}