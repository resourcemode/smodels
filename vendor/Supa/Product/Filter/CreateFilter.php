<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Filter;

use Supa\AbstractFilter;
use Supa\Product\Filter\CreateFilter;
use Supa\Product\Validator\AbstractRecord;

/**
 * Filter class for creating Products
 */
class CreateFilter extends AbstractFilter {
	    
    /**
	 * Validator for the page path
	 * @var AbstractRecord
	 */
    protected $pathValidator = null;
        
	/**
	 * Constructor for create product filter class
	 */
    public function __construct() {
    	parent::__construct();        
    }
    
    /**
     * Set the path validator
     * 
     * @param AbstractRecord $pathValidator
     * @return CreateFilter
     */
    public function setPathValidator(AbstractRecord $pathValidator) {
    	$this->pathValidator = $pathValidator;
    	return $this;
    }
    
    /**
     * Get the path validator
     * 
     * @return AbstractRecord
     */
    public function getPathValidator() {
    	return $this->pathValidator;
    }
    
    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	
    	$this->add(array(
                'name' => 'uid',
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits',
                    ),
                ),
            ));
    	
    	$this->add(array(
                'name' => 'title',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a Title.'
                    		)
                    	)
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^.{1,255}$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid Title, please re-enter'
                    		)
                    	)
                    ),
                ),
            ));
            
		$this->add(array(
                'name' => 'path',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a Path.'
                    		)
                    	)
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^[a-z0-9\-_\.]{1,255}$/",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'The URL must contain only lowercase alphanumeric characters, dashes, underscores or full-stops.'
                    		)
                    	)
                    ),
                    $this->pathValidator
                ),
            ));
           
          $this->add(array(
                'name' => 'description',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a description.'
                    		)
                    	)
                    )
                ),
            ));
            
          $this->add(array(
                'name' => 'price',
                'required' => true,
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    	'options' => array(
                    		'messages' => array (
                    			\Zend\Validator\NotEmpty::IS_EMPTY => 'Please enter a price.'
                    		)
                    	)
                    ),
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^[0-9]{0,8}(\.[0-9]{0,2})?$/",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Please enter a valid price.'
                    		)
                    	)
                    ),
                ),
            ));
            
           	$this->getFilter()->get('uid')->setRequired(false);
        // common filters
        // TODO: change to use: $this->addCreatedByFilter(); ?...
        //$this->add($this->getCreatedByFilter());
        //$this->add($this->getLastEditiedByFilter());
        //$this->add($this->getActiveFilter());
    }
}