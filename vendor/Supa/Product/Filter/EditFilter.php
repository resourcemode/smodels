<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product\Filter;

use Supa\AbstractFilter;
use Supa\Product\Filter\EditFilter;
use Supa\Product\Validator\AbstractRecord;

/**
 * Filter class for editing Products
 */
class EditFilter extends \Supa\Product\Filter\CreateFilter {
    
    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    }
}