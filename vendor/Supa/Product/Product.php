<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Product;

use Supa\User\User;

/**
 * Product class
 */
class Product implements \Supa\EntityInterface {
	
	/* @var $uid int */
	protected $uid;
	
	/* @var $active int */
	protected $active;
	
	/* @var $title string */
	protected $title;
	
	/* @var $path string */
	protected $path;
	
	/**
	 * The Product description
	 * @var string
	 */
	protected $description;
	
	/**
	 * User who created product
	 * @var User|int|null
	 */
	protected $createdBy;
	
	/**
	 * User who last editied product
	 * @var User|int|null
	 */
	protected $lastEditiedBy;
	
	/**
	 * Timestamp of when Category was last editied
	 * @var string
	 */
	protected $lastEditied;
	
	/**
	 * Timestamp of when Category was created
	 * @var string
	 */
	protected $created;
	
	/**
	 * The products categories
	 * @var \ArrayIterator
	 */
	protected $categories;
	
	/**
	 * Product attributes
	 * @var \ArrayIterator
	 */
	protected $attributes;
	
	/**
	 * Product images
	 * @var \ArrayIterator
	 */
	protected $images;
	
	/**
	 * Product default price
	 * @var float
	 */
	protected $price;
	
	/**
	 * Featured flag
	 * @var bool
	 */
	protected $featured;

	/**
	 * Constructor for product class
	 *
	 * @param array $data Product data
	 */
	public function __construct($data) {
		$this->assignClassVariables($data);
	}
	
	/**
	 * Determine if product is active
	 *
	 * @return bool True if product active, false otherwise
	 */
	public function isActive() {
		return (bool)$this->active;
	}
	
	/**
	 * Determine if product is featured
	 * 
	 * @return bool True if product 'featured', false otherwise
	 */
	public function isFeatured() {
	    return (bool)$this->featured;
	}
	
	/**
	 * Get the product title
	 * 
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * Get the product uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return $this->uid;
	}
	
	/**
	 * Set the product uid
	 * 
	 * @param int $uid
     * @return EntityInterface
	 */
	public function setUID($uid) {
		$this->uid = (int)$uid;
		return $this;
	}
	
	/**
	 * Get the product path
	 * 
	 * @return string 
	 */	
	public function getPath() {
		return $this->path;
	}
	
	/**
	 * Get the product description
	 * 
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * Set the product description
	 * 
	 * @param string $desc
	 */
	public function setDescription($desc) {
		$this->description = $desc;
	}
	
	/**
	 * Set the product title
	 * 
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Get the product url path
	 * 
	 * @return string 
	 */
	public function setPath($path) {
		$this->path = $path;
	}
	
	/**
	 * Set the uid of user who last editied this user
	 * 
	 * @param int $uid
	 */
	public function setLastEditiedBy($uid) {
		$this->lastEditiedBy = $uid;
	}

	/**
	 * Set the active state for the product
	 * 1 = active, 0 = not active
	 * 
	 * @param int $state A 1 (active) or 0 (not-active)
	 */
	public function setActiveState($state) {
		$this->active = (int)$state;
	}
	
	/**
	 * Get the uid of user who last editied this Product
	 * 
	 * @return \Supa\User\User
	 */
	public function getLastEditiedBy() {
		return \Supa\lazy_load_user($this->lastEditiedBy, 'UserMapper', 'loadByUid');
	}
	
	/**
	 * Get the timestamp of when this product was created
	 * 
	 * @return string
	 */	
	public function getCreated() {
		return $this->created;
	}
	
	/**
	 * Set the timestamp of when this product was created
	 * 
	 * @param string $strFormattedTime
	 */	
	public function setCreated($strFormattedTime) {
		$this->created = $strFormattedTime;
	}

	/**
	 * Get the user who created this product
	 * 
	 * @return \Supa\User\User
	 */
	public function getCreatedBy() {
		return \Supa\lazy_load_user($this->createdBy);
	}

	/**
	 * set the uid of user who created this product
	 * 
	 * @param int
	 */
	public function setCreatedBy($userId) {
		$this->createdBy = $userId;
	}
	
	/**
	 * Get timestamp of when product user was last editied
	 * 
	 * @return string|null
	 */
	public function getLastEditied() {
		return $this->lastEditied;
	}

	/**
	 * Set timestamp of when this product was last editied
	 * 
	 * @param string $lastEditiedTime
	 */
	public function setLastEditied($lastEditiedTime) {
		$this->lastEditied = $lastEditiedTime;
	}

	/**
	 * Set product attributes for this product
	 * 
	 * @param \ArrayIterator|int $attributes
	 */
	public function setAttributes($attributes) {
	    $this->attributes = $attributes;
	}

	/**
	 * Get all product attributes
	 * 
	 * @return \ArrayIterator
	 */
	public function getAttributes() {
		if(!($this->attributes instanceof \ArrayIterator)) {
			$this->attributes = lazy_load_by_mapper2($this, 'ProductAttributeMapper', 'getProductAttributes');
		}
		return $this->attributes;
	}
	
	/**
	 * Get the product's categories
	 * 
	 * @return \ArrayIterator
	 */
	public function getCategories() {
		if(!($this->categories instanceof \ArrayIterator))  {
			$this->categories = lazy_load_by_mapper2($this, 'CategoryMapper', 'loadProductCategories');
		}
		return $this->categories;
	}
	
	/**
	 * Set the product's categories
	 * 
	 * @param \ArrayIterator|int $categories
	 */
	public function setCategories($categories) {
		$this->categories = $categories;
	}
	
	/**
	 * Set default price
	 * 
	 * @param string|int|float $price
	 */
	public function setPrice($price) {
	    $this->price = (float)$price;
	}
	
	/**
	 * Get default price
	 * 
	 * @return float Default price for product
	 */
	public function getPrice() {
	    return (float)$this->price;
	}
	
	/**
	 * Get product images
	 * 
	 * @return \ArrayIterator
	 */
	public function getImages() {
		if(!($this->images instanceof \ArrayIterator)) {
			$this->images = lazy_load_by_mapper2($this, 'ImageMapper', 'loadProductImages');
		}
		return $this->images;
	}
	
	public function setFeatured($bool) {
	    $this->featured = $bool;
	}
	
	/**
	 * Set images for the product
	 * 
	 * @param \ArrayIterator|int $images
	 */
	public function setImages($images) {
		$this->images = $images;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
		// set as uid so we can lazy load
		$this->categories = $this->attributes = $this->images = (int)$this->uid;
	}
	
	public function toArray() {
	    return get_object_vars($this);
	}
}