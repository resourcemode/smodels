<?php

namespace Supa;

require_once('lazy_load_funcs.php');

use \Supa\MysqlDatabase;

// user
use \Supa\User\Mapper as UserMapper;
use \Supa\User\Service as UserService;
use \Supa\User\Filter\Edit as UserEditFilter;
use \Supa\User\Validator\NoRecordExists as NoUserRecordExists;
use \Supa\User\Filter\Create as UserCreateFilter;

// customer
use \Supa\User\Customer\Mapper as CustomerMapper;
use \Supa\User\Customer\Service as CustomerService;

// page
use \Supa\Page\Service as PageService;
use \Supa\Page\Mapper as PageMapper;
use \Supa\Page\Validator\NoRecordExists as NoPageRecordExists;
use \Supa\Page\Filter\CreateFilter as PageCreateFilter;
use \Supa\Page\Filter\EditFilter as PageEditFilter;

// product
use \Supa\Product\Service as ProductService;
use \Supa\Product\Mapper as ProductMapper;
use \Supa\Product\Validator\NoRecordExists as NoProductRecordExists;
use \Supa\Product\Filter\CreateFilter as ProductCreateFilter;
use \Supa\Product\Filter\EditFilter as ProductEditFilter;

// product attribute mapper
use \Supa\Product\Attribute\Service as ProductAttributeService;
use \Supa\Product\Attribute\Mapper as ProductAttributeMapper;
use \Supa\Product\Attribute\Validator\NoRecordExists as NoProdAttrRecordExists;
use \Supa\Product\Attribute\Filter\CreateFilter as ProductAttributeCreateFilter;
use \Supa\Product\Attribute\Filter\EditFilter as ProductAttributeEditFilter;

// category
use \Supa\Category\Service as CategoryService;
use \Supa\Category\Filter\CreateFilter as CategoryCreateFilter;
use \Supa\Category\Filter\EditFilter as CategoryEditFilter;
use \Supa\Category\Validator\NoRecordExists as NoCategoryRecordExists;
use \Supa\Category\Mapper as CategoryMapper;

// image
use \Supa\Image\Service as ImageService;
use \Supa\Image\Mapper as ImageMapper;
use \Supa\Image\Filter\CreateFilter as ImageCreateFilter;

// discount codes
use \Supa\Product\DiscountCode\Service as DiscountCodeService;
use \Supa\Product\DiscountCode\Mapper as DiscountCodeMapper;
use \Supa\Product\DiscountCode\Validator\NoRecordExists as NoDiscountCodeRecordExists;
use \Supa\Product\DiscountCode\Filter\Create as DiscountCodeCreateFilter;
use \Supa\Product\DiscountCode\Filter\Edit as DiscountCodeEditFilter;
use \Supa\Product\DiscountCode\DiscountCode;

// comments
use \Supa\Comment\Service as CommentService;
use \Supa\Comment\Mapper as CommentMapper;
use \Supa\Comment\Filter\Filter as CommentCreateFilter;
use \Supa\Comment\Filter\EditFilter as CommentEditFilter;
use \Supa\Comment\Comment;

// review
use \Supa\Comment\Review\Service as ReviewService;
use \Supa\Comment\Review\Mapper as ReviewMapper;
use \Supa\Comment\Review\Filter\Filter as ReviewCreateFilter;
use \Supa\Comment\Review\Filter\EditFilter as ReviewEditFilter;
use \Supa\Comment\Review\Review;

// search
use \Supa\Search\Mapper as SearchMapper;
use \Supa\Search\Service as SearchService;

// rest
use Supa\Rest\Category\Service as RestCategoryService;
use Supa\Rest\Review\Service as RestReviewService;
use Supa\Rest\Product\Service as RestProductService;

// auth
use Supa\Auth\Service as SupaAuthService;

// basket
use Supa\Basket\Service as BasketService;
use Supa\Basket\Product\Mapper as BasketProductMapper;
use Supa\Basket\Session\Service as BasketSessionService;
use Supa\Basket\Session\Session as BasketSession;

class SupaServiceManager {

	public function getServices() {
	    
        return array(
            'abstract_factories' => array(),
            'aliases' => array(),
            'factories' => array(
                'DbAdapter' => function($sm) {
                    $config = array();
                    if(!MysqlDatabase::isConnected() && $sm->has('Config')) {
                        $config = $sm->get('Config');
                    }
                    return MysqlDatabase::getConn($config);
                },
                'CategoryMapper' => function($sm) {
                    return new CategoryMapper($sm->get('DbAdapter'));
                },
                'CategoryCreateFilter' => function ($sm) {
        			$mapper = $sm->get('CategoryMapper');
        			$filter = new CategoryCreateFilter();
        			$filter->setPathValidator(new NoCategoryRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'path'
        			    ))
        			);
        			return $filter;
                },
                'CategoryEditFilter' => function ($sm) {
        			$mapper = $sm->get('CategoryMapper');
        			$filter = new CategoryEditFilter();
        			$filter->setPathValidator(new NoCategoryRecordExists(array(
        					'mapper' =>  $mapper,
        					'key'    => 'path'
        			    ))
        			);
        			return $filter;
                },
        		'CategoryService' => function ($sm) {
        			$service = new CategoryService($sm->get('CategoryMapper'));
        			$service->setCreateFilter($sm->get('CategoryCreateFilter'));
        			$service->setEditFilter($sm->get('CategoryEditFilter'));
        			return $service;
                },
        		'ImageMapper' => function ($sm) {
        			$mapper = new ImageMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
               	'ImageCreateFilter' => function ($sm) {
        			$filter = new ImageCreateFilter();
        			return $filter;
                },
                'ImageService' => function ($sm) {
        			$service = new ImageService($sm->get('ImageMapper'));
        			$service->setCreateFilter($sm->get('ImageCreateFilter'));
        			return $service;
                },
        		'ProductMapper' => function ($sm) {
        			$mapper = new ProductMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
                'ProductCreateFilter' => function ($sm) {
                	$mapper = $sm->get('ProductMapper');
        			$filter = new ProductCreateFilter();
        			$filter->setPathValidator(new NoProductRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'path'
        			    ))
        			);
        			return $filter;
                },
                'ProductEditFilter' => function ($sm) {
                	$mapper = $sm->get('ProductMapper');
        			$filter = new ProductEditFilter();
        			$filter->setPathValidator(new NoProductRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'path'
        			    ))
        			);
        			return $filter;
                },
        		'ProductService' => function ($sm) {
        			$service = new ProductService($sm->get('ProductMapper'));
        			$service->setCreateFilter($sm->get('ProductCreateFilter'));
        			$service->setEditFilter($sm->get('ProductEditFilter'));
        			return $service;
                },
        		'ProductAttributeMapper' => function ($sm) {
        			$mapper = new ProductAttributeMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
                'ProductAttributeCreateFilter' => function ($sm) {
                	$mapper = $sm->get('ProductAttributeMapper');
        			$filter = new ProductAttributeCreateFilter();
        			$filter->setNameValidator(new NoProdAttrRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'name'
        			    ))
        			);
        			return $filter;
                },
                'ProductAttributeEditFilter' => function ($sm) {
                	$mapper = $sm->get('ProductAttributeMapper');
                	$filter = new ProductAttributeEditFilter();
        			$filter->setNameValidator(new NoProdAttrRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'name'
        			    ))
        			);
        			return $filter;
                },
        		'ProductAttributeService' => function ($sm) {
        			$service = new ProductAttributeService($sm->get('ProductAttributeMapper'));
        			$service->setCreateFilter($sm->get('ProductAttributeCreateFilter'));
        			$service->setEditFilter($sm->get('ProductAttributeEditFilter'));
        			return $service;
                },
        		'DiscountCodeMapper' => function ($sm) {
        			$mapper = new DiscountCodeMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
                'DiscountCodeEditFilter' => function ($sm) {
        			$mapper = $sm->get('DiscountCodeMapper');
        			$filter = new DiscountCodeEditFilter();
        			$filter->setDiscountCodeValidator(new NoDiscountCodeRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'code'
        			    ))
        			);
        			return $filter;
                },
                'DiscountCodeCreateFilter' => function ($sm) {
        			$mapper = $sm->get('DiscountCodeMapper');
        			$filter = new DiscountCodeCreateFilter();
        			$filter->setDiscountCodeTypes(array(DiscountCode::AMOUNT, DiscountCode::PERCENTAGE));
        			$filter->setDiscountCodeValidator(new NoDiscountCodeRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'code'
        			    ))
        			);
        			return $filter;
                },
                'DiscountCodeService' => function ($sm) {
        			$service = new DiscountCodeService($sm->get('DiscountCodeMapper'));
        			$service->setCreateFilter($sm->get('DiscountCodeCreateFilter'));
        			$service->setEditFilter($sm->get('DiscountCodeEditFilter'));
        			return $service;
                },
        		'PageMapper' => function ($sm) {
        			$mapper = new PageMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
                'PageEditFilter' => function ($sm) {
        			$mapper = $sm->get('PageMapper');
        			$filter = new PageEditFilter();
        			$filter->setPathValidator(new NoPageRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'path'
        			    ))
        			);
        			return $filter;
                },
                'PageCreateFilter' => function ($sm) {
        			$mapper = $sm->get('PageMapper');
        			$filter = new PageCreateFilter();
        			$filter->setPathValidator(new NoPageRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'path'
        			    ))
        			);
        			return $filter;
                },
        		'PageService' => function ($sm) {
        			$service = new PageService($sm->get('PageMapper'));
        			$service->setCreateFilter($sm->get('PageCreateFilter'));
        			$service->setEditFilter($sm->get('PageEditFilter'));
        			return $service;
                },
        		'CommentMapper' => function ($sm) {
        			$mapper = new CommentMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
        		'CommentService' => function ($sm) {
        			$service = new CommentService($sm->get('CommentMapper'));
        			$service->setCreateFilter($sm->get('CommentCreateFilter'));
        			$service->setEditFilter($sm->get('CommentEditFilter'));
        			return $service;
                },
                'CommentCreateFilter' => function ($sm) {
        			$filter = new CommentCreateFilter();
        			return $filter;
                },
                'CommentEditFilter' => function ($sm) {
        			$filter = new CommentEditFilter();
        			return $filter;
                },
        		'ReviewMapper' => function ($sm) {
        			$mapper = new ReviewMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
        		'ReviewService' => function ($sm) {
        			$service = new ReviewService($sm->get('ReviewMapper'));
        			$service->setCreateFilter($sm->get('ReviewCreateFilter'));
        			$service->setEditFilter($sm->get('ReviewEditFilter'));
        			return $service;
                },
                'ReviewCreateFilter' => function ($sm) {
        			$filter = new ReviewCreateFilter();
        			return $filter;
                },
                'ReviewEditFilter' => function ($sm) {
        			$filter = new ReviewEditFilter();
        			return $filter;
                },
        		'UserMapper' => function ($sm) {
        			$mapper = new UserMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
                'UserEditFilter' => function ($sm) {
        			$filter = new UserEditFilter();
        			return $filter;
                },
                'UserCreateFilter' => function ($sm) {
        			$mapper = $sm->get('UserMapper');
        			$filter = new UserCreateFilter();
        			$filter->setEmailValidator(new NoUserRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'email'
        			    ))
        			);
        			$filter->setUserNameValidator(new NoUserRecordExists(array(
        					'mapper' =>  $mapper,
        			    	'key'    => 'username'
        			    ))
        			);
        			return $filter;
                },
        		'UserService' => function ($sm) {
        			$service = new UserService($sm->get('UserMapper'));
        			$service->setCreateFilter($sm->get('UserCreateFilter'));
        			$service->setEditFilter($sm->get('UserEditFilter'));
        			return $service;
                },
        		'CustomerMapper' => function ($sm) {
        			$mapper = new CustomerMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
        		'CustomerService' => function ($sm) {
        			$service = new CustomerService($sm->get('CustomerMapper'));
        			return $service;
                },
        		'SearchMapper' => function ($sm) {
        			$mapper = new SearchMapper($sm->get('DbAdapter'));
        			return $mapper;
                },
        		'SearchService' => function ($sm) {
        			$service = new SearchService($sm->get('SearchMapper'));
        			return $service;
                },
                'RestCategoryService' => function ($sm) {
                    $service = new RestCategoryService($sm->get('CategoryMapper'));
                    return $service;
                },
                'RestProductService' => function ($sm) {
                    $service = new RestProductService($sm->get('ProductMapper'));
                    return $service;
                },
                'RestReviewService' => function ($sm) {
                    $service = new RestReviewService($sm->get('ReviewMapper'));
                    return $service;
                },
                'SupaAuthService' => function ($sm) {
                    $service = new SupaAuthService();
                    $service->setUserService($sm->get('UserService'));
                    return $service;
                },
                'BasketProductMapper' => function ($sm) {
                    $mapper = new BasketProductMapper($sm->get('DbAdapter'));
                    return $mapper;
                },
                'BasketService' => function ($sm) {
                    $service = new BasketService();
                    $service->setBasketProductMapper($sm->get('BasketProductMapper'));
                    return $service;
                },
                'BasketSession' => function ($sm) {
                    $basketSession = new BasketSession();
                    return $basketSession;
                },
                'BasketSessionService' => function ($sm) {
                    $service = new BasketSessionService();
                    $service->setBasketSession($sm->get('BasketSession'));
                    return $service;
                },
            ),
            'invokables' => array(),
            'services' => array(),
            'shared' => array(),
        );
        
	}
}