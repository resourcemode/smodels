<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Search;

/**
 * Search collection class
 */
class Collection extends \ArrayIterator  {

	/**
	 * Constructor for Search collection class
	 * 
	 * @param array $array Array of objects implementing \Supa\EntityInterface
	 * @see \ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}