<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Search;

use Exception;
use PDO;
use Supa\Search\Collection as SearchCollection;

class Mapper extends \Supa\AbstractMapper {
    
	/**
	 * Constructor for Search mapper class
	 * 
	 * @param \PDO $db
	 */
	public function __construct($db) {
		parent::__construct($db);
	}
	
	/**
	 * Find Products' based on 'title' field
	 * 
	 * @param string $query The query string
	 * @return \ArrayIterator The search results
	 * @throws \Exception
	 */	
	public function findProducts($query) {
	    
	    $sql = 'SELECT *
                FROM `product`
                WHERE `title` LIKE :query
                LIMIT 0, 30';
		
	    $sth = $this->db->prepare($sql);
		$res = $sth->execute(array(':query'=>'%'.$query.'%'));
		
		if(false === $res) {
			throw new Exception('Error searching products: ' . $this->getDbError());
		}
		
		$searchCollection = new SearchCollection(array());
		$products = $sth->fetchAll(PDO::FETCH_ASSOC);
		if($products) {
		    foreach($products as $product) {
            	$searchCollection->append(new \Supa\Product\Product($product));
            }
		}
			
		return $searchCollection;
		
	}
}