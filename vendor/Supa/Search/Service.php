<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Search;

use Supa\Search\Mapper as SearchMapper;
use Supa\AbstractMapper;
use Supa\AbstractFilter;
use \Exception;
use \PDO;
use \ArrayIterator;

/**
 * Search service class
 */
class Service {
    
    /**
     * Data mapper for Search object
     * @var SearchMapper
     */
	protected $mapper;
	
	/**
	 * Array of registered messages
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by this class
	 * @var Exception
	 */
	protected $lastException = null;
	
	/**
	 * Constructor for page class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct($mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	    
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}

	/**
	 * Perform a product search
	 * 
	 * @param string $query The search term
	 * @return \ArrayIterator|false The result collection or false
	 * @throws Exception
	 */
	public function doProductSearch($query) {
		try {
		    
		    if(!preg_match('/^[a-z0-9\-_\. ]{1,255}$/i', $query)) {
		        throw new Exception('Invalid query string.');
		    }
		    
			$products = $this->mapper->findProducts($query);
		}catch(Exception $e) {
			$products = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $products;
	}
}