<?php

namespace Supa;

use PDO;

class MysqlDatabase {
	
	/* @var $instance \PDO */
    private static $instance;

    private function __construct() {}

    public static function getConn(array $config = array()) {
        if (!isset(self::$instance)) {
            
        	$driverOptions = array(
        	    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        	);
        	  	
            try {
                if(!empty($config) && isset($config['db']) && is_array($config['db'])) {
                    
                    $db = $config['db'];
                    if(!isset($db['dsn']) && !isset($db['username']) && !isset($db['password'])) {
                        throw new PDOException('Missing required db config keys');                     
                    }
                    
                    define('DB_DSN', $db['dsn']);
                    define('DB_USER', $db['username']);
                    define('DB_PASS', $db['password']);
                	
                }else {
                    if(!defined('DB_DSN') || !defined('DB_USER') || !defined('DB_PASS')) {
                		throw new PDOException('Some or all of DB_* constants not defined.');
                	}
                }
                
			    self::$instance = new PDO(DB_DSN, DB_USER, DB_PASS, $driverOptions);
			} catch (PDOException $e) {
			    echo 'Connection failed: ' . $e->getMessage();
			}
        }
        return self::$instance;
    }

    public static function isConnected() {
        return isset(self::$instance);
    }
    
    public function __clone() {
        trigger_error('__clone is not allowed.', E_USER_ERROR);
    }

    public function __wakeup() {
        trigger_error('__wakeup is not allowed.', E_USER_ERROR);
    }
}