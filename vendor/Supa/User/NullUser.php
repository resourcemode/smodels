<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */
namespace Supa\User;

/**
 * User class
 */
class NullUser extends \Supa\User\User {
	
    /**
     * Constructor for null user class
     * 
     * @param array $data User data
     */
	public function __construct(array $data = array()) {
		
	}
	
	/**
	 * Get the user uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return null;
	}
}