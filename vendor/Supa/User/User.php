<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */
namespace Supa\User;

use Supa\EntityInterface;

/**
 * User class
 */
class User implements EntityInterface {

	/* @var $uid int */
	protected $uid;
	
	/* @var $firstname string */
	protected $firstname;
	
	/* @var $surname string */
	protected $surname;
	
	/* @var $username string */
	protected $username;
	
	/* @var $password string */
	protected $password;
	
	/* @var $email string */
	protected $email;
	
	/* @var $created string */
	protected $created;
	
	/* @var $active int */
	protected $active;
	
    /**
     * Constructor for user class
     * 
     * @param array $data User data
     */
	public function __construct($data) {
       	$this->assignClassVariables($data);
	}

	/**
	 * Determine if user is active
	 * 
	 * @return bool True if user active, false other wise
	 */
	public function isActive() {
		return (bool)$this->active;
	}
	
	
	/**
	 * Get the users firstname
	 * 
	 * @return string 
	 */
	public function getFirstname() {
		return $this->firstname;
	}
	
	/**
	 * Get the users surname
	 * 
	 * @return string
	 */
	public function getSurname() {
		return $this->surname;
	}
	
	/**
	 * Get the users username
	 * 
	 * @return string
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 * Get the users password
	 * 
	 * @return string 
	 */
	public function getPassword() {
		return $this->password;
	}
	
	/**
	 * Get the timestamp this user was created
	 * 
	 * @return string 
	 */
	public function getCreated() {
	    return $this->created;
	}
	
	/**
	 * Get the user uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return $this->uid;
	}

	/**
	 * Set the user uid
	 * 
	 * @param unknown_type $uid
	 */
	public function setUid($uid) {
		$this->uid = $uid;
	}
	
	/**
	 * Get the users email
	 * 
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * Set the users email
	 * 
	 * @param string $email
	 */
	public function setEmail($email) {
	    $this->email = $email;
	}
	
	/**
	 * Set the users firstname
	 * 
	 * @param string $firstname
	 */
	public function setFirstname($firstname) {
	    $this->firstname = $firstname;
	}
	
	/**
	 * Set the users surname
	 * 
	 * @param string $surname
	 */
	public function setSurname($surname) {
	    $this->surname = $surname;
	}
	
	/**
	 * Set the users username
	 * 
	 * @param string $username
	 * @return void
	 */
	public function setUsername($username) {
	    $this->username = $username;
	}
	
	/**
	 * Set the users password
	 * 
	 * @param string $password
	 */
	public function setPassword($password) {
	    $this->password = $password;
	}

	/**
	 * Set the active state for the product
	 * 1 = active, 0 = not active
	 * 
	 * @param int $state A 1 (active) or 0 (not-active)
	 */
	public function setActiveState($state) {
		$this->active = $state;
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
	
	public function toArray() {
	    return get_object_vars($this);
	}
}