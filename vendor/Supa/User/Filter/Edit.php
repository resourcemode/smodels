<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\User\Filter;

use Supa\User\Validator\AbstractRecord;
use Supa\AbstractFilter;
use Zend\InputFilter\Factory;

/**
 * Filter class for editing a user
 */
class Edit extends \Supa\User\Filter\Create {
    
	/**
	 * Constructor for edit user filter class
	 */	
    public function __construct() {
    	parent::__construct();
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	parent::prepareFilter();
    	$this->getFilter()->get('uid')->setRequired(true);
    	$this->getFilter()->get('firstname')->setRequired(true);
    	$this->getFilter()->get('surname')->setRequired(true);
    	$this->getFilter()->get('username')->setRequired(false);
    	$this->getFilter()->get('email')->setRequired(false);
    	$this->getFilter()->get('password')->setRequired(false);
    	$this->getFilter()->get('passwordVerify')->setRequired(false);
    }
}