<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */


namespace Supa\User\Filter;

use Supa\User\Filter\CreateFilter;
use Supa\User\Validator\AbstractRecord;
use Supa\AbstractFilter;
use \Zend\InputFilter\Factory;

/**
 * Filter class for creating users
 */
class Create extends AbstractFilter {
	
	/**
	 * Validator for a username
	 * @var AbstractRecord
	 */
    protected $userNameValidator = null;
    
	/**
	 * Validator for a email address
	 * @var AbstractRecord
	 */
    protected $emailValidator = null;
    
    /**
	 * Constructor for create user filter class
	 */
    public function __construct() {
    	parent::__construct();
    }
    
    /**
     * Set the username validator
     * 
     * @param AbstractRecord $userNameValidator
     * @return CreateFilter
     */
    public function setUserNameValidator(AbstractRecord $userNameValidator) {
    	$this->userNameValidator = $userNameValidator;
    }
    
    /**
     * Set the email validator
     * 
     * @param AbstractRecord $emailValidator
     * @return CreateFilter
     */
    public function setEmailValidator(AbstractRecord $emailValidator) {
    	$this->emailValidator = $emailValidator;
    }
    
    /**
     * Get the email validator
     * 
     * @return AbstractRecord
     */
    public function getEmailValidator() {
    	return $this->emailValidator;
    }

    /**
     * Get the username validator
     * 
     * @return AbstractRecord
     */
    public function getUserNameValidator() {
    	return $this->userNameValidator;
    }

    /**
     * Add validators to the filter
     * 
     * @see AbstractFilter::prepareFilter()
     * @see \Zend\InputFilter\Factory
     * @return void
     */
    public function prepareFilter() {
    	$this->add(array(
                'name' => 'uid',
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'digits'
                    ),
                ),
            ));    
                 
    	$this->add(array(
                'name' => 'firstname',
    	        'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^[a-z\-' ]+$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid Firstname, please re-enter'
                    		)
                    	)
                    ),
                ),
            ));

         $this->add(array(
                'name' => 'surname',
                'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                    	'name' => 'regex',
                    	'options' => array(
                    		'pattern' => "/^[a-z\-' ]+$/i",
                    		'messages' => array (
                    			\Zend\Validator\Regex::NOT_MATCH => 'Invalid Surname, please re-enter'
                    		)
                    	)
                    ),
                ),
            ));
            
         $this->add(array(
                'name' => 'username',
                //'required' => true,
                'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
                    array(
                        'name' => 'string_length',
                        'options' => array(
                            'min' => 3,
                            'max' => 75
                        ),
                    ),
                    //$this->userNameValidator
                ),
            ));
        
        $this->add(array(
                'name' => 'email',
                //'required' => true,
                'filters'    => array(array('name' => 'StringTrim')),
                'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
                    array(
                        'name' => 'email_address',
                    ),
                    //$this->emailValidator
                ),
            ));
            
        $this->add(array(
            'name'       => 'password',
            //'required'   => true,
            'filters'    => array(array('name' => 'StringTrim')),
            'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
	                array(
	                    'name'    => 'StringLength',
	                    'options' => array(
	                        'min' => 6,
	                		'max' => 255
	                    ),
	                ),
            ),
        ));

        $this->add(array(
            'name'       => 'passwordVerify',
            //'required'   => true,
            'filters'    => array(array('name' => 'StringTrim')),
            'validators' => array(
                    array(
                        'name' => 'not_empty',
                    ),
                array(
                    'name'    => 'StringLength',
                    'options' => array(
                        'min' => 6,
                		'max' => 255
                    ),
                ),
                array(
                    'name'    => 'Identical',
                    'options' => array(
                        'token' => 'password',
                    ),
                ),
            ),
        ));
        
        $this->getFilter()->get('uid')->setRequired(false);
        
        if($this->userNameValidator instanceof \Zend\Validator\ValidatorInterface) {
        	$this->getFilter()->get('username')->getValidatorChain()->addValidator($this->userNameValidator);
        }
        
        if($this->emailValidator instanceof \Zend\Validator\ValidatorInterface) {
        	$this->getFilter()->get('email')->getValidatorChain()->addValidator($this->emailValidator);
        }
        
    }
}