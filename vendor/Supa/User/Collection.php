<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\User;

use Supa\User\User;

/**
 * User Collection class
 */
class Collection extends \ArrayIterator  {

	/**
	 * Constructor for user collection class
	 * 
	 * @param array $array Array of User objects
	 * @see User For Items in collection
	 * @see \ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}