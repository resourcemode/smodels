<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\User;

use \Exception;
use Supa\User\User;
use Supa\User\NullUser;
use Supa\User\Collection as UserCollection;
use PDO;

/**
 * User Mapper class
 */
class Mapper extends \Supa\AbstractMapper {
    
    /**
     * Constructor for user mapper class
     * 
     * @param PDO $db
     */	
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load a user by uid
	 * 
	 * @param int $uid
	 * @throws Exception
	 * @return User
	 */
	public function loadByUid($uid) {
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND `user`.`active` = 1 ';
		}
		
		$data = array();
		$uid = $this->db->quote($uid, PDO::PARAM_INT);	
		
		$sql = "SELECT `user`.`id` as uid, `user`.`username`, `user`.`firstname`, `user`.`surname`, `user`.`password`, `user`.`email`,
						`user`.`created`, `user`.`active`
				  FROM `user`
				 WHERE `user`.`id` = $uid
				   $activeCheckSQL
				 LIMIT 1";
		
		$statement = $this->query($sql);
		if(!$statement) {
	        throw new Exception('Error loading user by uid: ' . $this->getDbError());
		}
		
		$data = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$data) {
			$user = new NullUser();
		}else {
			$user = new User($data);	
		}
		
		return $user;
	}

	/**
	 * Load a user by username and password
	 * 
	 * @param string $username
	 * @param string $password
	 * @throws Exception
	 * @return User
	 */
	public function loadByUsernameAndPassword($username, $password) {
	
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND `user`.`active` = 1 ';
		}
		
		$username = $this->db->quote($username);
		$password = $this->db->quote($password);
		
	    $sql = "SELECT `user`.`id` as uid, `user`.`username`, `user`.`firstname`, `user`.`surname`, `user`.`password`, `user`.`email`,
						`user`.`created`, `user`.`active`
        	      FROM `user`
        	     WHERE (`user`.`username` = $username AND `user`.`password` = $password)
        	       $activeCheckSQL
        	     LIMIT 1";
        	       
	    $statement = $this->query($sql);
	
	    if(!$statement) {
	        throw new Exception('Error loading user by username and password: ' . $this->getDbError());
	    }
	    $data = $statement->fetch(PDO::FETCH_ASSOC);
	    if(!$data) {
	        throw new Exception('Failed loading User for given username and password: ' . $this->getDbError($statement));
	    }
	
	    $user = new User($data);
	
	    return $user;
	}
	
	/**
	 * Load a user by username
	 * 
	 * @param string $username
	 * @throws Exception
	 * @return User|null
	 */
	public function loadByUsername($username) {

		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND `user`.`active` = 1 ';
		}
		
		$username = $this->db->quote($username);
		
	    $sql = "SELECT `user`.`id` as uid, `user`.`username`, `user`.`firstname`, `user`.`surname`, `user`.`password`, `user`.`email`,
						`user`.`created`, `user`.`active`
        	      FROM `user`
        	     WHERE `user`.`username` = $username
        	     $activeCheckSQL
        	     LIMIT 1";
	
	    $statement = $this->query($sql);
	    if(!$statement) {
	        throw new Exception('Error when loading user by username: ' . $this->getDbError());
	    }
	    
	    $data = $statement->fetch(PDO::FETCH_ASSOC);
		$user = null;
	    if($data) {
	    	$user = new User($data);
	    }
	
	    return $user;
	}

	/**
	 * Load a user by email
	 * 
	 * @param string $email
	 * @throws Exception
	 * @return User
	 */
	public function loadByEmail($email) {

		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND `user`.`active` = 1 ';
		}
		
	    $sql = "SELECT `user`.`id` as uid, `user`.`username`, `user`.`firstname`, `user`.`surname`, `user`.`password`, `user`.`email`,
						`user`.`created`, `user`.`active`
        	      FROM `user`
        	     WHERE `user`.`email` = :email
        	     $activeCheckSQL
        	     LIMIT 1";
	
	    $statement = $this->query($sql, array(':email'=>$email));
	    if(!$statement) {
	        throw new Exception('Error when loading user by email: ' . $this->getDbError());
	    }
	    $data = $statement->fetch(PDO::FETCH_ASSOC);
	    $user = null;
	    if($data) {
	    	$user = new User($data);
	    }
	    return $user;
	}
	
	/**
	 * Create a user
	 * 
	 * @param User $user
	 * @throws Exception
	 * @return User
	 */
	public function createUser(User $user) {
		
		$insertData = array();
		$insertData[':firstname'] = $user->getFirstname();
		$insertData[':surname'] = $user->getSurname();
		$insertData[':username'] = $user->getUsername();
		$insertData[':password'] = $user->getPassword();
		$insertData[':email'] = $user->getEmail();
		
		$sql = "INSERT INTO `user`
						SET `firstname` = :firstname,
							`surname` = :surname,
							`username` = :username,
							`password` = :password,
							`email` = :email";
		
		$affected = $this->exec($sql, $insertData);
		if(0 === $affected || false === $affected) {
			throw new Exception('Error creating user: ' . $this->getDbError());
		}
		
		$uid = (int)$this->db->lastInsertId();
		if($uid <= 0) {
			throw new Exception("Last insert id return invalid when creating user: $uid");
		}
		$user->setUid($uid);
		$user->setActiveState(1);
		
		return $user;
	}
	
	/**
	 * Update a user
	 * 
	 * @param User $user
	 * @throws Exception
	 * @return User
	 */
	public function update(User $user) {
		
		$editData = array();
		$editData[':firstname'] = $user->getFirstname();
		$editData[':surname'] = $user->getSurname();
		$editData[':uid'] = (int)$user->getUID();
		
		$sql = 'UPDATE `user`
				   SET `firstname` = :firstname,
					   `surname` = :surname
				 WHERE `id` = :uid';
		
		$affected = $this->exec($sql, $editData);
		if(0 === $affected || false === $affected) {
			throw new Exception('Error updating user: ' . $this->getDbError());
		}
		return $user;
	}
	
	/**
	 * Update a users email
	 * 
	 * @param User $user
	 * @throws Exception
	 * @return User
	 */
	public function updateEmail(User $user) {
		
		$editData = array();
		$editData[':email'] = $user->getEmail();
		$editData[':uid'] = (int)$user->getUID();
		
		$sql = 'UPDATE `user`
				   SET `email` = :email
				 WHERE `id` = :uid';
		
		$affected = $this->exec($sql, $editData);
		if(0 === $affected || false === $affected) {
			throw new Exception('Error updating email ' . $this->getDbError());	    	
	    }				
		return $user;
	}
	
	/**
	 * Update a users username
	 * 
	 * @param User $user
	 * @throws Exception
	 * @return User
	 */
	public function updateUsername(User $user) {
		
		$editData = array();
		$editData[':username'] = $user->getUsername();
		$editData[':uid'] = (int)$user->getUID();
		
		$sql = 'UPDATE `user`
				   SET `username` = :username
				 WHERE `id` = :uid';
		
		$affected = $this->exec($sql, $editData);
		if(0 === $affected || false === $affected) {
			throw new Exception('Error updating username ' . $this->getDbError());	    	
	    }				
		return $user;
	}
	
	/**
	 * Update a users password
	 * 
	 * @param User $user
	 * @throws Exception
	 * @return User
	 */
	public function updatePassword(User $user) {
		
		$editData = array();
		$editData[':password'] = $user->getPassword();
		$editData[':uid'] = (int)$user->getUID();
		
		$sql = 'UPDATE `user`
				   SET `password` = :password
				 WHERE `id` = :uid';
		
	    $affected = $this->exec($sql, $editData);
	    if(0 === $affected || false === $affected) {
			throw new Exception('Error updating password ' . $this->getDbError());    	
	    }	
		return $user;
	}

	/**
	 * Load users
	 * 
	 * @throws Exception
	 * @return \ArrayIterator
	 */
	public function loadUsers() {
	    
	    $sql = 'SELECT `user`.`id` as uid, `user`.`username`, `user`.`firstname`, `user`.`surname`, `user`.`password`, `user`.`email`,
						`user`.`created`, `user`.`active`
				FROM `user`
				LIMIT 10';
	    
	    $statement = $this->db->query($sql);
		if(!$statement) {
			throw new Exception('Error loading users: ' . $this->getDbError());
		}
		
		$users = $statement->fetchAll(PDO::FETCH_ASSOC);
		
		$userCollection = new UserCollection(array());
				
		if($users) {
		    foreach($users as $user) {
    			$userCollection->append(new User($user));
    		}
		}
						
		return $userCollection;
		
	}

	/**
	 * Delete a user
	 * 
	 * @param \Supa\User\User $user
	 * @throws \Exception
	 * @return bool True on success, exception thrown otherwise
	 */
	public function deleteUser(\Supa\User\User $user) {
		
		$uid = (int)$user->getUID();
		
		$sql = "DELETE FROM `user` WHERE `id` = :uid LIMIT 1";
		
		$affected = $this->exec($sql, array(':uid'=>$uid));
		
		if(0 === $affected || false === $affected) {
			throw new Exception('User not deleted: ' . $this->getDbError());
		}
		
		return true;
		
	}
}