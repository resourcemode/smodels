<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\User;

use Supa\User\User;
use Zend\Crypt\Password\Bcrypt;
use Exception;

/**
 * UserService class
 * 
 */
class Service {

	/**
	 * Data mapper for User object
	 * @var \Supa\AbstractMapper
	 */
	protected $mapper;
	
	/**
	 * Holds success or error messages 
	 * from performed operations
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by service class
	 * @var \Exception
	 */
	protected $lastException;
	
	/**
	 * Filter for creating a User
	 * @var \Supa\AbstractFilter
	 */
	protected $createFilter;
		
	/**
	 * Filter for editing a User
	 * @var \Supa\AbstractFilter
	 */
	protected $editFilter;
	
	/**
	 * Constructor for User service class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Get a User by uid
	 * 
	 * @param int $uid The uid of the user
	 * @return \Supa\User\User|null
	 */
	public function getByUid($uid) {
		try {
			$user = $this->mapper->loadByUid($uid);
		}catch(Exception $e) {
			$user = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $user;
	}
	
	/**
	 * Get a User by username
	 * 
	 * @param string $username The user's username
	 * @return \Supa\User\User|null
	 */
	public function getUserByUsername($username) {
	    try {
	        $user = $this->mapper->loadByUsername($username);
	    }catch(Exception $e) {
	        $user = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $user;
	}
	
	/**
	 * Get a user by username and password
	 * 
	 * @param string $username The user's username
	 * @param string $password The user's password
	 * @return \Supa\User\User|null
	 */
	public function getUserByUsernameAndPassword($username, $password) {
	    try {
	    	
	    	// Cycle through the configured identity sources and test each
	        //$fields = $this->getOptions()->getAuthIdentityFields();
	        //while ( !is_object($userObject) && count($fields) > 0 ) {
	            //$mode = array_shift($fields);
	        $mode = 'username';
            switch($mode) {
                case 'username':
                    $user = $this->mapper->loadByUsername($username);
                    break;
            }
	        //}
	        
            if(is_null($user)) {
	            throw new Exception('Invalid credentials when loading user');         
            }
            
            $bcrypt = new Bcrypt();
	        if (!$bcrypt->verify($password, $user->getPassword())) {
	            throw new Exception('Invalid credentials when loading user');
	        }
	        
	    }catch(Exception $e) {
	        $user = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $user;
	}
	
	/**
	 * Get a user by email address
	 * 
	 * @param string $email The user's email address
	 * @return \Supa\User\User|null
	 */	
	public function getUserByEmail($email) {
	    try {
	        $user = $this->mapper->loadByEmail($email);
	    }catch(Exception $e) {
	        $user = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $user;
	}
	
	/**
	 * Create a user
	 * 
	 * @param array $data The new user data
	 * @return \Supa\User\User|null
	 */
	public function createUser(array $data = array()) {
	    
		try {

			$filter = $this->createFilter;//getServiceLocator()->get('UserCreateFilter');
			$filter->prepareFilter();
			$filter->setData($data);
			
			if(!$filter->isValid()) {
				$this->messages = $filter->getFlatMessages();
				throw new Exception('Invalid user data given when creating user');
			}
			
			$newUser = new User($data);
	        $bcrypt = new Bcrypt();
	        $newUser->setPassword($bcrypt->create($newUser->getPassword()));
	        $newUser = $this->mapper->createUser($newUser);
	    }catch(Exception $e) {
	        $newUser = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $newUser;
	}
	
	/**
	 * Edit a user
	 * 
	 * @param array $data The new data for the user
	 * @return \Supa\User\User|null
	 */	
	public function updateUser(array $data = array()) {
	    try {

			$filter = $this->editFilter; //getServiceLocator()->get('UserEditFilter');
			$filter->prepareFilter();
			$filter->setData($data);
			
			if(!$filter->isValid()) {
				$this->messages = $filter->getFlatMessages();
				throw new Exception('User data is not valid when trying to update user');
			}
	    	
	        $user = new User($data);
	        $user->setFirstname($data['firstname']);
	        $user->setSurname($data['surname']);
	        $user = $this->mapper->update($user);
	        
	    }catch(Exception $e) {
	        $user = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $user;
	}
	
	// TODO: Maybe pull current user from session?
	/**
	 * Change a user's password
	 * 
	 * @param array $data Array of data with the keys:
	 * 					  'uid' = uid of user
	 * 					  'credential' = current password
	 * 					  'newCredential' = new password
	 * @return \Supa\User\User|null
	 */	
    public function changePassword(array $data = array()) {
    	
		$currentUser = $this->getByUid($data['uid']);  	
		
		try {   	
			if(is_null($currentUser)) {
				throw new Exception('Invalid Current User');
			}
			
	    	$oldPass = $data['credential'];
	        $newPass = $data['newCredential'];
	        
	        $bcrypt = new Bcrypt();
	        if (!$bcrypt->verify($oldPass, $currentUser->getPassword())) {
	            throw new Exception('Invalid credentials when updating password');
	        }

        	$pass = $bcrypt->create($newPass);
        	$currentUser->setPassword($pass);
        	$currentUser = $this->mapper->updatePassword($currentUser);
		}catch(Exception $e) {
			$currentUser = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}
        return $currentUser;
    }

	// TODO: Maybe pull current user from session/locator?
    public function changeEmail(array $data) {
		
		$currentUser = $this->getByUid($data['uid']);
		
		try {
			
			if(is_null($currentUser)) {
				throw new Exception('Invalid Current User');
			}
		
			$email = trim($data['newIdentity']);
			if($this->getUserByEmail($email)) {
				throw new Exception('Email already taken, please enter another email');
			}
			
	        $bcrypt = new Bcrypt();
	        if (!$bcrypt->verify($data['credential'], $currentUser->getPassword())) {
	            throw new Exception('Invalid credentials when updating email');
	        }
	
	        $currentUser->setEmail($data['newIdentity']);
        

        	$currentUser = $this->mapper->updateEmail($currentUser);
		}catch(Exception $e) {
			$currentUser = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}

        return $currentUser;
    }
    
    public function changeUsername(array $data) {

		$currentUser = $this->getByUid($data['uid']);
		
		try {		
			if(is_null($currentUser)) {
				throw new Exception('Invalid Current User.');
			}
			
			$username = trim($data['username']);
			if($this->getUserByUsername($username)) {
				throw new Exception('Username already taken, please enter another username');
			}
			
	        $bcrypt = new Bcrypt();
	
	        if (!$bcrypt->verify($data['credential'], $currentUser->getPassword())) {
	            throw new Exception('Invalid credentials');
	        }
	
	        $currentUser->setUsername($username);
        

        	$currentUser = $this->mapper->updateUsername($currentUser);
		}catch(Exception $e) {
			$currentUser = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
		}

        return $currentUser;
    }
	
	/**
	 * Get users
	 * 
	 * @return \ArrayIterator The users
	 */
	public function getUsers() {
		try {
			$users = $this->mapper->loadUsers();
		}catch(Exception $e) {
			$users = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $users;
	}

	/**
	 * Delete a user
	 * 
	 * @param int $userUid
	 * @return bool True on success, false otherwise
	 */
	public function deleteUser($userUid) {
		try {
			$user = $this->getByUid($userUid);
			if(is_null($user)) throw new Exception('Failed getting user for deletion');
			$deleted = $this->mapper->deleteUser($user);
		}catch(Exception $e) {
			$deleted = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $deleted;
	}
    
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	    
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
	
	/**
	 * Set the filter for editing a User
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setEditFilter(\Supa\AbstractFilter $filter) {
    	$this->editFilter = $filter;
    }
    
	/**
	 * Set the filter for creating a User
	 * 
	 * @param \Supa\AbstractFilter $filter
	 */
    public function setCreateFilter(\Supa\AbstractFilter $filter) {
    	$this->createFilter = $filter;
    }
}