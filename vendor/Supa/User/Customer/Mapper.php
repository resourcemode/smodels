<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\User\Customer;

use Supa\User\Customer\Customer;

/**
 * Customer Mapper class
 */
class Mapper extends \Supa\User\Mapper {
    
    /**
     * Constructor for Customer mapper class
     * 
     * @param \PDO $db
     */	
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load a Customer by uid
	 * 
	 * @param int $uid
	 * @throws \Exception
	 * @return \Supa\User\Customer
	 */
	public function loadByUid($uid) {
		
		$activeCheckSQL = '';
		if($this->isActiveOnly()) {
			$activeCheckSQL = ' AND `user`.`active` = 1 ';
		}
		
		$data = array();
		$uid = $this->db->quote($uid, \PDO::PARAM_INT);	
		
		$sql = "SELECT `user`.`id` as uid, `user`.`username`, `user`.`firstname`, `user`.`surname`, `user`.`password`, `user`.`email`,
						`user`.`created`, `user`.`active`, `customer`.`phone`
				  FROM `user`
			INNER JOIN `customer` ON `customer`.`user_id` = `user`.`id`
				 WHERE `user`.`id` = $uid
				   $activeCheckSQL
				 LIMIT 1";
		
		$statement = $this->query($sql);
		if(!$statement) {
	        throw new Exception('Error loading customer by uid: ' . $this->getDbError());
		}
		
		$data = $statement->fetch(\PDO::FETCH_ASSOC);
		if(!$data) {
			$customer = new NullUser();
		}else {
			$customer = new Customer($data);	
		}
		
		return $customer;
	}
}