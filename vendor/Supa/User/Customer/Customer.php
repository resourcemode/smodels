<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */
namespace Supa\User\Customer;

use Supa\EntityInterface;

/**
 * Customer class
 */
class Customer extends \Supa\User\User {
    
    protected $phone;
    
    /**
     * Constructor for Customer class
     * 
     * @param array $data Customer data
     */
	public function __construct($data) {
	    parent::__construct($data);
	}
	
	/**
	 * Get phone number
	 * 
	 * @return string Customer phone number
	 */
	public function getPhone() {
	    return $this->phone;
	}
	
	/**
	 * Set phone number
	 * 
	 * @param string $phone
	 */
	public function setPhone($phone) {
	    $this->phone = $phone;
	}
}