<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\User\Customer;

use Supa\User\Customer\Customer;

/**
 * Customer service class
 * 
 */
class Service extends \Supa\User\Service {
	
	/**
	 * Constructor for Customer service class
	 * 
	 * @param \Supa\AbstractMapper $mapper
	 */
	public function __construct(\Supa\AbstractMapper $mapper) {
		$this->mapper = $mapper;
	}
	
	/**
	 * Get a Customer by uid
	 * 
	 * @param int $uid The uid of the Customer
	 * @return \Supa\User\Customer\Customer|null
	 */
	public function getByUid($uid) {
		try {
			$customer = $this->mapper->loadByUid($uid);
		}catch(\Exception $e) {
			$customer = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $customer;
	}
}