<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\User\Customer;

/**
 * Customer Collection class
 */
class Collection extends \ArrayIterator  {

	/**
	 * Constructor for Customer collection class
	 * 
	 * @param array $array Array of Customer objects
	 * @see \Supa\User\Customer\Customer For Items in collection
	 * @see \ArrayIterator For parent class
	 */
	public function __construct($array) {
		parent::__construct($array);
	}

}