<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Basket\Session;

/**
 * Basket Session service class
 */
class Service {
    
    /**
     * Basket session object
     * @var \Supa\Basket\Session\Session
     */
    protected $basketSession;
    
	/**
	 * Array of registered messages
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by this class
	 * @var \Exception
	 */
	protected $lastException = null;
	
	/**
	 * Constructor for Basket Session service class
	 */
	public function __construct() {
	}
	
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	    
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('no last exception');
		}
		return $this->lastException;
	}
	
	public function addProductToBasket(\Supa\Product\Product $product, array $basketData = array()) {
	    
        $data = array();
        
        $data['product'] = $product;
        $data['price'] = $product->getPrice();
        $data['qty'] = $basketData['qty'];
        $data['vat'] = $basketData['vat'];
        $data['notes'] = $basketData['notes'];
        $data['title'] = $product->getTitle();
        
        $basketProduct = new \Supa\Basket\Product\Product($data);

        $this->basketSession->add($basketProduct);
        
        return $basketProduct;
	}
	
	public function removeProductFromBasket($productUid) {
        return $this->basketSession->removeProduct($productUid);	    
	}
	
	public function emptyBasket() {
	    $this->basketSession->init(false);
	}
	
	/**
	 * Set the session object for basket data
	 * 
	 * @param \Supa\Basket\Session\Session $session
	 */
	public function setBasketSession(\Supa\Basket\Session\Session $session) {
	    $this->basketSession = $session;
	}
	
	/**
	 * Get the basket session data
	 * 
	 * @return \Supa\Basket\Session\Session
	 */
	public function getBasketSession() {
	    return $this->basketSession;
	}
}