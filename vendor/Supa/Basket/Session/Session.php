<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Basket\Session;

/**
 * Helper class to handle basket session data.
 * We define a 'basket' namespace ($_SESSION['basket'])
 * and set up keys which point to arrays used 
 * to hold basket session data.
 */
class Session {

    /**
     * Constructor. Sets initial session keys
     * to use for basket session data
     * 
     * @see \Supa\Basket\Sesssion\Session::init()
     */
	public function __construct() {
	    //$this->init();
	}
	
	/**
	 * Helper to (re-)initialise session keys
	 * 
	 * @param bool $preserve Flag to preserve current session data (if any) true by default
	 * @link http://www.php.net/manual/en/class.sessionhandler.php
	 */
	public function init($preserve = true) {
	    
	    if($preserve) {
    	    if(!isset($_SESSION['basket'])) {
    	        $_SESSION['basket'] = array();
    	    }
    	    if(!isset($_SESSION['basket']['products'])) {
    	        $_SESSION['basket']['products'] = array();
    	    }
	    }else {
	        $_SESSION['basket'] = array();
	        $_SESSION['basket']['products'] = array();
	    }
	    
	}

	/**
	 * Add a BasketProduct to basket session data
	 * 
	 * @param \Supa\Basket\Product\Product $basketProduct
	 */
	public function add(\Supa\Basket\Product\Product $basketProduct) {
	    $this->set('products', $basketProduct);
	}
	
	/**
	 * Remove a Product from basket
	 * 
	 * @param int $uid The \Supa\Product\Product uid
	 * @return bool True on succes, false otherwise
	 */
	public function removeProduct($uid) {
	    $ret = false;
	    $products = $this->get('products');
	    $cnt = count($products);
	    if(count($cnt) > 0) {
	        for($i = 0; $i < $cnt; $i++) {
	            $basketProduct = $products[$i]; // BasketProduct object
	            $product = $basketProduct->getProduct(); // Product object
	            if($product->getUid() == $uid) {
	                unset($_SESSION['basket']['products'][$i]);
	                $ret = true;
	                $i = $cnt; // gracefully exit
	            }
	        }
	    }
	    return $ret;
	}
	
	/**
	 * Get current BasketProducts
	 * 
	 * @return array Current BasketProducts' stored in session
	 */
	public function getProducts() {
	    return $this->get('products');
	}
	
	/**
	 * Get the basket data as array
	 * 
	 * @return array The current session basket data
	 */
	public function getData() {
	    if(!isset($_SESSION['basket']) || !is_array($_SESSION['basket'])) {
	        $_SESSION['basket'] = array();
	    }
	    return $_SESSION['basket'];
	}
	
	/**
	 * Get a session basket variable
	 * 
	 * @param string $key
	 * @return array
	 */
	protected function get($key) {
	    
	    if(!isset($_SESSION['basket']) || !is_array($_SESSION['basket'])) {
	        $_SESSION['basket'] = array();
	    }
	    
	    if(!isset($_SESSION['basket'][$key])) {
	        $_SESSION['basket'][$key] = array();
	    }
	    
	    return $_SESSION['basket'][$key];
	}
	
	/**
	 * Set basket session data by key
	 * 
	 * @param string $key
	 * @param mixed $value
	 * @param bool $append Append to array or re-initialise array
	 */
	protected function set($key, $value, $append = true) {
	    
	    // get basket data from session based on $key
	    // e.g $_SESSION['basket'][$key]
	    $basketData = $this->get($key);
	    
	    if($append) {
	        $basketData[] = $value;
	    }else {
	        $basketData = array($value);
	    }
	    
	    $_SESSION['basket'][$key] = $basketData;
	}
}