<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Basket;

/**
 * Basket service class
 */
class Service {
    
    /**
     * Data mapper for basket product object
     * @var \Supa\AbstractMapper
     */
	protected $basketProductMapper;
	
	/**
	 * Array of registered messages
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by this class
	 * @var \Exception
	 */
	protected $lastException = null;
	
	/**
	 * Constructor for Basket service class
	 */
	public function __construct() {
	}
	
	/**
	 * Get basket product by uid
	 * 
	 * @param int $uid
	 * @return \Supa\Basket\Product\Product|null
	 */
	public function getBasketProductByUid($uid) {
		try {
			$basketProduct = $this->basketProductMapper->loadByUid($uid);
		}catch(\Exception $e) {
			$basketProduct = null;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $basketProduct;
	}
	
	public function setBasketProductMapper(\Supa\AbstractMapper $mapper) {
	    $this->basketProductMapper = $mapper;
	}
	
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	    
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
}