<?php

namespace Supa\Basket\Product;

class Factory {
    
	public static function makeBasketProduct(\Supa\Product\Product $product, array $basketData = array()) {
	    
	    // TODO: validate!
	    
        $data = array();
        
        $data['product'] = $product;
        $data['price'] = $product->getPrice();
        $data['qty'] = $basketData['qty'];
        $data['vat'] = $basketData['vat'];
        $data['notes'] = $basketData['notes'];
        $data['title'] = $product->getTitle();
        
        $basketProduct = new \Supa\Basket\Product\Product($data);
        
        return $basketProduct;
	}
	
}