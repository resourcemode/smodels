<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Basket\Product;

/**
 * BasketProduct class that represents a product
 * added to a basket.
 */
class Product {
    
	/**
	 * Unique ID
	 * 
	 * @var int
	 */
	protected $uid;
	
	/**
	 * The Product
	 * 
	 * @var \Supa\EntityInterface
	 */
	protected $product;
	
	/**
	 * Proce of product
	 * 
	 * @var float
	 */
	protected $price;
	
	/**
	 * Buy count of product
	 * 
	 * @var int
	 */
	protected $qty;
	
	/**
	 * VAT charge on product
	 * 
	 * @var int
	 */
	protected $vat;

	/**
	 * Product specific notes
	 * 
	 * @var string
	 */
	protected $notes;
	
	/**
	 * Title of product
	 * 
	 * @var int
	 */
	protected $title;
	
	/**
	 * Creation timestamp
	 * 
	 * @var string
	 */
	protected $created;
	
	/**
	 * Constructor
	 * 
	 * @param array $data
	 */
	public function __construct($data) {
	    $this->assignClassVariables($data);
	}
	
	protected function assignClassVariables(array $data = array()) {
		$vars = get_object_vars($this);
		foreach($data as $var => $value) {
			if(array_key_exists($var, $vars)) {
				$this->$var = $value;
			}
		}
	}
	
	/**
	 * Get the page uid
	 * 
	 * @return int
	 */
	public function getUID() {
		return $this->uid;
	}
	
    public function getProduct() {
        return \Supa\lazy_load_entity($this->product, 'ProductMapper');
    }
    
	/**
	 * Get buy price
	 * 
	 * @return float Buy price
	 */    
    public function getPrice() {
        return $this->price;
    }
    
	/**
	 * Get buy count
	 * 
	 * @return int Buy count
	 */  
    public function getQty() {
        return $this->qty;
    }
    
    /**
	 * Get vat amount
	 * 
	 * @return float
	 */ 
    public function getVat() {
        return $this->vat;
    }

    /**
	 * Get product notes
	 * 
	 * @return string
	 */ 
    public function getNotes() {
        return $this->notes;
    }

    /**
	 * Get product title
	 * 
	 * @return string
	 */ 
    public function getTitle() {
        return $this->title;
    }

	/**
	 * Creation timestamp
	 * 
	 * @return string
	 */	
    public function getCreated() {
        return $this->created;
    }

    /**
	 * Set the uid
	 * 
	 * @param int $uid
	 */
    public function setUid($uid) {
        $this->uid = $uid;
    }

    /**
     * Set added product
     * 
     * @param \Supa\Product\Product|int $product
     */
    public function setProduct($product) {
        $this->product = $product;
    }

    /**
     * Set basket product price
     * 
     * @param string|float $product
     */
    public function setPrice($price) {
        $this->price = $price;
    }
    
    /**
     * Set buy count
     * 
     * @param int $qty
     */
    public function setQty($qty) {
        $this->qty = $x;
    }
    
    /**
     * Set vat amount
     * 
     * @param string|float $vat
     */    
    public function setVat($vat) {
        $this->vat = $vat;
    }
    
    /**
     * Set note
     * 
     * @param string $note
     */
    public function setNotes($notes) {
        $this->notes = $notes;
    }
    
    /**
     * Set basket product title
     * 
     * @param string $title
     */ 
    public function setTitle($title) {
        $this->title = $title;
    }
    
	/**
	 * Set the timestamp of when this basket
	 * product was added
	 * 
	 * @param string $strFormattedTime
	 */
    public function setCreated($created) {
        $this->created = $created;
    }
    
	public function toArray() {
	    return get_object_vars($this);
	}
}