<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Basket\Product;

use Supa\Basket\Product\Product as BasketProduct;

class Mapper extends \Supa\AbstractMapper {
    
	/**
	 * Constructor for Basket Product mapper class
	 * 
	 * @param \PDO $db
	 */
	public function __construct($db) {
		parent::__construct($db);
	}

	/**
	 * Load Basket Product by uid
	 * 
	 * @param int $uid
	 * @throws \Exception
	 * @return \Supa\Basket\Product\Product
	 */
	public function loadByUid($uid) {
		
		$uid = (int)$uid;
		
		$sql = "SELECT basket_product.id as uid, basket_product.product, basket_product.title, basket_product.created,
						basket_product.notes, basket_product.price, basket_product.vat, basket_product.qty
				  FROM basket_product
				 WHERE basket_product.id = :uid
				 LIMIT 1";
		
		$statement = $this->query($sql, array(':uid'=>$uid));
		
		if(!$statement) {
			throw new \Exception('Error when loading basket product by uid: ' . $this->getDbError());
		}
		$data = $statement->fetch(\PDO::FETCH_ASSOC);
		if(!$data) {
			throw new \Exception('Failed fetching data when loading basket product for uid: ' . $this->getDbError($statement));
		}
		
		$basketProduct = new BasketProduct($data);
		
		return $basketProduct;
	}
}