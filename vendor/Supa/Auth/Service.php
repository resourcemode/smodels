<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

namespace Supa\Auth;

use Exception;

/**
 * Auth service class
 */
class Service {
    
    /**
     * User service
     * @var \Supa\User\Service
     */
	protected $userService;
	
	/**
	 * Array of registered messages
	 * @var array
	 */
	protected $messages = array();
	
	/**
	 * Last exception thrown by this class
	 * @var \Exception
	 */
	protected $lastException = null;
		
	/**
	 * Constructor for Auth service class
	 */
	public function __construct() {}
	
	/**
	 * Authenticate using a user's username and password
	 * 
	 * @param string $username The user's username
	 * @param string $password The user's password
	 * @return \Supa\User\User|false
	 * @see \Supa\User\Service
	 */	
	public function doLogin($username, $password) {

	    // note: we try and give some "security through obscurity" by giving
	    // vague messages
	    
		try {
		    
		    if(empty($username) || empty($password)) {
		        throw new Exception('The username/password given is not a valid combination.');
		    }
		    
			$user = $this->userService->getUserByUsernameAndPassword($username, $password);
			
			if(is_null($user)) {
		        throw new Exception('The username/password given is not a valid combination.');	    
			}
			
		}catch(Exception $e) {
			$user = false;
			$this->messages[] = $e->getMessage();
			$this->lastException = $e;
		}
		return $user;	    
	}

	/**
	 * Set service for dealing with users
	 * 
	 * @param \Supa\User\Service $userService
	 */
	public function setUserService(\Supa\User\Service $userService) {
	    $this->userService = $userService;
	}
    
	/**
	 * Get any class messages
	 * 
	 * @return array
	 */	    
	public function getMessages() {
		return $this->messages;
	}
	
	/**
	 * Get the last execption raised by this class
	 * 
	 * @return \Exception;
	 */
	public function getLastException() {
		if(!isset($this->lastException)) {
		    $this->lastException = new Exception('');
		}
		return $this->lastException;
	}
}