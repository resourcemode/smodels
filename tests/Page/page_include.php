<?php
require_once($root . '/vendor/Supa/Page/Validator/AbstractRecord.php');
require_once($root . '/vendor/Supa/Page/Validator/NoRecordExists.php');
require_once($root . '/vendor/Supa/Page/Validator/RecordExists.php');
require_once($root . '/vendor/Supa/Page/Filter/CreateFilter.php');
require_once($root . '/vendor/Supa/Page/Filter/EditFilter.php');
require_once($root . '/vendor/Supa/Page/Service.php');
require_once($root . '/vendor/Supa/Page/Mapper.php');
require_once($root . '/vendor/Supa/Page/Page.php');