<?php

/**
 * Page test suite
 */
class PageTestSuite extends PHPUnit_Framework_TestSuite
{

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('PageTestSuite' );
		$this->addTestSuite('PageTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self ( );
	}
}