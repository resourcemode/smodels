<?php

use Supa\Page\Mapper as PageMapper;
use Supa\Page\Service as PageService;
use Supa\User\Service as UserService;
use Supa\Page\Filter\CreateFilter as PageCreateFilter;
use Supa\Page\Page;

/**
 * @backupGlobals disabled
 */
class PageTest extends PHPUnit_Framework_TestCase {
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
    public function testCanLoadPage() {
    	
		/* @var $pageService PageService */
		$pageService = AllTests::getService('PageService');
		
		$page = $pageService->getByUid(1);
		if(!$page) {
			$this->fail($catService->getLastException()->getMessage());
		}
				
		$this->assertTrue(($page instanceof Page));
		$this->assertEquals(1, $page->getUID());
		$this->assertEquals('Test Page', $page->getTitle());
		$this->assertEquals('this-is-a-test-page', $page->getPath());
		$this->assertEquals('Some Content', $page->getContent());
		$this->assertTrue($page->isActive());	
    }
    
    public function testCanCreatePage() {
    	
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		/* @var $pageService PageService */
		$pageService = AllTests::getService('PageService');
		
		$data['title'] = 'The Page Title';
		$data['path'] = 'the-page-title';
		$data['content'] = 'The Content';
		
		$page = $pageService->createPage($data, $user);
		
		if(!$page) {
			var_dump($pageService->getMessages());
			$this->fail($pageService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($page instanceof Page));
		$this->assertEquals('The Page Title', $page->getTitle());
		$this->assertEquals('the-page-title', $page->getPath());
		$this->assertEquals('The Content', $page->getContent());
		$this->assertTrue($page->isActive());
    }
    
    public function testCanNotCreatePageWithNonUniquePath() {
    	
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		/* @var $pageService PageService */
		$pageService = AllTests::getService('PageService');
		
		$data['title'] = 'The Page Title';
		$data['path'] = 'this-is-a-test-page'; // already exists, inserted in test_data_inserts.sql
		$data['content'] = 'The Content';
		
		$page = $pageService->createPage($data, $user);
		
		if($page) {
			$this->fail('createPage returned a Page when it should\'nt');
		}
    }
    
    public function testCanEditPage() {
    	
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		/* @var $pageService PageService */
		$pageService = AllTests::getService('PageService');
		
		$page = $pageService->getByUid(1);
		$this->assertEquals('Test Page', $page->getTitle());
		$this->assertEquals('this-is-a-test-page', $page->getPath());
		$this->assertEquals('Some Content', $page->getContent());
		
		$data['title'] = 'Test Page Updated';
		$data['uid'] = 1;
		$data['content'] = 'Some Content Updated';
		$data['createdBy'] = 1;
		$data['active'] = 1;
		$data['path'] = 'this-is-a-test-page-updated';
		$page = $pageService->updatePage($data, $user);
		
		if(!$page) {
			echo $pageService->getLastException()->getMessage();
		}
		
		$this->assertTrue(($page instanceof Page));
		$this->assertEquals('Test Page Updated', $page->getTitle());
		$this->assertEquals('this-is-a-test-page-updated', $page->getPath());
		$this->assertEquals('Some Content Updated', $page->getContent());
    }
    
    public function testCanLoadPageByPath() {
    	
    	/* @var $pageService PageService */
		$pageService = AllTests::getService('PageService');
		
		$page = $pageService->getPageByPath('this-is-a-test-page');
		if(!$page) {
			echo $pageService->getLastException()->getMessage();
		}
		
		$this->assertTrue(($page instanceof Page));
		$this->assertEquals(1, $page->getUID());
    }
}