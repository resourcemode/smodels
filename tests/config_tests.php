<?php
// set path for PHPUnit and test classes
set_include_path(
	'C:\pear\pear\\\'' . PATH_SEPARATOR . 
	'C:/Apache2/htdocs/smodels/smodels/tests/' .
    get_include_path() . PATH_SEPARATOR
);

spl_autoload_register(function ($class) {
	$path = str_replace('_', '/', $class);
    require_once($path . '.php');
});