<?php

use Supa\Basket\Service as BasketService;
use Supa\Product\Service as ProductService;
use Supa\Product\Product;
use Supa\User\Service as UserService;

/**
 * @backupGlobals disabled
 */
class BasketTest extends PHPUnit_Framework_TestCase {
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile(APP_ROOT . '/sql/test/basket.sql');
	}
	
	// reporting / save basket feature / ...
    public function testCanLoadBasketProductFromDB() {
		/* @var $basketService \Supa\Basket\Service */
		$basketService = AllTests::getService('BasketService');
		
		$basketProduct = $basketService->getBasketProductByUid(1);
		
		if(!$basketProduct) {
		    var_dump($basketService->getMessages());
			$this->fail($basketService->getLastException()->getMessage());
		}
				
		$this->assertTrue(($basketProduct instanceof \Supa\Basket\Product\Product));
		$this->assertEquals(1, $basketProduct->getUID());
    }
    
    public function testCanInstantiateABasketProductClassAndCorrectlySetMembers() {
        
        $productService = AllTests::getService('ProductService');
        $product = $productService->getByUid(2); // added in basket.sql
        $this->assertTrue($product instanceof \Supa\Product\Product);
                
        $data = array();
        $data['product'] = $product;
        $data['price'] = $product->getPrice();
        $data['qty'] = 1;
        $data['vat'] = 20;
        $data['notes'] = 'Test Notes';
        $data['title'] = $product->getTitle();
        $basketProduct = new \Supa\Basket\Product\Product($data);
        
        $this->assertTrue($basketProduct instanceof \Supa\Basket\Product\Product);
        $this->assertTrue($basketProduct->getProduct() instanceof \Supa\Product\Product);
        $this->assertEquals(1, $basketProduct->getQty());
        $this->assertEquals(20, $basketProduct->getVat());
        $this->assertEquals('Test Notes', $basketProduct->getNotes());
        $this->assertEquals('Test Product 2', $basketProduct->getTitle());
        
    }
}