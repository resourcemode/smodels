<?php

/**
 * Basket test suite
 */
class BasketTestSuite extends PHPUnit_Framework_TestSuite {
    

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('BasketTestSuite' );
		$this->addTestSuite('BasketTest');
		$this->addTestSuite('BasketSessionTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self();
	}
}