<?php

use Supa\Basket\Session\Service as BasketSessionService;
use Supa\Product\Service;
use Supa\Product\Product;
use Supa\User\Service as UserService;

/**
 * @backupGlobals disabled
 */
class BasketSessionTest extends PHPUnit_Framework_TestCase {
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile(APP_ROOT . '/sql/test/basket.sql');
	}
	
    public function testCanAddProductToBasket() {
        
        $basketSessionService = AllTests::getService('BasketSessionService');
        $basketSessionService->emptyBasket();// clear session
        
        // 1. get Product
        $productService = AllTests::getService('ProductService');
        $product = $productService->getByUid(2); // added in basket.sql 
        $this->assertTrue($product instanceof \Supa\Product\Product);
        
        // 2. set BasketProduct data
        $data = array();
        $data['qty'] = 1;
        $data['vat'] = 20;
        $data['notes'] = 'Test Notes';
        
        // 3. add to BasketSession        
        $this->assertTrue($basketSessionService instanceof BasketSessionService);
        $basketProduct = $basketSessionService->addProductToBasket($product, $data);
        $this->assertTrue($basketProduct instanceof \Supa\Basket\Product\Product);
        
        //$basketSessionService = AllTests::getService('BasketSessionService');
        $basketSession = $basketSessionService->getBasketSession();
        $this->assertTrue($basketSession instanceof \Supa\Basket\Session\Session);
        $this->assertEquals(1, count($basketSession->getProducts()));
    }
    
    public function testCanRemoveProductFromBasket() {
        
        $basketSessionService = AllTests::getService('BasketSessionService');
        $this->assertTrue($basketSessionService instanceof BasketSessionService);
        $basketSessionService->emptyBasket();// clear session
        
        // 1. get Product
        $productService = AllTests::getService('ProductService');
        $product = $productService->getByUid(2); // added in basket.sql
        $product2 = $productService->getByUid(1); // added in basket.sql
        $this->assertTrue($product instanceof \Supa\Product\Product);         
        $this->assertTrue($product2 instanceof \Supa\Product\Product);
        
        // 2. set BasketProduct data
        $data = array();
        $data['qty'] = 1;
        $data['vat'] = 20;
        $data['notes'] = 'Test Notes';
        
        // 3. add to BasketSession        
        $basketProduct = $basketSessionService->addProductToBasket($product, $data);
        $this->assertTrue($basketProduct instanceof \Supa\Basket\Product\Product);
        
        $basketProduct2 = $basketSessionService->addProductToBasket($product2, $data);
        $this->assertTrue($basketProduct2 instanceof \Supa\Basket\Product\Product);
                
        // check addition
        $basketSessionService = AllTests::getService('BasketSessionService');
        $basketSession = $basketSessionService->getBasketSession();
        $this->assertTrue($basketSession instanceof \Supa\Basket\Session\Session);
        
        $this->assertEquals(2, count($basketSession->getProducts()));
        
        // remove
        $basketSessionService = AllTests::getService('BasketSessionService');        
        $res = $basketSessionService->removeProductFromBasket(2); // Product uid
        $this->assertTrue($res);
        
        // check
        $this->assertEquals(1, count($basketSession->getProducts()));
        
    }
    
    public function testCanGetAllProductsInBasket() {
        
        $basketSessionService = AllTests::getService('BasketSessionService');
        $this->assertTrue($basketSessionService instanceof BasketSessionService);
        $basketSessionService->emptyBasket(); // clear session

        $added = $this->addProductToBasketSession(2);
        $this->assertTrue($added);
        $added = $this->addProductToBasketSession(1);
        $this->assertTrue($added);
        
        $this->assertEquals(2, count($basketSessionService->getBasketSession()->getProducts()));
    }
    
    // helper method
    protected function addProductToBasketSession($productUid) {
        
        $basketSessionService = AllTests::getService('BasketSessionService');
        
        // 1. get Product
        $productService = AllTests::getService('ProductService');
        $product = $productService->getByUid($productUid); // added in basket.sql
                
        // 2. set BasketProduct data
        $data = array();
        $data['qty'] = 1;
        $data['vat'] = 20;
        $data['notes'] = 'Test Notes';
        
        // 3. add to BasketSession        
        $basketProduct = $basketSessionService->addProductToBasket($product, $data);
        
        return ($basketProduct instanceof \Supa\Basket\Product\Product);
    }
}