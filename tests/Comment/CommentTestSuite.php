<?php

/**
 * Comment test suite
 */
class CommentTestSuite extends PHPUnit_Framework_TestSuite {

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('CommentTestSuite' );
		$this->addTestSuite('CommentTest');
		$this->addTestSuite('ReviewTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self ( );
	}
}