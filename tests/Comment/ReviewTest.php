<?php

use Supa\Comment\Comment;
use Supa\Comment\Review\Review;
use Supa\Comment\Review\Service as ReviewService;
use Supa\User\Service as UserService;
use Supa\Product\Service as ProductService;

/**
 * @backupGlobals disabled
 */
class ReviewTest extends PHPUnit_Framework_TestCase {
	
	public $reviewService;
	protected $productService;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->reviewService = AllTests::getService('ReviewService');
		$this->userService = $userService = AllTests::getService('UserService');
		$this->productService = AllTests::getService('ProductService');
	}
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/user.sql');
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/product.sql');
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/review.sql');
	}
	
    public function testCanLoadCommentByUid() {
    	
		$review = $this->reviewService->getByUid(1);
		if(!$review) {
			var_dump($this->reviewService->getMessages());
			$this->fail('Failed loading review');
		}
		
		$this->assertTrue($review instanceof Review);
		$this->assertEquals('Review Content 1', $review->getContent());
		$this->assertEquals(1, $review->getCreatedBy()->getUID());
		$this->assertEquals('localhost', $review->getAddedFrom());
		$this->assertNotNull($review->getCreated());
		$this->assertFalse($review->isApproved());
		$this->assertEquals(0, $review->getScore());
		
		$this->assertEquals(1, $review->getUID());
		$this->assertEquals('Test Review', $review->getTitle());
		$this->assertEquals('1', $review->getHelpfulCount());
		$this->assertEquals('2.5', $review->getRating());
		
    }
    
    public function testCanCreateReviewWithValidData() {
    	
    	/* @var $user User */
		$user = $this->userService->getByUid(1);
		
		$data['content'] = 'New Review';
		$data['addedFrom'] = 'localhost';
		$data['title'] = 'Review Title';
		
		$review = $this->reviewService->createReview($data, $user);
		if(!$review) {
			$this->fail($this->reviewService->getLastException()->getMessage());
		}
		
		$this->assertTrue($review instanceof Review);
		$this->assertEquals('New Review', $review->getContent());
		$this->assertEquals(1, $review->getCreatedBy()->getUID());
		$this->assertEquals('localhost', $review->getAddedFrom());
		$this->assertNotNull($review->getCreated());
		$this->assertFalse($review->isApproved());
		$this->assertEquals(0, $review->getScore());
		$this->assertTrue($review->getUID() > 0);
		$this->assertEquals('0', $review->getHelpfulCount());
		$this->assertEquals('Review Title', $review->getTitle());
		$this->assertEquals('0.0', $review->getRating());
    }
        
//    public function testCanUpdateReview() {
//    	
//    	/* @var $user User */
//		$user = $this->userService->getByUid(1);
//		
//		$data['content'] = 'Updated Review';
//		$data['approved'] = 1;
//		$data['score'] = 1;
//		$data['uid'] = 1;
//		$data['title'] = 'New Foo Title';
//		$data['rating'] = 7.5;
//		$data['helpfulCount'] = 2;
//		
//		// check before
//		$review = $this->reviewService->getByUid(1);
//		if(!$review) {
//			var_dump($this->reviewService->getMessages());
//			$this->fail('Failed loading review');
//		}
//		$this->assertTrue($review instanceof Review);
//		$this->assertEquals('Review Content 1', $review->getContent());
//		$this->assertEquals(1, $review->getCreatedBy()->getUID());
//		$this->assertEquals('localhost', $review->getAddedFrom());
//		$this->assertNotNull($review->getCreated());
//		$this->assertFalse($review->isApproved());
//		$this->assertEquals(0, $review->getScore());
//		$this->assertEquals(1, $review->getUID());
//		$this->assertEquals('Test Review', $review->getTitle());
//		$this->assertEquals('1', $review->getHelpfulCount());
//		$this->assertEquals('2.5', $review->getRating());
//		
//		// update
//		$review = $this->reviewService->updateReview($data, $user);
//
//		if(!$review) {
//			var_dump($this->reviewService->getMessages());
//			$this->fail($this->reviewService->getLastException()->getMessage());
//		}
//		
//		// check after
//		$review = $this->reviewService->getByUid(1);
//		
//		if(!$review) {
//			var_dump($this->reviewService->getMessages());
//			$this->fail('Failed loading review');
//		}
//		$this->assertTrue($review instanceof Review);
//		$this->assertEquals('Updated Review', $review->getContent());
//		$this->assertEquals(1, $review->getCreatedBy()->getUID());
//		$this->assertEquals('localhost', $review->getAddedFrom());
//		$this->assertNotNull($review->getCreated());
//		$this->assertTrue($review->isApproved());
//		$this->assertEquals(1, $review->getScore());
//		$this->assertEquals(1, $review->getUID());
//		$this->assertEquals('New Foo Title', $review->getTitle());
//		$this->assertEquals('2', $review->getHelpfulCount());
//		
//		$this->assertEquals('7.5', $review->getRating(true));
//		$this->assertEquals(3.5, $review->getRating());
//		
//    }
    
    public function testCanUpdateRating() {

        $review = $this->reviewService->getByUid(1);
		if(!$review) {
			var_dump($this->reviewService->getMessages());
			$this->fail('Failed loading review when updating rating [1]');
		}
		
		$this->assertEquals(1, $review->getHelpfulCount());		
		$this->assertEquals(2.5, $review->getRating());
		
		try {
			$this->reviewService->rate(1, 2.5);
		}catch(Exception $e) {
			$this->fail("fail");
		}
		
        $review = $this->reviewService->getByUid(1);
        //var_dump($review);
		if(!$review) {
			var_dump($this->reviewService->getMessages());
			$this->fail('Failed loading review when updating rating [2]');
		}
		
		$this->assertEquals(2, $review->getHelpfulCount());
		$this->assertEquals(2.5, $review->getRating());
    }
    
    public function testCanGetReviewsForAProduct() {
                
        // use a test product that already has reviews linked to it as
        // we are only testing the retrieval of reviews linked to a product
        $product = $this->productService->getByUid(1);
        
        $reviews = $this->reviewService->getProductReviews($product);
        $this->assertTrue($reviews instanceof \Supa\Comment\Review\Collection);
        $this->assertEquals(1, $reviews->count());
        
    }
        
    public function testCanAddReviewForAProduct() {
        
        $product = $this->productService->getByUid(1);
		
		/* @var $user User */
		$user = $this->userService->getByUid(1);
		
		$data['content'] = 'New Review';
		$data['addedFrom'] = 'localhost';
		$data['title'] = 'Review Title';
		
		$review = $this->reviewService->createProductReview($data, $product, $user);
		if(!$review) {
			$this->fail($this->reviewService->getLastException()->getMessage());
		}
		
		$reviews = $this->reviewService->getProductReviews($product);
        $this->assertTrue($reviews instanceof \Supa\Comment\Review\Collection);
        $this->assertEquals(2, $reviews->count());
    }
        
    public function testCanGetAllProductReviews() {
        $reviews = $this->reviewService->getAllProductReviews();
        $this->assertTrue($reviews instanceof \Supa\Comment\Review\Collection);
        $this->assertEquals(1, $reviews->count());
    }
    
    public function testCallingGetReviewsForProductWithZeroReviewsReturnsEmptyCollection() {
                
        // use a test product that already has reviews linked to it as
        // we are only testing the retrieval of reviews linked to a product
        $product = $this->productService->getByUid(2);
        
        $reviews = $this->reviewService->getProductReviews($product);
        $this->assertTrue($reviews instanceof \Supa\Comment\Review\Collection);
        $this->assertEquals(0, $reviews->count());
        
    }
}