<?php
require_once($root . '/vendor/Supa/Comment/CommentInterface.php');
require_once($root . '/vendor/Supa/Comment/Comment.php');
require_once($root . '/vendor/Supa/Comment/Collection.php');
require_once($root . '/vendor/Supa/Comment/Service.php');
require_once($root . '/vendor/Supa/Comment/Mapper.php');
require_once($root . '/vendor/Supa/Comment/Filter/Filter.php');
require_once($root . '/vendor/Supa/Comment/Filter/EditFilter.php');
require_once($root . '/vendor/Supa/Comment/Review/Review.php');
require_once($root . '/vendor/Supa/Comment/Review/Collection.php');
require_once($root . '/vendor/Supa/Comment/Review/Service.php');
require_once($root . '/vendor/Supa/Comment/Review/Mapper.php');
require_once($root . '/vendor/Supa/Comment/Review/Filter/Filter.php');
require_once($root . '/vendor/Supa/Comment/Review/Filter/EditFilter.php');