<?php

use Supa\Comment\Comment;
use Supa\Comment\Service as CommentService;
use Supa\User\Service as UserService;
use Supa\User\User;

/**
 * @backupGlobals disabled
 */
class CommentTest extends PHPUnit_Framework_TestCase {
	
	public $commentService;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->commentService = AllTests::getService('CommentService');
		$this->userService = $userService = AllTests::getService('UserService');
	}
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/user.sql');
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/comment.sql');
	}
	
    public function testCanLoadCommentByUid() {
    	
		$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment');
		}
		
		$this->assertTrue($comment instanceof Comment);
		$this->assertEquals('Comment Content 1', $comment->getContent());
		$this->assertEquals(1, $comment->getCreatedBy()->getUID());
		$this->assertEquals('localhost', $comment->getAddedFrom());
		$this->assertNotNull($comment->getCreated());
		$this->assertFalse($comment->isApproved());
		$this->assertEquals(0, $comment->getScore());
		
    }
    
    public function testCanCreateCommentWithValidData() {
    	
    	/* @var $user User */
		$user = $this->userService->getByUid(1);
		
		$data['content'] = 'New Comment';
		$data['addedFrom'] = 'localhost';
		
		$comment = $this->commentService->createComment($data, $user);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail($this->commentService->getLastException()->getMessage());
		}
		
		$this->assertTrue($comment instanceof Comment);
		$this->assertEquals('New Comment', $comment->getContent());
		$this->assertEquals(1, $comment->getCreatedBy()->getUID());
		$this->assertEquals('localhost', $comment->getAddedFrom());
		$this->assertNotNull($comment->getCreated());
		$this->assertFalse($comment->isApproved());
		$this->assertEquals(0, $comment->getScore());
		$this->assertTrue($comment->getUID() > 0);
    }
    
    public function testCanUpdateComment() {
    	
    	/* @var $user User */
		$user = $this->userService->getByUid(1);
		
		$data = array();
		$data['content'] = 'Updated Comment';
		$data['approved'] = 1;
		$data['score'] = 1;
		$data['uid'] = 1;
		
		// check before
		$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment in update test [1]');
		}
		$this->assertTrue($comment instanceof Comment);
		$this->assertEquals('Comment Content 1', $comment->getContent());
		$this->assertFalse($comment->isApproved());
		$this->assertEquals(0, $comment->getScore());
		
		// update
		$comment = $this->commentService->updateComment($data, $user);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail($this->commentService->getLastException()->getMessage());
		}
		
		// check after
		$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment in update test [2]');
		}
		$this->assertTrue($comment instanceof Comment);
		$this->assertEquals('Updated Comment', $comment->getContent());
		$this->assertTrue($comment->isApproved());
		$this->assertEquals(1, $comment->getScore());
		
    }
    
    public function testCanIncrementScore() {
    	
    	$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment when incrementing score [1]');
		}
		
		$this->assertEquals(0, $comment->getScore());
		
		$comment->incrementScore();
		$comment->incrementScore();
		$comment->incrementScore();
		
		$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment when incrementing score [2]');
		}
		
		$this->assertEquals(3, $comment->getScore());
		
    }
    
    public function testCanDecrementScore() {
    	
    	$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment decrementing score [1]');
		}
		
		$this->assertEquals(0, $comment->getScore());

		$this->commentService->score(1, '+');
		$this->commentService->score(1, '+');
		$this->commentService->score(1, '+');
		
		$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment when decrementing score [2]');
		}
		
		$this->assertEquals(3, $comment->getScore());

		$this->commentService->score(1, '-');
		$this->commentService->score(1, '-');
		
		$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment when decrementing score [3]');
		}
		
		$this->assertEquals(1, $comment->getScore());
		   	
    }
    
    public function testCanSetApprovedStatus() {
    	
    	$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment in set approved test [1]');
		}
		
		$this->assertFalse($comment->isApproved());
		
		$comment->setApproved(1);
		
		$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment when updating approved flag [2]');
		}
		
		$this->assertTrue($comment->isApproved());
		
		$comment->setApproved(0);
		
    	$comment = $this->commentService->getByUid(1);
		if(!$comment) {
			var_dump($this->commentService->getMessages());
			$this->fail('Failed loading comment when updating approved flag [3]');
		}
		
		$this->assertFalse($comment->isApproved());
		
    }
}