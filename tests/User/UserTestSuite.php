<?php

/**
 * User test suite
 */
class UserTestSuite extends PHPUnit_Framework_TestSuite {

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('UserTestSuite' );
		$this->addTestSuite('UserTest');
		$this->addTestSuite('CustomerTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self();
	}
}