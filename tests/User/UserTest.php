<?php

use Supa\User\NullUser;
use Supa\User\Service;
use Supa\User\User;

/**
 * @backupGlobals disabled
 */
class UserTest extends PHPUnit_Framework_TestCase {

    /* @var $userService \Supa\User\Service */
    protected $userService;
    
    public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->userService = AllTests::getService('UserService');
	}
		
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/user.sql');
	}
	
    public function testCanLoadUser() {
		
		$user = $this->userService->getByUid(1);
		
		if(!$user) {
			$this->fail($this->userService->getLastException()->getMessage());
		}
		
		$this->assertTrue($user instanceof User);
		$this->assertEquals('Test', $user->getFirstname());
		$this->assertEquals('User1', $user->getSurname());
		$this->assertEquals('tuser1', $user->getUsername());
		$this->assertEquals('tuser1@domain.com', $user->getEmail());
    }
    
    public function testCanLoadUserByUsername() {
    	
    	// User uid 1 (inserted in test_data_inserts.sql)
    	// has username of tuser1
    	
		$user = $this->userService->getUserByUsername('tuser1');
		if(!$user) {
			$this->fail('Failed loading user for given username');
		}
		
		$this->assertTrue($user instanceof User);
		$this->assertEquals(1, $user->getUID());
		$this->assertEquals('Test', $user->getFirstname());
		$this->assertEquals('User1', $user->getSurname());
		$this->assertEquals('tuser1@domain.com', $user->getEmail());
    }
    
	public function testCanLoadUserByUsernameAndPassword() {
    	
    	// User uid 1 (inserted in test_data_inserts.sql)
    	// has username of tuser1 and pass of pass123
		$user = $this->userService->getUserByUsernameAndPassword('tuser1', 'pass123');
		if(!$user) {
			$this->fail('Failed loading user for given username and password');
		}
		
		$this->assertTrue($user instanceof User);
		$this->assertEquals(1, $user->getUID());
		$this->assertEquals('Test', $user->getFirstname());
		$this->assertEquals('User1', $user->getSurname());
		$this->assertEquals('tuser1@domain.com', $user->getEmail());	
	
	}
    
    public function testCanCreateUser() {
		
		$data['firstname'] = 'Foo';
		$data['surname'] = "O'Shea";
		$data['username'] = 'FooBar';
		$data['password'] = 'fooBar123';
		$data['passwordVerify'] = 'fooBar123';
		$data['email'] = 'foo@domain.co.uk';
		$data['active'] = 1;
		
		$user = $this->userService->createUser($data);
		if(!$user) {
			$this->fail($this->userService->getLastException()->getMessage());
		}
		
		$this->assertTrue($user instanceof User);
		$this->assertEquals('Foo', $user->getFirstname());
		$this->assertEquals("O'Shea", $user->getSurname());
		$this->assertEquals('FooBar', $user->getUsername());
		$this->assertEquals('foo@domain.co.uk', $user->getEmail());
    }
    
    public function testCanNotCreateUserWithInvalidData() {
		
		$data['firstname'] = 'F3223oo';
		$data['surname'] = "3";
		$data['username'] = 'FooBar';
		$data['password'] = 'fooBar123';
		$data['passwordVerify'] = 'fooBar123';
		$data['email'] = 'foo@domain.co.uk';
		$data['active'] = 1;
		
		$user = $this->userService->createUser($data);
		if($user instanceof User) {
			$this->fail('createUser created a user when it should\'nt');
		}
		
		$this->assertTrue(true);
    }
    
    public function testCanChangeUserEmailAddress() {
		
		$data['uid'] = 1;
		$data['newIdentity'] = 'changed@domain.co.uk';
		$data['credential'] = 'pass123';
		
		$user = $this->userService->changeEmail($data);
		if(!$user) {
			$this->fail($this->userService->getLastException()->getMessage());
		}
		
		$this->assertTrue($user instanceof User);
		$this->assertEquals('changed@domain.co.uk', $user->getEmail());
    }
    
    public function testCanChangePassword() {
		
		$data['uid'] = 1;
		$data['newCredential'] = 'pass1234';
		$data['credential'] = 'pass123';
		
		$user = $this->userService->changePassword($data);
		if(!$user) {
			$this->fail($this->userService->getLastException()->getMessage());
		}
		
		$this->assertTrue($user instanceof User);
    }
    
    public function testCanUpdateUserUsername() {

		$data['uid'] = 1;
		$data['username'] = 'tuser2';
		$data['credential'] = 'pass123';
		
		// check
		$user = $this->userService->getByUid(1);
		$this->assertEquals('tuser1', $user->getUsername());
		
		$user = $this->userService->changeUsername($data);
		if(!$user) {
			$this->fail($this->userService->getLastException()->getMessage());
		}
		
		$this->assertTrue($user instanceof User);
		$this->assertEquals('tuser2', $user->getUsername());
    }
    
    public function testCanEditUser() {
		
		// updated username and surname
		$data['firstname'] = 'Foos';
		$data['surname'] = 'Bars';
		$data['uid'] = 1;
		
		// check
		$user = $this->userService->getByUid(1);
		$this->assertEquals('Test', $user->getFirstname());
		$this->assertEquals('User1', $user->getSurname());
		
		$user = $this->userService->updateUser($data);
		if(!$user) {
			var_dump($this->userService->getMessages());
			$this->fail($this->userService->getLastException()->getMessage());
		}
		
		$this->assertTrue($user instanceof User);
		$this->assertEquals('Foos', $user->getFirstname());
		$this->assertEquals('Bars', $user->getSurname());
    }
    
    public function testCanNotCreateUserWithSameEmailAsCurrentUser() {
		
		$data['firstname'] = 'Foo';
		$data['surname'] = 'Bar';
		$data['username'] = 'FooBar';
		$data['password'] = 'fooBar123';
		$data['passwordVerify'] = 'fooBar123';
		$data['email'] = 'tuser1@domain.com';
		$data['active'] = 1;
		
		$user = $this->userService->createUser($data);
		if($user instanceof User) {
			$this->fail($this->userService->getLastException()->getMessage());
		}
    }
    
    public function testCanNotCreateUserWithSameUsernameAsCurrentUser() {
		
		$data['firstname'] = 'Foo';
		$data['surname'] = 'Bar';
		$data['username'] = 'tuser1';
		$data['password'] = 'fooBar123';
		$data['passwordVerify'] = 'fooBar123';
		$data['email'] = 'tuser1@domain.com';
		$data['active'] = 1;
		
		$user = $this->userService->createUser($data);
		if($user instanceof User) {
			$this->fail($this->userService->getLastException()->getMessage());
		}
    }
    
	public function testCannotLoadUserByIncorrectUsername() {
    	
    	// User uid 1 (inserted in test_data_inserts.sql)
    	// has username of tuser1 and pass of pass123
		$user = $this->userService->getUserByUsernameAndPassword('tuser1s', 'pass123');
		if($user instanceof User) {
			$this->fail('wtf? A user has been loaded in: testCannotLoadUserByIncorrectUsername');
		}
	}
	
	public function testCanGetListOfUsersAsCollection() {

	    $users = $this->userService->getUsers();
	    
	    $this->assertTrue($users instanceof \Supa\User\Collection);
	    $this->assertEquals(1, $users->count());
	}
	
	public function testCallingGetUsersWithNoUsersToGetWillReturnEmptyCollection() {
	    
	    AllTests::truncateTables();

	    $users = $this->userService->getUsers();
	    
	    $this->assertTrue($users instanceof \Supa\User\Collection);
	    $this->assertEquals(0, $users->count());
	}
	
	public function testCanDeleteUser() {
	    
	    $deleted = $this->userService->deleteUser(1);
	    
	    $user = $this->userService->getByUid(1);
	    
	    if(!$deleted || !$user instanceof \Supa\User\NullUser) {
	        $this->fail('A user was loaded that should have been deleted.');
	    }
	}
	
	public function testDeletingNonExistentUserWillReturnFalse() {

	    $deleted = $this->userService->deleteUser(22);
	    
	    if($deleted) {
	        $this->fail('wtf? A user was apparently deleted that doesn\'t exist...');	        
	    }
	}
	
}