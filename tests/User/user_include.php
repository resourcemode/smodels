<?php
require_once($root . '/vendor/Supa/User/Validator/AbstractRecord.php');
require_once($root . '/vendor/Supa/User/Validator/NoRecordExists.php');
require_once($root . '/vendor/Supa/User/Validator/RecordExists.php');
require_once($root . '/vendor/Supa/User/Filter/Create.php');
require_once($root . '/vendor/Supa/User/Filter/Edit.php');
require_once($root . '/vendor/Supa/User/Service.php');
require_once($root . '/vendor/Supa/User/Mapper.php');
require_once($root . '/vendor/Supa/User/User.php');
require_once($root . '/vendor/Supa/User/NullUser.php');
require_once($root . '/vendor/Supa/User/Collection.php');
require_once($root . '/vendor/Supa/User/Customer/Customer.php');
require_once($root . '/vendor/Supa/User/Customer/Service.php');
require_once($root . '/vendor/Supa/User/Customer/Mapper.php');
require_once($root . '/vendor/Supa/User/Customer/Collection.php');