<?php

use Supa\User\NullUser;
use Supa\User\Customer\Service;
use Supa\User\Customer\Customer;

/**
 * @backupGlobals disabled
 */
class CustomerTest extends PHPUnit_Framework_TestCase {

    /* @var $customerService \Supa\User\Customer\Service */
    protected $customerService;
    
    public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->customerService = AllTests::getService('CustomerService');
	}
		
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/customer.sql');
	}
	
    public function testCanLoadUser() {
		
		$customer = $this->customerService->getByUid(1);
		
		if(!$customer) {
			$this->fail($this->customerService->getLastException()->getMessage());
		}
		
		$this->assertTrue($customer instanceof Customer);
		$this->assertEquals('John', $customer->getFirstname());
		$this->assertEquals('Smith', $customer->getSurname());
		$this->assertEquals('jsmith', $customer->getUsername());
		$this->assertEquals('john@smith.com', $customer->getEmail());
		$this->assertEquals('0161 123 4567', $customer->getPhone());
    }
}