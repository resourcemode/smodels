<?php

/**
 * Image test suite
 */
class ImageTestSuite extends PHPUnit_Framework_TestSuite {

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('ImageTestSuite' );
		$this->addTestSuite('ImageTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self ( );
	}
}