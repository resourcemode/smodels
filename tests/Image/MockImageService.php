<?php
/**
 * Smodels
 *
 * @copyright Copyright (c) 2012-2013 Daniel Latter.
 */

//use \Image\ImageService;
use Supa\User\User;
use Supa\Image\Filter\CreateFilter as ImageCreateFilter;

/**
 * ImageService class
 */
class MockImageService extends Supa\Image\Service {
	
	/**
	 * Overridden for testing
	 * Used MockImage object so we can test upload
	 * 
	 * Create a image record that relates to an image
	 * file on disk
	 * 
	 * @see Supa\Image\Service
	 * @see Supa\Image\MockImage
	 * @param array $data
	 * @param Supa\User\User $user
	 * @return \Image|null
	 */
	public function createImage(array $data = array(), Supa\User\User $user) {
	    try {
	    	
			$filter = new ImageCreateFilter();
			$filter->prepareFilter();
			$filter->setData($data);
			
	    	if(!$filter->isValid()) {
				throw new Exception('Unable to create image record: '.print_r($filter->getMessages(), true));
			}
			
			$image = new MockImage($data);
       		$image->setCreatedBy($user->getUID());
       		
       		// upload image
       		if(isset($data['filesArray']) && is_array($data['filesArray'])) {
       			$image->uploadFile($data['filesArray']);
       		}
       		
	        $image = $this->mapper->create($image);
	        
	    }catch(Exception $e) {
	        $image = null;
	        $this->messages[] = $e->getMessage();
	        $this->lastException = $e;
	    }
	    return $image;
	}
}