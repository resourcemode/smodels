<?php

use Supa\Image\Image;

class MockImage extends Supa\Image\Image {

    protected function move($tmpPath, $newPath) {
        return rename($tmpPath, $newPath);
    }
}