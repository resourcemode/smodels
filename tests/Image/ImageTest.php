<?php

use Supa\Category\Service as CategoryService;
use Supa\Image\Mapper as ImageMapper;
use Supa\Image\Service as ImageService;
use Supa\Product\Service as ProductService;
use Supa\User\Service as UserService;
use Supa\Image\Image;
use Supa\Image\Collection as ImageCollection;


/**
 * @backupGlobals disabled
 */
class ImageTest extends PHPUnit_Framework_TestCase {
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
    public function testCanLoadImage() {
    	
		/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$image = $imageService->getByUid(1);
		if(!$image) {
			$this->fail($imageService->getLastException()->getMessage());
		}
				
		$this->assertTrue(($image instanceof Image));
		$this->assertEquals(1, $image->getUID());
		$this->assertEquals('test_filename', $image->getFilename());
		$this->assertEquals('original_test_filename.jpg', $image->getOriginalFilename());
		$this->assertEquals(1, $image->getCreatedBy()->getUID());
		$this->assertEquals('127.0.0.1', $image->getUploadedFrom());
    }
    
    public function testCanCreateImageRecord() {
    	
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$data['filename'] = 'filename';
		$data['originalFilename'] = 'filename_original.jpg';
		$data['uploadedFrom'] = 'localhost';
		
		$image = $imageService->createImage($data, $user);
		
		if(!$image) {
			$this->fail($imageService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($image instanceof Image));
		$this->assertEquals('filename', $image->getFilename());
		$this->assertEquals('filename_original.jpg', $image->getOriginalFilename());
		$this->assertEquals('localhost', $image->getUploadedFrom());
		$this->assertEquals(1, $image->getCreatedBy()->getUID());
		$this->assertNotNull($image->getCreated());
    }
    
    public function testCanDeleteImage() {
    
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$image = $imageService->getByUid(1);
		
		// get filename so we can check if image file is actually deleted
		$filename = $image->getOriginalFilename();
		
		$imageService->deleteImage(1);
		
		if($imageService->getByUid(1)) {
			$this->fail('deleteImage didnt delete the image');
		}
		
		$this->assertFalse(file_exists('c:/wamp/www/smodels/smodels/images/products/' . $filename));
    }
    
    public function testCanGetProductImages() {

    	/* @var $productService ProductService */
		$productService = AllTests::getService('ProductService');
		
    	/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$product = $productService->getByUid(1); // has 2 linked images, inserted in test_data_inserts.sql
		
		$images = $product->getImages(); //$imageService->getProductImages($product);
		
		$this->assertTrue($images instanceof ImageCollection);
		$this->assertEquals(2, $images->count());
    }
    
//    public function testCanGetCategoryImages() {
//
//    	/* @var $categoryService CategoryService */
//		$categoryService = AllTests::getService('CategoryService');
//		
//    	/* @var $imageService ImageService */
//		$imageService = AllTests::getService('ImageService');
//		
//		$category = $categoryService->getByUid(2); // has 2 linked images, inserted in test_data_inserts.sql
//		
//		$images = $category->getImages();//$imageService->getCategoryImages($category);
//		if(!$images) {
//			$this->fail(var_dump($imageService->getMessages()));
//		}
//		
//		$this->assertTrue($images instanceof ImageCollection);
//		$this->assertEquals(2, $images->count());
//    }
    
//    public function testCanAddImagesToCategory() {
//
//    	/* @var $categoryService CategoryService */
//		$categoryService = AllTests::getService('CategoryService');
//		
//    	/* @var $imageService ImageService */
//		$imageService = AllTests::getService('ImageService');
//		
//		$category = $categoryService->getByUid(3); // has 0 linked images
//		
//		// check data
//		$images = $category->getImages();//$imageService->getCategoryImages($category);
//		$this->assertTrue($images instanceof ImageCollection);
//		$this->assertEquals(0, $images->count());
//		
//		$imageService->addImagesToCategory(array(1, 2), $category);
//		
//		// change to use Category object
//		$images = $category->getImages();//$imageService->getCategoryImages($category);
//		$this->assertTrue($images instanceof ImageCollection);
//		$this->assertEquals(2, $images->count());		
//		
//    }
    
    public function testCanRemoveImagesFromCategory() {
    	
    	/* @var $categoryService CategoryService */
		$categoryService = AllTests::getService('CategoryService');
		
    	/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$category = $categoryService->getByUid(2); // has 2 linked images
		
		// check data
		$images = $category->getImages(); // $imageService->getCategoryImages($category);
		$this->assertTrue($images instanceof ImageCollection);
		$this->assertEquals(2, $images->count());
		
		$imageService->removeCategoryImages(array(1, 2), $category);
		
		// change to use Category object
		$images = $category->getImages(); //$imageService->getCategoryImages($category);
		$this->assertTrue($images instanceof ImageCollection);
		$this->assertEquals(0, $images->count());    	
    }
    
    public function testCanAddImagesToProduct() {

    	/* @var $productService ProductService */
		$productService = AllTests::getService('ProductService');
		
    	/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$product = $productService->getByUid(2); // has 0 linked images
		
		// check data
		$images = $product->getImages(); // $imageService->getProductImages($product);
		$this->assertTrue($images instanceof ImageCollection);
		$this->assertEquals(0, $images->count());
		
		$imageService->addImagesToProduct(array(1, 2), $product);
		
		// change to use Product object
		$images = $product->getImages(); // $imageService->getProductImages($product);
		$this->assertTrue($images instanceof ImageCollection);
		$this->assertEquals(2, $images->count());		
		
    }
    
    public function testCanRemoveImagesFromProduct() {
    	
    	/* @var $productService ProductService */
		$productService = AllTests::getService('ProductService');
		
    	/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$product = $productService->getByUid(1); // has 2 linked images
		
		// check data
		$images = $product->getImages(); // $imageService->getProductImages($product);
		$this->assertTrue($images instanceof ImageCollection);
		$this->assertEquals(2, $images->count());
		
		$imageService->removeProductImages(array(1, 2), $product);
		
		// change to use Product object
		$images = $product->getImages(); // $imageService->getProductImages($product);
		$this->assertTrue($images instanceof ImageCollection);
		$this->assertEquals(0, $images->count());    	
    }
    
	public function testCanUploadImageFile() {
		
		// copy test file
        // ensure the file bbc-olympic.png exists!
        copy('c:/wamp/www/smodels/smodels/images/test/bbc-olympic.png', '/tmp/bbc-olympic.png');
		
		/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		//$user = $userService->getByUid(1);
		
		// ensure files do not exist in storage dir
        @unlink('c:/wamp/www/smodels/smodels/images/product/bbc-olympic.png');

        $_FILES = array(
	        'logo' => array(
	            'name' => 'bbc-olympic.png',
	            'type' => 'image/x-png',
	            'tmp_name' => 'C:/tmp/bbc-olympic.png',
	            'error' => 0,
	            'size' => 397106
        	)
        );
		
		$image = new MockImage();
		$image->setFilename('bbc-olympic');
        $image->setOriginalFilename('bbc-olympic.png');
		
        // upload test photograph
        $this->assertTrue($image->uploadFile($_FILES['logo']));
        
        // assert file was uploaded
        $this->assertTrue($image->isUploaded());
        
        // check properties set
        $this->assertEquals('bbc-olympic', $image->getFilename());
        $this->assertEquals('bbc-olympic.png', $image->getOriginalFilename());
	}
	
	public function testCanUploadImageFileAndCreateRecord() {
		
		// copy test file
        // ensure the file bbc-olympic.png exists!
        copy('c:/wamp/www/smodels/smodels/images/test/bbc-olympic.png', '/tmp/bbc-olympic.png');
		
		/* @var $imageService MockImageService */
		$imageService = new MockImageService(AllTests::getService('ImageMapper'));
		
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		// ensure files do not exist in storage dir
        @unlink('c:/wamp/www/smodels/smodels/images/product/bbc-olympic.png');

        $_FILES = array(
	        'logo' => array(
	            'name' => 'bbc-olympic.png',
	            'type' => 'image/x-png',
	            'tmp_name' => 'C:/tmp/bbc-olympic.png',
	            'error' => 0,
	            'size' => 397106
        	)
        );
        
        $data = array();
        $data['filename'] = 'bbc-olympic';
        $data['originalFilename'] = 'bbc-olympic.png';
        $data['uploadedFrom'] = 'localhost';
        $data['filesArray'] = $_FILES['logo'];
		
		$image = $imageService->createImage($data, $user);
		if(!$image) {
			$this->fail($imageService->getLastException()->getMessage());
		}
        
        // assert file was uploaded
        $this->assertTrue($image->isUploaded());
        
        // check properties set
        $this->assertEquals('bbc-olympic', $image->getFilename());
        $this->assertEquals('bbc-olympic.png', $image->getOriginalFilename());
        $this->assertEquals(1, $image->getCreatedBy()->getUID());
        $this->assertNotNull($image->getCreated());
        $this->assertNotNull($image->getUID());
        $this->assertEquals('localhost', $image->getUploadedFrom());
	}
	
	public function testCanGetFileExtensionOfLoadedImage() {
    	
		/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$image = $imageService->getByUid(1);
		if(!$image) {
			$this->fail($imageService->getLastException()->getMessage());
		}
				
		$this->assertTrue(($image instanceof Image));
		$this->assertEquals(1, $image->getUID());
		$this->assertEquals('.jpg', $image->getExtension());
			    
	}
		
	public function testCanGenerateCorrectFileNameForSizeSpecificImages() {
    	
		/* @var $imageService ImageService */
		$imageService = AllTests::getService('ImageService');
		
		$image = $imageService->getByUid(1);
		if(!$image) {
			$this->fail($imageService->getLastException()->getMessage());
		}
				
		$this->assertTrue(($image instanceof Image));
		$this->assertEquals(1, $image->getUID());
		$this->assertEquals('1t100x100.jpg', $image->getFilename('t', '100', '100'));
		
	}
}