<?php

use Supa\Product\Attribute\Attribute as ProductAttribute;
use Supa\Product\Attribute\Service as ProductAttributeService;
use Supa\Product\Service as ProductService;
use Supa\Product\Product;
use Supa\User\Service as UserService;

/**
 * @backupGlobals disabled
 */
class ProductAttrTest extends PHPUnit_Framework_TestCase {
	
	public $productService;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->productService = AllTests::getService('ProductService');
	}
	
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
    public function testCanLoadProductAttribute() {
    	
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$prodAttrUid = 1;
		
		$productAttr = $prodAttrService->getByUid($prodAttrUid);
		if(!$productAttr) {
			$this->fail($prodAttrService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($productAttr instanceof ProductAttribute));
		$this->assertEquals('Colour', $productAttr->getName());
		$this->assertEquals($prodAttrUid, $productAttr->getUID());
		$this->assertEquals(1, $productAttr->getCreatedBy()->getUID());
		$this->assertEquals(1, $productAttr->getLastEditiedBy()->getUID());
		$this->assertEquals('2012-11-01 00:00:00', $productAttr->getCreated());
		$this->assertEquals('2012-12-01 00:00:00', $productAttr->getLastEditied());
    }
    
    public function testCanCreateProductAttribute() {
    	
    	/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		$attributeName = 'New Test Attribute';
		
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$productAttr = $prodAttrService->createAttribute($attributeName, $user);
		if(!$productAttr) {
			$messages = $prodAttrService->getMessages();
			if(!empty($messages)) {
				$this->fail(print_r($messages, true));
			}else {
				$this->fail($prodAttrService->getLastException()->getMessage());
			}
		}
		
		$this->assertTrue(($productAttr instanceof ProductAttribute));
    }
    
    public function testCanNotCreateProductAttributeWithNonUniqueName() {
    	
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		$attributeName = 'Sizes'; // non unique
		
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$productAttr = $prodAttrService->createAttribute($attributeName, $user);
		if($productAttr) {
			$this->fail('createAttribute created a product attribute when it should\'nt');
		}
    }
    
    public function testCanCreateProductAttributeWithValues() {
    	
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
    	$attributeValues = array('Uber Attr Value');
		$attributeName = 'New Test Attribute';
		
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$productAttr = $prodAttrService->createAttribute($attributeName, $user, $attributeValues);
		if(!$productAttr) {
			$messages = $prodAttrService->getMessages();
			if(!empty($messages)) {
				$this->fail(print_r($messages, true));
			}else {
				$this->fail($prodAttrService->getLastException()->getMessage());
			}
		}
		
		$this->assertTrue(($productAttr instanceof ProductAttribute));
		
		$values = $productAttr->getValues();
		$this->assertTrue(($values instanceof \ArrayIterator));
		$this->assertEquals(1, $values->count());
    }
    
    public function testCanUpdateProductAttributeAddAction() {
    	
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
				
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$data = array();
		$prodAttrUid = $data['uid'] = 3;
		$data['name'] = 'Acme Inc'; // new name
		$data['values'] = array(
			array('uid' => 6, 'name' => 'Bar')
		);
		$data['currentAttrValueUids'] = array(6);
		$data['updatedAttrValueUids'] = array(6);
		$data['newAttrValues'] = array(
			array('name'=>'Bazio')
		);
		
		// check data
		$productAttr = $prodAttrService->getByUid($prodAttrUid);
		$this->assertEquals('Acme', $productAttr->getName());
		$this->assertTrue(($productAttr->getValues() instanceof \ArrayIterator));
		$this->assertEquals(1, $productAttr->getValues()->count());
		$attrValues = $productAttr->getValues();
		foreach($attrValues as $attrValue) {
			$this->assertEquals('Foo', $attrValue->getName());
		}
				
		$productAttr = $prodAttrService->updateAttribute($data, $user);
		if(!$productAttr) {
			var_dump($prodAttrService->getMessages());
			$this->fail('Failed updating product attribute');
		}
			
		$this->assertEquals('Acme Inc', $productAttr->getName());
		$this->assertTrue(($productAttr->getValues() instanceof \ArrayIterator));
		$this->assertEquals(2, $productAttr->getValues()->count());
		
		$attrValues = $productAttr->getValues();
		foreach($attrValues as $attrValue) {
			if($attrValue->getName() == 'Bar' || $attrValue->getName() == 'Bazio')
				continue;
			
			$this->fail("should not get here: " . $attrValue->getName());
		}
	}
	
    public function testCanUpdateProductAttributeRemoveAction() {
    	
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
				
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$data = array();
		$prodAttrUid = $data['uid'] = 3;
		$data['name'] = 'Acme Inc'; // new name
		$data['values'] = array(
			array('uid' => 6, 'name' => 'Bar')
		);
		$data['currentAttrValueUids'] = array(6);
		$data['updatedAttrValueUids'] = array();
		$data['newAttrValues'] = array(
			array('name'=>'Bazio')
		);
		
		// check data
		$productAttr = $prodAttrService->getByUid($prodAttrUid);
		$this->assertEquals('Acme', $productAttr->getName());
		$this->assertTrue(($productAttr->getValues() instanceof \ArrayIterator));
		$this->assertEquals(1, $productAttr->getValues()->count());
		$attrValues = $productAttr->getValues();
		foreach($attrValues as $attrValue) {
			$this->assertEquals('Foo', $attrValue->getName());
		}
				
		$productAttr = $prodAttrService->updateAttribute($data, $user);
		if(!$productAttr) {
			var_dump($prodAttrService->getMessages());
			$this->fail('Failed updating product attribute');
		}
		$productAttr->setValues($productAttr->getUID());				
		$this->assertEquals('Acme Inc', $productAttr->getName());
		$this->assertTrue(($productAttr->getValues() instanceof \ArrayIterator));
		$this->assertEquals(1, $productAttr->getValues()->count());
		
		$attrValues = $productAttr->getValues();
		foreach($attrValues as $attrValue) {
			if($attrValue->getName() == 'Bazio')
				continue;
				
			$this->fail("should not get here");
		}
	}
		
    public function testCanNotUpdateProductAttributeWithNonUniqueName() {
    	
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
				
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$data = array();
		$prodAttrUid = $data['uid'] = 3;
		$data['name'] = 'Sizes'; // nonunique name
		$data['values'] = array(
			array('uid' => 6, 'name' => 'Bar')		
		);
		
		// check data
		$productAttr = $prodAttrService->getByUid($prodAttrUid);
		$this->assertEquals('Acme', $productAttr->getName());
		$this->assertTrue(($productAttr->getValues() instanceof \ArrayIterator));
		$this->assertEquals(1, $productAttr->getValues()->count());
		$attrValues = $productAttr->getValues();
		foreach($attrValues as $attrValue) {
			$this->assertEquals('Foo', $attrValue->getName());
		}
				
		$productAttr = $prodAttrService->updateAttribute($data, $user);
		if($productAttr) {
			$this->fail('updateAttribute returned true when should have returned false');
		}
		
	}
	
	public function testCanDeleteProductAttribute() {
		
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$data = array();
		$prodAttrUid = 1;
		$data['name'] = 'Test Attribute Edit'; // new name
		
		$res = $prodAttrService->deleteAttribute($prodAttrUid);
		if(!$res) {
			var_dump($prodAttrService->getLastException()->getMessage());
			$this->fail('Failed deleting product attribute');
		}
		
		if($prodAttrService->getByUid($prodAttrUid)) {
			$this->fail('Product still exists');
		}
	}
}