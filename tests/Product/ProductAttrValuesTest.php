<?php

use Supa\Product\Attribute\Service as ProductAttributeService;
use Supa\Product\Service as ProductService;
use Supa\Product\Product;
use Supa\User\Service as UserService;

/**
 * @backupGlobals disabled
 */
class ProductAttrValuesTest extends PHPUnit_Framework_TestCase {
	
	public $productService;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->productService = AllTests::getService('ProductService');
	}
	
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
	public function testCanGetProductAttributesForAProduct() {
		
		/* @var $prodService ProductService */
		//$prodService = AllTests::getService('ProductService');

		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$attrValueUids = array();
		$product = $this->productService->getByUid(1);
		$attrValueUids = array(1, 2, 3, 4, 5);
		
		AllTests::addAttributeValuesToProduct($attrValueUids, $product);
		
		// check current attribute values
		$attrColl = $product->getAttributes();
		if(!$attrColl) {
			var_dump($this->productService->getMessages());
			$this->fail();
		}
		
		$this->assertTrue(($attrColl instanceof \ArrayIterator));
		$this->assertEquals(2, $attrColl->count());
	}
	
	public function testCanUpdateAttributeValuesForAProductRemoveAction() {
		
		/* @var $prodService ProductService */
		//$prodService = AllTests::getService('ProductService');

		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$attrValueUids = array();
		$product = $this->productService->getByUid(1);
		$attrValueUids = array(1, 2, 3);
		
		AllTests::addAttributeValuesToProduct($attrValueUids, $product);
		
		$attrs = $product->getAttributes();
		$this->assertEquals(1, $attrs->count());
		$attr = $attrs[0];
		$this->assertEquals(3, $attr->getValues()->count());
		
		$updated = array(1, 2);
		
		$updated = $prodAttrService->updateAttrValuesForProduct($product, $updated, $attrValueUids);
		$this->assertTrue($updated);
		
		$product->setAttributes($product->getUID());
		$attrs2 = $product->getAttributes();
		$this->assertEquals(1, $attrs2->count());
		$attr2 = $attrs2[0];
		$this->assertEquals(2, $attr2->getValues()->count());
		
	}
	
	public function testCanUpdateAttributeValuesForAProductAddAction() {
		
		/* @var $prodService ProductService */
		//$prodService = AllTests::getService('ProductService');

		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$attrValueUids = array();
		$product = $this->productService->getByUid(1);
		$attrValueUids = array(1, 2);
		
		AllTests::addAttributeValuesToProduct($attrValueUids, $product);
		
		$attrs = $product->getAttributes();
		$this->assertEquals(1, $attrs->count());
		$attr = $attrs[0];
		$this->assertEquals(2, $attr->getValues()->count());
		
		$updated = array(1, 2, 3);
		
		$updated = $prodAttrService->updateAttrValuesForProduct($product, $updated, $attrValueUids);
		$this->assertTrue($updated);
		
		$product->setAttributes($product->getUID());
		$attrs2 = $product->getAttributes();
		$this->assertEquals(1, $attrs2->count());
		$attr2 = $attrs2[0];
		$this->assertEquals(3, $attr2->getValues()->count());
		
	}
	
	public function testUpdatingAttributeValuesForAProductWithSameValuesWillHaveNoEffect() {
		
		/* @var $prodService ProductService */
		//$prodService = AllTests::getService('ProductService');

		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$attrValueUids = array();
		$product = $this->productService->getByUid(1);
		$attrValueUids = array(1, 2);
		
		AllTests::addAttributeValuesToProduct($attrValueUids, $product);
		
		$attrs = $product->getAttributes();
		$this->assertEquals(1, $attrs->count());
		$attr = $attrs[0];
		$this->assertEquals(2, $attr->getValues()->count());
		
		$updated = array(1, 2);
		
		$updated = $prodAttrService->updateAttrValuesForProduct($product, $updated, $attrValueUids);
		$this->assertTrue($updated);
		
		$product->setAttributes($product->getUID());
		$attrs2 = $product->getAttributes();
		$this->assertEquals(1, $attrs2->count());
		$attr2 = $attrs2[0];
		$this->assertEquals(2, $attr2->getValues()->count());
		
	}
}