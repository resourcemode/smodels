<?php

use Supa\Product\Service as ProductService;
use Supa\Product\Product;
use Supa\User\Service as UserService;

/**
 * @backupGlobals disabled
 */
class ProductTest extends PHPUnit_Framework_TestCase {
	
	public $productService;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->productService = AllTests::getService('ProductService');
	}
		
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
    public function testCanLoadProduct() {
    			
		$userUid = 1; // inserted in test_data_inserts.sql
		
		$product = $this->productService->getByUid(1);
		if(!$product) {
			var_dump($this->productService->getMessages());
			$this->fail('Failed loading product');
		}
		
		$this->assertTrue(($product instanceof Product));
		$this->assertEquals('Test Product 1', $product->getTitle());
		$this->assertEquals('test-product-1', $product->getPath());
		$this->assertEquals('Test Product Description 1', $product->getDescription());
		$this->assertEquals(1, $product->getCreatedBy()->getUID());
		$this->assertTrue($product->isActive());
		$this->assertEquals(1, $product->getUID());
		$this->assertNull($product->getLastEditied()); // not been editied
		$this->assertNotNull($product->getCreated());
		$this->assertEquals(100.00, $product->getPrice());
		
    }
    
    public function testCanCreateProduct() {
    			
		$userUid = 1;
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
			
		$prodData = array(
			'title' => 'Test Title 1',
			'path' => 'test-path-1',
			'description' => 'Test Description',
		    'price' => 123.33
		);
		
		$product = $this->productService->createProduct($prodData, $userService->getByUid($userUid));
		if(!$product) {
			$this->fail($this->productService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($product instanceof Product));
		$this->assertEquals($prodData['title'], $product->getTitle());
		$this->assertEquals($prodData['path'], $product->getPath());
		$this->assertEquals('Test Description', $product->getDescription());
		$this->assertEquals($userUid, $product->getCreatedBy()->getUID());
		$this->assertNull($product->getLastEditiedBy()->getUID());
		$this->assertEquals(123.33, $product->getPrice());
    }
    
    public function testCanNotCreateProductWithNonUniquePath() {
    			
		$userUid = 1;
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
			
		$prodData = array(
			'title' => 'Test Title 1',
			'path' => 'test-product-1', // not unique
			'description' => 'Test Description',
		    'price' => 123.33
		);
		
		$product = $this->productService->createProduct($prodData, $userService->getByUid($userUid));
		if($product instanceof Product) {
			$this->fail('Created a product when we should\'nt');
		}
    }
    
    public function testCanLoadMultipleProductsByCatUid() {
    			
		$catUid = 2;
		AllTests::insertProductCategoryLink(1, $catUid);
		AllTests::insertProductCategoryLink(2, $catUid);
		$prodColl = $this->productService->getProductsByCatUid($catUid);
		if(!$prodColl) {
			$this->fail($this->productService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($prodColl instanceof \ArrayIterator));
		$this->assertEquals(2, $prodColl->count());
    }
    
    public function testCanNotUpdateProductWithInvalidData() {
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		
		$user = $userService->getByUid(1);
		
		$updateData = array(
			'title'=>'New Product 1 Edited',
			'path'=>'new product edit', // invalid
			'description'=>'Test Product Description 1',
			'uid' => 1,
		    'price' => 100.00
		);
		
		$prodUid = 1;
		
		
		$product = $this->productService->updateProduct($updateData, $user);
		if($product instanceof Product) {
			$this->fail('updateProduct returned a Product when it should\'nt');
		}
		
    }
        
    public function testCanUpdateProduct() {
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		
		$user = $userService->getByUid(1);
		
		$updateData = array(
			'title'=>'New Product 1 Edited',
			'path'=>'new-product-1-edit',
			'description'=>'Test Product Description Update 1',
			'uid'=> '1',
		    'price' => '111'
		);
		
		$prodUid = 1;
		
		// check current data
		$product = $this->productService->getByUid($prodUid);
		$this->assertEquals('Test Product 1', $product->getTitle());
		$this->assertEquals('test-product-1', $product->getPath());
		$this->assertEquals('Test Product Description 1', $product->getDescription());
		$this->assertEquals(100.00, $product->getPrice());
				
		$product = $this->productService->updateProduct($updateData, $user);
		if(!$product) {
			var_dump($this->productService->getMessages());
			$this->fail($this->productService->getLastException()->getMessage());
		}
		
		// check updated data
		$this->assertTrue(($product instanceof Product));
		$this->assertEquals($product->getTitle(), 'New Product 1 Edited');
		$this->assertEquals($product->getPath(), 'new-product-1-edit');
		$this->assertEquals('Test Product Description Update 1', $product->getDescription());
		$this->assertEquals(111, $product->getPrice());
    }
    
    public function testCanNotUpdateProductWithNonUniquePath() {
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		
		$user = $userService->getByUid(1);
		
		$updateData = array(
			'title'=>'New Product 1 Edited',
			'path'=>'test-product-2', // not unique
			'description'=>'Test Desc Update',
			'uid'=> 1,
		    'price' => 100.00
		);
		
		$prodUid = 1;
		
		$product = $this->productService->updateProduct($updateData, $user);
		if($product instanceof Product) {
			$this->fail('updateProduct returned a product when it should\'nt');
		}
    }
    
	public function testCanDeleteProduct() {
				
		$prodUid = 2;
		
		$deleted = $this->productService->deleteProduct(2);
		if(!$deleted) {
			$this->fail('Failed deleting product');
		}
				
		$this->assertTrue((bool)$deleted);
		
		if($this->productService->getByUid($prodUid)) {
			$this->fail('Product still exists');
		}
	}
	
	public function testGetAttributesWillReturnEmptyCollectionOnSavedProductWithNoAttributes() {
		/* @var $prodService ProductService */
		$product = $this->productService->getByUid(1);
    	$this->assertTrue($product->getAttributes() instanceof \ArrayIterator);
	}
	
	public function testGetAttributesWillReturnEmptyCollectionOnUnSavedProductWithNoAttributes() {
		$product = new Product(array('path'=>'unsaved'));
    	$this->assertTrue($product->getAttributes() instanceof \ArrayIterator);
	}
	
	public function testGetCategoriesWillReturnEmptyCollectionOnSavedProductWithNoCategories() {
		/* @var $prodService ProductService */
		$product = $this->productService->getByUid(1);
    	$this->assertTrue($product->getCategories() instanceof \ArrayIterator);
	}
	
	public function testGetCategoriesWillReturnEmptyCollectionOnUnSavedProductWithNoCategories() {
		$product = new Product(array('path'=>'unsaved'));
    	$this->assertTrue($product->getCategories() instanceof \ArrayIterator);
	}
	
	public function testCanGetFeaturedProducts() {
		$prodColl = $this->productService->getFeaturedProducts();
		if(!$prodColl) {
			$this->fail($this->productService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($prodColl instanceof \ArrayIterator));
		$this->assertEquals(2, $prodColl->count());	    
	}
	
	public function testCanFlagProductsAsFeatured() {
	    
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
			
		$prodData = array(
			'title' => 'Test Title 1',
			'path' => 'test-path-1',
			'description' => 'Test Description',
		    'price' => 123.33
		);
		$product = $this->productService->createProduct($prodData, $userService->getByUid(1));
		if(!$product) {
			$this->fail($this->productService->getLastException()->getMessage());
		}
		$this->assertTrue(($product instanceof Product));
		$this->assertFalse($product->isFeatured());
		
		// check
		$p = $this->productService->setFeaturedStatus($product->getUID(), true);
		$updatedProd = $this->productService->getByUid($product->getUID());
		$this->assertTrue($updatedProd->isFeatured());
	}
	
	public function testCanGetAListOfProducts() {
		$prodColl = $this->productService->getProducts();
		if(!$prodColl) {
			$this->fail($this->productService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($prodColl instanceof \ArrayIterator));
		$this->assertEquals(2, $prodColl->count());	
	}
}