<?php

/**
 * Product test suite
 */
class ProductTestSuite extends PHPUnit_Framework_TestSuite {

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('ProductTestSuite' );
		$this->addTestSuite('ProductTest');
		$this->addTestSuite('ProductAttrTest');
		$this->addTestSuite('ProductAttrValuesTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self ( );
	}
}