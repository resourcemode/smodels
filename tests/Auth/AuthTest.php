<?php

use Supa\User\User;

/**
 * @backupGlobals disabled
 */
class AuthTest extends PHPUnit_Framework_TestCase {
    
    protected $authService;
    
    public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->authService = AllTests::getService('SupaAuthService');
	}
		
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/auth.sql');
	}
	
    public function testCanLoginInWithValidUsernameAndPassword() {
        
        $user = $this->authService->doLogin('tuser1', 'pass123');
        
		$this->assertTrue(($user instanceof User), $this->authService->getLastException()->getMessage());
		
		$this->assertEquals('Test', $user->getFirstname());
		$this->assertEquals('User1', $user->getSurname());
		$this->assertEquals('tuser1', $user->getUsername());
		$this->assertEquals('tuser1@domain.com', $user->getEmail());
    }
    
    public function testCanNotLoginInWithInValidUsernameAndPassword() {
        
        $user = $this->authService->doLogin('tuser1xxxxxx', 'pass123xxxxxxx');
           
		if($user instanceof User) {
		    $this->fail('wtf? A user has been loaded in: testCanNotLoginInWithInValidUsernameAndPassword');   
		}
    }
    
    public function testCanNotLoginInWithInValidUsername() {
        
        $user = $this->authService->doLogin('tuser1xxx', 'pass123');
            
		if($user instanceof User) {
		    $this->fail('wtf? A user has been loaded in: testCanNotLoginInWithInValidUsername');   
		}
    }
    
    public function testCanNotLoginInWithInValidPassword() {
        
        $user = $this->authService->doLogin('tuser1', 'pass123456');
        
		if($user instanceof User) {
		    $this->fail('wtf? A user has been loaded in: testCanNotLoginInWithInValidPassword');   
		}
    }
}