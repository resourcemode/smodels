<?php

/**
 * Auth test suite
 */
class AuthTestSuite extends PHPUnit_Framework_TestSuite
{

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('AuthTestSuite' );
		$this->addTestSuite('AuthTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self ( );
	}
}