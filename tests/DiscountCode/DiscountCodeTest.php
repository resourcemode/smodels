<?php

use Supa\Product\DiscountCode\PercentageDiscount;
use Supa\Product\DiscountCode\AmountDiscount;
use Supa\Product\DiscountCode\Mapper as DiscountCodeMapper;
use Supa\Product\DiscountCode\Service as DiscountCodeService;
use Supa\Product\DiscountCode\DiscountCode;
use Supa\User\Service as UserService;

/**
 * @backupGlobals disabled
 */
class DiscountCodeTest extends PHPUnit_Framework_TestCase {
	
	public $discountCodeService;
	
	public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->discountCodeService = AllTests::getService('DiscountCodeService');
		$this->userService = $userService = AllTests::getService('UserService');
	}
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/user.sql');
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/discount_code.sql');
	}
	
    public function testCanLoadPercentageDiscountCode() {
    	
		$code = $this->discountCodeService->getByUid(1);
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$strat = $code->getStrategy();
		$this->assertTrue(($strat instanceof PercentageDiscount));
		$this->assertEquals('code1', $strat->getCode());
		$this->assertNotNull($strat->getCreated());
		$this->assertNull($strat->getExpireDate());
		$this->assertEquals(5, $strat->getCount());
		
    }

    public function testCanLoadAmountDiscountCode() {
    	
		$code = $this->discountCodeService->getByUid(2);
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$strat = $code->getStrategy();
		$this->assertTrue(($strat instanceof AmountDiscount));
		$this->assertEquals('code2', $strat->getCode());
		$this->assertNotNull($strat->getCreated());
		$this->assertNull($strat->getExpireDate());
		$this->assertEquals(15, $strat->getCount());
		
    }
    
    public function testCanLoadDiscountCodeByCode() {
    	
		$code = $this->discountCodeService->getByCode('code2');
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$strat = $code->getStrategy();
		$this->assertTrue(($strat instanceof AmountDiscount));
		$this->assertEquals('code2', $strat->getCode());
		$this->assertNotNull($strat->getCreated());
		$this->assertNull($strat->getExpireDate());
		$this->assertEquals(15, $strat->getCount());
		
    }
    
    public function testCanValidateAValidDiscountCode() {
    	
    	$code = $this->discountCodeService->getByCode('code1');
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$this->assertTrue($code->isValid());
		    	
    }
    
    public function testADiscountCodeThatHasZeroCountsLeftWillNotValidate() {
    	
    	$code = $this->discountCodeService->getByCode('code3');
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$this->assertFalse($code->isValid());
		    	
    }
    
    public function testADiscountCodeThatHasExpiredWillNotValidate() {
    	
    	$code = $this->discountCodeService->getByCode('code4');
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$this->assertFalse($code->isValid());
		    	
    }
    
    public function testADiscountCodeThatHasExpiredAndZeroCountsWillNotValidate() {
    	
    	$code = $this->discountCodeService->getByCode('code5');
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$this->assertFalse($code->isValid());
		    	
    }

    public function testCanUpdateDiscountCode() {
    	
		$userService = AllTests::getService('UserService');
		
		$code = $this->discountCodeService->getByCode('code2');
		if(!$code) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Failed loading discount code');
		}
		
		$strat = $code->getStrategy();
		$this->assertTrue(($strat instanceof AmountDiscount));
		$this->assertEquals('code2', $strat->getCode());
		$this->assertNotNull($strat->getCreated());
		$this->assertNull($strat->getExpireDate());
		$this->assertEquals(15, $strat->getCount());
		
		$data['uid'] = 2;
		$data['count'] = 14;
		$data['code'] = 'code2_1';
		$data['expireDate'] = '2014-02-20 21:00:00';
		$code = $this->discountCodeService->updateDiscountCode($data, $userService->getByUid(1));
		
		$strat = $code->getStrategy();
		$this->assertEquals('code2_1', $strat->getCode());
		$this->assertEquals(14, $strat->getCount());
		$this->assertEquals('2014-02-20 21:00:00', $strat->getExpireDate());
		
    }
    
    public function testCanDeleteDiscountCode() {
    			
		$deleted = $this->discountCodeService->deleteDiscountCode(1);
		if(!$deleted) {
			$this->fail('Failed deleting discount code');
		}
				
		$this->assertTrue((bool)$deleted);
		
		if($this->discountCodeService->getByUid(1)) {
			$this->fail('discount code still exists');
		}    	
    }
    
    public function testCanNotCreateDiscountCodeIfSameCodeExists() {
    	
    	$userService = AllTests::getService('UserService');
		$data['code'] = 'code1';
		$data['discount'] = 1.0;
		$data['discountType'] = DiscountCode::PERCENTAGE;
		$data['count'] = $data['expire'] = null;

		$dc = $this->discountCodeService->createDiscountCode($data, $userService->getByUid(1));
		if($dc instanceof DiscountCode) {
			$this->fail('a discount code was created with same code that already exists');
		}
    }
    
    public function testCanCreateDiscountCode() {
    	
    	$userService = AllTests::getService('UserService');
		$data['code'] = 'code99';
		$data['discount'] = 1.0;
		$data['discountType'] = DiscountCode::PERCENTAGE;
		$data['count'] = $data['expire'] = null;

		$dc = $this->discountCodeService->createDiscountCode($data, $userService->getByUid(1));
		if(!$dc) {
			var_dump($this->discountCodeService->getMessages());
			$this->fail('Error creating discount code');
		}
		
		$strat = $dc->getStrategy();
		$this->assertEquals('code99', $strat->getCode());
		$this->assertNull($strat->getCount());
		$this->assertNull($strat->getExpireDate());
		$this->assertNotNull($strat->getCreated());
		$this->assertEquals(1.0, $strat->getDiscount());
		$this->assertTrue($dc instanceof DiscountCode);
    }
    
    public function testCanNotCreatePercentageDiscountCodeWithInvalidData() {
    	
    	$userService = AllTests::getService('UserService');
		$data['code'] = 'code99';
		$data['discount'] = 101;
		$data['discountType'] = DiscountCode::PERCENTAGE;
		$data['count'] = $data['expire'] = null;

		$dc = $this->discountCodeService->createDiscountCode($data, $userService->getByUid(1));
		$this->assertFalse($dc instanceof DiscountCode);
    }
    
    public function testCanNotCreatePercentageDiscountCodeWithInvalidData2() {
    	
    	$userService = AllTests::getService('UserService');
		$data['code'] = 'code99';
		$data['discount'] = 0;
		$data['discountType'] = DiscountCode::PERCENTAGE;
		$data['count'] = $data['expire'] = null;

		$dc = $this->discountCodeService->createDiscountCode($data, $userService->getByUid(1));
		$this->assertFalse($dc instanceof DiscountCode);
    }

    public function testCanNotCreateAmountDiscountCodeWithInvalidData() {
    	
    	$userService = AllTests::getService('UserService');
		$data['code'] = 'code99';
		$data['discount'] = 50000;
		$data['discountType'] = DiscountCode::AMOUNT;
		$data['count'] = $data['expire'] = null;

		$dc = $this->discountCodeService->createDiscountCode($data, $userService->getByUid(1));
		$this->assertFalse($dc instanceof DiscountCode);
    }

    public function testCanNotCreateAmountDiscountCodeWithInvalidData2() {
    	
    	$userService = AllTests::getService('UserService');
		$data['code'] = 'code99';
		$data['discount'] = 0;
		$data['discountType'] = DiscountCode::AMOUNT;
		$data['count'] = $data['expire'] = null;

		$dc = $this->discountCodeService->createDiscountCode($data, $userService->getByUid(1));
		$this->assertFalse($dc instanceof DiscountCode);
    }
    
    public function testWillThrowExceptionIfNoDiscountTypeGiven() {
    	$this->setExpectedException('Exception');
    	new DiscountCode();
    }
    
    public function testCanApplyPercentageDiscount() {
    	$dc = $this->discountCodeService->getByUid(6);
    	$this->assertEquals(80, $dc->applyDiscount(100));
    }
    
    public function testCanApplyAmountDiscount() {
    	$dc = $this->discountCodeService->getByUid(7);
    	$this->assertEquals(80, $dc->applyDiscount(100));
    }
}