<?php

/**
 * DiscountCode test suite
 */
class DiscountCodeTestSuite extends PHPUnit_Framework_TestSuite
{

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('DiscountCodeTestSuite' );
		$this->addTestSuite('DiscountCodeTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self ( );
	}
}