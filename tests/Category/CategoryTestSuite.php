<?php
/**
 * Category test suite
 */
class CategoryTestSuite extends PHPUnit_Framework_TestSuite {

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('CategoryTestSuite' );
		$this->addTestSuite('CategoryTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self ( );
	}
}