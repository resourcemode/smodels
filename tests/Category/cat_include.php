<?php
require_once($root . '/vendor/Supa/Category/Service.php');
require_once($root . '/vendor/Supa/Category/Validator/AbstractRecord.php');
require_once($root . '/vendor/Supa/Category/Validator/NoRecordExists.php');
require_once($root . '/vendor/Supa/Category/Validator/RecordExists.php');
require_once($root . '/vendor/Supa/Category/Filter/CreateFilter.php');
require_once($root . '/vendor/Supa/Category/Filter/EditFilter.php');
require_once($root . '/vendor/Supa/Category/Mapper.php');
require_once($root . '/vendor/Supa/Category/Collection.php');
require_once($root . '/vendor/Supa/Category/Tree.php');
require_once($root . '/vendor/Supa/Category/Path.php');
require_once($root . '/vendor/Supa/Category/Category.php');
