<?php

use Supa\User\NullUser;
use Supa\Category\Service as CategoryService;
use Supa\User\Service as UserService;
use Supa\Category\Filter\CreateFilter as CategoryCreateFilter;
use Supa\Category\Category;
use Supa\Category\Collection as CategoryCollection;
use Supa\Category\Path as CategoryPath;
use \Product\Service as ProductService;
use \Product\Product;

/**
 * @backupGlobals disabled
 */
class CategoryTest extends PHPUnit_Framework_TestCase {
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
    public function testCanLoadCategory() {
    	
		/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$category = $catService->getByUid(1);
		if(!$category) {
			$this->fail($catService->getLastException()->getMessage());
		}
				
		$this->assertTrue(($category instanceof Category));
		$this->assertEquals(1, $category->getUID());
		$this->assertEquals('Products', $category->getTitle());
		$this->assertEquals('products', $category->getPath());
		$this->assertEquals('Test Category Description 1', $category->getDescription());
		$this->assertNull($category->getParent());
		$this->assertEquals(1, $category->getLft());
		$this->assertEquals(6, $category->getRgt());
		$this->assertEquals(1, $category->getCreatedBy()->getUID());
		$this->assertNull($category->getLastEditied());
		$this->assertNotNull($category->getCreated());

		
    }
    
    public function testCallingGetCreatedByOnAnUnsavedCategoryReturnsNullObject() {
    	$category = new Category(array('title'=>'Unsaved'));
    	$this->assertTrue($category->getCreatedBy() instanceof NullUser);
    }
    
    public function testCanAddCategoryToTree() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		// new cat
		$newCat = array();
		$newCat['title'] = 'New Category';
		$newCat['path'] = 'a-new-cat';
		$newCat['description'] = 'Test Description';
		
		$parentCat = 2; // inserted in test_data_inserts.sql file
		
		$addedCat = $catService->addCategory($parentCat, $newCat, $user);
		
		if(!$addedCat) {
			$this->fail($catService->getLastException()->getMessage());
		}
		$this->assertTrue(($addedCat instanceof Category));
		$this->assertEquals(2, $addedCat->getParent()->getUID());
    }
    
    public function testCanNotAddCategoryToTreeWithSameUrlPath() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		// new cat
		$newCat = array();
		$newCat['title'] = 'New Category';
		$newCat['path'] = 'test-category-1'; // not unique
		$newCat['description'] = 'Test Description';
		
		$parentCat = 2; // inserted in test_data_inserts.sql file
		
		$addedCat = $catService->addCategory($parentCat, $newCat, $user);
		
		if($addedCat) {
			$this->fail('addCategory returned true when it should return false');
		}
    }
    
    public function testCanLoadProductCategories() {
    
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$product = AllTests::getService('ProductService')->getByUid(1);
		
		AllTests::insertProductCategoryLink(1, 2);
		AllTests::insertProductCategoryLink(1, 3);
		
		$categories = $catService->getProductCategories($product);

		$this->assertTrue(($categories instanceof CategoryCollection));
		$this->assertEquals(2, $categories->count());
    }
    
    public function testCanMoveCategoryToAnotherCategory() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		$catUid = 3;
		
		// check parent cat
		$category = $catService->getByUid($catUid);
		//$category->setParent(1);
		$this->assertEquals(1, $category->getParent()->getUID());
		
		$newParentCatUid = 2;
		$category = $catService->moveCategory($catUid, $newParentCatUid, $user);
		if(!$category) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		//$category->setParent(2);
		$this->assertEquals(2, $category->getParent()->getUID());
    }
    
    public function testCanGetCategoriesByParentUid() {
    	
	    /* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$catColl = $catService->getCategoriesByParentUid(1);
		
		if(!$catColl) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		$this->assertTrue($catColl instanceof CategoryCollection);
		$this->assertEquals(2, $catColl->count());
    }
    
    public function testCanEditCategory() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		$category = $catService->getByUid(2);
		
		// check data
		$this->assertEquals('Test Category 1', $category->getTitle());
		$this->assertEquals('test-category-1', $category->getPath());
		$this->assertEquals('Test Category Description 2', $category->getDescription());
		
		// updated category data
		$updateData = array();
		$updateData['title'] = 'Test Category Updated';
		$updateData['path'] = 'test-path';
		$updateData['uid'] = 2;
		$updateData['description'] = 'Updated Description';
		
		$category = $catService->updateCategory($updateData, $user);
		if(!$category) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		$this->assertEquals('Test Category Updated', $category->getTitle());
		$this->assertEquals('test-path', $category->getPath());
		$this->assertEquals('Updated Description', $category->getDescription());
    }
    
    public function testCanNotEditCategoryWithNonUniquePath() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		// updated category data
		$updateData = array();
		$updateData['title'] = 'Test Category Updated';
		$updateData['path'] = 'test-category-1';
		$updateData['uid'] = 2;
		$updateData['description'] = 'Updated Description';
		
		$category = $catService->updateCategory($updateData, $user);
		if($category) {
			$this->fail('updateCategory returned false when it should\'nt');
		}
    }
    
    public function testCanDeleteCategory() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$result = $catService->deleteCategory(2);
		if(!$result) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		if($catService->getByUid(2)) {
			$this->fail('Category still exists');
		}
    }
    
    public function testCanGetCorrectCategoryPath() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$paths = $catService->getCategoryPath(2);
		if(!$paths) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		$path = new CategoryPath();
		$this->assertEquals('Products > Test Category 1', $path->getPath($paths, ' > '));
    }
    
    public function testCanAddProductToCategories() {
    
    	/* @var $catService ProductService */
		$prodService = AllTests::getService('ProductService');
		
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$product = $prodService->getByUid(1);
		$catUids = array(2, 3);
		$updated = $catService->addProductToCategories($catUids, $product);
		if(!$updated) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		$product = $prodService->getByUid(1);
		$categories = $product->getCategories();
		$this->assertTrue(($categories instanceof CategoryCollection));
		$this->assertEquals(2, $categories->count());
    }
    
    public function testCanUpdateProductCategories() {
    	
    	/* @var $prodService ProductService */
		$prodService = AllTests::getService('ProductService');
		
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$prodUid = 1;
		AllTests::insertProductCategoryLink($prodUid, 2);
		AllTests::insertProductCategoryLink($prodUid, 3);
		
		// check cats before update
		$product = $prodService->getByUid($prodUid);
		$this->assertEquals(2, $product->getCategories()->count());
		
		$updatedCatUids = array(2);
		$currentCatUids = array(2, 3);
		
		$updated = $catService->updateProductCats($product, $updatedCatUids, $currentCatUids);
		if(!$updated) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		// check cats after update
		$product->setCategories($prodUid);
		$newCategories = $product->getCategories();
		$this->assertEquals(1, $newCategories->count());
		
		$this->assertTrue($updated);
    }
    
    public function testCanGetChildCategories() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$children = $catService->getCategoryChildren(1);
		if(!$children) {
			$this->fail($catService->getLastException()->getMessage());
		}
		
		$this->assertTrue(($children instanceof CategoryCollection));
		$this->assertEquals(3, $children->count()); // will return parent cat aswell
    }
    
    
    public function testCanLoadCategoryByPath() {
    	
		/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		$category = $catService->getByPath('test-category-1');
		if(!$category) {
			$this->fail($catService->getLastException()->getMessage());
		}
				
		$this->assertTrue(($category instanceof Category));
		$this->assertEquals(2, $category->getUID());
		$this->assertEquals('Test Category 1', $category->getTitle());
		$this->assertEquals('test-category-1', $category->getPath());
		$this->assertEquals('Test Category Description 2', $category->getDescription());
		$this->assertNotNull($category->getParent());
		$this->assertEquals(2, $category->getLft());
		$this->assertEquals(3, $category->getRgt());
		$this->assertEquals(1, $category->getCreatedBy()->getUID());
		$this->assertNull($category->getLastEditied());
		$this->assertNotNull($category->getCreated());		
    }
    
    public function testCanNotAddCategoryToTreeWhenNonExistantParentCatGiven() {
    	
    	/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid(1);
		
		// new cat
		$newCat = array();
		$newCat['title'] = 'New Category';
		$newCat['path'] = 'a-new-cat';
		$newCat['description'] = 'Test Description';
		
		$parentCat = 12; // doesn't exist
		
		$addedCat = $catService->addCategory($parentCat, $newCat, $user);
		
		if($addedCat instanceof \Supa\Category\Category) {
			$this->fail('A cat was created with non existant parent Id');
		}
		
		// check it hasn't fucked up the table
		$children = $catService->getCategoryChildren(1);
		if(!$children) {
			$this->fail($catService->getLastException()->getMessage());
		}
		$this->assertTrue(($children instanceof CategoryCollection));
		$this->assertEquals(3, $children->count());
        $root = $catService->getByUid(1);
		$this->assertEquals(1, $root->getLft());
		$this->assertEquals(6, $root->getRgt());
        $knownCat = $catService->getByUid(2);
		$this->assertEquals(2, $knownCat->getLft());
		$this->assertEquals(3, $knownCat->getRgt());
        $knownCat2 = $catService->getByUid(3);
		$this->assertEquals(4, $knownCat2->getLft());
		$this->assertEquals(5, $knownCat2->getRgt());
    }
}