<?php
$root = dirname(dirname(__DIR__));
$root = str_replace('\\', '/', $root);
require_once($root . '/config.php');

use Supa\Category\CategoryService;
use Supa\Category\CategoryTree;
use Supa\Category\CategoryPath;

/* @var $catService CategoryService */
$catService = $sm->get('CategoryService');

echo "Load category tree:<br />";

//Debug::dump($catService, '', true);

$categories = $catService->getCategoriesInTreeByUid(45);
if(!$categories) {
	echo $catService->getLastException()->getMessage();
	exit;
}
$tree = new CategoryTree();
echo $tree->getTree($categories);
