<?php

/**
 * Search test suite
 */
class SearchTestSuite extends PHPUnit_Framework_TestSuite {

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('SearchTestSuite' );
		$this->addTestSuite('SearchTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self();
	}
}