<?php

use Supa\Search\Collection as SearchCollection;
/**
 * @backupGlobals disabled
 */
class SearchTest extends PHPUnit_Framework_TestCase {
    
    protected $searchService;
    	
    public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->searchService = AllTests::getService('SearchService');
	}
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
	// db is prepped and should return 2 rows
    public function testValidSearchQueryWillReturnNonEmptyProductCollection() {
        $products = $this->searchService->doProductSearch('Product');
        $this->assertTrue($products instanceof SearchCollection);
        $this->assertEquals(2, $products->count());
    }
    
    public function testInValidSearchQueryWillReturnEmptyProductCollection() {
        $products = $this->searchService->doProductSearch('xxx');
        $this->assertTrue($products instanceof SearchCollection);
        $this->assertEquals(0, $products->count());
    }
    
    public function testWillReturnFalseOnInValidQueryStringInput() {
        $products = $this->searchService->doProductSearch('dd22//?');
        $this->assertFalse($products);      
    }
    
    public function testWillReturnFalseIfEmptyStringGiven() {
        $products = $this->searchService->doProductSearch('');
        $this->assertFalse($products);      
    }
}