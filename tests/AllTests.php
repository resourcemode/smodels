<?php

$root = dirname(__DIR__);
$root = str_replace('\\', '/', $root);

require_once('db_test.php');
require_once('config.php');
require_once('config_tests.php');

// prooduct
require_once('Product/ProductTestSuite.php');
require_once('Product/ProductTest.php');
require_once('Product/ProductAttrTest.php');
require_once('Product/ProductAttrValuesTest.php');

// category
require_once('Category/CategoryTestSuite.php');
require_once('Category/CategoryTest.php');

// user
require_once('User/UserTestSuite.php');
require_once('User/UserTest.php');
require_once('User/CustomerTest.php');

// page
require_once('Page/PageTestSuite.php');
require_once('Page/PageTest.php');

// image
require_once('Image/ImageTestSuite.php');
require_once('Image/ImageTest.php');

// discount code
require_once('DiscountCode/DiscountCodeTestSuite.php');
require_once('DiscountCode/DiscountCodeTest.php');

// comments, reviews
require_once('Comment/CommentTestSuite.php');
require_once('Comment/CommentTest.php');
require_once('Comment/ReviewTest.php');

// search
require_once('Search/SearchTestSuite.php');
require_once('Search/SearchTest.php');

// rest
require_once('Rest/RestTestSuite.php');
require_once('Rest/RestCategoryTest.php');
require_once('Rest/RestProductTest.php');
require_once('Rest/RestReviewTest.php');

// auth
require_once('Auth/AuthTestSuite.php');
require_once('Auth/AuthTest.php');

// basket
require_once('Basket/BasketTestSuite.php');
require_once('Basket/BasketTest.php');
require_once('Basket/BasketSessionTest.php');

use Supa\User\User;
use Supa\User\Service as UserService;
use Supa\Product\Product;
use Supa\Product\Service as ProductService;
use Supa\Product\Attribute\Service as ProductAttributeService;
use Supa\Category\Service as CategoryService;
use Supa\MysqlDatabase;

/**
 * Static test suite.
 */
class AllTests extends PHPUnit_Framework_TestSuite {
    
    static $identityMap = array();
    
	public function __construct(){
		$this->setName('TestSuite');
		$this->addTestSuite('ImageTestSuite');
		$this->addTestSuite('PageTestSuite');
		$this->addTestSuite('UserTestSuite');
		$this->addTestSuite('ProductTestSuite');
		$this->addTestSuite('CategoryTestSuite');
		$this->addTestSuite('DiscountCodeTestSuite');
		$this->addTestSuite('CommentTestSuite');
		$this->addTestSuite('SearchTestSuite');
		$this->addTestSuite('RestTestSuite');
		$this->addTestSuite('AuthTestSuite');
		$this->addTestSuite('BasketTestSuite');
	}
	
	public static function suite(){
		return new self();
	}
	
	/**
	 * Execute an SQL file on the db
	 * 
	 * @param string $filename The file name (full path) of SQL dump to execute
	 */
	public static function executeSQLFile($filename) {
		
		if (!file_exists($filename)){
			return false;
		}

		$db = MysqlDatabase::getConn();
		
		$sql = file_get_contents ( $filename );
		$sql = explode(';', $sql);
		foreach( $sql as $sql_stmt ) {
			$sql_stmt = trim($sql_stmt);
			if (!empty($sql_stmt)) {
				try{
					$db->exec($sql_stmt);
				}catch(Exception $e){
					echo "\nStatement:\n".$sql_stmt."\nError:\n".$e->getMessage()."\n";
				}
			}
		}
	}

    /**
     * Truncate (empty) all db tables (with or without rows) in db
     * 
     * @param bool $withRows Whether to delete tables with rows
     * @return string A success or error message
     */
	public static function truncateTables($withRows = true) {
		
		/* @var $db PDO */
		$db = MysqlDatabase::getConn();		
	
		$sql = 'SELECT table_name FROM INFORMATION_SCHEMA.tables WHERE table_schema = ?';
		if(!$withRows) {
			$sql .= ' AND table_rows > 0';
		}
		
		/* @var $sth PDOStatement */
        $sth = $db->prepare($sql);
		if (!$sth->execute(array(DB_NAME))) return 'Error executing statement when getting schema tables in truncateTables()';
        
		$tables = $sth->fetchAll(PDO::FETCH_COLUMN);
		
		$tblCount = count($tables);
		
		if ($tblCount > 0) {
			$db->query('SET FOREIGN_KEY_CHECKS=0');
			for($i = 0; $i < $tblCount; $i++) {
				$db->exec('TRUNCATE `'.$tables[$i].'`');
			}
			$db->query('SET FOREIGN_KEY_CHECKS=1');
		}
		
		return 'OK';
	}
	
	public static function insertProductAttribute($attributeName) {
		
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$productAttr = $prodAttrService->createAttribute($attributeName);
		if(!$productAttr) {
			$messages = $prodAttrService->getMessages();
			if(!empty($messages)) {
				dump($messages);
				exit;
			}else {
				echo $prodAttrService->getLastException()->getMessage();
				exit;
			}
		}
		
		return $productAttr->getUID();
	}
	
	public static function insertUser(array $data = null) {
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		
		$user = $userService->createUser($data);
		if(!$user) {
			throw new Exception($userService->getLastException()->getMessage());
		}
		
		return $user->getUID();
	}
	
	public static function insertProduct(array $prodData = null, $userUid) {
		
		/* @var $prodService ProductService */
		$prodService = AllTests::getService('ProductService');
		
		/* @var $userService UserService */
		$userService = AllTests::getService('UserService');
		$user = $userService->getByUid($userUid);
		
		$product = $prodService->createProduct($prodData, $user);
		if(!$product) {
			throw new Exception($prodService->getLastException()->getMessage());
		}
		
		return $product->getUID();
    }
    
	public static function insertProductCategoryLink($prodUid, $catUid) {
		
		/* @var $catService CategoryService */
		$catService = AllTests::getService('CategoryService');
		
		/* @var $prodService ProductService */
		$prodService = AllTests::getService('ProductService');
		
		$updated = $catService->addProductToCategories(array($catUid), $prodService->getByUid($prodUid));
		if(!$updated) {
			throw new Exception($prodService->getLastException()->getMessage());
		}
		
		return true;
	}
	
	public static function addAttributeValuesToProduct($attrValueUids, $product) {
		
		/* @var $prodAttrService ProductAttributeService */
		$prodAttrService = AllTests::getService('ProductAttributeService');
		
		$added = $prodAttrService->updateAttrValuesForProduct($product, $attrValueUids);
		if(!$added) {
			throw new Exception($prodService->getLastException()->getMessage());
		}
		
		return $added;
	}
    
    public static function getService($serviceName) {
        if(isset(AllTests::$identityMap[$serviceName])) {
            return AllTests::$identityMap[$serviceName];
        }
    	$service = AppServiceManager::getInstance()->get($serviceName);
    	AllTests::$identityMap[$serviceName] = $service;
    	return $service;
    }
}