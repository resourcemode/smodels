<?php

/**
 * Rest test suite
 */
class RestTestSuite extends PHPUnit_Framework_TestSuite {

	/**
	 * Constructs the test suite handler.
	 */
	public function __construct() {
		$this->setName('RestTestSuite' );
		$this->addTestSuite('RestCategoryTest');
		$this->addTestSuite('RestProductTest');
		$this->addTestSuite('RestReviewTest');
	}

	/**
	 * Creates the suite.
	 */
	public static function suite() {
		return new self();
	}
}