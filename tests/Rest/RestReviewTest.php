<?php

/**
 * @backupGlobals disabled
 */
class RestReviewTest extends PHPUnit_Framework_TestCase {
    
    protected $restReviewService;
    
    public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->restReviewService = AllTests::getService('RestReviewService');
	}
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/review.sql');
	}
	
	// sucess, info, messages, result
	public function testCanGetChildCategories() {
	    
	    // should return 1 review
	    $this->restReviewService->setDataFormat('array');
        $data = $this->restReviewService->getAllProductReviews();
        
        $this->assertTrue(is_array($data));
        $this->assertTrue(isset($data['success']));
        $this->assertTrue($data['success']);
        $this->assertTrue(isset($data['result']));
        $this->assertTrue(is_array($data['result']));
        $this->assertEquals(1, count($data['result']));
        
	}
}