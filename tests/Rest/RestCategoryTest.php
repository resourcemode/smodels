<?php

/**
 * @backupGlobals disabled
 */
class RestCategoryTest extends PHPUnit_Framework_TestCase {
    
    protected $restCategoryService;
    
    public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->restCategoryService = AllTests::getService('RestCategoryService');
	}
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test_data_inserts.sql');
	}
	
	// sucess, info, messages, result
	public function testCanGetChildCategories() {
	    
	    // should return 3 cats
	    $this->restCategoryService->setDataFormat('array');
        $data = $this->restCategoryService->getCategoryChildren(1); // root
        
        $this->assertTrue(is_array($data));
        $this->assertTrue(isset($data['success']));
        $this->assertTrue($data['success']);
        $this->assertTrue(isset($data['result']));
        $this->assertTrue(is_array($data['result']));
        $this->assertEquals(3, count($data['result']));
        
	}
}