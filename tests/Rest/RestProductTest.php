<?php

/**
 * @backupGlobals disabled
 */
class RestProductTest extends PHPUnit_Framework_TestCase {
    
    protected $restProductService;
    
    public function __construct($name = NULL, array $data = array(), $dataName = '') {
		parent::__construct($name, $data, $dataName);
		$this->restProductService = AllTests::getService('RestProductService');
	}
	
	public function tearDown() {
		AllTests::truncateTables();			
	}
	
	public function setUp() {
		AllTests::truncateTables();
		AllTests::executeSQLFile('C:/wamp/www/smodels/smodels/sql/test/product.sql');
	}
	
	// sucess, info, messages, result
	public function testCanGetProducts() {
	    
	    // should return 2 products
	    $this->restProductService->setDataFormat('array');
        $data = $this->restProductService->getProducts();
        
        $this->assertTrue(is_array($data));
        $this->assertTrue(isset($data['success']));
        $this->assertTrue($data['success']);
        $this->assertTrue(isset($data['result']));
        $this->assertTrue(is_array($data['result']));
        $this->assertEquals(2, count($data['result']));
        
	}
}