<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

use Zend\Authentication\AuthenticationService;
use Application\Auth\Store as AppAuthStore;
use Application\Auth\Auth as AppAuth;

return array(
    'router' => array(
        'routes' => array( // LIFO
            'auth' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/auth[/:action[/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Auth',
                        'action' => 'index',
                    ),
                ),
            ),
            'search' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search[/:action[/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Search',
                        'action' => 'index',
                    ),
                ),
            ),
            'review' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/review[/:action[/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Review',
                        'action' => 'index',
                    ),
                ),
            ),
            'category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/category[/:action[/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Category',
                        'action' => 'index',
                    ),
                ),
            ),
            'product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product[/:action[/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Product',
                        'action' => 'index',
                    ),
                ),
            ),
            'basket' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/basket[/:action[/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Application\Controller\Basket',
                        'action' => 'index',
                    ),
                ),
            ),
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'AppAuth' => function ($sm) {
                $appAuth = new AppAuth();
                $appAuth->setAuthService($sm->get('SupaAuthService')); // 'SupaAuthService' is from vendor/Supa
                return $appAuth;
            },
            'AppAuthStore' => function ($sm) {
                return new AppAuthStore('supa_app');
            },
            'AppAuthService' => function($sm) {
                $authService = new AuthenticationService();
                $authService->setAdapter($sm->get('AppAuth'));
                $authService->setStorage($sm->get('AppAuthStore'));
                
                return $authService;
            },
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Product' => 'Application\Controller\ProductController',
            'Application\Controller\Category' => 'Application\Controller\CategoryController',
            'Application\Controller\Review' => 'Application\Controller\ReviewController',
            'Application\Controller\Search' => 'Application\Controller\SearchController',
			'Application\Controller\Auth' => 'Application\Controller\AuthController',
			'Application\Controller\Basket' => 'Application\Controller\BasketController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/500'               => __DIR__ . '/../view/error/500.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        )
    ),
    'view_helpers' => array(
        'factories' => array(
            'ImageSlider' => function($sm) {
                $helper = new \Application\Helper\ImageSlider();
                return $helper;
            },
            'productLinkList' => function($sm) {
                $helper = new \Application\Helper\ProductLinkList();
                return $helper;
            },
            'productList' => function($sm) {
                $helper = new \Application\Helper\ProductList();
                return $helper;
            },
            'footerNav' => function($sm) {
                $helper = new \Application\Helper\FooterNav();
                return $helper;
            },
            'MainNav' => function($sm) {
                $helper = new \Application\Helper\MainNav();
                return $helper;
            },
        )
    )
);
