<?php

namespace Application\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthenticationResult;

class Auth implements AdapterInterface {

    protected $authService;
    
    protected $username;
    
    protected $password;
    
    /**
     * Constructor
     *
     */
    public function __construct() { }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate() {

        $user = $this->authService->doLogin($this->username, $this->password);
        $code = 1;
        $identity = $this->username;
        
        if(!$user) {
            $message = $this->authService->getLastException()->getMessage();
            $code = 0;
            $identity = '';
            //throw new \Zend\Authentication\Adapter\Exception\RuntimeException($message);
        }
        
        // TODO: set storage!

        $result = new AuthenticationResult($code, $identity);

        return $result;
    }
    
    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }
    
    public function getUsername() {
        return $this->username;
    }
    
    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }
    
    public function getPassword() {
        return $this->password;
    }
    
    public function setAuthService(\Supa\Auth\Service $authService) {
        $this->authService = $authService;
    }
    
    public function getAuthService() {
        return $this->authService;
    }
}