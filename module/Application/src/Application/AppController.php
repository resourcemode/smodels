<?php
namespace Application;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Model\ViewModel;

class AppController extends AbstractActionController {
    
    /**
     * Main nav view helper
     * @var AbstractHelper
     */
    protected $mainNavHelper = null;
    
    /**
     * Shared view model for controllers
     * @var ViewModel;
     */
    protected $viewModel;
    
    public function __construct() {
        $this->viewModel = new ViewModel();
    }
    
    protected function getMainNavViewHelper() {
        
        if($this->mainNavHelper instanceof AbstractHelper) {
            return $this->mainNavHelper;
        }
        
        $services = $this->getServiceLocator();
        $categoryService = $services->get('CategoryService');
        $this->mainNavHelper = $services->get('ViewHelperManager')->get('MainNav');
        $this->mainNavHelper->setMainNavItems($categoryService->getCategoryChildren(1));
        return $this->mainNavHelper;
    }
    
    protected function getViewModelWithMainNavViewHelperSet() {
        $this->viewModel->setVariable('mainNav', $this->getMainNavViewHelper());
        return $this->viewModel;
    }
}
