<?php

namespace Application\Helper;

use \Exception;
use \ArrayIterator;

class MainNav extends \Zend\View\Helper\AbstractHelper  {
    
    /**
     * Main nav link objects
     * @var ArrayIterator
     */
    protected $mainNavItems;
    
    public function mainNav(array $options = array()) {
        
        if(!($this->mainNavItems instanceof ArrayIterator) || $this->mainNavItems->count() == 0) {
            return "";
        }
        
        $html = '<nav id="nav" class="nav reveal">';
        $html .= '<ul role="navigation">';
        foreach($this->mainNavItems as $cat) {
            if($cat->getTitle() == 'Products') continue;
            $uid = $cat->getUid();
            $url = $this->view->url('category', array('action' => 'index', 'id' => $uid));
            $html .= '<li><a href="'.$url.'">';
            $html .= htmlspecialchars($cat->getTitle(), ENT_QUOTES, 'UTF-8');
            $html .= '</a></li>';
        }
        $html .= '</ul>';
        $html .= '</nav>';
        
        return $html;
    }
    
    /**
     * Turn helper into string
     *
     * @return string
     */
    public function toString() {
        return (string) $this->mainNav();
    }

    /**
     * Cast to string
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }
    
    public function setMainNavItems(\ArrayIterator $mainNavItems) {
        $this->mainNavItems = $mainNavItems;
    }
}