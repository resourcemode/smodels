<?php

namespace Application\Helper;

use \Zend\View\Helper\AbstractHelper;
use \Exception;
use \ArrayIterator;

class ProductList extends AbstractHelper  {
    
    /**
     * Product objects
     * @var ArrayIterator
     */
    protected $products;
    
    public function productList(array $options = array()) {
        
        if(!($this->products instanceof ArrayIterator) || $this->products->count() == 0) {
            return "0 Products to list.";
        }
        
        $html = '<div id="p-reviews" aria-labeledby="tab-reviews">';
        $html .= '<ol class="reviews-list">';
        foreach($this->products as $prod) {
            $uid = $prod->getUid();
            $url = $this->view->url('product', array('action' => 'index', 'id' => $uid));
            $html .= '<li>';
            $html .= '<img src="/images/avatar.png" alt="Commenter Name" />';
            
            $html .= '<div class="review-meta">';
            $html .= '<ol class="star">';
            $html .= '<li class="on">&#9733;</li><li class="on">&#9733;</li><li class="on">&#9733;</li><li class="on">&#9733;</li><li>&#9734;</li>';
            $html .= '</ol>';
            $html .= '<h3><a href="'.$url.'">' . htmlspecialchars($prod->getTitle(), ENT_QUOTES, 'UTF-8') . '</a></h3>';
            $html .= '</div>';
            
            $html .= '<div class="review-content">';
            $html .= '<p>' . htmlspecialchars($prod->getDescription(), ENT_QUOTES, 'UTF-8') . '</p>';
            $html .= '</div>';
            
            $html .= '</li>';
        }
        $html .= '</ol>';
        $html .= '</div>';
        
        return $html;
                
//return <<<'END_OF_HTML'
//        <div id="p-reviews" aria-labeledby="tab-reviews">
//            <ol class="reviews-list">
//                <li>
//                    <img src="/images/avatar.png" alt="Commenter Name" />
//                    <div class="review-meta">
//                        <ol class="star">
//                            <li class="on">&#9733;</li>
//                            <li class="on">&#9733;</li>
//                            <li class="on">&#9733;</li>
//                            <li class="on">&#9733;</li>
//                            <li>&#9734;</li>
//                        </ol>
//                        <h3><a href="">Awesome shirt!</a></h3>
//                    </div>
//                    <div class="review-content">
//                    	<p>This shirt looks awesome and is really comfortable to wear. It did shrink quite a lot when washed, but that could have just been how I ran it. All in all, it's my favourite shirt, and I love wearing it.</p>
//                    </div>
//                </li>
//            </ol>
//        </div>
//END_OF_HTML;
    }
    
    /**
     * Turn helper into string
     *
     * @return string
     */
    public function toString() {
        return (string) $this->productList();
    }

    /**
     * Cast to string
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }
    
    public function setProducts(\ArrayIterator $products) {
        $this->products = $products;
    }
}