<?php

namespace Application\Helper;

use \Zend\View\Helper\AbstractHelper;
use \Exception;

class ImageSlider extends AbstractHelper  {
    
    public function __invoke($images = null, array $options = array()) {
        
        if(is_null($images)) {
            return 'TODO (products witout images)';
        }
        
        $current = $images->current();
        $initial = $current->getFilename('t', 561, 378);
        
        $html = '';
        $html .= '<div id="product-img" class="product-img">';
        $html .= '<figure class="img-container" id="img-container">';
        $html .= '<img src="/images/product/'.$initial.'" alt="Super Ffly T-shirt" />';
        $html .= '</figure>';
        $html .= '<nav>';
        $html .= '<ul>';
        foreach($images as $image) {
            $main = $image->getFilename('t', 561, 378);
            $thumb = $image->getFilename('t', 94, 63);
            $html .= '<li>';
            $html .= '<a href="/images/product/'.$main.'">';
            $html .= '<img src="/images/product/'.$thumb.'" alt="Super Ffly Men\'s Shirt" />';
            $html .= '</a>';
            $html .= '</li>';
        }
        $html .= '</ul>';
        $html .= '</nav>';
        $html .= '</div>';
        return $html;
    }
}