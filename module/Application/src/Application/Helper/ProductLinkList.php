<?php

namespace Application\Helper;

use \Zend\View\Helper\AbstractHelper;
use \Exception;
use \ArrayIterator;

class ProductLinkList extends AbstractHelper  {
    
    /**
     * Product objects
     * @var ArrayIterator
     */
    protected $products;
    
    public function productLinkList(array $options = array()) {
        
        if(!($this->products instanceof ArrayIterator) || $this->products->count() == 0) {
            return "0 Products to list.";
        }
        
        $html = '<div id="related-list" class="related-list">';
        $html .= '<ul>';
        foreach($this->products as $prod) {
            $html .= '<li><a href="#">';
            $html .= '<img alt="Product Name" src="/images/related_1.jpg">'; // $prod->getImage(...)?
            $html .= '</a></li>';
        }
        $html .= '</ul>';
        $html .= '</div>';
        
        return $html;
    }
    
    /**
     * Turn helper into string
     *
     * @return string
     */
    public function toString() {
        return (string) $this->productLinkList();
    }

    /**
     * Cast to string
     *
     * @return string
     */
    public function __toString() {
        return $this->toString();
    }
    
    public function setProducts(\ArrayIterator $products) {
        $this->products = $products;
    }
}