<?php
namespace Application\Controller;

use Application\AppController;
use Zend\View\Model\ViewModel;

use \Supa\Product\Service;

class ProductController extends AppController {
	
    public function __construct() {
        parent::__construct();
    }
    
    public function indexAction() {
        
        $productUid = $this->getEvent()->getRouteMatch()->getParam('id');
        if(is_null($productUid) || !is_numeric($productUid)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $vm = new ViewModel(); //$this->getViewModelWithMainNavViewHelperSet();  
        // TODO: use js "aux" method? generate HTML page from admin?
                
        $services = $this->getServiceLocator();
        
        /* @var $productService \Supa\Product\Service */ 
        $productService = $services->get('ProductService');
        $imageService = $services->get('ImageService');
        
        $product = $productService->getByUid($productUid);
        if(is_null($product)) {
            $this->getResponse()->setStatusCode(404);
            return;                
        }
        $vm->setVariable('product', $product);
        
        $products = $productService->getFeaturedProducts();
        if(!is_null($products)) {
            $productLinkList = $services->get('ViewHelperManager')->get('ProductLinkList');
            $productLinkList->setProducts($products);
        }
        
        $images = $imageService->getProductImages($product);
        if(!is_null($images) && $images->count() > 0) {
            $vm->setVariable('images', $images);
        }
        
        return $vm;
    }
}