<?php
namespace Application\Controller;

use Application\AppController;
use Zend\View\Model\ViewModel;
use ArrayIterator;

class AuthController extends AppController {
	
    public function __construct() {
        parent::__construct();
    }

    public function indexAction() {
        return new ViewModel();
    }
    
    public function loginAction() {
                
        $vm = new ViewModel();
        
        $services = $this->getServiceLocator();
        $authService = $services->get('AppAuthService');
        
        if ($authService->hasIdentity()) {
            $vm->setVariable('status', 'logged in');
            return $vm;
        }
        
        $status = 'NOT logged in';
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost()->getArrayCopy();
            $username = $post['u'];
            $password = $post['p'];
            $authService->getAdapter()->setUsername($username)->setPassword($password);
            $result = $authService->authenticate();
            if ($result->isValid()) {
                $authService->getStorage()->write($username);
                $status = 'Logged in';
            }else {
                $status = 'Invalid username and password combination';
            }
        }
        $vm->setVariable('status', $status);
        
        return $vm;
    }
}