<?php
namespace Application\Controller;

use Application\AppController;
use Zend\View\Model\ViewModel;

use Supa\Comment\Review\Service;

class ReviewController extends AppController {
	
    public function __construct() {
        parent::__construct();
    }
    
    public function indexAction() {
        $vm = new ViewModel();
        return $vm;
    }
    
    public function productAction() {
        
        $productUid = $this->getEvent()->getRouteMatch()->getParam('id');
        if(is_null($productUid) || !is_numeric($productUid)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
        
        $vm = new ViewModel();
        
        $services = $this->getServiceLocator();
        $reviewService = $services->get('ReviewService');        
        $productService = $services->get('ProductService');
        
        $product = $productService->getByUid($productUid);        
        if(is_null($product)) {
            $this->getResponse()->setStatusCode(404);
            return;                
        }
        
        /* @var $reviewService \Supa\Comment\Review\Service */
        $reviews = $reviewService->getProductReviews($product); 
        
        $vm->setVariable('reviews', $reviews);
        $vm->setVariable('product', $product);
        
        return $vm;                
    }
}