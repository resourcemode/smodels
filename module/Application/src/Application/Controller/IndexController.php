<?php
namespace Application\Controller;

use Application\AppController;
use Zend\View\Model\ViewModel;

class IndexController extends AppController {
	
    public function __construct() {
        parent::__construct();
    }
    
    public function indexAction() {
        $vm = new ViewModel(); //$this->getViewModelWithMainNavViewHelperSet();  
        
        // TODO: use js "aux" method? generate HTML page from admin?
        $services = $this->getServiceLocator();
        
        $productService = $services->get('ProductService');
        $pageService = $services->get('PageService');
        
        $products = $productService->getFeaturedProducts();
        if(!is_null($products)) {
            $productLinkList = $services->get('ViewHelperManager')->get('ProductLinkList');
            $productLinkList->setProducts($products);
        }
        
        $homePage = $pageService->getByUid(1);
        if(!is_null($homePage)) {
            $vm->setVariable('home', $homePage);
        }
        return $vm;
    }
}
