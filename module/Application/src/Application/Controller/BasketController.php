<?php
namespace Application\Controller;

use Application\AppController;
use Zend\View\Model\ViewModel;
use \ArrayIterator;

use Zend\Session\Container;

class BasketController extends AppController {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function indexAction() {
                
        $vm = new ViewModel();
        $request = $this->getRequest();
        
        $basketSession = new Container('basktez');
        $vm->setVariable('basket', $basketSession->products);
        return $vm;
    }
    
    public function addAction() {
                
        $vm = new ViewModel();
        $request = $this->getRequest();
        $vm->setVariable('message', 'Nothing to do');
        
        if ($request->isPost()) {
            
            $error = 'isPost';           
            // posted product uid to add to basket
            $post = $request->getPost()->getArrayCopy();
            $productUid = (int)$post['productUid'];
                        
            $services = $this->getServiceLocator();
            
            // 1. get Product
            $productService = $services->get('ProductService');
            $product = $productService->getByUid($productUid);
            if(!$product instanceof \Supa\Product\Product) {
                $error = 'Failed get product by passed uid';             
            }
                        
            // 2. set BasketProduct data
            $data = array();
            $data['qty'] = 1;
            $data['vat'] = 20;
            $data['notes'] = 'Test Notes';
            
            // 3. create BasketProduct and add to BasketSession
            $basketProduct = \Supa\Basket\Product\Factory::makeBasketProduct($product, $data);
            if(!($basketProduct instanceof \Supa\Basket\Product\Product)) {
                $error = 'addProductToBasket returned null';
            }
            $basketSession = new Container('basktez');
            if(isset($basketSession->products) && is_array($basketSession->products)) {
                $basketSession->products[] = $basketProduct;   
            }else {
                $basketSession->products = array($basketProduct); 
            }
        }
        
        return $this->redirect()->toRoute('basket', array('action'=>'index'));
    }
}