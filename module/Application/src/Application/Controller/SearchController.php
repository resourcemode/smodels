<?php
namespace Application\Controller;

use Application\AppController;
use Zend\View\Model\ViewModel;
use \ArrayIterator;

class SearchController extends AppController {
	
    public function __construct() {
        parent::__construct();
    }
    
    public function indexAction() {
                
        $vm = new ViewModel();
        $request = $this->getRequest();
        $vm->setVariable('query', 'You didn\'t search for anything');
        
        if ($request->isPost()) {
            
            $services = $this->getServiceLocator();
            $searchService = $services->get('SearchService');
            
            $post = $request->getPost()->getArrayCopy();
            $query = $post['search'];
            $vm->setVariable('query', "Searching for '$query'");
                            
            $products = $searchService->doProductSearch($query);
            
            if($products instanceof \ArrayIterator && $products->count() > 0) {
                $productList = $services->get('ViewHelperManager')->get('ProductList');
                $productList->setProducts($products);
            }
                     
        }
        
        return $vm;
    }
}