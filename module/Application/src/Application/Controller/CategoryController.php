<?php
namespace Application\Controller;

use Application\AppController;
use Zend\View\Model\ViewModel;

class CategoryController extends AppController {
	
    public function __construct() {
        parent::__construct();
    }
    
    public function indexAction() {
        
        $categoryUid = $this->getEvent()->getRouteMatch()->getParam('id');
        if(is_null($categoryUid) || !is_numeric($categoryUid)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }
                
        $vm = new ViewModel(); //$this->getViewModelWithMainNavViewHelperSet();
        // TODO: use js "aux" method? generate HTML page from admin?
        
        $services = $this->getServiceLocator();
        $productService = $services->get('ProductService');
        $categoryService = $services->get('CategoryService');
        
        $category = $categoryService->getByUid($categoryUid);        
        if(is_null($category)) {
            $this->getResponse()->setStatusCode(404);
            return;                
        }
        $vm->setVariable('category', $category);
                
        $products = $productService->getProductsByCatUid($categoryUid);
        // TODO: may not need to do as 'getProductsByCatUid' returns an
        // empty ArrayIterator object, which we check in view helper anyway
        if(!is_null($products)) {
            $productList = $services->get('ViewHelperManager')->get('ProductList');
            $productList->setProducts($products);
        }
        
        return $vm;
        
    }
}