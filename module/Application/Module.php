<?php
namespace Application;

use Zend\ServiceManager\Config;
use Zend\ServiceManager\ServiceManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
//use Zend\Session\Container;
//use Zend\Session\SessionManager;

use Supa\SupaServiceManager;

class Module implements \Zend\ModuleManager\Feature\ServiceProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        
        //$manager = new SessionManager();
        //Container::setDefaultManager($manager);

        $e->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
    
    public function getServiceConfig() {
        $sm = new SupaServiceManager();
        $appServices = $sm->getServices();
        return $appServices;
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
