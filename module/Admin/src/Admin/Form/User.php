<?php
namespace Admin\Form;

use Zend\Form\Form;

class User extends Form {
	
    public function __construct($name = null) {
    	
        // we want to ignore the name passed
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'uid',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'firstname',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Firstname',
            ),
        ));
        $this->add(array(
            'name' => 'surname',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Surname',
            ),
        ));
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Username',
            ),
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Password',
            ),
        ));
        $this->add(array(
            'name' => 'passwordVerify',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Password again',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
            ),
            'options' => array(
                'label' => 'Email',
            ),
        ));
        $this->add(array(
        	'type' => 'Zend\Form\Element\Csrf',
        	'name' => 'csrf',
        	'options' => array(
        		'csrf_options' => array(
        			'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Submit',
                'id' => 'submitbutton',
            ),
        ));
        
    }
}