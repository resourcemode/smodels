<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Supa\User\Service;
use Admin\Form\User as UserForm;

class UserController extends AbstractActionController {
    
	public function indexAction() {

        $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $services = $this->getServiceLocator();
                
        /* @var $userService \Supa\User\Service */ 
        $userService = $services->get('UserService');
        $users = $userService->getUsers();
                
        return new ViewModel(array(
        	'users' => $users
        ));		
	}

	public function addAction() {
	    
	    $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $form = new UserForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $services = $this->getServiceLocator();
            
            /* @var $userService \Supa\User\Service */ 
            $userService = $services->get('UserService');

            $userData = $request->getPost()->getArrayCopy();
            
            $user = $userService->createUser($userData);
            
            if(!$user instanceof \Supa\User\User) {
                var_dump($userService->getLastException()->getMessage());
                $form->setData($request->getPost());
            }else {
                // redirect to list of users
                return $this->redirect()->toRoute('users');                
            }
        }
        
        return array('form' => $form);		
	}
	
	public function editAction() {
	            
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('users', array(
                'action' => 'add'
            ));
        }
               
	    $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $services = $this->getServiceLocator();
        
        /* @var $userService \Supa\User\Service */
        $userService = $services->get('UserService');
        
        $form = new UserForm();
        $form->get('submit')->setValue('Save');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $updateData = $request->getPost()->getArrayCopy();
            
            // here we check the id in url with the one in hidden form element, 
            // to make sure both the same
            $postUid = (int)$updateData['uid'];
            if($id !== $postUid) {
                // redirect to list of users
                return $this->redirect()->toRoute('users');                   
            }
            
            $user = $userService->updateUser($updateData);
    		if(!$user instanceof \Supa\User\User) {
    			var_dump($userService->getLastException()->getMessage());
    			var_dump($userService->getMessages());
                $form->setData($request->getPost());
    		}else {
                // redirect to list of users
                return $this->redirect()->toRoute('users');                
            }
    		
        }else {
            
        	/* @var $user \Supa\User\User */
            $user = $userService->getByUid($id);
            if(!$user instanceof \Supa\User\User) {
                return $this->redirect()->toRoute('users');                
            }
            $form->setData($user->toArray());
        }

        return array(
            'id' => $id,
            'form' => $form,
        );        		
	}
	
	public function deleteAction() {
		
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('users');
        }
	    
        $layout = $this->layout();
        $layout->setTemplate('admin/layout');
                
        $services = $this->getServiceLocator();
              
        /* @var $userService \Supa\User\Service */
        $userService = $services->get('UserService');
                
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            // here we check the id in url with the one in hidden form element, 
            // to make sure both the same
            $postUid = (int)$request->getPost('id');
            if($id !== $postUid) {
                // redirect to list of products
                return $this->redirect()->toRoute('users');                   
            }
            
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') {
                $deleted = $userService->deleteUser($postUid);
                if(!$deleted) {
        	        var_dump('A user was loaded that should have been deleted.');
        	        exit;
        	    }
            }

            // Redirect to list of products
            return $this->redirect()->toRoute('users');
        }
        
        $user = $userService->getByUid($id);
        
        return array(
            'id' => $id,
        	'user' => $user
        );
            		
	}
}