<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Supa\Product\Service;
use \Admin\Form\Product as ProductForm;

class ProductController extends AbstractActionController {
    
	public function indexAction() {

        $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $services = $this->getServiceLocator();
                
        /* @var $productService \Supa\Product\Service */ 
        $productService = $services->get('ProductService');
        $products = $productService->getProducts();
                
        return new ViewModel(array(
        	'products' => $products
        ));		
	}

	public function addAction() {
	    
	    $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $form = new ProductForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $services = $this->getServiceLocator();
            
            /* @var $productService \Supa\Product\Service */ 
            $productService = $services->get('ProductService');
            $userService = $services->get('UserService');

            $prodData = $request->getPost()->getArrayCopy();
            
            $product = $productService->createProduct($prodData, $userService->getByUid(1));
            
            if(!$product) {
                var_dump($productService->getLastException()->getMessage());
                $form->setData($request->getPost());
            }else {
                // redirect to list of products
                return $this->redirect()->toRoute('products');                
            }
        }
        
        return array('form' => $form);		
	}
	
	public function editAction() {
	            
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('products', array(
                'action' => 'add'
            ));
        }
               
	    $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $services = $this->getServiceLocator();
        
        /* @var $productService \Supa\Product\Service */
        $productService = $services->get('ProductService');
        
        $form = new ProductForm();
        $form->get('submit')->setValue('Save');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $userService = $services->get('UserService');
            
            $updateData = $request->getPost()->getArrayCopy();
            
            // here we check the id in url with the one in hidden form element, 
            // to make sure both the same
            $postUid = (int)$updateData['uid'];
            if($id !== $postUid) {
                // redirect to list of products
                return $this->redirect()->toRoute('products');                   
            }
            
            $product = $productService->updateProduct($updateData, $userService->getByUid(1));
    		if(!$product instanceof \Supa\Product\Product) {
    			var_dump($productService->getLastException()->getMessage());
    			var_dump($productService->getMessages());
                $form->setData($request->getPost());
    		}else {
                // redirect to list of products
                return $this->redirect()->toRoute('products');                
            }
    		
        }else {
            
        	/* @var $product \Supa\Product\Product */
            $product = $productService->getByUid($id);
            if(!$product instanceof \Supa\Product\Product) {
                return $this->redirect()->toRoute('products');                
            }
            $form->setData($product->toArray());
        }

        return array(
            'id' => $id,
            'form' => $form,
        );        		
	}
	
	public function deleteAction() {
		
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('products');
        }
	    
        $layout = $this->layout();
        $layout->setTemplate('admin/layout');
                
        $services = $this->getServiceLocator();
              
        /* @var $productService \Supa\Product\Service */
        $productService = $services->get('ProductService');
                
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            // here we check the id in url with the one in hidden form element, 
            // to make sure both the same
            $postUid = (int)$request->getPost('id');
            if($id !== $postUid) {
                // redirect to list of products
                return $this->redirect()->toRoute('products');                   
            }
            
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') {
                $productService->deleteProduct($postUid);
            }

            // Redirect to list of products
            return $this->redirect()->toRoute('products');
        }
        
        $product = $productService->getByUid($id);
        
        return array(
            'id' => $id,
        	'product' => $product
        );
            		
	}
}