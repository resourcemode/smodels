<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Supa\Category\Service;
use \Admin\Form\Category as CategoryForm;

class CategoryController extends AbstractActionController {
    
	public function indexAction() {

        $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $services = $this->getServiceLocator();
                
        /* @var $categoryService \Supa\Category\Service */ 
        $categoryService = $services->get('CategoryService');
        $cats = $categoryService->getCategoryChildren(1);

        $tree = new \Supa\Category\Tree();
        $treeHtml = $tree->getTree($cats);
        
        return new ViewModel(array(
        	'categories' => $cats,
            'treeHtml' => $treeHtml
        ));		
	}

	public function addAction() {
	    
	    $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $form = new CategoryForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $services = $this->getServiceLocator();
            
            /* @var $categoryService \Supa\Category\Service */ 
            $categoryService = $services->get('CategoryService');
            $userService = $services->get('UserService');

            $newCat = $request->getPost()->getArrayCopy();
    		
    		$parentCat = 2; // TODO: CHANGE!!
    		
    		$addedCat = $categoryService->addCategory($parentCat, $newCat, $userService->getByUid(1));
    		
    		if(!$addedCat) {
                var_dump($categoryService->getLastException()->getMessage());
                $form->setData($request->getPost());
    		}else {
                // redirect to list of categories
                return $this->redirect()->toRoute('categories');                
            }
        }
        
        return array('form' => $form);		
	}
	
	public function editAction() {
	            
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('categories', array(
                'action' => 'add'
            ));
        }
               
	    $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        $services = $this->getServiceLocator();
        
        /* @var $categoryService \Supa\Category\Service */
        $categoryService = $services->get('CategoryService');
        
        $form = new CategoryForm();
        $form->get('submit')->setValue('Save');
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $userService = $services->get('UserService');
            
            $updateData = $request->getPost()->getArrayCopy();
            
            // here we check the id in url with the one in hidden form element, 
            // to make sure both the same
            $postUid = (int)$updateData['uid'];
            if($id !== $postUid) {
                // redirect to list of categories
                return $this->redirect()->toRoute('categories');                   
            }
            
            $category = $categoryService->updateCategory($updateData, $userService->getByUid(1));
    		if(!$category instanceof \Supa\Category\Category) {
    			var_dump($categoryService->getLastException()->getMessage());
    			var_dump($categoryService->getMessages());
                $form->setData($request->getPost());
    		}else {
                // redirect to list of categories
                return $this->redirect()->toRoute('categories');                
            }
    		
        }else {
            
            $category = $categoryService->getByUid($id);
    		if(!$category instanceof \Supa\Category\Category) {
                return $this->redirect()->toRoute('categories');                
            }
            $form->setData($category->toArray());
        }

        return array(
            'id' => $id,
            'form' => $form,
        );        		
	}
	
	public function deleteAction() {
		
        $id = (int)$this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('categories');
        }
	    
        $layout = $this->layout();
        $layout->setTemplate('admin/layout');
                
        $services = $this->getServiceLocator();
              
        /* @var $categoryService \Supa\Category\Category */
        $categoryService = $services->get('CategoryService');
                
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            // here we check the id in url with the one in hidden form element, 
            // to make sure both the same
            $postUid = (int)$request->getPost('id');
            if($id !== $postUid) {
                // redirect to list of categories
                return $this->redirect()->toRoute('categories');                   
            }
            
            $del = $request->getPost('del', 'No');
            if ($del == 'Yes') {
                $categoryService->deleteCategory($postUid);
            }

            // Redirect to list of categories
            return $this->redirect()->toRoute('categories');
        }
        
        $category = $categoryService->getByUid($id);
        
        return array(
            'id' => $id,
        	'category' => $category
        );		
	}
}