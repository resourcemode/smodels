<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Admin\Model\Admin;
use Admin\Form\AdminForm;

class AdminController extends AbstractActionController {
	
	protected $adminTable;	
	
	public function indexAction() {

        $layout = $this->layout();
        $layout->setTemplate('admin/layout');
        
        return new ViewModel();		
	}

	public function addAction() {
        $form = new AdminForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $album = new Admin();
            $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $album->exchangeArray($form->getData());
                $this->getAdminTable()->saveAlbum($album);

                // Redirect to list of albums
                return $this->redirect()->toRoute('admin');
            }
        }
        return array('form' => $form);		
	}

	public function editAction() {
		
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin', array(
                'action' => 'add'
            ));
        }
        $album = $this->getAdminTable()->getAlbum($id);

        $form  = new AdminForm();
        $form->bind($album);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($album->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getAdminTable()->saveAlbum($form->getData());

                // Redirect to list of albums
                return $this->redirect()->toRoute('admin');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
        		
	}

	public function deleteAction() {
		
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('admin');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->getAdminTable()->deleteAlbum($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('admin');
        }
        
        $album = $this->getAdminTable()->getAlbum($id);
        
        return array(
            'id' => $id,
        	'album' => $album
        );
            		
	}
	
    public function getAdminTable() {
        if (!$this->adminTable) {
            $sm = $this->getServiceLocator();
            $this->adminTable = $sm->get('Admin\Model\AdminTable');
        }
        return $this->adminTable;
    }
}