<?php
namespace Admin;

use Admin\Model\Admin;
use Admin\Model\AdminTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Supa\SupaServiceManager;

class Module {
	
    public function onBootstrap($e) {
    	
        // Register a dispatch event
        //$app = $e->getParam('application');
        //$app->getEventManager()->attach('dispatch', array($this, 'setLayout'));
    }

    public function setLayout($e){
    	
//        $matches    = $e->getRouteMatch();
//        $controller = $matches->getParam('controller');
//        if (false !== strpos($controller, __NAMESPACE__)) {
//            // not a controller from this module
//            return;
//        }
//
//        // Set the layout template
//        $viewModel = $e->getViewModel();
//        $viewModel->setTemplate('admin/layout');
    }
    
    public function getServiceConfig() {
        $sm = new SupaServiceManager();
        $appServices = $sm->getServices();
        return $appServices;
    }
    	
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }
}