<?php
return array(
	'Rest\Module'                                  => __DIR__ . '/Module.php',
	'Rest\PostProcessor\AbstractPostProcessor'     => __DIR__ . '/src/Rest/PostProcessor/AbstractPostProcessor.php',
	'Rest\PostProcessor\Json'                      => __DIR__ . '/src/Rest/PostProcessor/Json.php',
	'Rest\PostProcessor\Image'                     => __DIR__ . '/src/Rest/PostProcessor/Image.php',

	'Rest\Controller\InfoController' => __DIR__ . '/src/Rest/Controller/InfoController.php'
);
