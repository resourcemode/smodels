<?php
return array(
	'errors' => array(
		'post_processor' => 'json-pp',
		'show_exceptions' => array(
			'message' => true,
			'trace'   => true
		)
	),
	'di' => array(
		'instance' => array(
			'alias' => array(
				'json-pp'  => 'Rest\PostProcessor\Json',
				'image-pp' => 'Rest\PostProcessor\Image',
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'info' => 'Rest\Controller\InfoController',
			'review' => 'Rest\Controller\ReviewController',
			'product' => 'Rest\Controller\ProductController',
			'category' => 'Rest\Controller\CategoryController',
		)
	),
	'router' => array(
		'routes' => array(
			'restful' => array(
				'type'    => 'Zend\Mvc\Router\Http\Segment',
				'options' => array(
					'route'       => '/api/[:controller][.:formatter][/:id]',
					'constraints' => array(
						'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
						'formatter'  => '[a-zA-Z][a-zA-Z0-9_-]*',
						'id'         => '[a-zA-Z0-9_-]*'
					),
				),
			),
		),
	),
);
