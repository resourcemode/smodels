<?php

namespace Rest\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;

/**
 *
 */
class CategoryController extends AbstractRestfulController {
    
	/**
	 * Return list of resources
	 *
	 * @return array
	 */
	public function getList() {
        $services = $this->getServiceLocator();
        $categoryService = $services->get('RestCategoryService');
        $cats = $categoryService->getCategoryChildren(1); // root
        return $cats;
	}

	/**
	 * Return single resource
	 *
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {}
}
