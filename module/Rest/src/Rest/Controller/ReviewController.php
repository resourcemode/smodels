<?php

namespace Rest\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;

/**
 *
 */
class ReviewController extends AbstractRestfulController {
    
	/**
	 * Return list of resources
	 *
	 * @return array
	 */
	public function getList() {
        $services = $this->getServiceLocator();
        $reviewService = $services->get('RestReviewService');
        $reviews = $reviewService->getAllProductReviews();
        return $reviews;
	}

	/**
	 * Return single resource
	 *
	 * @param mixed $id
	 * @return mixed
	 */
	public function get($id) {}

	/**
	 * Create a new resource
	 *
	 * @param mixed $data
	 * @return mixed
	 */
	public function create($data) {}

	/**
	 * Update an existing resource
	 *
	 * @param mixed $id
	 * @param mixed $data
	 * @return mixed
	 */
	public function update($id, $data) {}

	/**
	 * Delete an existing resource
	 *
	 * @param  mixed $id
	 * @return mixed
	 */
	public function delete($id) {}
}
