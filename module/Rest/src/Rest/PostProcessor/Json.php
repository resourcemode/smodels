<?php

namespace Rest\PostProcessor;

/**
 *
 */
class Json extends AbstractPostProcessor {
    
	public function process() {

		$this->_response->setContent($this->_vars);

		$headers = $this->_response->getHeaders();
		$headers->addHeaderLine('Content-Type', 'application/json');
		$this->_response->setHeaders($headers);
	}
}
